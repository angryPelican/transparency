<?php session_start(); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="The official website of the Schools Division of Marikina City">
  <meta name="keywords" content="DepEd Marikina, DepEd, Marikina, SDO, Education, Schools, NCR">
  <meta name="author" content="Ryan Lee Regencia">

  <title>Schools Division of Marikina City</title>
  <link rel="shortcut icon" type="image/x-icon" href="/images/DO-logo.png" title=" Division of City Schools Marikina"/>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <link rel="stylesheet" href="/css/custom.css" type="text/css" />	
  <link rel="stylesheet" href="/css/login.css" type="text/css" />

<script>
function check_empty_fields()
{
var ValidateInput = true;

$(".validate #email,#password").each(function(){
    if ($.trim($(this).val()).length == 0){
        $(this).addClass("highlight");
        ValidateInput = false;
        $(this).focus();
    }
    else{
        $(this).removeClass("highlight");
    }
});

if (!ValidateInput) {
	$("#login-failed").fadeIn(200);
	$("#login-failed").text('Log in failed!');				
}
  return ValidateInput;  
 }
 </script>
 
  <script type="text/javascript" src="js/jquery-1.11.3-jquery.min.js"></script>
  <script type="text/javascript" src="js/validation.min.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
  
</head>

<body>

<div class="container">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
		<p style="text-align: center"><img src="../img/seal.jpg" width="80%" title="" /></p>   
		
		<hr class="colorgraph"><br />
		
		<form class="form-signin" method="post" id="login-form">
        <h4 class="form-signin-heading">Log in to your account.</h4><hr />
        <div id="error">
        <!-- error will be shown here ! -->
        </div>
        <div class="form-group">
        <input type="email" class="form-control" placeholder="Email address" name="user_email" id="user_email" />
        <span id="check-e"></span>
        </div>
        
        <div class="form-group">
        <input type="password" class="form-control" placeholder="Password" name="password" id="password" />
        </div>
       
     	<hr />
        
        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-primary btn-block" name="btn-login" id="btn-login">
    		&nbsp; Sign In
			</button> 
        </div>  
      </form>  
	</div>
		<div class="col-sm-4"></div>
	</div>
	<p align="center">Schools Division Office - Marikina City</p>
</div>

</body>
</html>