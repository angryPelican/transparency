<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

<script type="text/javascript" class="init">	
$(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 0, "desc" ]],
    } );	
} );

</script>

<table id="example" class="table table-striped">
	<thead>
		<tr>
			<th class="base-text" width="10%">Date</th>
			<th class="base-text" width="20%">Quotation Number</th>
			<th class="base-text" width="40%">Description</th>					
			<th class="base-text" width="15%" align="right">Budget (PhP)</th>
			<th class="base-text" width="15%">Remarks</th>
		</tr>
	</thead>
	<tbody>
		<?php		
				include('dbconnect/trans_connect.php');		
				$sql = mysql_query("SELECT * FROM rfq_pqf ORDER BY qn DESC");
				while($row = mysql_fetch_assoc($sql)) {
				$date = $row['date'];
				$qn = $row['qn'];
				$title = $row['title'];
				$url= $row['url'];
				$budget = $row['budget'];
				$remarks = $row['remarks'];
				echo"<tr>
        				<td>$date</td>	
						<td>$qn</td>
						<td><a href=$url>$title</a></td>	
						<td style='text-align: right'>$budget</td>
						<td>$remarks</td>
						</tr>";
				}
				?>
	</tbody>
</table>