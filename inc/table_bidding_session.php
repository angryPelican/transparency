<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

<script type="text/javascript" class="init">	
$(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 1, "desc" ]],
    } );	
} );

</script>

<div class="panel with-nav-tabs panel-default">
	<div class="panel-body">
		<!-- ADD RFQ -->
		<div class="row">
			<div class="col-sm-12">
				<p><button class="btn btn-success pull-right clearfix" data-toggle="modal" data-target="#myModal1"><span class="glyphicon glyphicon-plus"></span></button></p>
			</div>	
		</div>
		<div>&nbsp;</div>
		<!-- END -->	
		
		<table id="example" class="table table-striped">
			<thead>
				<tr>
					<th class="">Date</th>
					<th class="">Quotation Number</th>
					<th class="">Description</th>					
					<th class="" align="right">Budget (PhP)</th>
					<th class="">Remarks</th>
					<th class="">&nbsp;</th>
					<th class="">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php		
					include "dbconnect/trans_connect.php";	
						
					$fetch_bidding = mysql_query("SELECT * FROM public_bidding");
					mysql_query("SET NAMES utf8"); 
					while($rfq_row = mysql_fetch_assoc($fetch_bidding))
						{
				echo"<tr>
				<td class=''>{$rfq_row['date']}</td>
				<td class=''>{$rfq_row['ref_no']}</td>
				<td class=''><a href='{$rfq_row['url']}' target='_blank'>{$rfq_row['description']}</a></td>
				<td class='' align='right'>{$rfq_row['budget']}</td>
				<td class=''>{$rfq_row['remarks']}</td>
				<td class='' style='text-align: center;'><a href='#' data-toggle='modal' data-target='#myModal2' data-id='{$rfq_row['id']}' id='bidding'><span class='glyphicon glyphicon glyphicon-pencil'></span></a></td>
				<td class='' style='text-align: center;'><a href='../inc/delete_bidding.php?id={$rfq_row['id']}' onclick=\"return  confirm('Are you sure you want to delete record?')\"><span class='glyphicon glyphicon glyphicon-trash'></span></a></td>
				</tr>";
						}
				?>
			</tbody>
		</table>
	</div>
</div>


<!-- Modal -->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add PhilGEPS Opportunity</h4>
      </div>
      <div class="modal-body">
		<form  name="bidding" action="../inc/table_bidding_action.php" method="post" enctype="multipart/form-data">		
		  <div class="form-group">
			<label for="num">Reference No.</label>
			<input name="ref_no" type="" class="form-control" id="ref_no">
		  </div>		
		  <div class="form-group">
			<label for="desc">Description</label>
			<input name="desc" type="" class="form-control" id="desc">
		  </div>
		  <div class="form-group">
			<label for="desc">PhilGEPS URL</label>
			<input name="url" type="" class="form-control" id="url">
		  </div>		  
		  <div class="form-group">
			<label for="budg">Approved Budget</label>
			<input name="budg" type="" class="form-control" id="budg">
		  </div>
		  <div class="form-group">
			<label for="rem">Remarks</label>
			<select name="sel1" class="form-control" id="sel1">
				<option></option>
				<option>Active</option>
				<option>Closed</option>
			</select>
		  </div>		  
		  <div class="checkbox">
			<label><input type="checkbox" onchange="document.getElementById('sendNewSms').disabled = !this.checked;"> All information are similar to PhilGEPS posting.</label>
		  </div>
		  
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" class="btn btn-default" id="sendNewSms" disabled="disabled">Submit</button>
		</form>
      </div>
    </div>

  </div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
	<div class="modal-dialog">
    <!-- Modal content-->
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Edit PhilGEPS Opportunity</h4>
      		</div>
      		<div class="modal-body">	  
	  			<div id="modal-loader" style="display: none; text-align: center;">
                    <img src="../inc/ajax-loader.gif">
                </div>				                          
			   <!-- content will be load here -->                          
			   <div id="dynamic-content"></div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	
	$(document).on('click', '#bidding', function(e){
		
		e.preventDefault();
		
		//var ID = $(this).data('ID');   // it will get id of clicked row
		var ID = ($(this).attr("data-id"));
		
		$('#dynamic-content').html(''); // leave it blank before ajax call
		$('#modal-loader').show();      // load ajax loader
		
		$.ajax({
			url: '../inc/table_bidding_edit.php',
			type: 'POST',
			data: 'ID='+ID,
			dataType: 'html'
		})
		.done(function(data){
			console.log(data);	
			$('#dynamic-content').html('');    
			$('#dynamic-content').html(data); // load response 
			$('#modal-loader').hide();		  // hide ajax loader	
		})
		.fail(function(){
			$('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#modal-loader').hide();
		});
		
	});
	
});

</script>