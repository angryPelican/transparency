<h2>Compliance with Section 93 (Transparency Seal) R.A. No. 10633</h2>
<div class="panel-group">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4><a data-toggle="collapse" href="#collapse1">The Agency's Mandates and Functions, Name of its Officials with their Position and Designation, and Contact Information</a></h4>
		</div>
		<div id="collapse1" class="panel-collapse collapse">
			<div class="list-group">
				<a class="list-group-item" href="#">DepEd's Vision, Mission, Mandates, and Objectives</a>
				<a class="list-group-item" href="#">Organizational Structure</a>
				<a class="list-group-item" href="#">Division Directory</a>
			</div>        
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4><a href="/charter/citizens_charter.php" target="_blank">Citizen's Charter</a></h4>
		</div>
		<div id="collapse2" class="panel-collapse collapse">
			<div class="list-group">
				<a class="list-group-item" href="#">Office of the Schools Division Superintendent</a>
				<a class="list-group-item" href="#">Curriculul and Implementation Division</a>
				<a class="list-group-item" href="#">School Governance and Operation Division</a>
			</div>        
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4><a data-toggle="collapse" href="#collapse3">Annual Reports</a></h4>
		</div>
		<div id="collapse3" class="panel-collapse collapse">
			<div class="list-group">
				<a class="list-group-item" href="reports/annual/SDO_Annual_Report_2016.pdf" target="_blank">FY 2016</a>
			</div>        
		</div>
	</div>	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4><a data-toggle="collapse" href="#collapse4">Financial Reports</a></h4>
		</div>
		<div id="collapse4" class="panel-collapse collapse">
			<div class="list-group">
				<a type="button" class="list-group-item" href="" data-toggle="modal" data-target="#fin2017">FY 2017</a>
				<a type="button" class="list-group-item" href="" data-toggle="modal" data-target="#fin2016">FY 2016</a>
			</div>        
		</div>
	</div>	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4><a data-toggle="collapse" href="#collapse5">Projects, Programs and Activities, Beneficiaries, and Status of Implementation</a></h4>
		</div>
		<div id="collapse5" class="panel-collapse collapse">
			<div class="list-group">
				<a class="list-group-item" href="#">Projects, Programs</a>
				<a class="list-group-item" href="#">Beneficiaries</a>
				<a class="list-group-item" href="#">Status of Implementation</a>
			</div>        
		</div>
	</div>					
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4><a data-toggle="collapse" href="#collapse6">Annual Procurement Plan</a></h4>
		</div>
		<div id="collapse6" class="panel-collapse collapse">
			<div class="list-group">
				<a class="list-group-item" href="reports/app/MOOE_2017_Revised.pdf" target="_blank">FY 2017 (Revised)</a>
				<a class="list-group-item" href="reports/app/MOOE_2017.pdf" target="_blank">FY 2017</a>
				<a class="list-group-item" href="reports/app/MOOE_2016.pdf" target="_blank">FY 2016</a>
			</div>        
		</div>
	</div>	
</div>

<?php include ('/reports/inc/financial_2017.php');
	  include ('/reports/inc/financial_2016.php'); ?>
