<div class="panel with-nav-tabs panel-default">
	<div class="panel-heading">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1default" data-toggle="tab">Request for Quotation</a></li>			
			<li><a href="#tab2default" data-toggle="tab">Create RFQ</a></li>
		</ul>
	</div>	
	<div class="panel-body">
		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab1default">
				<?php include ('../inc/table_rfq_session.php'); ?>
			</div>				
			<div class="tab-pane fade" id="tab2default">
				<?php include ('../inc/create_form_rfq.php'); ?>
				<?php include ('../inc/table_rfp_session.php'); ?>
			</div>
			<!--
			<div class="tab-pane fade" id="tab3default">
				<'?php include ('inc/table_rei_session.php'); ?>
			</div>-->
		</div>
	</div>
</div><!--END OF NAV-TABS-->	