<!-- ADD RFQ -->
<div class="row">
	<div class="col-sm-12">
		<p><button class="btn btn-success pull-right clearfix" data-toggle="modal" data-target="#myModal2"><span class="glyphicon glyphicon-plus"></span></button></p>
	</div>	
</div>
<div>&nbsp;</div>
<!-- END -->

<table id="example1" class="table table-striped">
	<thead>
		<tr>
			<th class="base-text">Date</th>
			<th class="base-text">Quotation Number</th>
			<th class="base-text">Description</th>					
			<th class="base-text">Budget (PhP)</th>
			<th class="base-text" style="text-align: center;"><span class="glyphicon glyphicon-pencil"></span></th>
			<th class="base-text" style="text-align: center;"><span class="glyphicon glyphicon-trash"></span></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="base-text">June 22, 2017</td>
			<td class="base-text">DepEd-17-NCR-ISQ-26</td>
			<td class="base-text"><a href="docs/QN DepEd-17-NCR-ISQ-26.pdf">Certificate/Specialty Paper, Legal Cream for the First Aide Training for SDO Personnel on July 27, 2017</a></td>
			<td class="base-text" align="right">150.00</td>
			<td class="base-text" style="text-align: center;">&nbsp;</td>
			<td class="base-text" style="text-align: center;">&nbsp;</td>
		</tr>			
	</tbody>
</table>

<!-- Modal -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Post RFP</h4>
      </div>
      <div class="modal-body">
		<form>
		  <div class="form-group">
			<label for="num">Quotation No.</label>
			<input type="" class="form-control" id="num">
		  </div>		
		  <div class="form-group">
			<label for="desc">Description</label>
			<input type="" class="form-control" id="desc">
		  </div>
		  <div class="form-group">
			<label for="budg">Approved Budget</label>
			<input type="" class="form-control" id="budg">
		  </div>
		  <div class="form-group">
			<label for="rem">Remarks</label>
			<input type="" class="form-control" id="rem">
		  </div>		  
		  <div class="checkbox">
			<label><input type="checkbox"> All the requirements prior to posting the ISQ have been complied with.</label>
		  </div>
		  <button type="submit" class="btn btn-default" onClick="history.go(0)">Submit</button>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>