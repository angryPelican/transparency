<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="The official website of the Schools Division of Marikina City">
  <meta name="keywords" content="DepEd Marikina, DepEd, Marikina, SDO, Education, Schools, NCR">
  <meta name="author" content="Ryan Lee Regencia">
  
  <meta http-equiv='cache-control' content='no-cache'>
  <meta http-equiv='expires' content='0'>
  <meta http-equiv='pragma' content='no-cache'>

  <title>Schools Division of Marikina City</title>
  <link rel="shortcut icon" type="image/x-icon" href="/images/DO-logo.png" title=" Division of City Schools Marikina" />
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  
  <!-- DataTable -->
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
  
  <link rel="stylesheet" href="../css/stylesheet.css" />
  <link rel="stylesheet" href="../css/login.css" />

</head>

<body>
<div class="container">
	<?php include ('../inc/navbar_transparency.php'); ?>
	<div class="row">
		<div class="col-sm-12">
			<h1>Procurement Management System for Schools</h1>
		</div>
	</div>
	<div class="row">		
		<div class="col-sm-12">
			<?php
				include ('inc/panel_po_display.php');
			?>
		
			<!--
			<'?php 
			if(isset($_SESSION['user_session'])) {
			//$user_id = $_SESSION['user_id'];
				include ('../inc/panel_rfq_session.php');
				}
			else {
				include ('../inc/panel_rfq_display.php');
				}						
			 ?>-->
		</div>
	</div>
</div>
<div class="container">
	<div class="row footer">
		<div class="col-sm-5">
			191 Shoe Ave., Sta. Elena, Marikina City, 1800, Philippines<br />
			(02) 682-2472 / 682-3989 <br />
			sdo.marikina@deped.gov.ph
		</div>
	</div>
</div>

</body>
</html>
