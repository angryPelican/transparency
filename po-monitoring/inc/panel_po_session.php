<div class="panel with-nav-tabs panel-default">
	<div class="panel-heading">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1default" data-toggle="tab">Purchase Requests</a></li>			
			<li><a href="#tab2default" data-toggle="tab">Request for Quotation</a></li>
			<li><a href="#tab3default" data-toggle="tab">Purchase Orders</a></li>
		</ul>
	</div>
	<div class="panel-body">
		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab1default">
				<table id="rfq_tbl" class="table table-bordered display">
					<thead>
						<th>Date</th>
						<th>RFQ Number</th>
						<th>Description</th>
						<th>Budget</th>
						<th>Remarks</th>
					</thead>
				</table>
			</div>
			<div class="tab-pane fade in" id="tab2default">
				<table id="pr_tbl" class="table table-bordered display">
					<thead>
						<th>Date</th>
						<th>PR Number</th>
						<th>Description</th>
						<th>Budget</th>
						<th>Remarks</th>
					</thead>
				</table>
			</div>		
			<div class="tab-pane fade in" id="tab3default">
				<table id="po_tbl" class="table table-bordered display">
					<thead>
						<th>Date</th>
						<th>PO Number</th>
						<th>Description</th>
						<th>Amount</th>
						<th>Remarks</th>
					</thead>
				</table>
			</div>	
		</div>
	</div>
</div><!--END OF NAV-TABS-->	

<script>
	$(document).ready(function() {
	$('#rfq_tbl, #pr_tbl, #po_tbl ').DataTable();
	} );
</script>