<!DOCTYPE html>
<html lang="en">
<head>
	<title>PPMP | Division Procurement Projects</title>
	<?php
		include ('Connect.php'); 
		include('auth.php');
		require ('inc/header.php');
	?>
	<link rel="stylesheet" type="text/css" href="css/dashboard_so.css">
</head>
	<body>
		<?php
		require ('inc/navbar_transparency.php');
		?>

		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h2 class="mt-4">Division Procurement Projects</h2>
					<nav aria-label="breadcrumb">
		  				<ol class="breadcrumb">
		    				<li class="breadcrumb-item active"><a href="#">Home</a></li>
		    				<li class="breadcrumb-item active" aria-current="page">Division Procurement Projects</li>
		  				</ol>
					</nav>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-9">
					<?php
						include ('php/so/edit_project_title_so.php');
					?>
				</div>
				<div class="col-sm-3">
					<div class="card">
						<div class="card-header bg-dark text-white">
							<h6 class="m-0">Project Accomplishment Report</h6>
						</div>
						<div class="card-body table-responsive">
							<table class="table table-striped table-bordered table-sm display" id="proj_acc">
								<thead>
									<tr>
										<th align="center">Active</th>
										<th align="center">Completed</th>
										<th align="center">% of Completion</th>
									</tr>
								</thead>
								<tbody id="tbod-dark">
									<script type="text/javascript">
										var role="all_project_status";
										var s_id=<?php echo $_SESSION['s_id']; ?>;
											$.ajax({
												url: "login_webservice.php",
												type: "post",
												data: {
													tag: role,
													user_id: s_id,
												},
												success: function(response){
													console.log(response);
													if(response=="none") {
														document.getElementById("tbod-dark").innerHTML += "<tr><td colspan='3'>No data available in table.</td></tr>";
													} else if (response!="none") {
														var data=JSON.parse(response);
														for (var key in data) {
															document.getElementById("tbod-dark").innerHTML += "<tr><td>" + data[key].row1 + "</td>" + "<td>" + data[key].row2 +  "</td>" + "<td>" + data[key].row3 + "</td></tr>";
														}
													}
												}
											})
									</script>
								</tbody>
							</table>
						</div>
					</div>	
					<div>&nbsp;</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="card border-dark">
						<div class="card-header bg-dark">
							<h6 class="text-white m-0"><i class="fab fa-elementor"></i> Legends</h6>
						</div>
						<div class="card-body">
							<table class="table table-striped table-bordered display table-sm">
								<tr>
									<td><b>PPMP-AD</b></td>
									<td>Project Procurement Management Plan - Approved Date</td>
								</tr>
								<tr>
									<td><b>PR No.</b></td>
									<td>Purchase Request Number</td>
								</tr>
								<tr>
									<td><b>RFQ No.</b></td>
									<td>Request for Quotation Number</td>
								</tr>
								<tr>
									<td><b>PO No.</b></td>
									<td>Purchase Order Number</td>
								</tr>
								<tr>
									<td><b>EB</b></td>
									<td>Estimated Budget</td>
								</tr>
								<tr>
									<td><b>AC</b></td>
									<td>Actual Cost</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="card">
						<div class="card-header bg-dark text-white">
							<h6 class="m-0">Procurement Calendar</h6>
						</div>
						<div class="card-body table-responsive">

						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>