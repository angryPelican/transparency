<!DOCTYPE html>
<html lang="en">
<head>
	<title>PMS | View School Projects</title>
	<?php
		include('Connect.php');
		include('auth.php');
		require ('inc/header.php');
	?>
<body>
	<?php
		require ('inc/navbar_transparency.php');
	?>
	<div class="container-fluid">
		<table class="table table-bordered table-sm" style="table-layout: fixed;">
			<thead>
				<tr>
					<th>School ID</th>
					<th>School Name</th>
					<th>Project Name</th>
					<th>Amount</th>
					<th>PR</th>
					<th>RFQ</th>
					<th>AOQ</th>
					<th>PO</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>MES</td>
					<td></td>
					<td><a href="#">Supply and Delivery of Office Supplies</a></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>