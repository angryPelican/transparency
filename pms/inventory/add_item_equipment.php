<?php include('Connect.php'); ?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h2 id="key"><?php echo $_GET['category']; ?></h2>
		</div>
			<div class="card-body">	
				<div class="form-group row">
					<div class="col-sm-12 col-md-12 table-responsive">
						<table class="table table-striped table-bordered display table-sm" width="100%" id="tableid">
							<thead>
								<tr>
									<th>Qty</th>
									<th>Unit</th>
									<th>Description</th>
									<th>Property No.</th>
									<th>Date Acquired</th>
									<th>Cost</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		<div class="card-footer">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_inv">
			<i class="fas fa-plus-circle"></i> Add item
			</button>
			<a class="btn btn-primary" href="download_summary.php"><i class="fas fa-print"></i> Print</a>
		</div>	
	</div>
</div>
<!-- Modal for adding item from inventory -->
<div class="modal fade" id="add_inv" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
	    	<div class="modal-body">
	    		<form action="" method="post">
		        	<div class="form-group row">
						<div class="col-sm-12 col-md-12">
							<label for="item_name">Item Name</label>
							<input type="text" id="item_name" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6 col-md-6">
							<label for="Quantity">Quantity</label>
							<input type="number" id="item_qty_s" class="form-control">
						</div>
						<div class="col-sm-6 col-md-6">
							<label for="Quantity">Unit</label>
							<select class="form-control" id="item_unit">
								<option value="Choose...">Choose...</option>
								<option value="ream">ream</option>
								<option value="pc">pc</option>
								<option value="unit">unit</option>
								<option value="box">box</option>
								<option value="pax">pax</option>
								<option value="set">set</option>
								<option value = "bottle">bottle</option>
								<option value = "can">can</option>
								<option value = "bottle">gallon</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<label for="Quantity">Unit Cost</label>
							<input type="number" id="item_unit_cost" class="form-control" step="any">
						</div>
						<div class="col-sm-6">
							<label for="property_number">Property No : </label>
							<input type="property_number" id="prop_no" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<label for="property_number">Date : </label>
							<input type="date" id="date" class="form-control">
						</div>
						<div class="col-sm-6">
							<label for="property_number">Lifespan (in years): </label>
							<input type="number" id="date_lifespan" class="form-control" placeholder="* No. of years">
						</div>
					</div>
				</div>
			    <div class="modal-footer">
			        <button type="button" id="submit" class="btn btn-primary">Add</button>
			    </div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var role="sel_equipment_item";
		var key=$('#key').text();
		if(key=="Office Equipment") {
			key="office_equipment";
		}
		if(key=="ICT Equipment") {
			key="ict_equipment";
		}
		if(key=="Furnitures and Fixtures") {
			key="furnitures_and_fixtures";
		}
		$.ajax({
			url: "select_webservice.php",
			type: "post",
			data: {
				tag: role,
				category: key,
			},
			success: function(response){
				var data=JSON.parse(response);
				if ($.fn.DataTable.isDataTable("#tableid")) {
					$('#tableid').DataTable().clear().destroy();
				}
                $('#tableid').DataTable({
					"bSort": false,
					"aaData": data,
					'columnDefs': [{
					    "targets": [5], // your case first column
					    "className": "text-right",
					}],
				});
			}
		});
	});
	$('#submit').click(function(){
		var role="add_item_equipment";
		var itemname=$('#item_name').val();
		var itemqty=$('#item_qty_s').val();
		var itemunit=$('#item_unit').val();
		var itemunitcost=$('#item_unit_cost').val();
		var prop_num=$('#prop_no').val();
		var date=$('#date').val();
		var lifespan1=$('#date_lifespan').val();
		var key=$('#key').text();
		console.log(key);
		if(key=="Office Equipment") {
			key="office_equipment";
		}
		if(key=="ICT Equipment") {
			key="ict_equipment";
		}
		if(key=="Furnitures and Fixtures") {
			key="furnitures_and_fixtures";
		}
		$.ajax({
			url: 'select_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			item_name: itemname,
			item_qty: itemqty,
			item_unit: itemunit,
			item_unit_cost: itemunitcost,
			prop_no: prop_num,
			date_acquired: date,
			lifespan: lifespan1,
			category: key	
			},
			success: function(response) {
				console.log(response);
			}
		});
	});
</script>