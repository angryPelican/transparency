<div class="container-fluid">
	<input type="hidden" name="hidden_id" id="hidden_category" value="<?php echo $_GET['category']; ?>" />
	<input type="hidden" id="hidden_id" value="<?php if(isset($_GET['eventid'])){ echo $_GET['eventid']; } else echo 0; ?>" />
	<div class="col-sm-12">
		<table class="table" id="table_header" style="margin:0; border: none;">
			<tr style="border: none;">
				<td style="border: none;" width="15%" align="center"><img src="seal1.png" alt="Smiley face" width="100"></img></td>
				<td style="border: none;" width="70%" style="margin: 0; font-size: 0.8em;" align="center">REPUBLIC OF THE PHILIPPINES<br>
					<h5 style="margin: 0;">DEPARTMENT OF EDUCATION</h5>
					NATIONAL CAPITAL REGION<br>
					<h5 style="margin: 0;">SCHOOLS DIVISION OFFICE - MARIKINA CITY</h5>
				</td>
				<td style="border: none;" width="15%" align="center"><img src="deped_logo.png" alt="Smiley face" width="100"></img></td>
			</tr>
		</table>
		<h3 id="h2"><center>PROPERTY ACKNOWLEDGEMENT RECEIPT</center></h3>
		<table class="table table-bordered display table-sm">
			<thead>
				<tr>
					<td>Office:</td>
					<td colspan="2" id="dynamic_unit"><?php if(empty($_SESSION['position'])) {
			//do nothing
		} else {
			echo $_SESSION['position'];
		} ?></td>
					<td colspan="2"><b>SDO Marikina City</b></td>
					<td></td>
				</tr>
				<tr>
					<td rowspan="2"><b>Qty</b></td>
					<td rowspan="2"><b>Unit</b></td>
					<td rowspan="2"><b>Description</b></td>
					<td><b>Property</b></td>
					<td><b>Date</b></td>
					<td rowspan="2"><b>Cost</b></td>
					<tr>
						<td><b>No.</b></td>
						<td><b>Acquired</b></td>
					</tr>
				</tr>
			</thead>
			<tbody id="tbodydark">
				<script type="text/javascript">
					$(document).ready(function(){
					var role="select_item_by_trans_equipment";
					var trans=$('#hidden_id').val();
					var unique=$('#submit1').data('id');
						$.ajax({
							url: 'select_webservice.php',
							type: 'POST',
							data: {
							tag: role,
							eventid: trans,
							uniqueid: unique
							},
							success: function(response) {
								if(response=="wala"){
									console.log(response);
								} else if (response!="wala"){
								console.log(response);
								var data = JSON.parse(response);
									for(var key in data) {
										document.getElementById("tbodydark").innerHTML += "<tr><td>" + data[key].item_qty + "</td><td>" + data[key].item_unit + "</td><td>" + data[key].item_desc + "</td><td>" + data[key].property_no + "</td>" + "<td>" + data[key].date_acquired + "</td>" + "<td>" + data[key].cost + "</td>" + "</tr>";
									}
								}
							}
						});
					});
				</script>
			</tbody>
			<tfoot id="tfoot">
				<tr>
					<td colspan="2">Received from: </td>
					<td></td>
					<td>Received by: </td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 30px;"></td>
					<td colspan="3" style="height: 30px;"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="3"><b>ANNA MARIE P. EXEQUIEL</b></td>
					<td colspan="3" id="dynamic_name" style="font-weight: bold; text-transform: uppercase;">
					<?php 
						if(empty($_SESSION['full_name'])) {

							} else {
								echo $_SESSION['full_name'];
							} 
					?>
					</td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="3">Administrative Office IV/Supply Unit</td>
					<td colspan="3" id="dynamic_pos">
					<?php 
						if(empty($_SESSION['position'])) {
								//do nothing
						} else {
							echo $_SESSION['position'];
						} ?>
					</td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="3"><?php echo date('F d,Y'); ?></td>
					<td colspan="3"><?php echo date('F d,Y'); ?></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="3">Date</td>
					<td colspan="3">Date</td>
				</tr>
			</tfoot>
		</table>
			<button type="button" id="add_item" class="btn btn-primary" data-toggle="modal" data-target="#issue_item"><i class="fas fa-plus-circle"></i> Issue Item
			</button>
			<button type="submit" name="save_view" class="btn btn-primary" id="save_view">
			<i class="fas fa-save"></i> Save & Preview
			</button>
			<button type="button" class="btn btn-primary" id="print_now">
			<i class="fas fa-print"></i> Print
			</button>
	</div>
</div>

<!--add item modal-->
<div class="modal fade" id="issue_item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Issue item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" method="post">
					<div class="form-group row">
						<div class="col-sm-12 col-md-12">
							<label for="item_name">Description</label>
							<input type="text" list="items" class="form-control item_drop" list="items" name="">
								<datalist id="items" class="item_drop">
								<option></option>
								<?php
									$category=$_GET['category'];
									if($category=="Office Equipment") {
										$category="office_equipment";
									}
									if($category=="ICT Equipment") {
										$category="ict_equipment";
									}
									if($category=="Furnitures and Fixtures") {
										$category="furnitures_and_fixtures";
									}
									$select_i="SELECT * FROM " . $category . "_table";
									$select_i_exec=$pdo->prepare($select_i);
									$select_i_exec->execute();
									$arr1=array();
									while($row_i=$select_i_exec->fetch(PDO::FETCH_ASSOC)) {
									array_push($arr1, $row_i['items']);
									}
									foreach ($arr1 as $key => $value) {
									# code...
									echo"<option value='$value'>$value</option>";
									}
								?>
							</datalist>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4 col-md-4">
							<label for="stock">On Stock</label>
							<input type="number" name="item_stock" class="form-control stock_avail" disabled>
						</div>
						<script type="text/javascript">
						$('.item_drop').on('keyup', function(){
						var role="select_stock";
						var category=$('#hidden_category').val();
							if(category=="Office Equipment"){
								category="office_equipment";
							}
							if(category=="ICT Equipment") {
								category="ict_equipment";
							}
							if(category=="Furnitures and Fixtures"){
								category="furnitures_and_fixtures";
							}
						var item=$(this).val();
							$.ajax({
								url: 'select_webservice.php',
								type: 'POST',
								data: {
								tag: role,
								item_name: item,
								category: category,
								},
								success: function(response) {
									var data=JSON.parse(response);
									var stock=data['stock'];
									$('.stock_avail').val(stock);
								}
							});
						});
						</script>
						<div class="col-sm-4 col-md-4">
							<label for="Quantity">Quantity</label>
							<input type="number" id="item_qty" class="form-control">
						</div>
						<div class="col-sm-4 col-md-4">
							<label for="property_number">Property Number</label>
							<input class="form-control" type="text" id="property_number" placeholder="Place property number here ...">
						</div>
					</div>
					<div class="form-group row">	
						<div class="col-sm-6 col-md-6">
							<label for="property_number">Date Acquired</label>
							<input class="form-control" type="date" id="date_acquired">
						</div>
						<div class="col-sm-6 col-md-6">
							<label for="property_number">Cost</label>
							<input class="form-control" type="number" id="cost" placeholder="Place cost here ...">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="submit1" data-id="1" class="btn btn-primary">Add</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#submit1').click(function(){
		var role="add_issued_item_equipment";
		var item=$('.item_drop').val();
		var quantity=$('#item_qty').val();
		var id=$('#hidden_id').val();
		var property_no=$('#property_number').val();
		var date_acquired=$('#date_acquired').val();
		var cost=$('#cost').val();
		var category=$('#hidden_category').val();
		if(category=="Office Equipment"){
			category="office_equipment";
		}
		if(category=="ICT Equipment") {
			category="ict_equipment";
		}
		if(category=="Furnitures and Fixtures"){
			category="furnitures_and_fixtures";
		}
		$.ajax({
			url: 'select_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			item_name: item,
			item_qty: quantity,
			eventid: id,
			property_no: property_no,
			date_acquired: date_acquired,
			cost: cost,
			category: category,
			},
			success: function(response) {
				location.reload();
				// console.log(response);
			}
		});
	});
</script>

<!--SCRIPT-->
<script type="text/javascript">
	$(document).ready(function(){
		$('#add_item').hide();
				if(localStorage.getItem('req_fullname') != null){
					$('#add_item').show();
				}
		$('#table_header').hide();
		$('#tfoot').hide();
		$('#print_now').hide();
		var getfullname=localStorage.getItem('req_fullname');
		var getposition=localStorage.getItem('req_position');
		var getunit=localStorage.getItem('req_unit');
		//check item
		var role="check_item_equipment";
		var trans=$('#hidden_id').val();
		$.ajax({
			url: 'select_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			trans_id: trans,
			},
			success: function(response) {
				console.log(response);
				if(response=="true"){
					$('#save_view').show();
				} else if(response=="false") {
					$('#save_view').hide();
				}
			}
		});
		$('#save_view').click(function() {
			// Client-side
			var bb = $('#hidden_id').val();
			var url="issuances_display.php?eventid=" + bb + "";
			swal({
					title: "Issuance Saved!",
					html: "Note: you can view this issuance by going to View Issuances tab.",
					type: 'success',
					animation: true,
					timer: 2000,
					showConfirmButton: false,
				})
				.then((result) => {
					localStorage.removeItem('req_fullname');
					localStorage.removeItem('req_position');
					localStorage.removeItem('req_unit');
					$('#table_header').show();
					$('#tfoot').show();
					$('#print_now').show();
					$('#add_item').hide();
					$('#save_view').hide();
				});
		});
		$('#print_now').click(function() {
			$('#add_item, #print_now, #save_view, #print, #list').hide();
			$('#table_header, #table_header2, #table_footer').show();
			$('.purpose').css('border', 'none');
			$('#view_tab').removeClass('col-sm-10');
			$('#view_tab').addClass('col-sm-12');
			window.print();
			$('#add_item, #print_now, #save_view, #print, #list').show();
			$('#table_header, #table_header2, #table_footer').hide();
			$('.purpose').css('border', '');
			$('#view_tab').removeClass('col-sm-12');
			$('#view_tab').addClass('col-sm-10');
			$('#tabledark tbody').remove();
			var url=window.location.href;
			var urlObject = new URL(url);
			urlObject.searchParams.delete('eventid');
			window.location.href=urlObject.href;
		});
	});
</script>