<?php include('Connect.php'); ?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h2>Drugs and Medicine Supplies</h2>
		</div>
			<div class="card-body">	
				<div class="form-group row">
					<div class="col-sm-12 col-md-12">
						<table class="table table-bordered table-sm" id="table_drugs" style="width: 100%;">
							<thead>
								<tr>
									<th>Particular</th>
									<th colspan="3">Purchases</th>
									<th colspan="3">Available Stocks for the Month</th>
									<th>Issuances</th>
									<th>Balance as of <?php echo date('m/d/y'); ?></th>
									<th>Per Count</th>
									<th>Shortages</th>
									<th style="border-bottom: none;">Remarks</th>
								</tr>
								<tr>
									<th>Items</th>
									<th>Qty</th>
									<th>Unit Cost</th>
									<th>Amount</th>

									<th>Qty</th>
									<th>Unit Cost</th>
									<th>Amount</th>

									<th>Qty</th>
									<th>Qty</th>
									<th>Qty</th>
									<th>Qty</th>
									<th style="border-top: none;"></th>
								</tr>
							</thead>
								<tbody>
									<?php
										$select="SELECT * FROM medicine_and_drugs_table";
											$select_exec=$pdo2->prepare($select);
											$select_exec->execute();
											while($row=$select_exec->fetch(PDO::FETCH_ASSOC)){
												$item_name=$row['items'];
												$item_qty=$row['mt_qty'];
												$qty_outside=$row['qty_from_issuances'];
												$balance=$row['balance'];
												$item_unit_cost=number_format($row['mt_unit_cost'], 2);
												$item_amount=number_format($row['mt_amount'], 2);
												echo"<tr>
														<td>$item_name</td>
														<td></td>
														<td></td>
														<td></td>
														<td>$item_qty</td>
														<td align='right'>$item_unit_cost</td>
														<td align='right'>$item_amount</td>
														<td>$qty_outside</td>
														<td>$balance</td>
														<td></td>
														<td></td>
														<td></td>
													</tr>";
											}
									?>
								</tbody>
						</table>
					</div>
				</div>
			</div>
		<div class="card-footer">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_inv_janitorial">
			<i class="fas fa-plus-circle"></i> Add item
			</button>
			<a class="btn btn-primary" href="download_summary_devices.php"><i class="fas fa-print"></i> Print</a>
			<!--<script type="text/javascript">
			function exportToExcel(tableid, filename = ''){
			var downloadLink;
			var dataType = 'application/vnd.ms-excel';
			var tableSelect = document.getElementById(tableid);
			var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
			console.log(tableHTML);

			// Specify file name
			filename = filename?filename+'.xls':'excel_data.xls';

			// Create download link element
			downloadLink = document.createElement("a");

			document.body.appendChild(downloadLink);

			if(navigator.msSaveOrOpenBlob){
			var blob = new Blob(['\ufeff', tableHTML], {
			type: dataType
			});
			navigator.msSaveOrOpenBlob( blob, filename);
			} else {
			// Create a link to the file
			downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

			// Setting the file name
			downloadLink.download = filename;

			//triggering the function
			downloadLink.click();
			}
			}
			</script>-->
		</div>	
	</div>
</div>

<!-- Modal for adding item from inventory -->
<div class="modal fade" id="add_inv_janitorial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
	    	<div class="modal-body">
	    		<form action="" method="post">
		        	<div class="form-group row">
						<div class="col-sm-12 col-md-12">
							<label for="item_name">Item Name</label>
							<input type="text" id="drug_item_name" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4 col-md-4">
							<label for="Quantity">Quantity</label>
							<input type="number" id="drug_item_qty" class="form-control">
						</div>
						<div class="col-sm-4 col-md-4">
							<label for="Quantity">Unit</label>
							<select class="form-control" id="drug_item_unit">	
								<option value="ream">ream</option>
								<option value="pc">pc</option>
								<option value="unit">unit</option>
								<option value="box">box</option>
								<option value="pax">pax</option>
								<option value="set">set</option>
							</select>
						</div>
						<div class="col-sm-4 col-md-4">
							<label for="Quantity">Unit Cost</label>
							<input type="number" id="drug_item_unit_cost" class="form-control" step="any">
						</div>
					</div>
				</div>
			    <div class="modal-footer">
			        <button type="button" id="submit_janitorial" class="btn btn-primary">Add</button>
			    </div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		 $('#table_drugs').DataTable({
		 	bSort: false
		 });
			$('#submit_janitorial').click(function(){
				var role="add_item_drugs";
				var itemname=$('#drug_item_name').val();
				var itemqty=$('#drug_item_qty').val();
				var itemunit=$('#drug_item_unit').val();
				var itemunitcost=$('#drug_item_unit_cost').val();
			$.ajax({
				url: 'select_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				item_name: itemname,
				item_qty: itemqty,
				item_unit: itemunit,
				item_unit_cost: itemunitcost
				},
				success: function(response) {
					location.reload();
				}
			});
		});
	});


</script>