<table class="table" id="table_header" style="margin:0; border: none;">
	<input type="hidden" name="hidden_id" id="hidden_trans_id" value="" />
		<tr style="border: none;">
			<td style="border: none;" width="15%" align="center"><img src="seal1.png" alt="Smiley face" width="100"></img></td>
			<td style="border: none;" width="70%" style="margin: 0; font-size: 0.8em;" align="center">REPUBLIC OF THE PHILIPPINES<br>
				<h5 style="margin: 0;">DEPARTMENT OF EDUCATION</h5>
				NATIONAL CAPITAL REGION<br>
				<h5 style="margin: 0;">SCHOOLS DIVISION OFFICE - MARIKINA CITY</h5>
			</td>
			<td style="border: none;" width="15%" align="center"><img src="deped_logo.png" alt="Smiley face" width="100"></img></td>
		</tr>
	</table>
<script type="text/javascript">
$(document).ready(function() {
	$('#table_header').hide();
	$('#table_header2').hide();
	$('#table_footer').hide();
});
</script>
<h3><center>Requisition and Issuance</center></h3>
<table class="table table-bordered display table-sm" id="table_header2">
	<tr>
		<td>Entity Name: <span>SDO MARIKINA</span></td>
		<td>Fund Cluster: <span>1101101</span></td>
	</tr>
	<tr>
		<td>Division: </td>
		<td>Responsibility Center Code: </td>
	</tr>
	<tr>
		<td>Office: </td>
		<td>RIS No. : </td>
	</tr>
</table>
<table class="table table-striped table-bordered display table-sm" id="tabledark">
	<thead>
		<tr>
			<th colspan="4"><center>Requisition</center></th>
			<th colspan="3"><center>Issue</center></th>
		</tr>
		<tr>
			<th width="7%">Stock No.</th>
			<th width="7%">Unit</th>
			<th width="45%">Description</th>
			<th width="15%">Quantity</th>
			<th>Quantity</th>
			<th colspan="2" style="vertical-align: middle;">Remarks</th>
		</tr>
	</thead>
	<tbody id="tbodydark">
		<input type="hidden" id="hidden_id" value="<?php if(isset($_GET['eventid'])){ echo $_GET['eventid']; } else echo 0; ?>" />
		<input type="hidden" id="unique_identifier" value="2">
			<script type="text/javascript">
			$(document).ready(function(){
			var role="select_item_by_trans";
			var trans=$('#hidden_id').val();
			var unique_i=$('#unique_identifier').val();
				$.ajax({
					url: 'select_webservice.php',
					type: 'POST',
					data: {
					tag: role,
					eventid: trans,
					uniqueid: unique_i
					},
					success: function(response) {
						if(response=="wala"){
						} else if (response!="wala"){
						console.log(response);
						var data = JSON.parse(response);
							for(var key in data) {
								document.getElementById("tbodydark").innerHTML += "<tr><td></td>" + "<td>" + data[key].item_unit + "</td><td>" + data[key].item_name + "</td><td>" + data[key].qty + "</td><td></td>" + "<td></td>" + "</tr>";
							}
						}
					}
				});
			});
		</script>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2">
				<label>Purpose</label>
			</td>
			<td><input type="text" placeholder="Type your purpose here ..." class="form-control purpose" name="purpose"></input></td>
			<td colspan="4"><a href="#" id="edit_purpose"><i>Edit</i></a></td>
		</tr>
		<script type="text/javascript">
		$('.purpose').on('focusout', function(){
			swal({
			title: "Purpose set successfully!",
			html: "you can change the purpose by clicking edit at the right of the screen.",
			type: 'success',
			animation: true,
			timer: 1100,
			showConfirmButton: false,
			})
			.then((result) => {
				$('.purpose').attr('disabled', true);
			});
			$('#edit_purpose').click(function(){
				$('.purpose').prop('disabled', false);
			});
		});
		</script>
	</tfoot>
</table>
<table id="table_footer" cellpadding="0" border="0" width="100%">
	<tr>
		<td width="15%" valign="bottom" style="padding: 0; margin: 0; vertical-align: bottom; border-bottom: none;"></td>
		<td width="25%" style="border-bottom: none;">Requested By:</td>
		<td width="20%" style="border-bottom: none;">Approved By:</td>
		<td width="20%" style="border-bottom: none;">Issued By: </td>
		<td width="20%" style="border-bottom: none;">Received By: </td>
	</tr>
	<tr>
		<td style="border-top: none;">Signature: </td>
		<td style="border-top: none;"></td>
		<td style="border-top: none;"></td>
		<td style="border-top: none;"></td>
		<td style="border-top: none;"></td>
	</tr>
	<tr>
		<td>Printed Name: </td>
		<td><span id="req_name" style="text-transform: uppercase; font-weight: bold;">
			<?php if(empty($_SESSION['full_name'])) {
	
		} else {
			echo $_SESSION['full_name'];
		} ?>
		</span></td>
		<td><b>ANNA MARIE P. EXEQUIEL</b></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>Designation: </td>
		<td><?php if(empty($_SESSION['position'])) {
			//do nothing
		} else {
			echo $_SESSION['position'];
		} ?></td>
		<td>AO IV-Supply Unit</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>Date: </td>
		<td><?php echo date('m d,Y'); ?></td>
		<td><?php echo date('m d,Y'); ?></td>
		<td></td>
		<td></td>
	</tr>
</table>
<button type="button" id="add_item" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1"><i class="fas fa-plus-circle"></i> Issue Item
</button>
<button type="button" class="btn btn-primary" id="print_now">
<i class="fas fa-print"></i> Print
</button>
<button type="submit" name="save_view" class="btn btn-primary" id="save_view">
<i class="fas fa-save"></i> Save & Preview
</button>
<script type="text/javascript">
	$(document).ready(function(){
		$('#add_item').hide();
				if(localStorage.getItem('req_fullname') != null) {
					$('#add_item').show();
				}
		$('#print_now').hide();
		var role="check_item";
		var trans=$('#hidden_id').val();
		var uniqueid_c=$('#unique_identifier').val();
		$.ajax({
			url: 'select_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			trans_id: trans,
			uniqueid: uniqueid_c
			},
			success: function(response) {
				console.log(response);
				if(response=="true"){
					$('#save_view').show();
				} else if(response=="false") {
					$('#save_view').hide();
				}
			}
		});	
		$('#print_now').click(function() {
			$('#add_item, #print_now, #save_view, #print, #list, #list, #edit_purpose').hide();
			$('#table_header, #table_header2, #table_footer').show();
			$('.purpose').css('border', 'none');
			$('#view_tab').removeClass('col-sm-10');
			$('#view_tab').addClass('col-sm-12');
			window.print();
			$('#add_item, #print_now, #save_view, #print, #list, #edit_purpose').show();
			$('#table_header, #table_header2, #table_footer').hide();
			$('.purpose').css('border', '');
			$('#view_tab').removeClass('col-sm-12');
			$('#view_tab').addClass('col-sm-10');
			$('#tabledark tbody').remove();
			var url=window.location.href;
			var urlObject = new URL(url);
			urlObject.searchParams.delete('eventid');
			window.location.href=urlObject.href;
		});
		$('#save_view').click(function() {
			// Client-side
			var bb = $('#hidden_id').val();
			var url="issuances_display.php?eventid=" + bb + "";
			swal({
					title: "Issuance Saved!",
					html: "Note: you can view this issuance by going to View Issuances tab.",
					type: 'success',
					animation: true,
					timer: 2000,
					showConfirmButton: false,
				})
				.then((result) => {
					localStorage.removeItem('req_fullname');
					localStorage.removeItem('req_position');
					localStorage.removeItem('req_unit');
					$('#table_header2, #table_footer, #print_now').show();
					$('#save_view').hide();
					$('#print_now').show();
				});
		});
	});
</script>
<!-- Modal for adding item from inventory -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" method="post">
					<div class="form-group row">
						<div class="col-sm-12 col-md-12">
							<label for="item_name">Description</label>
								<input type="text" list="items" class="form-control item_drop" name="">
							<datalist id="items" class="item_drop">
								<option></option>
								<?php
								$select_i="SELECT * FROM medicine_and_drugs_table";
								$select_i_exec=$pdo2->prepare($select_i);
								$select_i_exec->execute();
								$arr1=array();
								while($row_i=$select_i_exec->fetch(PDO::FETCH_ASSOC)) {
									array_push($arr1, $row_i['items']);
								}
								foreach ($arr1 as $key => $value) {
								# code...
								echo"<option value='$value'>$value</option>";
								}
								?>
							</input>
						</div>
					</div>
				
					<div class="form-group row">
						<div class="col-sm-6 col-md-6">
							<label for="stock">On Stock</label>
							<input type="number" name="item_stock" class="form-control stock_avail" disabled>
						</div>
						<script type="text/javascript">
						$('.item_drop').on('keyup', function(){
						var role="select_stock_meds";
						var item=$(this).val();
							$.ajax({
								url: 'select_webservice.php',
								type: 'POST',
								data: {
								tag: role,
								item_name: item,
								},
								success: function(response) {
									var data=JSON.parse(response);
									var stock=data['stock'];
									$('.stock_avail').val(stock);
								}
							});
						});
						</script>
						<div class="col-sm-6 col-md-6">
							<label for="Quantity">Quantity</label>
							<input type="number" id="item_qty" class="form-control">
						</div>
					</div>
				</div>
					<div class="modal-footer">
						<button type="button" id="submit1" data-id="2" class="btn btn-primary">Add</button>
					</div>
				</form>	
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#submit1').click(function(){
		var role="add_issued_item_meds";
		var item=$('.item_drop').val();
		var quantity=$('#item_qty').val();
		var id=$('#hidden_id').val();
		var unique=$(this).data('id');
		$.ajax({
			url: 'select_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			item_name: item,
			item_qty: quantity,
			eventid: id,
			unique_ident: unique
			},
			success: function(response) {
				location.reload();
			}
		});
	});
</script>