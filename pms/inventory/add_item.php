<?php include('Connect.php'); ?>
<div class="container-fluid">
	<div class="card">
		<div class="card-header">
			<h2 id="key"><?php echo $_GET['category']; ?></h2>
		</div>
			<div class="card-body">	
				<div class="form-group row">
					<div class="col-sm-12 col-md-12 table-responsive">
						<table class="table table-striped table-bordered display table-sm" width="100%" id="tableid">
							<thead>
								<tr>
									<th>Particular</th>
									<th colspan="3">Purchases</th>
									<th colspan="3">Available Stocks for the Month</th>
									<th>Issuances</th>
									<th>Balance as of <?php echo date('m/d/y'); ?></th>
									<th>Per Count</th>
									<th>Shortages</th>
									<th rowspan="2">Remarks</th>
								</tr>
								<tr>
									<th>Items</th>
									<th>Qty</th>
									<th>Unit Cost</th>
									<th>Amount</th>

									<th>Qty</th>
									<th>Unit Cost</th>
									<th>Amount</th>

									<th>Qty</th>
									<th>Qty</th>
									<th>Qty</th>
									<th>Qty</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		<div class="card-footer">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_inv">
			<i class="fas fa-plus-circle"></i> Add item
			</button>
			<a class="btn btn-primary" href="download_summary.php?category=<?php echo $_GET['category']; ?>"><i class="fas fa-print"></i> Print</a>
			<!--<script type="text/javascript">
			function exportToExcel(tableid, filename = ''){
			var downloadLink;
			var dataType = 'application/vnd.ms-excel';
			var tableSelect = document.getElementById(tableid);
			var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
			console.log(tableHTML);

			// Specify file name
			filename = filename?filename+'.xls':'excel_data.xls';

			// Create download link element
			downloadLink = document.createElement("a");

			document.body.appendChild(downloadLink);

			if(navigator.msSaveOrOpenBlob){
			var blob = new Blob(['\ufeff', tableHTML], {
			type: dataType
			});
			navigator.msSaveOrOpenBlob( blob, filename);
			} else {
			// Create a link to the file
			downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

			// Setting the file name
			downloadLink.download = filename;

			//triggering the function
			downloadLink.click();
			}
			}
			</script>-->
		</div>	
	</div>
</div>

<!-- Modal for adding item from inventory -->
<div class="modal fade" id="add_inv" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
	    	<div class="modal-body">
	    		<form action="" method="post">
		        	<div class="form-group row">
						<div class="col-sm-12 col-md-12">
							<label for="item_name">Item Name</label>
							<input type="text" id="item_name" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4 col-md-4">
							<label for="Quantity">Quantity</label>
							<input type="number" id="item_qty_s" class="form-control">
						</div>
						<div class="col-sm-4 col-md-4">
							<label for="Quantity">Unit</label>
							<select class="form-control" id="item_unit">	
								<option value="ream">ream</option>
								<option value="pc">pc</option>
								<option value="unit">unit</option>
								<option value="box">box</option>
								<option value="pax">pax</option>
								<option value="set">set</option>
								<option value = "bottle">bottle</option>
								<option value = "can">can</option>
								<option value = "bottle">gallon</option>
							</select>
						</div>
						<div class="col-sm-4 col-md-4">
							<label for="Quantity">Unit Cost</label>
							<input type="number" id="item_unit_cost" class="form-control" step="any">
						</div>
					</div>
				</div>
			    <div class="modal-footer">
			        <button type="button" id="submit" class="btn btn-primary">Add</button>
			    </div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var role="sel_office_item";
		var key=$('#key').text();
		if(key=="Common Office Supplies"){
			key="office_supplies";
		}
		if(key=="Drugs and Medicine Expenses") {
			key="medicine_and_drugs";
		}
		if(key=="Medical, Dental, Laboratory Supplies"){
			key="medical";
		}
		if(key=="Other Supplies and Materials") {
			key="other_supplies";
		}
		if(key=="Accountable Forms") {
			key="accountable_forms";
		}
		if(key=="Semi-Expendable") {
			key="semi_expendable";
		}
		if(key=="Office Equipment") {
			key="office_equipment";
		}
		if(key=="ICT Equipment") {
			key="ict_equipment";
		}
		if(key=="Furnitures and Fixtures") {
			key="furnitures_and_fixtures";
		}
		$.ajax({
			url: "select_webservice.php",
			type: "post",
			data: {
				tag: role,
				category: key,
			},
			success: function(response){
				var data=JSON.parse(response);
				if ($.fn.DataTable.isDataTable("#tableid")) {
					$('#tableid').DataTable().clear().destroy();
				}
                $('#tableid').DataTable({
					"bSort": false,
					"aaData": data,
					'columnDefs': [{
					    "targets": [5, 6], // your case first column
					    "className": "text-right",
					}],
				});
			}
		});
	});
	$('#submit').click(function(){
		var role="add_item";
		var itemname=$('#item_name').val();
		var itemqty=$('#item_qty_s').val();
		var itemunit=$('#item_unit').val();
		var itemunitcost=$('#item_unit_cost').val();
		var key=$('#key').text();
		console.log(key);
		if(key=="Common Office Supplies"){
			key="office_supplies";
		}
		if(key=="Drugs and Medicine Expenses") {
			key="medicine_and_drugs";
		}
		if(key=="Medical, Dental, Laboratory Supplies"){
			key="medical";
		}
		if(key=="Other Supplies and Materials") {
			key="other_supplies";
		}
		if(key=="Accountable Forms") {
			key="accountable_forms";
		}
		if(key=="Semi-Expendable") {
			key="semi_expendable";
		}
		if(key=="Office Equipment") {
			key="office_equipment";
		}
		if(key=="ICT Equipment") {
			key="ict_equipment";
		}
		if(key=="Furnitures and Fixtures") {
			key="furnitures_and_fixtures";
		}
		$.ajax({
			url: 'select_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			item_name: itemname,
			item_qty: itemqty,
			item_unit: itemunit,
			item_unit_cost: itemunitcost,
			category: key,
			},
			success: function(response) {
				location.reload();
			}
		});
	});
</script>