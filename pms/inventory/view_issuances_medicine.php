<div class="card">
	<div class="card-header">
		<ul class="nav nav-pills" id="myTab">
			<li class="nav-item"><a class="nav-link office tab_content" href="#tab1default" data-toggle="tab">Office Supplies Issuance</a></li>
			<li class="nav-item dropdown"><a class="nav-link dropdown-toggle other_office tab_office" href="#" data-toggle="dropdown">Issuance by Office</a>
			 <div class="dropdown-menu">
			 	<?php
			 		$select_office="SELECT * FROM office_table";
					$select_o_exec=$pdo2->prepare($select_office);
					$select_o_exec->execute();
						$arr1=array();
						while($row=$select_o_exec->fetch(PDO::FETCH_ASSOC)) {
							array_push($arr1, $row['office']);
						}
						$i=0;
						foreach ($arr1 as $key => $value) {
							echo"<a class='dropdown-item link_office' id='link_office' data-office='$value' data-toggle='tab' href='#tab2default'>$value</a>";
						}
			 	?>
			    		<div class="dropdown-divider"></div>
			    	<a class="dropdown-item" href="#">Separated link</a>
			    </div>
			</li>
			<li class="nav-item"><a class="nav-link some_office tab_content" href="#tab3default" data-role="select_complete" data-toggle="tab">Issuance by Request</a></li>
		</ul>
	</div>
	<div class="card-body">
		<div class="tab-content">
			<div class="tab-pane fade" id="tab1default">
				<div class="col-sm-12 col-md-12" style="padding: 0;">
					<table class="table table-striped table-bordered display table-sm compact" id="table_office">
						<thead>
							<tr>
								<th>Unit</th>
								<th>Description</th>
								<th>Issued</th>
								<th style="border-top: none;">Remarks</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$select="SELECT * FROM medicine_and_drugs_table";
							$select_exec=$pdo2->prepare($select);
							$select_exec->execute();
							$issuances=0;
							while($row=$select_exec->fetch(PDO::FETCH_ASSOC)){
							$item_name=$row['items'];
							$item_unit=$row['item_unit'];
							$item_qty=$row['balance'];
							$other_qty=$row['qty_from_issuances'];
							//$date_issued=date('m/d/Y', strtotime($row['date_issued']));
							//$office=$row['office_name'];
							echo"<tr>
								<td>$item_unit</td>
								<td>$item_name</td>
								<td>$other_qty</td>
								<td>$item_qty</td>
							</tr>";
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="tab-pane fade" id="tab2default">
				<h4><span id="office"></span></h4>
					<table class="table table-bordered display table-sm compact" id="table_office2">
     					<thead>
     						<tr>
     							<th>Unit</th>
     							<th>Description</th>
     							<th>Issued</th>
     							<th>Date Issued</th>
     							<th>Remarks</th>
     						</tr>
     					</thead>
     							<tbody id="tabledark">
     							</tbody>
     				</table>
			</div>
			<div class="tab-pane fade" id="tab3default">
				<table class="table table-bordered table-striped display table-sm compact" id="table_req">
					<thead>
						<tr>
							<th>Office Name</th>
							<th>Date Issued</th>
							<th>Remarks</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$get_unique="2";
						$select_by_req="SELECT * FROM supply_issuances_table WHERE unique_category=? GROUP BY trans_id ORDER BY issuances_id DESC";
						$key_by_req=$pdo2->prepare($select_by_req);
						$key_by_req->execute([$get_unique]);
						while($row_key=$key_by_req->fetch(PDO::FETCH_ASSOC)) {
						echo"<tr>
							<td><a target='_blank' href='issuances_display.php?eventid=".$row_key['trans_id']."'>".$row_key['office_name']."</a></td>
							<td>".$row_key['date_issued']."</td>
							<td></td>
						</tr>";
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.link_office').click(function() {
		$('#tabledark').html("").fadeOut(0);
		var data=$(this).data("office");
		console.log(data);
		var role="office_sel";
				$('span#office').text(data).fadeOut(0);
				$('span#office').text(data).fadeIn(100);
				$.ajax({
                	url: 'select_webservice.php',
                	type: 'POST',
                	data: {
                	tag: role,
                	office: data,
                    },
                    success : function(response) {
                    	$('#tabledark').append(response).fadeIn(100);
                    }
                });
	});
	$('#table_office').DataTable();
		$('#table_req').DataTable({
			"bSort": false,
		});
	$('#table_office2').DataTable();
</script>