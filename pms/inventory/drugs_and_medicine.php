	<div class="container-fluid" style="padding-top: 20px;">	
		<div class="row">
			<div class="col-sm-2" id="list">
				<div class="card" style="padding: 0;">
					<div class="card-header">
						<h4>Drugs and Medicine Supplies</h4>
					</div>
					<div class="card-body" style="padding: 0;">
						<div class="list-group-flush" id="list-tab" role="tablist">
							<div class="list-group" id="list-tab" role="tablist">
								<a class="list-group-item list-group-item-action" data-value="office_supplies" id="tab" data-toggle="tab" href="#tab1">Summary</a>
								<a class="list-group-item list-group-item-action new_trans" id="tab" data-value="new_iss" data-toggle="tab" href="#tabnew">New Issuance</a>
								<a href="#tabview" id="tab" class="list-group-item list-group-item-action" data-toggle="tab">View Issuances</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-10" id="view_tab">
				<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade" id="tabview" role="tabpanel">
						<?php include('view_issuances_medicine.php'); ?>
					</div>
					<div class="tab-pane fade" id="tabnew" role="tabpanel" style="border: none;">
						<?php include('issuances_form_medicine.php'); ?>
					</div>
					<div class="tab-pane fade" id="tab1" role="tabpanel">
						<?php include('add_item_medicine.php'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
		<!--request modal-->
<div class="modal fade" id="set_req_med" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form action="" method="post">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Set Requested By: </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
		<div class="modal-body">
	      	<div class="form-group row">
	      		<div class="col-sm-12">
		      		<label>Requested By </label>
			        <select class="form-control accounts" name="accounts">
			        	<option></option>
			        	<?php
			        		$select_accounts="SELECT * FROM accounts";
			        		$exec_acc=$pdo->prepare($select_accounts);
			        		$exec_acc->execute();
			        			while ($row_acc=$exec_acc->fetch(PDO::FETCH_ASSOC)) {
			        				# code...
			        				$first_name=$row_acc['user_name'];
			        				$last_name=$row_acc['last_name'];
			        				$user_id=$row_acc['user_id'];
			        				$full_name=$first_name . " " . $last_name;
			        				echo"<option value='$user_id'>$full_name</option>";
			        			}
			        	?>
			        </select>
			    </div>
	    	</div>
		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary" id="save_acc" name="btn_save">Save changes</button>
      		</div>
  		</form>
    </div>
  </div>
</div>
<!--end-->
<script type="text/javascript">
$(document).ready(function() {
	//localStorage.removeItem('req_fullname');
	//console.log(localStorage.getItem('req_fullname'));
	$('#save_acc').click(function(){
		var role="acc_sess";
		var acc=$('.accounts').val();
		$('#set_req_med').modal('hide');
		console.log(acc);
		$.ajax({
			url: 'select_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			accounts: acc
			},
			success: function(response) {
				var role="add_trans_no";
				var get_cat="<?php echo $_GET['category']; ?>";
					$.ajax({
						url: 'select_webservice.php',
						type: 'POST',
						data: {
						tag: role,
						},
						success: function(response) {
						var newurl = "?category=" + get_cat + "&eventid=" + response;
						$('#hidden_trans_id').val(response);
						window.history.pushState('', '', newurl);
						}
					});
				console.log(response);
				var data=JSON.parse(response);
				localStorage.setItem('req_fullname', data['fullname']);
				localStorage.setItem('req_position', data['position']);
				localStorage.setItem('req_unit', data['unit']);
				var getfullname=localStorage.getItem('req_fullname');
				var getposition=localStorage.getItem('req_position');
				var getfullname=localStorage.getItem('req_unit');
				console.log(getfullname);
				swal({
					title: "Account set successfully!",
					html: "Note: You must save the active issuance in order to make a new issuance.",
					type: 'success',
					animation: true,
					timer: 2000,
					showConfirmButton: false,
				})
				.then((result) => {					
					location.reload();
				});
			}
		});
	});
	if(localStorage.getItem('activeTab')==null){
			$('#list-tab a[href="#tab1"]').tab('show');
	}
	$('a[id="tab"]').on('show.bs.tab', function(e) {
		localStorage.setItem('activeTab', $(e.target).attr('href'));
	});
		var activeTab = localStorage.getItem('activeTab');
		if(activeTab) {
			$('#list-tab a[href="' + activeTab + '"]').tab('show');
			$('#list-tab a[href="' + activeTab + '"]').tab('dispose');
			console.log(activeTab);
			if(activeTab == '#tabnew') {
				if(localStorage.getItem('req_fullname') == null) {
					$('#set_req_med').modal('show');
				} else {
					// do nothing
					$('#set_req_med').modal('hide');
				}
			} else if(activeTab == '#tabnew_devices') {
				if(localStorage.getItem('req_fullname') == null) {
					$('#set_req_med').modal('show');
					alert('pangalawa');
					var role="add_trans_no";
					var get_cat="<?php echo $_GET['category']; ?>";
					$.ajax({
						url: 'select_webservice.php',
						type: 'POST',
						data: {
						tag: role,
						},
						success: function(response) {
						var newurl = "?category=" + get_cat + "&eventid=" + response;
						$('#hidden_trans_id').val(response);
						window.history.pushState('', '', newurl);
						}
					});
				} else {
					// do nothing
					$('#set_req_med').modal('hide');
				}
			}
		}
		$('.new_trans').click(function() {
			localStorage.removeItem('req_fullname');
			localStorage.removeItem('req_position');
			localStorage.removeItem('req_unit');
			location.reload();
		/*var role="add_trans_no";
		var get_cat="<?php //echo $_GET['category']; ?>";*/
		/*$.ajax({
			url: 'select_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			},
			success: function(response) {
			var newurl = "?category=" + get_cat + "&eventid=" + response;
			$('#hidden_trans_id').val(response);
			window.history.pushState('', '', newurl);
			}
		});*/
	});

		$('a[data-value="new_iss"]').on('shown.bs.tab', function (e) {
			if(localStorage.getItem('req_fullname') == null){
				$('#set_req_med').modal('show');
				alert('pangatlo');
			} else {
				// do nothing
				$('#set_req_med').modal('hide');
			}
		});
		
		$('a[data-value="office_supplies"]').click(function(){
			localStorage.removeItem('req_fullname');
			localStorage.removeItem('req_position');
			localStorage.removeItem('req_unit');

		});

		$('a[data-value="new_devices"]').on('shown.bs.tab', function (e) {
			if(localStorage.getItem('req_fullname') == null){
				$('#set_req_med').modal('show');
				alert('pangapat');
			} else {
				// do nothing
				$('#set_req_med').modal('hide');
			}
		});

		$('a[href="#tabview"]').click(function() {
			var url=window.location.href;
			var urlObject = new URL(url);
			urlObject.searchParams.delete('eventid');
			window.location.href=urlObject.href;
		});

		$('a[data-value="office_supplies"]').click(function() {
			var url=window.location.href;
			var urlObject = new URL(url);
			urlObject.searchParams.delete('eventid');
			window.location.href=urlObject.href;
		});
});
</script>