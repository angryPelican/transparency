<div class="card">
	<div class="card-header">
		<ul class="nav nav-pills" id="myTab">
			<li class="nav-item"><a class="nav-link office tab_content" href="#tab1default" data-toggle="tab"><?php echo $_GET['category'] ?></a></li>
			<li class="nav-item dropdown"><a class="nav-link dropdown-toggle other_office tab_office" href="#" data-toggle="dropdown">Issuance by Office</a>
			 <div class="dropdown-menu" id="drop">
			 	<?php
			 		$select_office="SELECT * FROM office_table";
					$select_o_exec=$pdo->prepare($select_office);
					$select_o_exec->execute();
						$arr1=array();
						while($row=$select_o_exec->fetch(PDO::FETCH_ASSOC)) {
							array_push($arr1, $row['office']);
						}
						foreach ($arr1 as $key => $value) {
							echo"<a class='dropdown-item link_office' id='link_office' data-office='$value' data-toggle='tab' href='#tab2default'>$value</a>";
						}
			 	?>
			    </div>
			</li>
			<li class="nav-item"><a class="nav-link some_office tab_content" href="#tab3default" id="by_issuance" data-role="select_complete" data-toggle="tab">Issuance by Request</a></li>
		</ul>
	</div>
	<div class="card-body">
		<div class="tab-content">
			<div class="tab-pane fade" id="tab1default">
				<div class="col-sm-12 col-md-12" style="padding: 0;">
					<table class="table table-striped table-bordered display table-sm compact" style="width: 100%;" id="table_office">
						<thead>
							<tr>
								<th>Unit</th>
								<th>Description</th>
								<th>Issued</th>
								<th>Remarks</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="tab-pane fade" id="tab2default">
				<h4><span id="office"></span></h4>
					<table class="table table-bordered display table-sm compact" style="width: 100%;" id="table_office2">
     					<thead>
     						<tr>
     							<th>Unit</th>
     							<th>Description</th>
     							<th>Issued</th>
     							<th>Date Issued</th>
     							<th>Remarks</th>
     						</tr>
     					</thead>
     				</table>
			</div>
			<div class="tab-pane fade" id="tab3default">
				<table class="table table-bordered table-striped display table-sm compact" style="width: 100%;" id="table_req">
					<thead>
						<tr>
							<th>Office Name</th>
							<th>Date Issued</th>
							<th>Remarks</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('a[href="#tab1default"]').on('shown.bs.tab', function (e) {
		var key=$(this).text();
		if (key=="Office Equipment" || key=="ICT Equipment" || key=="Furnitures and Fixtures") {
			var status="section2";
		} else {
			var status="section1";
		}
		if(status=="section1") {
			
			if(key=="Common Office Supplies"){
				key="office_supplies";
			}
			if(key=="Drugs and Medicine Expenses") {
				key="medicine_and_drugs";
			}
			if(key=="Medical, Dental, Laboratory Supplies"){
				key="medical";
			}
			if(key=="Other Supplies and Materials") {
				key="other_supplies";
			}
			if(key=="Accountable Forms") {
				key="accountable_forms";
			}
			if(key=="Semi-Expendable") {
				key="semi_expendable";
			}
			var role1="sel_office_items";
			$.ajax({
				url: "select_webservice.php",
				type: "post",
				data: {
					tag: role1,
					category: key,
				},
				success: function(response) {
					var data=JSON.parse(response);
					if ($.fn.DataTable.isDataTable("#table_office")) {
						$('#table_office').DataTable().clear().destroy();
					}
					$('#table_office').DataTable({
						"bSort": false,
						"aaData": data,
					});
				}
			});
		} else if (status=="section2") {
			
			if(key=="Office Equipment") {
				key="office_equipment";
			}
			if(key=="ICT Equipment") {
				key="ict_equipment";
			}
			if(key=="Furnitures and Fixtures") {
				key="furnitures_and_fixtures";
			}
			var role="sel_equipment_items";
			$.ajax({
				url: "select_webservice.php",
				type: "post",
				data: {
					tag: role,
					category: key,
				},
				success: function(response) {
					var data=JSON.parse(response);
					if ($.fn.DataTable.isDataTable("#table_office")) {
						$('#table_office').DataTable().clear().destroy();
					}
					$('#table_office').DataTable({
						"bSort": false,
						"aaData": data,
					});
				}
			});
		}
	});
	$('.link_office').click(function() {
		var key=$('a[href="#tab1default"]').text();
		if (key=="Office Equipment" || key=="ICT Equipment" || key=="Furnitures and Fixtures") {
			var status="section2";
		} else {
			var status="section1";
		}
		if(status=="section1") {
			var data=$(this).data("office");
			console.log(data);
			var role="office_sel";
			$('span#office').text(data).fadeOut(0);
			$('span#office').text(data).fadeIn(100);
			$.ajax({
	        	url: 'select_webservice.php',
	        	type: 'POST',
	        	data: {
	               	tag: role,
	               	office: data,
	            },
	            success : function(response) {
	            	var data=JSON.parse(response);
	            	console.log(data);
	                if ($.fn.DataTable.isDataTable("#table_office2")) {
						$('#table_office2').DataTable().clear().destroy();
					}
	                $('#table_office2').DataTable({
						"bSort": false,
						"aaData": data,
					});
	            }
	        });
		} else if (status=="section2") {
			var data=$(this).data("office");
			console.log(data);
			var role="equipment_sel";
			$('span#office').text(data).fadeOut(0);
			$('span#office').text(data).fadeIn(100);
			$.ajax({
	        	url: 'select_webservice.php',
	        	type: 'POST',
	        	data: {
	               	tag: role,
	               	office: data,
	            },
	            success : function(response) {
	            	var data=JSON.parse(response);
	            	console.log(data);
	                if ($.fn.DataTable.isDataTable("#table_office2")) {
						$('#table_office2').DataTable().clear().destroy();
					}
	                $('#table_office2').DataTable({
						"bSort": false,
						"aaData": data,
					});
	            }
	        });
		}
	});
	$('#by_issuance').click(function(){
		var key=$('a[href="#tab1default"]').text();
		if (key=="Office Equipment" || key=="ICT Equipment" || key=="Furnitures and Fixtures") {
			var status="section2";
		} else {
			var status="section1";
		}
		if (status=="section1") {
			var role="by_issuance";
			$.ajax({
				url: "select_webservice.php",
				type: "post",
				data: {
					tag: role,
				},
				success: function(response){
					var data=JSON.parse(response);	
					if ($.fn.DataTable.isDataTable("#table_req")) {
						$('#table_req').DataTable().clear().destroy();
					}
	                $('#table_req').DataTable({
						"bSort": false,
						"aaData": data,
					});
				}
			})
		} else if (status=="section2") {
			var role="by_issuance_equip";
			$.ajax({
				url: "select_webservice.php",
				type: "post",
				data: {
					tag: role,
				},
				success: function(response){
					var data=JSON.parse(response);	
					if ($.fn.DataTable.isDataTable("#table_req")) {
						$('#table_req').DataTable().clear().destroy();
					}
	                $('#table_req').DataTable({
						"bSort": false,
						"aaData": data,
					});
				}
			})
		}
	});
</script>