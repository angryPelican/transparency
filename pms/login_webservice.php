<?php
include("Connect.php");
	$tag=$_POST['tag'];
	switch ($tag) {
		case 'change_pass':
			$newpass=$_POST['new_password'];
			$new_md5_pass=md5($newpass);
			$user_id=$_POST['user_id'];
			$update_pass="UPDATE accounts SET user_password=? WHERE user_id=?";
			$update_pass_exec=$pdo->prepare($update_pass);
			if($update_pass_exec->execute([$new_md5_pass,$user_id])) {
				echo "success";
			} else {
				$update_pass_exec->errorInfo();
			}
			break;
		case 'all_project_status':
			$status='0000-00-00';
			$user_id=$_POST['user_id'];
			$arr1=array();
			$arr2=array();
			$outer_array=array();
			$all_q="SELECT * FROM project_table WHERE submit_date<>?";
			$all_q_exec=$pdo->prepare($all_q);
			$all_q_exec->execute([$status]);
			$all_q_cnt=$all_q_exec->rowCount();

			$q1="SELECT * FROM project_table WHERE submit_date<>?";
			$q1_exec=$pdo->prepare($q1);
			$q1_exec->execute([$status]);
			$cnt_row1=$q1_exec->rowCount();
			//2
			$status2=10;
			$q2="SELECT * FROM project_table WHERE delivery_status=?";
			$q2_exec=$pdo->prepare($q2);
			$q2_exec->execute([$status2]);
			$cnt_row2=$q2_exec->rowCount();
			if(empty($all_q_cnt)) {
				$percentage = "none";		
				echo $percentage;
			} else {
				$percentage = ($cnt_row2 / $all_q_cnt) * 100 . "%";
				$arr1["row1"]=$cnt_row1;
				$arr1["row2"]=$cnt_row2;
				$arr1["row3"]=$percentage;
				$outer_array[]=$arr1;
				echo json_encode($outer_array);
			}
			break;
		case 'project_status':
			$status='0000-00-00';
			$user_id=$_POST['user_id'];
			$arr1=array();
			$arr2=array();
			$outer_array=array();
			$all_q="SELECT * FROM project_table WHERE user_id=?";
			$all_q_exec=$pdo->prepare($all_q);
			$all_q_exec->execute([$user_id]);
			$all_q_cnt=$all_q_exec->rowCount();

			$q1="SELECT * FROM project_table WHERE submit_date<>? AND user_id=?";
			$q1_exec=$pdo->prepare($q1);
			$q1_exec->execute([$status, $user_id]);
			$cnt_row1=$q1_exec->rowCount();

			//2
			$status2=10;
			$q2="SELECT * FROM project_table WHERE delivery_status=? AND user_id=?";
			$q2_exec=$pdo->prepare($q2);
			$q2_exec->execute([$status2, $user_id]);
			$cnt_row2=$q2_exec->rowCount();
			if(empty($all_q_cnt)) {
				$percentage = "none";
				echo $percentage;
			} else  {
				$percentage = ($cnt_row2 / $all_q_cnt) * 100 . "%";
				$arr1["row1"]=$cnt_row1;
				$arr1["row2"]=$cnt_row2;
				$arr1["row3"]=$percentage;
				$outer_array[]=$arr1;
				echo json_encode($outer_array);
			}
			break;
		case 'login':
			// session_set_cookie_params(0);
			session_start();
			$login_username = $_POST["login_username"];
			$login_password = $_POST["login_password"];
			$password_md5 = md5($login_password);
			$Query_Login = "SELECT * FROM accounts WHERE user_email=? AND user_password=?";
			$stmt=$pdo->prepare($Query_Login);
			$stmt->execute([$login_username, $password_md5]);
			$rowcount=$stmt->rowCount();
			if($rowcount==0){
				echo "no data available";
			} else {
				$status=1;
				$insert_status="UPDATE accounts SET status=? WHERE user_email=?";
				$stmt3=$pdo->prepare($insert_status);
				$stmt3->execute([$status, $login_username]);
				while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
						$user_name_sess=$row['user_name'];
						$section_sess=$row['section'];
						$end_user=$row['user_type'];
						$user_id=$row['user_id'];
						//store session
						$_SESSION['s_section'] = $section_sess;
						$_SESSION['s_username'] = $user_name_sess;
						$_SESSION['s_id'] = $user_id;
						$_SESSION['s_user_type'] = $end_user;
					echo $end_user;
				}
				$datetime=date("Y-m-d H:i:s");
				$q1="INSERT INTO login_logs_table (username,user_type,user_id,date_time) VALUES (?,?,?,?)";
				$q1_exec=$pdo->prepare($q1);
				if($q1_exec->execute([$user_name_sess,$end_user,$user_id,$datetime])){
					
				} else {
					$q1_exec->errorInfo();
				}
			}
			break;
		case 'add_project':
			//GET USER ID FROM TABLE
			$username_id=$_POST['s_id'];
			$title_created=$_POST['get_title'];
			$datenow=date("Y-m-d");
			// INSERT PROJECT TITLE QUERY
			$title_insert_query="INSERT INTO project_table (project_title, date_created, user_id) VALUES (?,?,?)";
			$key2=$pdo->prepare($title_insert_query);
			if($key2->execute([$title_created,$datenow,$username_id])) {
				// SELECT PROJECT QUERY
				$query_select="SELECT * FROM project_table WHERE project_title=?";
				$key3=$pdo->prepare($query_select);
				$key3->execute([$title_created]);
				while($row_get_title=$key3->fetch(PDO::FETCH_ASSOC))
				{
				$title_id=$row_get_title['project_id'];
				$get_code=$row_get_title['code'];
				$title_name= $row_get_title['project_title'];
				}
				//INSERT PROJECT TITLE AND ID TO MILESTONE TABLE QUERY
				$query_mile="INSERT INTO milestone_table (milestone_title_handler, user_id, milestone_handler) VALUES (?,?,?)";
				$key4=$pdo->prepare($query_mile);
				$key4->execute([$title_created,$username_id,$title_id]);
				echo $title_id;
			} else {
				echo"mali";
			}
			break;
	}
?>