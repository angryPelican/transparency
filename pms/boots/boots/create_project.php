<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link rel = "stylesheet" href = "boots/css/bootstrap.min.css" />
	<script type = "text/javascript" src = "boots/js/jquery-2.1.4.min.js"></script>
	<script type = "text/javascript" src = "boots/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">  
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

</head>
<body>
<form name = "myform" action = "" method = "post">
<div class="container">
	<?php
		include ('inc/navigation.php');
	?>
	<h1>Create PPMP</h1>
	<div class="row">
		<div class="col-sm-12">
			<form action="" method="post">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="form-group">
						<label>Project Title:</label>
						<input type="text" style="width: 500px"></input>
						<?php
							include('savetitle.php');
							?>
					</div>
				</div>
				<div class="panel-body table-responsive">
					<!--MODAL-->
					<p class="text-right"><button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#addItem"><i class="glyphicon glyphicon-plus"></i> Add Item</button></p>
					<?php
						include('viewingtitles.php');
						?>
					<p>&nbsp;</p>
					<div class="well">
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<label>Mode of Procurement</label>						
									<select name="unit" style="width:100%;">
										<option>Small Value Procurement</option>
										<option>Shopping</option>
									</select>							
								</div>
							</div>
							<div class="col-sm-8">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<td>Milestone</td>
											<td>Target Date</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
										</tr>								
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<p class="text-right"><button class="btn btn-success"><i class="glyphicon glyphicon-floppy-save"></i> Save</button></p>
					</div>		
				</div>
			</div>
		</div>
	</div>
	</form>
<div class="container">
	<!-- Modal -->
		<div id="addItem" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Item</h4>
					</div>
						<div class="modal-body">
							<form action="" method="post">
			<div class="form-group">
				<label>General Description</label>
				<input type="textarea" style="width:100%; height: 200px;" name = "gets" id="getsi"></input>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label>Quantity/Size:</label>						
						<input type="number" name = "quan" id = "quanti" ></input>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label>Unit:</label><br />						
						<select name="unit" style="width:100%;" id = "units">
							<option value = "pc">pc</option>
							<option value = "unit">unit</option>
							<option value = "set">set</option>
							<option value = "box">box</option>
							<option value = "pax">pax</option>
						</select>
					</div>
				</div>	
				<div class="col-sm-4">
					<div class="form-group">
						<label>Estimated Budget</label><br />						
						<input type="number" name = "buds" id = "est"></input>
					</div>
				</div>	
			</div>
      <div class="modal-footer">
        <button type = "submit" id = "insert_data" name = "ins" class = "btn btn-info btn-lg">+</button>
       </div>
	   		      </form>
	</div>
  </div>
</div>
</div>
		  <?php
			include("insertdata.php");
			?>
</div>
<form action= "" method = "get">
<div id="editItem" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Item</h4>
      </div>
      <div class="modal-body">
        <form action="">
			<div class="form-group">
				<label>General Description</label>
				<input type="textarea" style="width:100%; height: 200px;" name = "gets" id="getsi1"></input>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label>Quantity/Size:</label>						
						<input type="number" name = "quan" id = "quanti1" ></input>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label>Unit:</label><br />						
						<select name="unit" style="width:100%;" id = "units1">
							<option value = "pc">pc</option>
							<option value = "unit">unit</option>
							<option value = "set">set</option>
							<option value = "box">box</option>
							<option value = "pax">pax</option>
						</select>
					</div>
				</div>	
				<div class="col-sm-4">
					<div class="form-group">
						<label>Estimated Budget</label><br />						
						<input type="number" name = buds id = "est1"></input>
					</div>
				</div>
			</div>
		</form>
      </div
      <div class="modal-footer">
        <button type="button" onclick="" class="btn btn-default btn-success" data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>
</form>
</form>
</body>
</html> 