<style type="text/css">
		.modal-footer{
			border-top: 1px solid rgba(0,0,0,0.1);
			background-color: #EEF2F8;
		}
		.form-control{
			border-radius: 1px;
			padding-left: 5px;
			padding-right: 5px;
			padding-top: 2px;
			padding-bottom: 2px;
			font-size: 12px;
			height: 30px;
		}
		.jumbotron{
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1e5799+0,7db9e8+100 */
background: #1e5799; /* Old browsers */
background: -moz-linear-gradient(top, #1e5799 0%, #7db9e8 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #1e5799 0%,#7db9e8 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #1e5799 0%,#7db9e8 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=0 ); /* IE6-9 */
color: white;
		}
		.btn{
				font-size: 12px;
			    border-radius: 3px;
			    border: 1px solid #999;
			    padding-left: 30px;
			    padding-right: 30px;
			    padding-top: 2px;
			    padding-bottom: 2px;
			    background: #F0F0F0;
			    background: -moz-linear-gradient(top,  #F0F0F0 50%, #D4D4D4 50%);
			    background: -webkit-gradient(linear, left top, left bottom, color-stop(50%,#F0F0F0), color-stop(50%,#D4D4D4));
			    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#F0F0F0', endColorstr='#D4D4D4',GradientType=0 );
			    color: black !important;
		}
		.modal-content{
			overflow: hidden;
			box-shadow: 0px 5px 20px rgba(0,0,0,0.7);
			padding: 5px;
			border: 1px solid white;
			/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#e0f3fa+0,d8f0fc+50,b8e2f6+51,b6dffd+100;Shape+2+Style */
			background: #e0f3fa; /* Old browsers */
			background: -moz-linear-gradient(top, #e0f3fa 0%, #d8f0fc 50%, #b8e2f6 51%, #b6dffd 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(top, #e0f3fa 0%,#d8f0fc 50%,#b8e2f6 51%,#b6dffd 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to bottom, #e0f3fa 0%,#d8f0fc 50%,#b8e2f6 51%,#b6dffd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e0f3fa', endColorstr='#b6dffd',GradientType=0 ); /* IE6-9 */
		}
		.modal-body{
			background-color: white;
			border-left: 1px solid rgba(0,0,0,0.3);
			border-right: 1px solid rgba(0,0,0,0.3);
			border-top: 1px solid rgba(0,0,0,0.3);
		}
		.modal-header{
			font-size: 13px !important;
			padding: 5px;
			border-bottom: 1px rgba(0,0,0,0.3);
			text-shadow: 0px 0px 10px white !important;
			background: transparent !important;
			color: black!important;
		}
		.modal-footer{
			border: 1px solid rgba(0,0,0,0.3);
		}
		tr{
			padding: 5px !important;
			font-size: 12px;
		}
		td{
			padding: 5px !important;
			font-size: 12px;

		}
		th{
			padding: 5px !important;
			background-color: #EEF2F8;
			text-align: center;
			font-size: 12px;
		}
</style>