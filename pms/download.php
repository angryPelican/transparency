<?php
require_once('plugins/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
include('php/connectdb.php');
//GET THE TITLE NAME FOR DOWNLOAD
$get_id = $_GET['eventid'];
session_start();
$get_title_name="SELECT * FROM project_table WHERE project_id=?";
$res=$pdo->prepare($get_title_name);
$res->execute([$get_id]);
while($row_name=$res->fetch(PDO::FETCH_ASSOC)):
	$project_title=$row_name['project_title'];
	$user_id = $row_name['user_id'];
	$total = number_format($row_name['total_estimated_budget'], 2);
endwhile;
//user credentials
$username=$_SESSION['s_username'];
$query_select_user = "SELECT * FROM accounts WHERE user_id=?";
$select_user=$pdo->prepare($query_select_user);
$select_user->execute([$user_id]);
$row_select = $select_user->fetch(PDO::FETCH_ASSOC);
$get_username = $row_select['user_name'];
$get_user_type=$row_select['user_type'];
$space=" ";
$get_lastname = $row_select['last_name'];
$get_unit=$row_select['section'];
$get_position=$row_select['position'];
$final_username=strtoupper($get_username);
$final_lastname=strtoupper($get_lastname);
$output = '
<!DOCTYPE html>
<html lang="en">
		<head>
				<title>PPMP</title>
				<title>Bootstrap Example</title>
				<meta charset="utf-8">
				<meta name = "viewport" content = "width=device-width, initial-scale=1">
				<link rel = "stylesheet" href = "boots/boots/css/bootstrap.min.css" />
				<link rel = "stylesheet" type = "text/css" href = "css/theme.css" />
				<link rel = "stylesheet" type = "text/css" href="style.css" />
				<script type = "text/javascript" src = "boots/boots/js/jquery-2.1.4.min.js"></script>
				<script type = "text/javascript" src = "boots/boots/js/bootstrap.min.js"></script>
		</head>
	<body>
		<div class="container" id="emp">
			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<center><p id="p_header"><b>Schools Division of Marikina City</b></p></center>
							<center><p id="p_header">Project Procurement Management Plan (PPMP)</p></center>
							<center><p id="p_header">END-USER / Unit: '.$get_unit.', SDO-MARIKINA CITY</p></center>
							<br>
							<center><p style="font-size:18px; text-transform: uppercase;"><b>'.$project_title.'</b></p></center>
						</div>
						<div class="panel-body table-responsive" style="padding: 0; margin: 0;">
							<table  id="viewTable" class="table display">
								<thead>
									<tr>
										<th>Code</th>
										<th>General Description</th>
										<th>Qty/Size</th>
										<th>Estimated Budget</th>
										<th>Mode of Procurement</th>
										<th>Milestone of Achievements</th>
										<th>Remarks</th>
									</tr>
								</thead>';
								//FIRST QUERY
								$first_query = "SELECT * FROM items_table WHERE p_title_handler=?";
								$first_result = $pdo->prepare($first_query);
								$first_result->execute([$get_id]);
								$count = $first_result->rowCount();
								//SECOND QUERY
								$second_query = "SELECT * FROM items_table WHERE p_title_handler=?";
								$second_result=$pdo->prepare($second_query);
								$second_result->execute([$get_id]);
								$i = 0;
								$i1 = 0;
								$i2 = 0;
								//loop each items
								while($row1 = $second_result->fetch(PDO::FETCH_ASSOC)){
								$sx = $row1['p_title_name'];
								$s1 = $row1['p_description'];
								$s2 = $row1['p_quantity_size'];
								$s3 = $row1['p_unit'];
								$s4 = $row1['p_estimated_budget'];
								$s5 = $row1['code'];
								$formattedNum = number_format($s4, 2);
								$output .= "<tr>";
									//Code Column Rowspan
									if($i1 === 0) {
									$output .= "<td rowspan='".$count."' class='td_items'>$s5</td>";
									$i1=1;
									}
									$output .= "<td>".$s1."</pre></td>
									<td class='td_items'>".$s2." - ".$s3."</td>
									<td align='right' class='td_items'>".$formattedNum."</td>";
									//MODE OF PROCUREMENT QUERY
									$querymile = "SELECT * FROM project_table JOIN milestone_table ON project_table.project_id = milestone_table.milestone_handler WHERE project_id='$get_id'";
									$resultmile = $pdo->prepare($querymile);
									$resultmile->execute();
									while($row_get=$resultmile->fetch(PDO::FETCH_ASSOC)){
									$mode = $row_get['mode_of_procurement'];
									$fund=$row_get['source_of_fund'];
									$milestone_check1 = $row_get['approval_of_request'];
									$milestone_check2 = $row_get['philgeps_posting'];
									$milestone_check3 = $row_get['pre_bid'];
									$milestone_check4 = $row_get['opening_of_bid'];
									$milestone_check5 = $row_get['post_qualification'];
									$milestone_check6 = $row_get['notice_of_award'];
									$milestone_check7 = $row_get['notice_of_proceed'];
									if($i == 0){
									$output .= "<td rowspan='$count' align='center'>$mode</td>
									<td rowspan=$count><ol>";
										if($milestone_check1 != '0000-00-00'){
										$formattedDate1=date('F d Y', strtotime($milestone_check1));
										$output .= "<li>Implementation Date - $formattedDate1</li>";
										}
										else if($milestone_check1 == '0000-00-00'){
										}
										if($milestone_check2 != '0000-00-00'){
										$formattedDate2=date('F d Y', strtotime($milestone_check2));
										$output .= "<li>Purchase Order - $formattedDate2</li>";
										}
										else if($milestone_check2 == '0000-00-00'){
										}
										if($milestone_check3 != '0000-00-00'){
										$formattedDate3=date('F d Y', strtotime($milestone_check3));
										$output .= "<li>Abstract of Quotation - $formattedDate3</li>";
										}
										else if($milestone_check3 == '0000-00-00'){
										}
										if($milestone_check4 != '0000-00-00'){
										$formattedDate4=date('F d Y', strtotime($milestone_check4));
										$output .= "<li>Request for Quotation - $formattedDate4</li>";
										}
										else if($milestone_check4 == '0000-00-00'){
										}
										if($milestone_check5 != '0000-00-00'){
										$formattedDate5=date('F d Y', strtotime($milestone_check5));
										$output .= "<li>Purchase Request - $formattedDate5</li>";
										}
										else if($milestone_check5 == '0000-00-00'){
										}
									$output .=  "</ol>
								</td>
								<td rowspan=$count>$fund</td>";
								$i=1;
								}
								}
								}
							$output .= "</tbody>
							<tfoot>
								<tr>
									<td colspan='3' class='text-right'><b>TOTAL</b></td>
									<td align='right'>PHP $total</td>
									<td colspan='4'></td>
								</tr>
							</tfoot>
						</table>
						</div>
							<div class='panel-footer'>
								
							</div>";
					$output .= "
					</div> 

				</div>
 		
		<div class='container removeall'>
			<div class='row removeall'>
				
				<div class='col-sm-6 col-md-6 pull-left'>
			<p>Prepared by:</p>&nbsp; <br/>
				<label class='removeall'><b> $final_username $final_lastname </b></label>

				<div class='col-sm-6 col-md-6 pull-right'>
			<p>Approved by:</p> <br/>
				<p class='uppercase'><b> JOEL T. TORRECAMPO </b><br> </p>
				<p>Officer In-Charge, Office of the SDS</p>
				</div>

				<br/>
				<p class='removeall2'>$get_position</p>
				</div>

			</div>

			</div>
		</div>

</body>
</html>";
$document = new Dompdf();
$document->loadHtml($output);
ob_end_clean();
$document->setPaper('legal', 'landscape');
$document->render();
$pdf = $document->output();
$document->stream("$project_title: ".time(), array("Attachment"=>1));
?>