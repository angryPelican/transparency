<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
	<meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link rel = "stylesheet" href = "boots/css/bootstrap.min.css" />
	<script type = "text/javascript" src = "boots/js/jquery-2.1.4.min.js"></script>
	<script type = "text/javascript" src = "boots/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">  
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

  <style>
	.th_milestone
	{
		text-align: center;	
	}
	.th_date
	{
		text-align: center;
	}
	#table_svp
	{
		display: none;
	}
	#table_shopping
	{
		display: none;
	}
	</style>
</head>
<body>
<form name = "myform" action = "" method = "post">
<div class="container">
	<?php
		include ('inc/navigation.php');
	?>
	<h1>Create PPMP</h1>
	<div class="row">
		<div class="col-sm-12">
			<form action="" method="post" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="form-group">
						<label><?php echo $_GET['category']; ?></label>
					</div>
				</div>
				<div class="panel-body table-responsive">
					<!--MODAL-->
					<p class="text-right"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addItem"><i class="glyphicon glyphicon-plus"></i> Add Item</button></p>
					<table id="myTable" class="table table-striped display">
							<tr>
							<th width="400">Item Description</th>
							<th width="30">Quantity</th>
							<th width="30">Unit</th>
							<th width="300">Estimated Budget</th>
							<th width="30">Edit</th>
							<th width= "30">Delete</th>
							<?php
								include('viewingtitles.php');
								?>
						</tr>
							</table>
					<br><div class="form-group">
						<label>Mode of Procurement</label>						
						<select id = "itemtype" name="unit" style="width:20%;">
							<option value = "default">Default</option>
							<option value = "svp">Small Value Procurement</option>
							<option value = "shopping">Shopping</option>
						</select>
						<button id = "show_modal" type = "button" onclick = "showTable()" class="btn btn-primary">Show</button>						
					</div>
					<p>&nbsp;</p>
					<div class="well">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="col-sm-12">
								<table id = "table_default" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th width = '500px' class = "th_milestone">Milestone</th>
											<th width = '600px' class = "th_date">Target Date</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td height='30'></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								<table id = "table_svp" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th width = '500px' class = "th_milestone">Milestone</th>
											<th width = '600px' class = "th_date">Target Date</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td height='30'> 1. Approval of request </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 2. PhilGEPS Posting </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 3. Pre-bid </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 4. Opening of bid </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 5. Post Qualification </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 6. Notice of Award </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 7. Notice of Proceed </td>
											<td></td>
										</tr>										
									</tbody>
								</table>
								<table id = "table_shopping" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th width = '500px' class = "th_milestone">Shopping</th>
											<th width = '600px' class = "th_date">Target Date</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td height='30'> 1. Shopping </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 2. Shopping </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 3. Shopping </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 4. Shopping </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 5. Shopping </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 6. Shopping </td>
											<td></td>
										</tr>
										<tr>
											<td height='30'> 7. Shopping </td>
											<td></td>
										</tr>										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<p class="text-right"><button class="btn btn-success"><i class="glyphicon glyphicon-floppy-save"></i> Save</button></p>
					</div>		
				</div>
			</div>
		</div>
	</div>
<div class="container">
	<!-- Modal -->
		<div id="addItem" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Item</h4>
					</div>
						<div class="modal-body">
							<form action="" method="post">
			<div class="form-group">
				<label>General Description</label>
				<input type="textarea" style="width:100%; height: 200px;" name = "gets" id="getsi"></input>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label>Quantity/Size:</label>						
						<input type="number" name = "quan" id = "quanti" ></input>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label>Unit:</label><br />						
						<select name="unit" style="width:100%;" id = "units">
							<option value = "pc">pc</option>
							<option value = "unit">unit</option>
							<option value = "set">set</option>
							<option value = "box">box</option>
							<option value = "pax">pax</option>
						</select>
					</div>
				</div>	
				<div class="col-sm-4">
					<div class="form-group">
						<label>Estimated Budget</label><br />						
						<input type="number" name = "buds" id = "est"></input>
					</div>
				</div>	
			</div>
      <div class="modal-footer">
        <button type = "submit" id = "insert_data" name = "ins" class = "btn btn-info btn-lg">+</button>
       </div>
	   		      </form>
	</div>
  </div>
</div>
</div>
		  <?php
			include("insertdata.php");
			?>	
</div>
<script>
$(document).ready(function()
{
    $('#myTable').DataTable();
});
function showTable()
{
	var tbl_default = document.getElementById("table_default");
	var tbl_svp = document.getElementById("table_svp");
	var tbl_shopping = document.getElementById("table_shopping");

	if (document.getElementById("itemtype").selectedIndex == "0")
	{
		tbl_default.style.display = "block";
		tbl_svp.style.display = "none";
		tbl_shopping.style.display = "none";
	}
	else if (document.getElementById("itemtype").selectedIndex == "1")
	{
		tbl_svp.style.display = "block";
		tbl_shopping.style.display = "none";
		tbl_default.style.display = "none";
	}	
	else if (document.getElementById("itemtype").selectedIndex == "2")
	{
		tbl_shopping.style.display = "block";
		tbl_svp.style.display = "none";
		tbl_default.style.display = "none";
	}
}
</script>
</body>
</html> 