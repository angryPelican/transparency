<!DOCTYPE html>
<html lang="en">
<head>
	<title>PPMP | Division Procurement Projects</title>
	<?php
		include ('Connect.php'); 
		include('auth.php');
		require ('inc/header.php');
	?>
	<link rel="stylesheet" type="text/css" href="css/dboard_sds.css">
</head>
<body>
	<?php
	
	require ('inc/navbar_transparency.php');
	?>

<div class="container-fluid">
	<h2 class="mt-3 mb-0">Dashboard</h2>

	<div class="row">
		<div class="col">	
			<nav aria-label="breadcrumb">
  				<ol class="breadcrumb">
    				<li class="breadcrumb-item active"><a href="#">Home</a></li>
    				<li class="breadcrumb-item active" aria-current="page">Division Procurement Projects</a></li>
  				</ol>
			</nav>
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-9">
			<?php
				include('php/sds/edit_project_title_sds.php');
			?>
		</div>
		<div class="col-sm-3">
			<div class="card border-dark mb-4">
				<div class="card-header bg-dark text-white">
					<h6 class="m-0"><i class="fas fa-columns"></i> Project Accomplishment Report</h6>
				</div>
				<div class="card-body table-responsive">
					<table class="table table-striped table-sm display" id="proj_accomp">
						<thead>
							<tr>
								<th align="center">Active</th>
								<th align="center">Completed</th>
								<th align="center">% of Completion</th>
							</tr>
						</thead>
						<tbody id="tbod-dark">
							<script type="text/javascript">
								var role="all_project_status";
								var s_id=<?php echo $_SESSION['s_id']; ?>;
									$.ajax({
										url: "login_webservice.php",
										type: "post",
										data: {
											tag: role,
											user_id: s_id,
										},
										success: function(response){
											console.log(response);
											var data=JSON.parse(response);
											for (var key in data) {
												document.getElementById("tbod-dark").innerHTML += "<tr><td>" + data[key].row1 + "</td>" + "<td>" + data[key].row2 +  "</td>" + "<td>" + data[key].row3 + "</td></tr>";
											}
										}
									})
							</script>
						</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-6 col-sm-6 col-xs-6">
			<div class="card border-dark">
				<div class="card-header bg-dark">
					<h6 class="text-white m-0"><i class="fab fa-elementor"></i> Legends</h6>
				</div>
				<div class="card-body">
					<table class="table table-striped table-bordered display table-sm">
						<tr>
							<td><b>PPMP-AD</b></td>
							<td>Project Procurement Management Plan - Approved Date</td>
						</tr>
						<tr>
							<td><b>PR No.</b></td>
							<td>Purchase Request Number</td>
						</tr>
						<tr>
							<td><b>RFQ No.</b></td>
							<td>Request for Quotation Number</td>
						</tr>
						<tr>
							<td><b>PO No.</b></td>
							<td>Purchase Order Number</td>
						</tr>
						<tr>
							<td><b>EB</b></td>
							<td>Estimated Budget</td>
						</tr>
						<tr>
							<td><b>AC</b></td>
							<td>Actual Cost</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<div class="card border-dark">
				<div class="card-header bg-dark text-white">
					<h6 class="m-0"><i class="far fa-calendar-alt"></i> Procurement Calendar</h6>
				</div>
				<div class="card-body table-responsive">

				</div>
			</div>
		</div>			
	</div>	
</div>
</body>
</html>