<?php
require_once('plugins/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
$output = '<html lang="en">
  <head>
    <title>App</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  </head>
  <style type="text/css">
  body{
    margin: 0;
    padding: 0;
  }
  #aux {
    vertical-align: middle;
    text-align: center;
  }
  p {
    margin: 0;
    text-align: center;
  }
table {    
  table-layout: fixed;
  width: 100%;
  padding: 0;
}
table th, td {
  word-wrap: break-word;
  word-break: break-all;
  margin: 0;
  font-size: 9px;
  padding: 0;
}
thead {
  padding: 0 !important;
  white-space: normal;
}
tbody td{
  padding: 2px !important;
  white-space: normal;
}
</style>
  <body>
   <div class="container-fluid">
        <p>Department of Education</p>
        <p>SDO-MARIKINA CITY</p>
        <p>Shoe Avenue, Sta. Elena, Marikina City</p>
        <p>SUMMARY OF ANNUAL PROCUREMENT PLAN FOR FY 2018</p>
      <table class="table table-bordered table-condensed">
        <thead>
          <tr>
            <th style="padding: 0 !important";  width="7%"  rowspan="2" id="aux">Code (PAP)</th>
            <th style="padding: 0 !important";  width="15%" rowspan="2" id="aux">Procurement Program/Project</th>
            <th style="padding: 0 !important";  width="8%"  rowspan="2" id="aux">PMO/End-User</th>
            <th style="padding: 0 !important";  width="15%" rowspan="2" id="aux">Mode of Procurement</th>
            <th style="padding: 0 !important";  width="20%" colspan="4" id="aux">Schedule for Each Procurement</th>
            <th style="padding: 0 !important";  width="10%" rowspan="2" id="aux">Source of Funds</th>
            <th style="padding: 0 !important";  width="25%" colspan="3" id="aux">Estimated Budget (PhP)</th>
            <th style="padding: 0 !important";  width="10%" rowspan="2" id="aux">Remarks (brief description of Program/Activity/Project)</th>
          </tr>
          <tr>
            <th style="padding: 0 !important"; id="aux">Advertisement/Posting of IB/REI</th>
            <th style="padding: 0 !important"; id="aux">Submission/Opening of Bids</th>
            <th style="padding: 0 !important"; id="aux">Notice of Award</th>
            <th style="padding: 0 !important"; id="aux">Contract Signing</th>
            <th style="padding: 0 !important"; id="aux">Total</th>
            <th style="padding: 0 !important"; id="aux">MOOE</th>
            <th style="padding: 0 !important"; id="aux">CO</th>
          </tr>
        </thead>
          <tbody>
            <tr>
              <td>50-2030-1000</td>
              <td>Common Office Supplies</td>
              <td rowspan="26" id="aux">Division Office</td>
              <td>NP-53.5 Agency to Agency</td>
              <td>Feb</td>
              <td>March</td>
              <td>Apr</td>
              <td>Apr</td>
              <td>GoP</td>
              <td>1,567,299.02</td>
              <td>1,567,299.02</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>50-2030-7000</td>
              <td>Drugs and Medicine Expenses</td>
              <td>NP-53.9 Small Value Procurement</td>
              <td>Feb</td>
              <td>March</td>
              <td>Apr</td>
              <td>Apr</td>
              <td>GoP</td>
              <td>50,000.00</td>
              <td>50,000.00</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>50-2030-8000</td>
              <td>Medical, Dental, Laboratory Supplies</td>
              <td>NP-53.9 Small Value Procurement</td>
              <td>Feb</td>
              <td>March</td>
              <td>Apr</td>
              <td>Apr</td>
              <td>GoP</td>
              <td>10,000.00</td>
              <td>10,000.00</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="2">test</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>50-2136-4000</td>
              <td>REPAIR/MAINTENANCE/ SERVICES</td>
              <td>NP-53.9 Small Value Procurement</td>
              <td>Feb</td>
              <td>March</td>
              <td>Apr</td>
              <td>Apr</td>
              <td>GoP</td>
              <td>460,000.00</td>
              <td>460,000.00</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="2">test</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>50-3039-9000</td>
              <td>OTHER EXPENSES</td>
              <td>NP-53.9 Small Value Procurement</td>
              <td>Feb</td>
              <td>March</td>
              <td>Apr</td>
              <td>Apr</td>
              <td>GoP</td>
              <td>362,000.00</td>
              <td>362,000.00</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td>Other Supplies and Materials</td> 
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td>Food Supplies</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td></td>
              <td>Accountable Forms</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr> 
            <tr>
              <td></td>
              <td>Advertising, Printing, Publication</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>50204010/020</td>
              <td>UTILITIES</td>
              <td>Direct Contracting</td>
              <td>Feb</td>
              <td>March</td>
              <td>Apr</td>
              <td>Apr</td>
              <td>GoP</td>
              <td>1,820,000.00</td>
              <td>1,820,000.00</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>50-6040-5002</td>
              <td>SEMI-EXPENDABLE</td>
              <td>NP-53.9 Small Value Procurement</td>
              <td>Feb</td>
              <td>March</td>
              <td>Apr</td>
              <td>Apr</td>
              <td>GoP</td>
              <td>195,000.00</td>
              <td>195,000.00</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="2">test</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>50205010/020/030</td>
              <td>BONDS/TRAVELING/TRAINING/</td>
              <td>NP-53.5 Agency-to-Agency</td>
              <td>Feb</td>
              <td>March</td>
              <td>Apr</td>
              <td>Apr</td>
              <td>GoP</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>50-2150-2000</td>
              <td>Bonds</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>25,000.00</td>
              <td>25,000.00</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>50-2010-1000</td>
              <td>Travelling/Representation Expenses</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>280,000.00</td>
              <td>280,000.00</td>
              <td></td>
              <td></td>
            </tr>
            <tr>
            <td>50-2020-1000</td>
            <td>Trainings</td>
            <td>NP-53.9 Small Value Procurement</td>
            <td>Feb</td>
            <td>March</td>
            <td>Apr</td>
            <td>Apr</td>
            <td>GoP</td>
            <td>2,555,700.98</td>
            <td>2,555,700.98</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td><b>TOTAL MOOE</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>7,325,000.00</b></td>
            <td>7,325,000.00</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>1,372,000.00</b></td>
            <td>1,372,000.00</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>50-2020-1000</td>
            <td><b>IN-SERVICE-TRAININGS</b></td>
            <td>NP-53.9 Small Value Procurement</td>
            <td>Feb</td>
            <td>March</td>
            <td>Apr</td>
            <td>Apr</td>
            <td>GoP</td>
            <td><b>500,000.00</b></td>
            <td></td>
            <td>500,000.00</td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2">test</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td><b>CAPITAL OUTLAY</b></td>
            <td>NP-53.9 Small Value Procurement</td>
            <td>Feb</td>
            <td>March</td>
            <td>Apr</td>
            <td>Apr</td>
            <td>GoP</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>50-6040-5002</td>
            <td>Office Equipment</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>50-6040-5003</td>
            <td>ICT Equipment</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>50-6040-7001</td>
            <td>Furnitures and Fixtures</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td><b>GRAND TOTAL</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Php</td>
            <td><b>9,197,000.00</b></td>
            <td></td>
            <td></td>
          </tr>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="2" style="border: none;">Prepared by: </td>
              <td colspan="3" style="border: none;">Certified Funds Available: </td>
              <td colspan="5" style="border: none;">Recommending Approval: </td>
              <td colspan="3" style="border: none;">Approved: </td>
            </tr>
            <tr>
              <td colspan="2" style="border: none;"></td>
              <td colspan="3" style="border: none;"></td>
              <td colspan="5" style="border: none;"></td>
              <td colspan="3" style="border: none;"></td>
            </tr>
            <tr>
              <td colspan="2" id="aux" style="border: none;"><b>LEILANI N. VILLANUEVA</b><br>
              <span style="font-size: 10px">EPS/ Division BAC Secretariat</span></td>
              <td colspan="3" id="aux" style="border: none;"><b>IVY R. RUALLO</b><br>
              <span style="font-size: 10px">OIC-ADAS III, Accounting Unit</span></td>
              <td colspan="5" id="aux" style="border: none;"><b>ELISA O. CERVEZA</b><br>
              <span style="font-size: 10px">CID Chief/ Division BAC Chairperson</span></td>
              <td colspan="3" id="aux" style="border: none;"><b>SHERYLL T. GAYOLA</b><br>
              <span style="font-size: 10px">OIC, Schools Division Superintendent</span></td>
            </tr>
          </tfoot>
        </table>
      </div>
  </body>
</html>';
        $document = new Dompdf();
        $document->loadHtml($output);
        $document->setPaper('legal', 'landscape');
        $document->render();
        $pdf = $document->output();
        ob_end_clean();
        $document->stream("download_app : ".time(), array("Attachment"=>1));
      ?>