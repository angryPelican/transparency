<?php
require_once('plugins/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
include('Connect.php');
$date_now=date("F d,Y");
$date_now_formatted=date('m/d/Y');
$output = '
<!DOCTYPE html>
<html lang="en">
		<head>
			<title>Issuances Dashboard</title>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<!-- Latest compiled and minified CSS -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
			<!-- Optional theme -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
			<!-- Latest compiled and minified JavaScript -->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
				<style media="print">
				table.print-friendly tr td, table.print-friendly tr th {
			        page-break-inside: avoid !important;
			    }
				</style>';
$output .= '</head>
	<body style="font-size: 1.1	em;">
				<div class="form-group clearfix" style="margin-bottom: 0;">
			<center><p style="margin: 0; padding: 0;">Division of Marikina</p>
			<p style="margin: 0; padding: 0;">Devices Supplies Taking</p>
			<p style="margin: 0; padding: 0;">as of '.$date_now.'</center>
		</div>
		<table class="table table-bordered display table-condensed print-friendly" >
			
				<tr style="text-align: center;">
					<th width="25%">Particular</th>
					<th colspan="3">Purchases</th>
					<th colspan="3">Available Stocks for the Month</th>
					<th width="8%">Bal as of '.$date_now_formatted.'</th>
					<th width="8%">Per Count</th>
					<th width="8%">Shortages</th>
					<th rowspan="2" valign="top">REMARKS</th>
				</tr>
				<tr style="text-align: center;">
					<th>Items</th>

					<th>Qty</th>
					<th>Unit Cost</th>
					<th>Amount</th>

					<th>Qty</th>
					<th>Unit Cost</th>
					<th>Amount</th>

					<th>Qty</th>
					<th>Qty</th>
					<th>Qty</th>
				</tr>
			
			<tbody>
				<tr>
					<td colspan="11"><b>DEVICES SUPPLIES</b></td>
				</tr>';
					$select="SELECT * FROM devices_table";
					$select_exec=$pdo2->prepare($select);
					$select_exec->execute();
					$total=0;
					$i=0;
					while($row=$select_exec->fetch(PDO::FETCH_ASSOC)) {
					$item_name=$row['items'];
					$item_qty=$row['mt_qty'];
					$qty_outside=$row['qty_from_issuances'];
					$balance=$row['balance'];
					$amount=$row['mt_amount'];
					$total=$total+$amount;
					$item_unit_cost=number_format($row['mt_unit_cost'], 2);
					$item_amount=number_format($row['mt_amount'], 2);
					$total_amount=number_format($total, 2);
						$output.="<tr>
								<td>$item_name</td>
								<td></td>
								<td></td>
								<td></td>
								<td>$item_qty</td>
								<td align='right'>$item_unit_cost</td>
								<td align='right'>$item_amount</td>
								<td>$qty_outside</td>
								<td>$balance</td>
								<td></td>
								<td></td>
							</tr>";
					}
						$output.="<tr>
								<td colspan='6'><b>TOTAL</b></td>
								<td align='right'>$total_amount</td>
								<td colspan='4'></td>
							</tr>";
						$output.="<tr>
								<td colspan='6'><b>Grand Total</b></td>
								<td align='right'>$total_amount</td>
								<td colspan='4'></td>
							</tr>";
					
			$output .= '</tbody>
					</table>';
					include('signatory_table.php');
				$output .= '</div>
			</body>
		</html>';
$document = new Dompdf();
$document->loadHtml($output);
ob_end_clean();
$document->setPaper('legal', 'landscape');
$document->render();
$pdf = $document->output();
$document->stream("Devices Supplies: ".time(), array("Attachment"=>2));
?>