<?php
require_once('plugins/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
include('php/connectdb.php');
$selected_category = $_GET['category'];
$selected_class = $_GET['class'];
$select_project="SELECT * FROM project_table WHERE project_id=?";
$result_project=$pdo->prepare($select_project);
$result_project->execute([$selected_category]);
while($row=$result_project->fetch(PDO::FETCH_ASSOC)):
$title=$row['project_title'];
// $rfq_no=$row['rfq_no'];
$rfq_date=date('F, d, Y', strtotime($row['rfq_date']));
endwhile;
$sel_rfq="SELECT * FROM rfq_table WHERE category=?";
$sel_rfq_exec=$pdo->prepare($sel_rfq);
$sel_rfq_exec->execute([$selected_class]);
	$row_rfq=$sel_rfq_exec->fetch(PDO::FETCH_ASSOC);
	$selected_rfq_numb=$row_rfq['rfq_number'];
$year=date("y");
$output = '<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Request for Quotation</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="boots/boots/css/bootstrap.min.css">
		<script src="boots/boots/js/jquery-2.1.4.min.js"></script>
		<script src="boots/boots/js/bootstrap.min.js"></script>
		<style>
		body {
		font-size: 1.4em;
		margin: 0 auto;
		}
		.fortelandfax{
		width: 80pt;
		}
		.ss{
		width: 150pt;
		}
		.tin{
		width: 40pt;
		}
		.date{
		width: 20pt;
		}
		.email{
		width: 45pt;
		}
		.pol{
		font-size: 1.2em;
		margin: 0 auto;
		}
		.kol {
		border-top: 1px solid black;
		width: 50%;
		}
		.head_title{
		font-size: 2em;
		font-weight: bold;
		}
		@media print {
			.head_title{
			font-size: 1.5em;
			font-weight: bold;
			}
		}
		</style>
	</head>
	<body>
			<center><p class="head_title">Request for Quotation (RFQ)</p></center>
			<table class="table table-bordered table-condensed" style="margin: 0;">
				<!--2-->
				<tr>
					<td style= "float:right">Date: <br> Quotation No. </td>
					<!--3-->
					<td style="width:150pt">'.$rfq_date.' <br> DepEd-'.$year.'-NCR-RFQ-'.$selected_rfq_numb.'</td>
				</tr>
			</table>
			<!--4-->
			<strong>To all Eligible Bidders:</strong>
			<!--5-->
			<ol type= "I" style="margin: 0;">
				<li>Please quote your lowest price appraisal inclusive of VAT on the "____________________________". This is subject to the Terms and Conditions of this RFQ. ________________________ . For more information please call the BAC  Secretariat at Telephone No. (02) 682-39-89.</li>
				<!--6-->
				<table class="table table-bordered table-condensed" style="margin: 0;">
					<tr>
						<td align="right" colspan=3><b>ELISA O. CERVEZA</b><br>
						BAC Chairperson</td>
					</tr>
				</table>';
				$get_total_ = "SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
				$result_total = $pdo->prepare($get_total_);
				$result_total->execute([$selected_category,$selected_class]);
				$sum_total = 0;
				while($row_total=$result_total->fetch(PDO::FETCH_ASSOC))
				{
				$sum_total = $sum_total + $row_total['p_estimated_budget'];
				$formattedNumss = number_format($sum_total, 2);
				}
				$get_title = "SELECT * FROM project_table WHERE project_id=?";
				$result_title = $pdo->prepare($get_title);
				$result_title->execute([$selected_category]);
				$sum_total = 0;
				while($row_title=$result_title->fetch(PDO::FETCH_ASSOC))
				{
				$project_no=$row_title['project_id'];
				$project_title=$row_title['project_title'];
				}
				$output .='<li><strong>TOTAL APPROVED BUDGET FOR THE CONTRACT: <u>Php'.$formattedNumss.'</u></strong></li>
				<!--8-->
				<li><strong>SUMMARY OF WORKS</strong></li>
				<!--9-->
				<table class="table table-bordered table-condensed" style="margin: 0;">
					<tr>
						<td class="ItemNo.">Stock No.</td>
						<td align="center">Project Title</td>
						<td align="center"> TOTAL COST </br>
							(Php)
						</td>
					</tr>
					<!--10-->
					<tr>
						<td></td>
						<td><i>'.$project_title.'</i> (see </br>
						attached Detailed Estimate/Description)</td>
						<td align="center"></br>'.$formattedNumss.'</td>
					</tr>
				</table>
				<!--12-->
				<p style="margin: 0;">This is to submit our price quotations in the above indicated item subject to the terms and conditions of this RFQ.</p>
				<!--13-->
				<table class="table table-bordered table-condensed" style="margin: 0;">
					<tr>';
						$output .= "<td class='ss'>Bidder's Company Name:</td>
						<td colspan='2'></td>
						<td class='tin'>TIN:</td>
						<td colspan='2'></td>
					</tr>
					<!--14-->
					<tr>
						<td>Address:</td>
						<td colspan='5'></td>
					</tr>
					<!--15-->
					<tr>
						<td class='fortelandfax'>Telephone No.:</td>
						<td></td>
						<td class='email'>Fax No.:</td>
						<td></td>
						<td class='email'>E-mail:</td>
						<td></td>
					</tr>
					<!--16-->
					<tr>
						<td>
							Supplier's Authorized </br>
							Representative's Signature </br>
							over Printed Name:
						</td>
						<td colspan='3'></td>
						<td class='date'>Date:</td>
						<td></td>
					</tr>
				</table>
				<!--17-->
				<li><strong>Terms and Condition</strong></li>
				<ol type= 'A'>
					<li><strong>Submission of Requirements</strong></li>
				</ol>
				<ol type= '1'>
					<li> Sealed quotations and other requirements stated below shall be submitted to the Bids and Awards Committee (BAC) at DepEd Division of Marikina City, Shoe Ave., Sta. Elena, Marikina City through the Records Section.</li>
					<li> Supplier shall submit the following requirements:</li>
					<ol type= 'a'>
						<li> Duly signed Request for Quotation (RFQ). Prices shall be quoted in Philippine Pesos.</li>
						<li> G-EPS / PhilGeps Registration Certificate.</li>
						<li> Filled up detailed Estimate.</li>
					</ol>
				</ol>
				<li><strong>Award</strong></li>
				<ol type= '1'>
					<li> The Procuring Entity will award the contract to the supplier / bidder whose offer has been determined to be substantially responsive and who offered the lowest evaluated price quotation.</li>
					<li>The Procuring Entity reserves the right to accept or reject any quotation and to cancel the process of competition and reject all quotations at  anytime prior to the award of the Contract, without thereby incurring any liability to the affected Supplier(s) / Bidder(s) or any obligation to inform the affected Supplier(s) / Bidder(s) of the grounds for the Procuring entity's decision.</li>
					<li>The Supplier / Bidder whose quotation has been accepted will be notified by the Procuring Entity prior to the expiration of the validity period of the Quotation.</li>
				</ol>
				<div style='page-break-inside: avoid !important;'>
				<ol start='6' type='I' style='margin: 0;'>
				<li><strong>Delivery, Inspection and Acceptance</strong></li>
				<ol>
					<li> Delivery of items or goods (specified in the RFQ) for the events shall be delivered to the Supply Office, Division Office of Marikina.</li>
					<li> Delivery of items or goods shall be delivered on the specific date indicated in the Detailed Estimate and time as greed by the supplier and the procuring entity.</li>
				</ol>
				</div>
			</ol>
			<br>
			<div class='container' style='page-break-before: always !important;'>
			<p><center><b>NAME OF SUPPLIER</b></center></p>
			<br>
			";
				$output .= "<p class='pol'><b>Project Title: $project_title</b></p>
				<br>
				<p class='pol'><b>Location: Division Office, Marikina</b></p>
				<br>
				<p class='pol'><b>Owner: Department of Education</b></p>
				<br>
				<p class='pol'><b>Subject: Detailed Estimate</b></p>
				<br>
				<br>";
				$second_query = "SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
				$resulttitle = $pdo->prepare($second_query);
				$sum = 0;
				if($resulttitle->execute([$selected_category, $selected_class]))
				{
				$output.='<table id="myTable" class="table table-striped table-bordered display">
					<thead>
						<tr>
							<th>Item No.</th>
							<th>Item Description</th>
							<th>Unit</th>
							<th>Quantity</th>
						</tr>
					</thead>
					<tbody>';
						while($row1=$resulttitle->fetch(PDO::FETCH_ASSOC))
						{
						$sx = $row1['project_id'];
						$s1 = $row1['p_description'];
						$s2 = $row1['p_quantity_size'];
						$s3 = $row1['p_unit'];
						$s4 = $row1['p_estimated_budget'];
						$sum = $sum + $row1['p_estimated_budget'];
						$formattedNum = number_format($sum, 2);
						$output .='<tr>';
							$output .="<td>$sx</td>
							<td>$s1</td>
							<td>$s2</td>
							<td>$s3</td>
						</tr>";
						}
						}
					$output .="</tbody>
					<tfoot>
					<tr>
						<td colspan='2'><b>Total Cost:</b></td>
						<td><b>$formattedNum</b></td>
						<td><b></b></td>
					</tr>
					</tfoot>
				</table>
				<p class='pol'>Prepared by:</p>
				<br>
				<br>
				<br>
				<br>
				<p class='kol'>Name of Supplier or duly Authorized Representative</p>
				<br>
				<br>
				<p class='kol'>Contact Number</p>";
			$output .="</div>
	</body>
</html>";
$f;
$l;
$document = new Dompdf();
$document->loadHtml($output);
$document->setPaper('letter', 'portrait');
$document->render();
$pdf = $document->output();
ob_end_clean();
$document->stream("$title : ".time(), array("Attachment"=>1));
?>