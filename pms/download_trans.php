<?php
require_once('plugins/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
include('Connect.php');
$output = '
<!DOCTYPE html>
<html lang="en">
		<head>
			<title>PPMP</title>
			<title>Bootstrap Example</title>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<!-- Latest compiled and minified CSS -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
			<!-- Optional theme -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
			<!-- Latest compiled and minified JavaScript -->
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>';
$output .= '</head>
	<body style="font-size: 1.1	em; font-family: ""; ">
		<div class="container-fluid" id="emp">
		<table class="table">
			<tr>
				<td width="15%" align="center"><img src="seal1.png" alt="Smiley face" width="80"></img></td>
				<td width="70%" align="center">Republic of the Philippines<br>
				<h6 style="margin: 0;>DEPARTMENT OF EDUCATION</h6>
				NATIONAL CAPITAL REGION<br>
				<h4 style="margin: 0;">SCHOOLS DIVISION OFFICE - MARIKINA CITY</h4>
				</td>
				<td width="15%" align="center"><img src="deped_logo.svg" alt="Smiley face" width="80"></img></td>
			</tr>
		</table>
			<div class="panel panel-default" style="margin: 0; padding: 0;">
				<div class="panel-heading" style="margin: 0; padding: 0;">
					<table class="table table-bordered table-striped" style="margin: 0;">
						<tr>
							<td>Entity Name: <span>SDO MARIKINA</span></td>
							<td>Fund Cluster: <span>1101101</span></td>
						</tr>
						<tr>
							<td>Division: </td>
							<td>Responsibility Center Code: </td>
						</tr>
						<tr>
							<td>Office: </td>
							<td>RIS No. : </td>
						</tr>
					</table>
				</div>
				<div class="panel-body" style="margin: 0; padding: 0;">
					<table class="table table-bordered table-striped" style="margin: 0;">
						<thead>
							<tr>
								<th colspan="4"><center>Requisition</center></th>
								<th colspan="3"><center>Issue</center></th>
							</tr>
							<tr>
								<th>Stock No.</th>
								<th width="7%">Unit</th>
								<th width="45%">Description</th>
								<th width="15%">Quantity</th>
								<th>Quantity</th>
								<th colspan="2">Remarks</th>
							</tr>
						</thead>
					<tbody>';
						$trans_id_sess=$_GET['eventid'];
						$select_s="SELECT * FROM supply_issuances_table WHERE trans_id=?";
							$sel_s_exec=$pdo2->prepare($select_s);
							if($sel_s_exec->execute([$trans_id_sess])) {
								while($row_s=$sel_s_exec->fetch(PDO::FETCH_ASSOC)) {
									$item_name=$row_s['item_name'];
									$item_unit=$row_s['item_unit'];
									$item_qty=$row_s['qty'];
										$output .= "<tr>
											<td></td>
											<td>$item_unit</td>
											<td>$item_name</td>
											<td>$item_qty</td>
											<td>$item_qty</td>
											<td colspan='2'></td>
										</tr>";
								}
							} else {
									$output .= '<tr>
										<td>mali</td>
										<td>mali</td>
										<td>mali</td>
										<td>mali</td>
										<td>mali</td>
										<td>mali</td>
									</tr>';
								}
				$output.='</tbody>
					</table>
					<table 	class="table table-bordered" style="margin: 0;">
							<tr>
								<td width="20%" valign="bottom" style="padding: 0; margin: 0; vertical-align: bottom; border-bottom: none;"></td>
								<td width="20%" style="border-bottom: none;">Requested By:</td>
								<td width="30%" style="border-bottom: none;">Approved By:</td>		
								<td width="15%" style="border-bottom: none;">Issued By: </td>	
								<td width="15%" style="border-bottom: none;">Received By: </td>
							</tr>
							<tr>
								<td style="border-top: none;">Signature: </td>
								<td style="border-top: none;"></td>
								<td style="border-top: none;"></td>
								<td style="border-top: none;"></td>
								<td style="border-top: none;"></td>
							</tr>
							<tr>
								<td valign="bottom">Printed Name: </td>
								<td></td>
								<td>ANNA MARIE P. EXEQUIEL</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td valign="bottom">Designation: </td>
								<td></td>
								<td>AO IV-SUPPLY UNIT</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td valign="bottom">Date: </td>
								<td></td>	
								<td></td>
								<td></td>
								<td></td>
							</tr>
					</table>
				</div>
			</div>
		</div>';

$output .= "</body>
</html>";
$document = new Dompdf();
$document->loadHtml($output);
ob_end_clean();
$document->setPaper('legal', 'portrait');
$document->render();
$pdf = $document->output();
$document->stream("ss: ".time(), array("Attachment"=>0));
?>