<?php
$output .= '
<!--FIRST ROW-->
<div style="page-break-inside: avoid !important;">
<table class="table table-bordered display">	
<tr>
	<td colspan="7" style="border-bottom: none; border-right: none;">Prepared By: </td>
	<td colspan="4" style="border-bottom: none; border-left: none;">Certified Correct: </td>
</tr>
<tr>
	<td colspan="4" align="center" style="border-top: none; border-right: none; border-bottom: none;"><b>TIMOTEO R. PAÑO</b><br>&nbsp;Admin Aide I</td>
	<td colspan="3" align="center" style="border-top: none; border-right: none; border-left: none; border-bottom: none;"><b>CHRIS EPETIA</b><br>&nbsp;Admin Aide VI</td>
	<td colspan="4" align="center" style="border-top: none; border-left: none; border-bottom: none;"><b>ANNA MARIE P. EXEQUIEL</b><br>&nbsp;AO IV, Supply Unit</td>
</tr>
<!--SECOND ROW-->
<tr>
	<td colspan="11" style="border-bottom: none; border-right: none;">Checked/Inspected By: <br>&nbsp;</td>
</tr>
<tr>
	<td colspan="2" align="center" style="border-top: none; border-right: none; border-bottom: none;"><b>CONSORCIA JOVITA MANI</b><br>&nbsp;Inventory Team Chairman</td>
	<td colspan="3" align="center" style="border-top: none; border-right: none; border-left: none; border-bottom: none;"><b>IVY R. RUALLO</b><br>&nbsp;Vice Chairman</td>
	<td style="border: none;"></td>
	<td colspan="2" align="center" style="border-top: none; border-right: none; border-left: none; border-bottom: none;"><b>REINAN IGNACIO</b><br>&nbsp;Member</td>
	<td style="border: none;"></td>
	<td colspan="2" align="center" style="border-top: none; border-left: none; border-bottom: none;"><b>JAY ALVIN CABUTUTAN</b><br>&nbsp;Member</td>
</tr>
<!--THIRD ROW-->
<tr>
	<td colspan="8" style="border-bottom: none; border-right: none;">WITNESS BY: <br>&nbsp;</td>
	<td colspan="3" style="border-bottom: none; border-right: none; border-left: none;">NOTED:</td>
</tr>
<tr>
	<td colspan="4" align="center" style="border-top: none; border-right: none; border-bottom: none;"><b>ROSE SHEENA C. LUMANOG</b><br>&nbsp;State Auditor II, COA</td>
	<td colspan="3" align="center" style="border-top: none; border-right: none; border-left: none; border-bottom: none;"><b>ELIZABETH S. SARMIENTO</b><br>&nbsp;OIC-State Auditor III, COA</td>
	<td colspan="4" align="center" style="border-top: none; border-left: none; border-bottom: none;"><b>SHERYLL T. GAYOLA</b><br>&nbsp;OIC-Office of the Schools Division Superintendent</td>
</tr>
</table>
</div>';
?>