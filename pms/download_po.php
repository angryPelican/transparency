<?php
require_once('plugins/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
include('php/connectdb.php');
$year=date('y');
$get_id=$_GET['eventid'];
$selected_cat=$_GET['category'];
$select_project="SELECT * FROM project_table WHERE project_id=?";
$result_project=$pdo->prepare($select_project);
$result_project->execute([$get_id]);
while($row=$result_project->fetch(PDO::FETCH_ASSOC)):
$title=$row['project_title'];
$po_date=date('m-d-Y', strtotime($row['po_date']));
$mode_of_procurement=$row['mode_of_procurement'];
endwhile;
$select_po="SELECT * FROM po_table WHERE project_id=? AND category=?";
$sel_po_exec=$pdo->prepare($select_po);
$sel_po_exec->execute([$get_id, $selected_cat]);
$row_po=$sel_po_exec->fetch(PDO::FETCH_ASSOC);
$po_no=$row_po['po_no'];
$year=date("y");
$q1="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
$q1_exec=$pdo->prepare($q1);
$q1_exec->execute([$get_id, $selected_cat]);
$row_q1=$q1_exec->fetchall(PDO::FETCH_ASSOC);
$json_decode=json_decode($row_q1[0]['supplier_data'], true);
	$q_sel_items="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
	$q_exec=$pdo->prepare($q_sel_items);
	$q_exec->execute([$get_id, $selected_cat]);
	$cnt_row_items=$q_exec->rowCount();
	while($rs_quan=$q_exec->fetch(PDO::FETCH_ASSOC)) {
		$arr_q=array();
		$quantity=$rs_quan['p_quantity_size'];
		array_push($arr_q, $quantity);
		$json_encode=json_decode(json_encode(array_values($json_decode)), true);
		$arr1=array();
		$arr2=array();
		$arr3=array();
		for ($i=0; $i < $cnt_row_items; $i++) {
			$arr_winner=array();
			$arr_names=array();
			$prices1=$json_encode[0]['supp_price'][$i];
			$prices2=$json_encode[1]['supp_price'][$i];
			$prices3=$json_encode[2]['supp_price'][$i];
			$names1=$json_encode[0]['supp_name'];
			$names2=$json_encode[1]['supp_name'];
			$names3=$json_encode[2]['supp_name'];
			$arr1[]=$prices1;
			$arr2[]=$prices2;
			$arr3[]=$prices3;
			$summed1=array_sum($arr1);
			$summed2=array_sum($arr2);
			$summed3=array_sum($arr3);
			$arr_names[$names1]=$summed1;
			$arr_names[$names2]=$summed2;
			$arr_names[$names3]=$summed3;
		}
		$xxsupp=array_keys($arr_names, min(array_filter($arr_names)));
	}
$output = '<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Purchase Order</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="boots/boots/css/bootstrap.min.css">
		<script src="boots/boots/js/jquery-2.1.4.min.js"></script>
		<script src="boots/boots/js/bootstrap.min.js"></script>
		<style>
		body {
		margin: 0 auto;
		}
		</style>
	</head>
	<body>
		<p style="font-size: 1.5em; margin-bottom: 0px; text-align: center; font-weight: bold;">PURCHASE ORDER</p>
		<div class="panel panel-default">
			<div class="panel-heading">
				<p style="text-transform: uppercase; font-size: 1.2em; margin-bottom: 0px;"><center><b>DEP-ED MARIKINA</b></center></p>
				<p style="text-transform: uppercase; font-size: 1.2em; margin-bottom: 0px;"><center>Agency / Procuring Entity</center></p>
			</div>';
			$output .= '<table class="table table-striped table-bordered display" style="margin-bottom:0">
				<thead>
					<tr>
						<td colspan="3">Supplier:<b> '.$xxsupp[0].' </b><br> Address: Marikina City <br>
							Telephone No. <br>
							TIN:
						</td>
						<td colspan="3">P.O No. DepEd-'.$year.'-NCR-PO-'.$po_no.'<br>
							Date: '.$po_date.' <br>
							Mode of Procurement: '.$mode_of_procurement.'
						</td>
					</tr>
					<tr>
						<td colspan="6">Gentlemen: <br>
							<p style="text-indent: 50px;">Please furnish this office the following articles subject to the terms and conditions contained herein:</p>
						</td>
					</tr>
					<tr>
						<td colspan="3">Place of Delivery:<br>
						Date of Delivery:</td>
						<td colspan="3">
							Delivery Term:<br>
						Payment Term:</td>
					</tr>
					<tr>
						<th>STOCK NO.</th>
						<th style="text-align: center;">UNIT</th>
						<th>DESCRIPTION</th>
						<th style="text-align: center;">QTY.</th>
						<th align="right">UNIT COST</th>
						<th align="right">AMOUNT</th>
					</tr>
				</thead>
				<tbody>';
					/* SELECT ALL ITEMS FROM ITEMS TABLE BASED ON SUPPLIER NUMBER */
					$stopper=0;
					$stopper1=0;
					$select_item="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
					$res_item=$pdo->prepare($select_item);
					$res_item->execute([$get_id,$selected_cat]);
					$cnt_row=$res_item->rowCount();
					$xx=0;
						
					while($row_item=$res_item->fetch(PDO::FETCH_ASSOC)) {
						$arr1=array();
						$arr2=array();
						$arr3=array();
						$bigone1=array();
						$p_unit=$row_item['p_unit'];
						$p_description=$row_item['p_description'];
						$p_quantity_size=$row_item['p_quantity_size'];
						$output.= "<tr>
							<td></td>
							<td style='text-align: center;'>$p_unit</td>
							<td>$p_description</td>
							<td style='text-align: center;'>$p_quantity_size</td>";
							$sel_abs="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
							$sel_abs_exec=$pdo->prepare($sel_abs);
							$sel_abs_exec->execute([$get_id,$selected_cat]);
							$row_abs=$sel_abs_exec->fetchall(PDO::FETCH_ASSOC);
							$json_decode=json_decode($row_abs[0]['supplier_data'], true);
							for ($i=0; $i < $cnt_row; $i++) {
								$prices1=$json_decode[0]['supp_price'][$i];
								$prices2=$json_decode[1]['supp_price'][$i];
								$prices3=$json_decode[2]['supp_price'][$i];
								array_push($arr1, $prices1);
								array_push($arr2, $prices2);
								array_push($arr3, $prices3);
							}
							$sk=$xx++;
			            $multiplied1[]=$arr1[$sk] * $p_quantity_size;
			            $multiplied2[]=$arr2[$sk] * $p_quantity_size;
			            $multiplied3[]=$arr3[$sk] * $p_quantity_size;
			         
			            	$sum1=array_sum($multiplied1);
				            $sum2=array_sum($multiplied2);
				            $sum3=array_sum($multiplied3);
				           	array_push($bigone1, $sum1);
				           	array_push($bigone1, $sum2);
				           	array_push($bigone1, $sum3);
									$mins=min(array_filter($bigone1));
								if($mins==$sum1) {
									$output.= "<td align='right'>".number_format($arr1[$sk], 2)."</td>
												<td align='right'>".number_format($multiplied1[$sk], 2)."</td>";
								}
								if($mins==$sum2) {
									$output.= "<td align='right'>".number_format($arr3[$sk], 2)."</td>
												<td align='right'>".number_format($multiplied2[$sk], 2)."</td>";
								}
								if($mins==$sum3) {
									$output.= "<td align='right'>".number_format($arr3[$sk], 2)."</td>
												<td align='right'>".number_format($multiplied3[$sk], 2)."</td>";
								}

						}//while loop
						$ones = array( 
							1 => "one", 
							2 => "two", 
							3 => "three", 
							4 => "four", 
							5 => "five", 
							6 => "six", 
							7 => "seven", 
							8 => "eight", 
							9 => "nine", 
							10 => "ten", 
							11 => "eleven", 
							12 => "twelve", 
							13 => "thirteen", 
							14 => "fourteen", 
							15 => "fifteen", 
							16 => "sixteen", 
							17 => "seventeen", 
							18 => "eighteen", 
							19 => "nineteen" 
						); 
						$tens = array( 
							1 => "ten",
							2 => "twenty", 
							3 => "thirty", 
							4 => "forty", 
							5 => "fifty", 
							6 => "sixty", 
							7 => "seventy", 
							8 => "eighty", 
							9 => "ninety" 
						); 
						$hundreds = array( 
							"hundred", 
							"thousand", 
							"million", 
							"billion", 
							"trillion", 
							"quadrillion" 
						); //limit t quadrillion 
						$num = number_format($mins,2,".",","); 
						$num_arr = explode(".",$num); 
						$wholenum = $num_arr[0]; 
						$decnum = $num_arr[1]; 
						$whole_arr = array_reverse(explode(",",$wholenum)); 
						krsort($whole_arr); 
						$rettxt = ""; 
						foreach($whole_arr as $key => $i){ 
							if($i < 20){ 
								$rettxt .= $ones[$i]; 
							} else if ($i < 100){ 
								$rettxt .= $tens[substr($i,0,1)]; 
								$rettxt .= " ".$ones[substr($i,1,1)]; 
							} else { 
								$rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
								$rettxt .= " ".$tens[substr($i,1,1)]; 
								$rettxt .= " ".$ones[substr($i,2,1)]; 
							} 
							if($key > 0){ 
								$rettxt .= " ".$hundreds[$key]." "; 
							} 
						} 
						if($decnum > 0){ 
								$rettxt .= " and "; 
							if($decnum < 20){ 
								$rettxt .= $ones[$decnum]; 
							} else if ($decnum < 100) { 
								$rettxt .= $tens[substr($decnum,0,1)]; 
								$rettxt .= " ".$ones[substr($decnum,1,1)]; 
							} 
						} 
						$output .='
								<tr>';
									// $f = new NumberFormatter("en_US", NumberFormatter::SPELLOUT);
									// IF THE PRICE HAS CENTS IN WORD $final=($f->format($exp[0])) . ' PESOS and ' . ($f->format($exp[1])) . ' centavos ';
									$no_format=number_format($mins, 2);
									$final=$rettxt . ' pesos only';
									$output .="<td colspan='5' style='text-transform: uppercase; font-size: 0.7em;'>(Total amount in Word) <b>$final</b></td>";
									$output.='<td align="right">Php <b>'.$no_format.'</b></td>
								</tr>
							</tbody>
							<tfoot>
							<tr>
								<td colspan="6"><p>In case of failure to make full delivery within the time specified above, a penalty of one tenth (1/10) of one (1) percent for every day of delay shall be imposed on the undelivered item/s.<br></p>
								</td>
							</tr>
							<tr>
								<td colspan="3" style="padding:0px 0px 0px 8px; border-bottom: 0;">
								<p>Conforme: </p>
								</td>
								<td colspan="3" style="padding:0px 0px 0px 8px; border-bottom: 0;"><span>Very truly yours,</span>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center" style="border-top: 0;"><b>'.$supp_name.'</b> <br /> Signature over printed name of Supplier</td>
								<td colspan="3" align="center" style="border-top: 0;"><b>JOEL T. TORRECAMPO </b> <br /> OIC, Office of the SDS</td>
							</tr>
							<tr>
								<td colspan="3">Date:</td>
								<td colspan="3"></td>
							</tr>
						<tr>
							<td colspan="3">Fund Cluster: <br>
							Funds Available: </td>
							<td colspan="5" rowspan="2">ORS/BURS No.: <br>
							Date Alobs No.: <br>
							Amount: </td>
						</tr>
						<tr>
							<td style="border-right: 0;"></td>
							<td colspan="2" align="center" style="border-left: 0;"><b>IVY R. RUALLO </b> <br />
									ADAS III/OIC-Accounting Unit</td>
						</tr>
						</tfoot>
					</table>';
				$output .="</div>
			</div>
		</body>
	</html>";
	$document = new Dompdf();
	$document->loadHtml($output);
	$document->setPaper('a4', 'portrait');
	$document->render();
	$pdf = $document->output();
	ob_end_clean();
	$document->stream("$title : ".time(), array("Attachment"=>1));
	?>