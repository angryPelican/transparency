

<!--BOOTSTRAP 4.1.3-->
<!-- 	<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
<!--DATATABLE-->
<!-- 	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"> -->
<!-- ANIMATE -->
<!-- 	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css"> -->
<!-- md5 js -->
<!-- 	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-md5/2.10.0/js/md5.js"></script> -->
<!--htmltopdf-->
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.4/jspdf.plugin.autotable.js"></script> -->
<!-- offline plugins -->
	<link rel="stylesheet" type="text/css" href="fade-in.css" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="boots/fontawesome/css/all.css">

<!-- BACKUP PLUGINS -->
<!--full calendar -->
<!-- <script type="text/javascript" src="boots/fullcalendar-3.9.0/fullcalendar.min.js"></script>
<link rel="stylesheet" type="text/css" href="boots/fullcalendar-3.9.0/fullcalendar.min.css">
<link rel="stylesheet" type="text/css" href="boots/fullcalendar-3.9.0/fullcalendar.print.css"> -->

<!--datatable-->
<script type="text/javascript" src="boots/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="boots/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="boots/datatable/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" type="text/css" href="boots/datatable/css/dataTables.bootstrap4.min.css">

<!--bootstrap 4.1.3-->
<link rel="stylesheet" href="boots/css/bootstrap.min.css">
<script src="boots/popper/popper.min.js"></script>
<script src="boots/js/bootstrap.min.js"></script>

<!--swal2,md5,offline_styles-->
<script src="boots/package/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="boots/crypto_js/rollups/md5.js"></script>
<link rel="stylesheet" type="text/css" href="fade-in.css">
<link rel = "stylesheet" type = "text/css" href="style.css" />