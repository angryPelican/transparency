<!DOCTYPE html>
<html>
<head>
	<?php include('links.php'); include('Connect.php'); ?>
	<title></title>
</head>
<body>
	<div class="container-fluid">
		<div class="form-group clearfix" style="margin-bottom: 0;">
			<center><p style="margin: 0; padding: 0;">Division of Marikina</p>
			<p style="margin: 0; padding: 0;">Supplies Inventory Taking</p>
			<p style="margin: 0; padding: 0;">as of <?php echo date('F d,Y'); ?></p></center>
		</div>
		<table class="table table-bordered display table-sm">
			<thead>
				<tr style="text-align: center;">
					<th width="25%">Particular</th>
					<th colspan="3">Purchases</th>
					<th colspan="3">Available Stocks for the Month</th>
					<th width="8%">Bal as of <?php echo date('m/d/Y'); ?></th>
					<th width="8%">Per Count</th>
					<th width="8%">Shortages</th>
					<th rowspan="2" valign="top">REMARKS</th>
				</tr>
				<tr style="text-align: center;">
					<th>Items</th>

					<th>Qty</th>
					<th>Unit Cost</th>
					<th>Amount</th>

					<th>Qty</th>
					<th>Unit Cost</th>
					<th>Amount</th>

					<th>Qty</th>
					<th>Qty</th>
					<th>Qty</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="11"><b>OFFICE SUPPLIES</b></td>
				</tr>
				<?php
					$select="SELECT * FROM office_supplies_table";
					$select_exec=$pdo2->prepare($select);
					$select_exec->execute();
					$total=0;
					$i=0;
					while($row=$select_exec->fetch(PDO::FETCH_ASSOC)) {
					$item_name=$row['items'];
					$item_qty=$row['mt_qty'];
					$qty_outside=$row['qty_from_issuances'];
					$balance=$row['balance'];
					$amount=$row['mt_amount'];
					$total=$total+$amount;
					$item_unit_cost=number_format($row['mt_unit_cost'], 2);
					$item_amount=number_format($row['mt_amount'], 2);
					$total_amount=number_format($total, 2);
						echo"<tr>
								<td>$item_name</td>
								<td></td>
								<td></td>
								<td></td>
								<td>$item_qty</td>
								<td align='right'>$item_unit_cost</td>
								<td align='right'>$item_amount</td>
								<td>$qty_outside</td>
								<td>$balance</td>
								<td></td>
								<td></td>
							</tr>";
					}
						echo"<tr>
								<td colspan='6'><b>TOTAL</b></td>
								<td align='right'>$total_amount</td>
								<td colspan='4'></td>
							</tr>";
						echo"<tr>
								<td colspan='6'><b>Grand Total</b></td>
								<td align='right'>$total_amount</td>
								<td colspan='4'></td>
							</tr>";
					include('signatory_table.php');
				?>
			</tbody>
		</table>
	</div>
</body>
</html>