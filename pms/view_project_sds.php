<!DOCTYPE html>
<html lang="en">
<head>
	<title>PPMP | Division Procurement Projects</title>
	<?php
		include ('Connect.php'); 
		include('auth.php');
		require ('inc/header.php');
	?>	
</head>
<link rel="stylesheet" type="text/css" href="css/view_proj_sds.css">
<body>
	<?php
		require ('inc/navbar_transparency.php');
		require('php/php_functions.php');
	?>
<div class="container-fluid">
	<!-- MILESTONE DATE DISPLAY FUNCTION -->
	<h2 class="clearfix mt-3">
		<?php include 'php/dynamic_title.php';
			echo ucfirst($project_title);
			echo " <i style='font-size: 16px; color: #95a5a6;'>";
			echo ucfirst($get_username); echo " "; echo ucfirst($get_lastname);
			echo "</i>";
		?>
	</h2>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item active"><a href="#">Home</a></li>
			<li class="breadcrumb-item active"><a href="#">Division Procurement Projects</a></li>
			<li class="breadcrumb-item active" aria-current="page"><a href="#"></a></li>
		</ol>
	</nav>
	<div class="row">
		<div class="col-md-12 col-sm-12 mb-4">
			<?php
				include ('php/project_ppmp_sds.php');
			?>
		</div>
	</div>
	<div class="row">
		<div class="colo-md-4 col-sm-4">
			<h5>MILESTONES</h5>
			<table class="table table-striped table-bordered table-sm	 display" id="mile_table">
				<thead>
					<tr>
						<th>Req</th>
						<th>Target</th>
						<th>Created</th>
						<th>Approved</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>PPMP</td>
						<td></td>
						<td><?php include 'php/dynamic_dates.php';
						echo $echo_submit_date;?></td>
						<td><?php include 'php/dynamic_dates.php';
						echo $approval_date;?></td>
					</tr>
					<tr>
						<td>PR</td>
						<td><?php include('php/dynamic_dates.php');
						echo $date5; ?></td>
						<td><?php include 'php/dynamic_dates.php';
						echo $pr_date;?></td>
						<td><?php include 'php/dynamic_dates.php';
						echo $pr_approved_date;?></td>
					</tr>
					<tr>
						<td>RFQ</td>
						<td><?php include('php/dynamic_dates.php');
						echo $date4; ?></td>
						<td><?php include 'php/dynamic_dates.php';
						echo $rfq_dates;?></td>
						<td>
							<?php
								get_rfq_date();
							?>
						</td>
					</tr>
					<tr>
						<td>AOQ</td>
						<td><?php include('php/dynamic_dates.php');
						echo $date3; ?></td>
						<td><?php include 'php/dynamic_dates.php';
						echo $rfq_dates;?></td>
						<td><?php include 'php/dynamic_dates.php';
							echo $abstract_dates; ?>
						</td>
					</tr>
					<tr>
						<td>PO</td>
						<td><?php include('php/dynamic_dates.php');
						echo $date2; ?></td>
						<td><?php include 'php/dynamic_dates.php';
						echo $po_dates;?></td>
						<td><?php
								get_po();
							?>
						</td>
					</tr>
					<tr>
						<td>ID</td>
						<td><?php include('php/dynamic_dates.php');
						echo $date1; ?></td>
						<td colspan="2"><?php include 'php/dynamic_dates.php';
							echo $delivery_dates;
						?></td>
					</tr>
				</tbody>
			</table>
			<h4>Remarks</h4>
			<table class="table table-striped table-bordered" id="remarks_table">
				<?php
					comment_so_sds();
				?>
			</table>
		</div>
		<div class="col-md-8 col-sm-8">
			<!--DISPLAY PANELS BASED ON PROJECT PROGRESS-->
			<?php
			include('php/connectdb.php');
			$get_id=$_GET['eventid'];
			$query = "SELECT * FROM project_table WHERE project_id=?";
			$stmt2=$pdo->prepare($query);
			$stmt2->execute([$get_id]);
			while($row=$stmt2->fetch(PDO::FETCH_ASSOC)){
			$check_purpose = $row['pr_date'];
			$check_rfq = $row['rfq_date'];
			$opening_date = $row['opening_date'];
			$po_date=$row['po_date'];
			$abstract_date = $row['abstract_date'];
			}
			if ($check_purpose != '0000-00-00'){
			include ('php/project_pr_sds.php');
			}
			if ($check_rfq != '0000-00-00') {
			include ('php/project_rfq_sds.php');
			}
			$query_rfq="SELECT * FROM rfq_table WHERE project_id=?";
			$query_rfq_exec=$pdo->prepare($query_rfq);
			$query_rfq_exec->execute([$get_id]);
			$count_rfq=$query_rfq_exec->rowCount();
			if($count_rfq==0){
				//do nothing
			} else if ($count_rfq!=0) {
			while($row_rfq_exec=$query_rfq_exec->fetch(PDO::FETCH_ASSOC)){
				$rfq_opening_date=$row_rfq_exec['opening_date'];

			}
				if ($rfq_opening_date != '0000-00-00') {
						include ('php/create_abstract_sds.php');
				}
			}	
			if ($po_date != '0000-00-00') {
			include ('php/project_po_sds.php');
			}
			?>
		</div>
	</div>
</body>
</html>
<?php
if(isset($_POST['comment'])) {
	$get_id=$_GET['eventid'];
	$user=$_SESSION['s_username'];
	$comment_value=$_POST['inputed_comment'];
	$update_comment="INSERT INTO comment_table (project_id, comment, user_comment) VALUES (?,?,?)";
	$stmt3=$pdo->prepare($update_comment);
	$stmt3->execute([$get_id,$comment_value,$user]);
	echo "<meta http-equiv='refresh' content='0'>";
}
?>