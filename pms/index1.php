<?php
 function getallacc () {
	include("php/end_user/connectdb.php");
	$query="SELECT * FROM accounts";
	$q_exec=$pdo->prepare($query);
	if($q_exec->execute()) {
		while($row=$q_exec->fetch(PDO::FETCH_ASSOC)) {
			$id=$row['user_id'];
			$first_name=$row['user_name'];
			$last_name=$row['last_name'];
			$user_email=$row['user_email'];
			$user_type=$row['user_type'];
			$section=$row['section'];
?>
		<tr>
			<td><?php echo $first_name; ?></td>
			<td><?php echo $last_name; ?></td>
			<td><?php echo $user_email; ?></td>
			<td><?php echo $user_type; ?></td>
			<td><?php echo $section; ?></td>
			<td align="center">
				<a data-role='update' data-accname='<?php echo $first_name; ?>' data-lastname='<?php echo $last_name; ?>' 
					data-email='<?php echo $user_email; ?>' data-type='<?php echo $user_type; ?>' 
					data-section='<?php echo $section; ?>' data-id='<?php echo $id; ?>' href='#' class='edit_acc btn btn-default btn_edit' id='edit_acc' data-toggle='modal' data-target='#editmodal'><i class='far fa-edit' style='color: white;'></i></a>
				<a data-role='delete' href='#' data-toggle='modal' data-name='<?php echo $first_name; ?>' data-target='#deletemodal' class='delete_acc btn btn-default btn_delete' data-id='<?php echo $id; ?>'><i class='fas fa-user-minus' style='color: white;'></i></a>
			</td>
		</tr>
<?php
		}
	}
}
Class samplefunction{
		
	public function displaydata(){

		include("php/end_user/connectdb.php");

		$query=$pdo->prepare("SELECT user_name, last_name, user_email, user_type, section FROM accounts");

			$query->execute();

		 return $row=$query->fetchAll(PDO::FETCH_ASSOC);

	}

	public function insertdata($username,$lastname,$email,$password,$usertype,$section){

		include("php/end_user/connectdb.php");	

		$query=$pdo->prepare("INSERT INTO accounts (user_name,last_name,user_email,user_password,user_type,section) VALUES (:Username,:Lastname,:Email,:Password,:Usertype,:Section)");

		$query->bindParam(":Username", $username);
		$query->bindParam(":Lastname", $lastname);
		$query->bindParam(":Email", $email);
		$query->bindParam(":Password", $password);
		$query->bindParam(":Usertype", $usertype);
		$query->bindParam(":Section", $section);

		$query->execute();


	}

	public function displaydata2(){
		include("php/end_user/connectdb.php");

			$query=$pdo->prepare("SELECT * FROM accounts");

		if($query->execute()){
			$queryfetchall = $query->fetchall(PDO::FETCH_ASSOC);
				$tablecontents="";
					for($i=0; $i < count($queryfetchall); $i++){
						$tablecontents .= "<tr>";

						$tablecontents .= "<td>" .$queryfetchall[$i]['user_name'] . "</td>";
						$tablecontents .= "<td>" .$queryfetchall[$i]['last_name'] . "</td>";
						$tablecontents .= "<td>" .$queryfetchall[$i]['user_email'] . "</td>";
						$tablecontents .= "<td>" .$queryfetchall[$i]['user_password'] . "</td>";
						$tablecontents .= "<td>" .$queryfetchall[$i]['end_user'] . "</td>";
						$tablecontents .= "<td>" .$queryfetchall[$i]['section'] . "</td>";

						$tablecontents .= "</tr>";

					}
					return $tablecontents;
			}else{
				return false;
			}
	}
}
?>