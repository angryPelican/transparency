<?php
include("Connect.php");

session_start();

$status=0;

$user_name=$_SESSION['s_username'];

$insert_status="UPDATE accounts SET status=? WHERE user_name=?";
$stmt=$pdo->prepare($insert_status);
$stmt->execute([$status, $user_name]);

if($stmt->execute([$status, $user_name])){

// Destroying All Sessions
session_destroy();

// Redirecting To Home Page
header("Location: index.php");
}
?>