<?php
	include('Connect.php');
	session_start();
	$tag=$_POST['tag'];
		switch ($tag) {
			case 'add_issued_item_equipment':
					$item=$_POST['item_name'];
					$item_qty=$_POST['item_qty'];
					$trans_id_sess=$_POST['eventid'];
					//1 for common office supplies
					$prop_no=$_POST['property_no'];
					$date_aq=$_POST['date_acquired'];
					$cost=$_POST['cost'];
					$category=$_POST['category'];
					//select unit based from item name
					$select_u="SELECT * FROM " . $category . "_table WHERE items=?";
					$select_u_exec=$pdo->prepare($select_u);
					if($select_u_exec->execute([$item])) {
						while($row_u=$select_u_exec->fetch(PDO::FETCH_ASSOC)) {
							$unit=$row_u['item_unit'];
							$quantity=$row_u['balance'] - $item_qty;
						}
					}
					$sel_o_item="SELECT * FROM equipment_table WHERE item_desc=?";
					$sel_o_item_exec=$pdo->prepare($sel_o_item);
					$sel_o_item_exec->execute([$item]);
					$row_item=$sel_o_item_exec->fetch(PDO::FETCH_ASSOC);
					$issue_qty=$row_item['item_qty'];
					$count_item=$sel_o_item_exec->rowCount();
					if($count_item==0) {
						$final_qty=$item_qty;
					} else if ($count_item!=0) {
						$final_qty=$issue_qty+$item_qty;
					}
					$update_u="UPDATE " . $category . "_table SET balance=?, qty_from_issuances=? WHERE items=?";
					$update_u_exec=$pdo->prepare($update_u);
					$update_u_exec->execute([$quantity,$final_qty,$item]);
					$office=$_SESSION['section'];
					//end
					$insert="INSERT INTO equipment_table (item_desc,trans_id,office,item_qty,item_unit,property_no,date_acquired,cost) VALUES (?,?,?,?,?,?,?,?)";
					$insert_p=$pdo->prepare($insert);
					if($insert_p->execute([$item,$trans_id_sess,$office,$item_qty,$unit,$prop_no,$date_aq,$cost])) {
						echo "yes";
					} else {
						$error=$insert_p->errorInfo();
						echo"<script>alert('$error');</script>";
					}
					//update trans_id
					$sel_with_trans="SELECT * FROM equipment_table WHERE trans_id=?";
					$sel_trans_exec=$pdo->prepare($sel_with_trans);
					$sel_trans_exec->execute([$trans_id_sess]);
					$count_id=$sel_trans_exec->rowCount();
					if(empty($count_id)) {
					// as is
					} else if (!empty($count_id)) {
						$update_trans="UPDATE transaction_id_table SET transaction_no=?";
						$update_trans_exec=$pdo->prepare($update_trans);
						$update_trans_exec->execute([$trans_id_sess]);
					}
				break;
			case 'sel_office_item':
				$category=$_POST['category'];
				$select="SELECT * FROM " . $category . "_table ORDER BY item_id DESC";
				$select_exec=$pdo->prepare($select);
				$select_exec->execute();
				$outer_array=array();
				while($row=$select_exec->fetch(PDO::FETCH_ASSOC)) {
					$inner_array=array();
					$item_name=$row['items'];
					$item_qty=$row['mt_qty'];
					$qty_outside=$row['qty_from_issuances'];
					$balance=$row['balance'];
					$item_unit_cost=number_format($row['mt_unit_cost'], 2);
					$item_amount=number_format($row['mt_amount'], 2);
					$inner_array[]=$item_name;
					$inner_array[]="";
					$inner_array[]="";
					$inner_array[]="";
					$inner_array[]=$item_qty;
					$inner_array[]=$item_unit_cost;
					$inner_array[]=$item_amount;
					$inner_array[]=$qty_outside;
					$inner_array[]=$balance;
					$inner_array[]="";
					$inner_array[]="";
					$inner_array[]="";
					$outer_array[]=$inner_array;
				}
				echo json_encode($outer_array);
				break;
			case 'sel_equipment_item':
				$category=$_POST['category'];
				$q1="SELECT * FROM " . $category . "_table ORDER BY item_id DESC";
				$q1_exec=$pdo->prepare($q1);
				if ($q1_exec->execute()) {
					$outer_array=array();
					while($row=$q1_exec->fetch(PDO::FETCH_ASSOC)) {
						$inner_array=array();
						$item_qty=$row['mt_qty'];
						$item_unit=$row['item_unit'];
						$item_name=$row['items'];
						$prop_number=$row['property_number'];
						$date_acquired=$row['date_acquired'];
						$date_lifespan=$row['date_lifespan'];
						$unit_cost=$row['mt_unit_cost'];
						$item_unit_cost=number_format($unit_cost, 2);
						$inner_array[]=$item_qty;
						$inner_array[]=$item_unit;
						$inner_array[]=$item_name;
						$inner_array[]=$prop_number;
						$inner_array[]=$date_acquired;
						$inner_array[]=$item_unit_cost;
						$outer_array[]=$inner_array;
					}
					echo json_encode($outer_array);
				}
				break;
			case 'by_issuance':
				$select_by_req="SELECT * FROM supply_issuances_table GROUP BY trans_id ORDER BY issuances_id DESC";
						$key_by_req=$pdo->prepare($select_by_req);
						$key_by_req->execute();
						$outer_array=array();
						while($row_key=$key_by_req->fetch(PDO::FETCH_ASSOC)) {
							$inner_array=array();
							$inner_array[]="<a target='_blank' href='issuances_display.php?eventid=".$row_key['trans_id']."'>".$row_key['office_name']."</a>";
							$inner_array[]=$row_key['date_issued'];
							$inner_array[]="";
							$outer_array[]=$inner_array;
						}
					echo json_encode($outer_array);
				break;
			case 'by_issuance_equip':
				$select_by_req="SELECT * FROM equipment_table GROUP BY trans_id ORDER BY item_id DESC";
						$key_by_req=$pdo->prepare($select_by_req);
						$key_by_req->execute();
						$outer_array=array();
						while($row_key=$key_by_req->fetch(PDO::FETCH_ASSOC)) {
							$inner_array=array();
							$inner_array[]="<a target='_blank' href='equipment_issuance.php?eventid=".$row_key['trans_id']."&office=".$row_key['office']."'>".$row_key['office']."</a>";
							$inner_array[]=$row_key['date_acquired'];
							$inner_array[]="";
							$outer_array[]=$inner_array;
						}
					echo json_encode($outer_array);
				break;
			case 'sel_office_items':
				$dynamic_category=$_POST['category'];
				$select="SELECT * FROM " . $dynamic_category . "_table";
				$select_exec=$pdo->prepare($select);
				$select_exec->execute();
				$issuances=0;
				$outer_array=array();
					while($row=$select_exec->fetch(PDO::FETCH_ASSOC)) {
						$inner_array=array();
						$item_name=$row['items'];
						$item_unit=$row['item_unit'];
						$item_qty=$row['balance'];
						$other_qty=$row['qty_from_issuances'];
						$inner_array[]=$item_unit;
						$inner_array[]=$item_name;
						$inner_array[]=$other_qty;
						$inner_array[]="";
						$outer_array[]=$inner_array;
					}
					echo json_encode($outer_array);
				break;
			case 'sel_equipment_items':
				$dynamic_category=$_POST['category'];
				$select="SELECT * FROM " . $dynamic_category . "_table";
				$select_exec=$pdo->prepare($select);
				$select_exec->execute();
				$issuances=0;
				$outer_array=array();
					while($row=$select_exec->fetch(PDO::FETCH_ASSOC)) {
						$inner_array=array();
						$item_name=$row['items'];
						$item_unit=$row['item_unit'];
						$item_qty=$row['balance'];
						$other_qty=$row['qty_from_issuances'];
						$inner_array[]=$item_unit;
						$inner_array[]=$item_name;
						$inner_array[]=$other_qty;
						$inner_array[]="";
						$outer_array[]=$inner_array;
					}
					echo json_encode($outer_array);
				break;
			case 'offices':
				$query1="SELECT * FROM office_table";
				$exec_q1=$pdo->prepare($query1);
				$exec_q1->execute();
				$outer_array=array();
					while($row=$exec_q1->fetch(PDO::FETCH_ASSOC)) {
						$inner_array=array();
						$inner_array[]="<a class='dropdown-item link_office' id='link_office' data-office='".$row['office']."' data-toggle='tab' href='#tab2default'>".$row['office']."</a>";
						$outer_array[]=$inner_array;
					}
					echo json_encode($outer_array);
				break;
			case 'select_stock':
				# code...
				$item_name=$_POST['item_name'];
				$category=$_POST['category'];
				$select_stock="SELECT * FROM " . $category . "_table WHERE items=?";
					$select_stock_exec=$pdo->prepare($select_stock);
					$select_stock_exec->execute([$item_name]);
						$row=$select_stock_exec->fetch(PDO::FETCH_ASSOC);
						$arr1=array();
						$arr1["stock"]=$row['balance'];
					echo json_encode($arr1);
				break;
			case 'select_stock_meds':
				# code...
				$item_name=$_POST['item_name'];
				$select_stock="SELECT * FROM medicine_and_drugs_table WHERE items=?";
					$select_stock_exec=$pdo->prepare($select_stock);
					$select_stock_exec->execute([$item_name]);
						$row=$select_stock_exec->fetch(PDO::FETCH_ASSOC);
						$arr1=array();
						$arr1["stock"]=$row['balance'];
					echo json_encode($arr1);
			break;
			case 'add_trans_no':
				# code..
					$select_trans="SELECT * FROM transaction_id_table";
					$sel_trans_exec=$pdo->prepare($select_trans);
					$sel_trans_exec->execute();
						while($row=$sel_trans_exec->fetch(PDO::FETCH_ASSOC)){
							$trans_no=$row['transaction_no']+1;
						}
							$artifice=$trans_no+1;
						// $update_trans="UPDATE transaction_id_table SET transaction_no=?";
						// $update_trans_exec=$pdo->prepare($update_trans);
						// $update_trans_exec->execute([$trans_no]);
					// 	session_start();
							$_SESSION['trans_id']=$artifice;
						echo $_SESSION['trans_id'];
					
				break;
			case 'get_office':
				# code...

				$select_office="SELECT * FROM office_table";
					$select_office_exec=$pdo->prepare($select_office);
					$select_office_exec->execute();
					echo json_encode($select_office_exec->fetchall(PDO::FETCH_ASSOC));
				break;
			case 'select_supply':
				# code...
				$value_id=$_POST['eventid'];
				$select_s="SELECT * FROM supply_issuances_table WHERE trans_id=?";
					$sel_s_exec=$pdo->prepare($select_s);
					$sel_s_exec->execute([$value_id]);
						while($row_s=$sel_s_exec->fetch(PDO::FETCH_ASSOC)) {
							$item_name=$row_s['item_name'];
							$item_unit=$row_s['item_unit'];
							$item_qty=$row_s['qty'];
								echo'<tr>
										<td></td>
										<td>'.$item_unit.'</td>
										<td>'.$item_name.'</td>
										<td>'.$item_qty.'</td>
										<td>'.$item_qty.'</td>
										<td colspan="2"></td>
									</tr>';
						}
				break;
			case 'select_acc':
				# code...
				$user_id=$_POST['user_id'];
				$sel_acc="SELECT * FROM accounts WHERE user_id=?";
				$sel_acc_exec=$pdo->prepare($sel_acc);
				$sel_acc_exec->execute([$user_id]);
					echo json_encode($sel_acc_exec->fetchall(PDO::FETCH_ASSOC));
				break;
			case 'office_sel':
				# code...
				$value=$_POST['office'];
				$select_by_office="SELECT * FROM supply_issuances_table WHERE office_name=? ";
     			$select_by_office=$pdo->prepare($select_by_office);
     			$select_by_office->execute([$value]);
     			$outer_array=array();
	     			while($row_office=$select_by_office->fetch(PDO::FETCH_ASSOC)) {
	     				$inner_array=array();
	     				$unit=$row_office['item_unit'];
	     				$desc=$row_office['item_name'];
	     				$qty=$row_office['qty'];
	     				$date_issued=$row_office['date_issued'];
	     				$inner_array[]=$unit;
	     				$inner_array[]=$desc;
	     				$inner_array[]=$qty;
	     				$inner_array[]=$date_issued;
	     				$inner_array[]="";
	     				$outer_array[]=$inner_array;
	     			}
	     			echo json_encode($outer_array);
				break;
		case 'equipment_sel':
				# code...
				$value=$_POST['office'];
				$select_by_office="SELECT * FROM equipment_table WHERE office=? ";
     			$select_by_office=$pdo->prepare($select_by_office);
     			$select_by_office->execute([$value]);
     			$outer_array=array();
	     			while($row_office=$select_by_office->fetch(PDO::FETCH_ASSOC)) {
	     				$inner_array=array();
	     				$unit=$row_office['item_unit'];
	     				$desc=$row_office['item_desc'];
	     				$qty=$row_office['item_qty'];
	     				$date_issued=$row_office['date_acquired'];
	     				$inner_array[]=$unit;
	     				$inner_array[]=$desc;
	     				$inner_array[]=$qty;
	     				$inner_array[]=$date_issued;
	     				$inner_array[]="";
	     				$outer_array[]=$inner_array;
	     			}
	     			echo json_encode($outer_array);
				break;
			case 'acc_sess':
				# code...
				$user_id=$_POST['accounts'];
				$sel_acc="SELECT * FROM accounts WHERE user_id=?";
				$sel_acc_exec=$pdo->prepare($sel_acc);
					if($sel_acc_exec->execute([$user_id])) {
						$arr_acc=array();
						while($row_acc=$sel_acc_exec->fetch(PDO::FETCH_ASSOC)) {
							$first_name=$row_acc['user_name'];
				        	$last_name=$row_acc['last_name'];
				        	$arr_acc['fullname']=$first_name . " " . $last_name;
				        	$arr_acc['position']=$row_acc['position'];
				        	$arr_acc['unit']=$row_acc['unit'];
				        	$full_name=$first_name . " " . $last_name;
				        	$position=$row_acc['position'];
				        	$section=$row_acc['unit'];
				        	$_SESSION['full_name'] = $full_name;
				        	$_SESSION['position'] = $position;
				        	$_SESSION['section'] = $section;
				        	echo json_encode($arr_acc);
						}
					} else {
						$sel_acc_exec->errorInfo();
					}
				break;
			case 'check_item':
				$transaction_number=$_POST['trans_id'];
				if(empty($_POST['uniqueid'])){
					$uniqueid="";
				} else {
					$uniqueid=$_POST['uniqueid'];
				}
				$select_issued_item="SELECT * FROM supply_issuances_table WHERE trans_id=? AND unique_category=?";
					$sel_issued_exec=$pdo->prepare($select_issued_item);
					$sel_issued_exec->execute([$transaction_number,$uniqueid]);
						$countrow=$sel_issued_exec->rowCount();
						if($countrow != 0){
							echo "true";
						} else {
							echo "false";
						}
				break;
			case 'check_item_equipment':
				$transaction_number=$_POST['trans_id'];	
				$select_issued_item="SELECT * FROM equipment_table WHERE trans_id=?";
					$sel_issued_exec=$pdo->prepare($select_issued_item);
					$sel_issued_exec->execute([$transaction_number]);
						$countrow=$sel_issued_exec->rowCount();
						if($countrow != 0){
							echo "true";
						} else {
							echo "false";
						}
				break;
			case 'add_issued_item':
				# code...
					$item=$_POST['item_name'];
					$item_qty=$_POST['item_qty'];
					$trans_id_sess=$_POST['eventid'];
					//1 for common office supplies
					$get_category=$_POST['unique_ident'];
					$category=$_POST['category'];
					//select unit based from item name
					$select_u="SELECT * FROM " . $category . "_table WHERE items=?";
					$select_u_exec=$pdo->prepare($select_u);
					if($select_u_exec->execute([$item])) {
						while($row_u=$select_u_exec->fetch(PDO::FETCH_ASSOC)) {
						$unit=$row_u['item_unit'];
						$quantity=$row_u['balance'] - $item_qty;
						}
					}
					$sel_o_item="SELECT * FROM supply_issuances_table WHERE item_name=?";
					$sel_o_item_exec=$pdo->prepare($sel_o_item);
					$sel_o_item_exec->execute([$item]);
					$row_item=$sel_o_item_exec->fetch(PDO::FETCH_ASSOC);
					$issue_qty=$row_item['qty'];
					$count_item=$sel_o_item_exec->rowCount();
					if($count_item==0){
						$final_qty=$item_qty;
					} else if ($count_item!=0) {
						$final_qty=$issue_qty+$item_qty;
					}
					$update_u="UPDATE " . $category . "_table SET balance=?, qty_from_issuances=? WHERE items=?";
					$update_u_exec=$pdo->prepare($update_u);
					$update_u_exec->execute([$quantity,$final_qty,$item]);
					$office=$_SESSION['section'];
					//end
					$date=date('Y-m-d');
					$insert="INSERT INTO supply_issuances_table (item_name,trans_id,unique_category,office_name,qty,item_unit,date_issued) VALUES (?,?,?,?,?,?,?)";
					$insert_p=$pdo->prepare($insert);
					if($insert_p->execute([$item,$trans_id_sess,$get_category,$office,$item_qty,$unit,$date])) {
						echo "yes";
					} else {
						$error=$insert_p->errorInfo();
						echo"<script>alert('$error');</script>";
					}
					//update trans_id
					$sel_with_trans="SELECT * FROM supply_issuances_table WHERE trans_id=?";
					$sel_trans_exec=$pdo->prepare($sel_with_trans);
					$sel_trans_exec->execute([$trans_id_sess]);
					$count_id=$sel_trans_exec->rowCount();
					if(empty($count_id)) {
					// as is
					} else if (!empty($count_id)) {
						$update_trans="UPDATE transaction_id_table SET transaction_no=?";
						$update_trans_exec=$pdo->prepare($update_trans);
						$update_trans_exec->execute([$trans_id_sess]);
					}
				break;
			case 'add_issued_item_meds':
				# code...
					$item=$_POST['item_name'];
					$item_qty=$_POST['item_qty'];
					$trans_id_sess=$_POST['eventid'];
					//1 for common office supplies
					$get_category=$_POST['unique_ident'];
					//select unit based from item name
					$select_u="SELECT * FROM medicine_and_drugs_table WHERE items=?";
					$select_u_exec=$pdo->prepare($select_u);
					if($select_u_exec->execute([$item])) {
						while($row_u=$select_u_exec->fetch(PDO::FETCH_ASSOC)) {
						$unit=$row_u['item_unit'];
						$quantity=$row_u['balance'] - $item_qty;
						}
					}
					$sel_o_item="SELECT * FROM supply_issuances_table WHERE item_name=?";
					$sel_o_item_exec=$pdo->prepare($sel_o_item);
					$sel_o_item_exec->execute([$item]);
					$row_item=$sel_o_item_exec->fetch(PDO::FETCH_ASSOC);
					$issue_qty=$row_item['qty'];
					$count_item=$sel_o_item_exec->rowCount();
					if($count_item==0){
						$final_qty=$item_qty;
					} else if ($count_item!=0) {
						$final_qty=$issue_qty+$item_qty;
					}
					$update_u="UPDATE medicine_and_drugs_table SET balance=?, qty_from_issuances=? WHERE items=?";
					$update_u_exec=$pdo->prepare($update_u);
					$update_u_exec->execute([$quantity,$final_qty,$item]);
					$office=$_SESSION['section'];
					//end
					$date=date('Y-m-d');
					$insert="INSERT INTO supply_issuances_table (item_name,trans_id,unique_category,office_name,qty,item_unit,date_issued) VALUES (?,?,?,?,?,?,?)";
					$insert_p=$pdo->prepare($insert);
					if($insert_p->execute([$item,$trans_id_sess,$get_category,$office,$item_qty,$unit,$date])) {
						echo "yes";
					} else {
						$error=$insert_p->errorInfo();
						echo"<script>alert('$error');</script>";
					}
					//update trans_id
					$sel_with_trans="SELECT * FROM supply_issuances_table WHERE trans_id=?";
					$sel_trans_exec=$pdo->prepare($sel_with_trans);
					$sel_trans_exec->execute([$trans_id_sess]);
					$count_id=$sel_trans_exec->rowCount();
					if(empty($count_id)) {
					// as is
					} else if (!empty($count_id)) {
						$update_trans="UPDATE transaction_id_table SET transaction_no=?";
						$update_trans_exec=$pdo->prepare($update_trans);
						$update_trans_exec->execute([$trans_id_sess]);
					}
				break;
			case 'select_item_by_trans':
				# code...
				if(empty($_POST['eventid'])) {
					$trans_id_sess=0;
				} else {
					$trans_id_sess=$_POST['eventid'];
				}
				if(empty($_POST['uniqueid'])) {
					$unique_identifier="";
				} else {
					$unique_identifier=$_POST['uniqueid'];
				}
					$select_s="SELECT * FROM supply_issuances_table WHERE trans_id=? AND unique_category=?";
					$sel_s_exec=$pdo->prepare($select_s);
					$sel_s_exec->execute([$trans_id_sess,$unique_identifier]);
					$count_trans=$sel_s_exec->rowCount();
						if($count_trans==0) {
							echo "wala";
						} else if ($count_trans != 0) {
							echo json_encode($sel_s_exec->fetchall(PDO::FETCH_ASSOC));
						}
				break;
			case 'select_item_by_trans_equipment':
				# code...
				if(empty($_POST['eventid'])) {
					$trans_id_sess=0;
				} else {
					$trans_id_sess=$_POST['eventid'];
				}
					$select_s="SELECT * FROM equipment_table WHERE trans_id=?";
					$sel_s_exec=$pdo->prepare($select_s);
					$sel_s_exec->execute([$trans_id_sess]);
					$count_trans=$sel_s_exec->rowCount();
						if($count_trans==0) {
							echo "wala";
						} else if ($count_trans != 0) {
							echo json_encode($sel_s_exec->fetchall(PDO::FETCH_ASSOC));
						}
				break;
			case 'add_item_equipment':
				# code...
				$item=$_POST['item_name'];
				$item_qty=$_POST['item_qty'];
				$item_unit=$_POST['item_unit'];
				$item_unit_cost=$_POST['item_unit_cost'];
				$property_no=$_POST['prop_no'];
				$category=$_POST['category'];
				$date_acq=$_POST['date_acquired'];
				$lifespan=$_POST['lifespan'];
				//computation
				$number=0;
				$number=$number + $item_unit_cost * $item_qty;
					$insert="INSERT INTO " . $category . "_table (items,mt_qty,item_unit,mt_unit_cost,property_number,date_acquired,date_lifespan) VALUES (?,?,?,?,?,?,?)";
					$insert_p=$pdo->prepare($insert);
					if($insert_p->execute([$item,$item_qty,$item_unit,$item_unit_cost,$property_no,$date_acq,$lifespan])) {
						echo "correct";
					} else {
						$insert_p->errorInfo();
					}
				break;
			case 'add_item':
				# code...
				$item=$_POST['item_name'];
				$item_qty=$_POST['item_qty'];
				$item_unit=$_POST['item_unit'];
				$item_unit_cost=$_POST['item_unit_cost'];
				$category=$_POST['category'];
				//computation
				$number=0;
				$number=$number + $item_unit_cost * $item_qty;
					$insert="INSERT INTO " . $category . "_table (items,item_unit,mt_qty,balance,mt_unit_cost,mt_amount) VALUES (?,?,?,?,?,?)";
					$insert_p=$pdo->prepare($insert);
					if($insert_p->execute([$item,$item_unit,$item_qty,$item_qty,$item_unit_cost,$number])) {
					echo "correct";
					} else {
					$insert_p->errorInfo();
					}
				break;
			case 'add_item_drugs':
				# code...
				$item=$_POST['item_name'];
				$item_qty=$_POST['item_qty'];
				$item_unit=$_POST['item_unit'];
				$item_unit_cost=$_POST['item_unit_cost'];
				//computation
				$number=0;
				$number=$number + $item_unit_cost * $item_qty;
				$insert="INSERT INTO medicine_and_drugs_table (items,item_unit,mt_qty,balance,mt_unit_cost,mt_amount) VALUES (?,?,?,?,?,?)";
				$insert_p=$pdo->prepare($insert);
					if($insert_p->execute([$item,$item_unit,$item_qty,$item_qty,$item_unit_cost,$number])) {
						echo "yes";
					} else {
						$insert_p->errorInfo();
					}
				break;
		}
?>