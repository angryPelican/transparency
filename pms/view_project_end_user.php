<!DOCTYPE html>
<html lang="en">
<head>
	<title>PMS | View Project</title>
	<?php
		include('Connect.php');
		include('auth.php');
		require ('inc/header.php');
	?>
<link rel="stylesheet" type="text/css" href="css/view_items_css.css">
	<body>
	<?php
		require ('inc/navbar_transparency.php');
		require ('php/php_functions.php');
	?>
		<div class="container-fluid">
			<h2 class="mt-4">My Procurement Project</h2>
			<div class="form-group row">
				<div class="col-xs-6 col-sm-6 col-md-6">
					<nav aria-label="breadcrumb">
		  				<ol class="m-0 breadcrumb">
		    				<li class="breadcrumb-item active"><a href="#">Home</a></li>
		    				<li class="breadcrumb-item active" aria-current="page">My Procurement Projects</li>
		  				</ol>
					</nav>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 text-right">
					<a class="btn btn-primary otherproj" href="#otherprojmodal" data-toggle="modal"><i class="fas fa-book-open"></i> Other Projects</a>
				</div>
			</div>
			<div class="form-group row mb-4">
				<div class="col-sm-12">
					<div class="card border-dark">
						<div class="card-header text-white bg-dark">
							<!--PROJECT TITLE-->
							<h5 style="margin: 0;">
							<?php
								get_title();
							?>
							</h5>
						</div>
						<div class="card-body p-0 mb-0 pr-0 table-responsive">
							<table class="table table-bordered table-sm" id="main_table">
								<thead>
									<tr>
										<th width="10%">Code</th>
										<th width="25%">General Description</th>
										<th width="5%">Qty/Size</th>
										<th width="15%">Estimated Budget</th>
										<th width="15%">Mode of Procurement</th>
										<th width="20%">Milestone of Activities</th>
										<th width="10%">Remarks</th>
									</tr>
								</thead>
								<tbody>
									<?php
										include('php/end_user/view_items_end_user.php');
									?>
								</tbody>
							</table>
							<div class="card-footer">
								<div class="row">
									<div class="col" id="badges">
									</div>
									<div class="col text-right">
										<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Action<span class="caret"></span></button>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
											<li><a class="btn btn-link" href ="create_project_items.php?eventid=<?php echo $_GET['eventid']; ?>" name="edit" >Edit</a></li>
											<li><a class="btn btn-link" href="#addppmpComment" data-toggle="modal" >Comment</a></li>
											<li><a href="#" data-role="submit_ppmp" name="sends" class="btn btn-link btn-submit-ppmp">Submit PPMP</a></li>
											<li><a class="btn btn-link" href="download.php?eventid=<?php echo $_GET['eventid']; ?>">Download</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div><!--card body -->
					</div><!--card-->
				</div><!--col-sm-12-->
			</div><!--first row-->
			<div class="form-group row">
				<div class="col-sm-8 m-0">
					<div class="card">
						<div class="card-header bg-dark text-white">
							<i class="fas fa-calendar-alt"></i><b> Milestones</b>
						</div>
						<div class="card-body table-responsive table-sm">
							<table class="table table-striped table-bordered" id="mile_table">
								<thead>
									<tr>
										<th>Phases</th>
										<th>Target</th>
										<th>Created</th>
										<th>Approved </th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>PPMP</td>
										<td></td>
										<td><?php include('php/dynamic_dates.php');
										echo $echo_date_created; ?></td>
										<td><?php include('php/dynamic_dates.php');
										echo $approval_date; ?></td>
									</tr>
									<tr>
										<td>Purchase Request</td>
										<td><?php include('php/dynamic_dates.php');
										echo $date5; ?></td>
										<td><?php include('php/dynamic_dates.php');
										echo $pr_date; ?></td>
										<td><?php include('php/dynamic_dates.php');
										echo $pr_approved_date; ?></td>
									</tr>
									<tr>
										<td>Request For Quotation</td>
										<td><?php include('php/dynamic_dates.php');
										echo $date4; ?></td>
										<td><?php include('php/dynamic_dates.php');
										echo $rfq_dates; ?></td>
										<td>
											<?php
												get_rfq_date();
											?>
										</td>
									</tr>
									<tr>
										<td>Abstract of Quotation</td>
										<td><?php include('php/dynamic_dates.php');
										echo $date3; ?></td>
										<td><?php include('php/dynamic_dates.php');
										echo $abstract_dates; ?></td>
										<td><?php include('php/dynamic_dates.php');
										echo $abstract_dates; ?></td>
									</tr>
									<tr>
										<td>Purchase Order</td>
										<td><?php include('php/dynamic_dates.php');
										echo $date2; ?></td>
										<td><?php include('php/dynamic_dates.php');
										echo $po_dates; ?></td>
										<td>
											<?php
												get_po();
											?>
										</td>
									</tr>
									<tr>
										<td>Implementation Date</td>
										<td><?php include('php/dynamic_dates.php');
										echo $date1; ?></td>
										<td colspan="2" align="center">
											<?php
												get_delivery();
											?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div><!--card-->
				</div><!--col-sm-4-->
				<div class="col-sm-4 m-0">
					<div class="card">
						<div class="card-header bg-dark text-white">
							<i class="fas fa-comments"></i><b> Remarks</b>
						</div>
						<div class="card-body">
							<div class="list-group">
								<table class="table table-striped table-bordered" id="comment">
									<?php
										comment_display();
									?>
								</table>
							</div>
						</div>
					</div>
				</div><!--col-sm-4 -->
			</div>
		</div>
	</body>
</html>

<!-- Modal -->
<div class="modal fade" id="otherprojmodal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">My Other Projects</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body table-responsive">
				<table id="otherProjects" class="table table-striped table-sm table-bordered">
					<thead>
						<tr>
							<th width="50">Project Title</th>
							<th width="50">Date Created</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!--comment modal -->
<div id="addppmpComment" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="background: #0652DD; color: white;">
				<h5 class="modal-title" style="margin: 0;">Add Comment</h5>
				<button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
			</div>
			<div class="modal-body">
				<form action="" method="post" id="formcomment">
					<div class="form-group">
						<label>Comment</label>
						<textarea class="form-control" form="formcomment" name="inputed_comment" rows="5" ></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="submit" name="comment" form="formcomment" style="background: #0652DD; color: white;" class="btn btn-default btn-default">Add</button>
			</div>
		</div>
	</div>
</div>

<?php
	comment();
?>

<script type="text/javascript">
	var role="select_projects";
	var user_id="<?php echo $_SESSION['s_id']; ?>";
	$.ajax({
		url: "php/end_user/mode_of_proc_webservice.php",
		type: "post",
		data: {
			tag: role,
			user_id: user_id
		},
		success: function(response) {
			console.log(response);
			var data=JSON.parse(response);
			if ($.fn.DataTable.isDataTable("#otherProjects")) {
				$('#otherProjects').DataTable().clear().destroy();
			}
			$('#otherProjects').DataTable({
				"aaData": data,
				"lengthMenu": [[4], [4]],
				"iDisplayLength": 4,
			});
		}
	})
</script>
<script type="text/javascript">
	var role="view_project_action";
	var id=<?php echo $_GET['eventid']; ?>;
		$.ajax({
			url: "php/end_user/mode_of_proc_webservice.php",
			type: "POST",
			data: {
				eventid: id,
				tag: role
			},
			success: function(response) {
				var data=JSON.parse(response);
				console.log(response);
				for (var key in data) {
					var submit_date=data[key].submit_date;
					var approval_date=data[key].approval_date;
					var pr_approved_date=data[key].pr_approved_date;
					if (submit_date == '0000-00-00' && pr_approved_date == '0000-00-00') {
						// show all
						$('#badges').html('<span class="badge badge-secondary" id="badge_secondary">Not Submitted</span>');
					}
					else if (submit_date != '0000-00-00' &&  approval_date == '0000-00-00') {
						$('#badges').html('<span class="badge badge-success" id="badge_success">Submitted</span>');
						$('a[name=edit]').hide();
						$('a[name=sends]').hide();
					}
					else if (submit_date != '0000-00-00' &&  pr_approved_date != '0000-00-00') {
						$('#badges').html('<span class="badge badge-success" id="badge_success">Submitted</span>');
						$('a[name=edit]').hide();
						$('a[name=sends]').hide();
						$('a[href=#addppmpComment]').hide();
					}
				}
			}
		});
	$('.btn-submit-ppmp').click(function(){
		var role=$(this).data("role");
		var hidden_id=<?php echo $_GET['eventid']; ?>;
			$.ajax({
				url: 'php/end_user/mode_of_proc_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				id: hidden_id
				},
				success : function(response) {
					swal({
						title: "Success",
						text: "Project was submitted!",
						type: "success",
						timer: 1100,
						showConfirmButton: false
					})
					.then((value) => {
						location.reload();
					});
				}
			});
	});
</script>