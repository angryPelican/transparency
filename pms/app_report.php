<!DOCTYPE html>
<html lang="en">
	<head>
		<title>PPMP | Division Procurement Projects</title>
		<?php
			include ('Connect.php');
			include('auth.php');
			require ('inc/header.php');
		?>
	</head>
	<body>
		<?php
			require ('inc/navbar_transparency.php');
		?>
		<div class="container">
			<h4> App Report </h4>
			<table class="table table-bordered table-striped table-sm" style="table-layout: fixed; width: 100%;">
				<thead>
					<tr>
						<th>Category</th>
						<th>APP Budget</th>
						<th>Total PO Amount</th>
						<th>APP Balance</th>
					</tr>
				</thead>
				<tbody id="tbod">
					<?php
						$q_categ="SELECT * FROM category_table";
						$exec_q_categ=$pdo->prepare($q_categ);
						if($exec_q_categ->execute()) {
							$arr_categ=array();
							while($row=$exec_q_categ->fetch(PDO::FETCH_ASSOC)) {	
								$inner_array=array();
								$inner_array["category"]=$row['category'];
					?>
						<tr>
							<td><?php echo $row['category']; ?></td>
							<td align="right"><?php 	 ?></td>
							<td align="right"><?php 	 ?></td>
							<td align="right"><?php 	 ?></td>
						</tr>
					<?php
							}
						}
					?>
				</tbody>
			</table>
		</div>
	</body>
</html>
<!-- <script type="text/javascript">
	var role="get_cat";
		$.ajax({
			url: "app_report_function.php",
			type: "POST",
			data: {
				tag: role,
			},
			success: function(response) {
				console.log(response);
				var data=JSON.parse(response);
				for(var key in data) {
					document.getElementById("tbod").innerHTML += "<tr><td>" + data[key].category + "</td><td></td><td></td><td></td></tr>"
				}
			}
		});
</script> -->