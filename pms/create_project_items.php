<!DOCTYPE html>
<html lang="en">
<head>
	<title>PPMP | Division Procurement Projects</title>
	<?php
		include ('Connect.php'); 
		include('auth.php');
		require ('inc/header.php');
		include("php/end_user/php_functions.php");
		//hidden id's
		$eventid=$_GET['eventid'];
		$s_id=$_SESSION['s_id'];
	?>	
</head>
	<body>
		<?php
		require ('inc/navbar_transparency.php');
		?>
		<div class="container">	
			<div class="row">
				<div class="col-sm-12">
					<h2 class="mt-3 mb-4">Create/Edit Project Items</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<form action=""	 method="post" id="yey">
					</form>
					<div class="card border-secondary mb-4">
						<div class="card-header text-white" style="background: #2d3430; margin: 0;">
							<div class="clearfix" style="vertical-align: middle; margin: 0;">
								<table class="table table-sm" style="margin: 0; border: none !important; border-color: transparent;">
									<tr>
										<td align="left" style="vertical-align: middle; border: none !important;">
											<p class="h4 card-text">
												<?php
													get_project_title();
												?>
												<small><button class="btn btn-link" id="edit_title" data-toggle="modal" data-target="#editTitle"><span style="text-transform: lowercase;"> <i class="far fa-edit text-white"></i></span></button>
												</small>
											</p>
										</td>
										<td align="right" style="border: none !important;"><button data-role="add_item" data-target="#addItem" data-toggle="modal" class="btn btn-outline-primary text-white"><i class="fas fa-plus-circle"></i> Add Item</button></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="card-body table-responsive text-dark" style="background: #ecf0f1;" id="bods">
							<?php
								include('php/end_user/create_items.php');
							?>
						</div>
						<div class="card-footer table-responsive-md text-dark" style="background: #ecf0f1;">
							<?php
								include ('php/end_user/dynamic_milestone.php');
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php 
			include('php/end_user/modals.php');
		?>
		<script type="text/javascript">
			//ADDING ITEM FUNCTION
			$('#addItem').on('show.bs.modal', function (e) {
				$('#training_days').hide();
			})
			$('#addItem').on('show.bs.modal', function (e) {
				$('#input_desc').hide();
				$('#input_text_unit').hide();
			});
			$('#categories').on('change', function(){
				$('#input_est').val("");
				var text=$(this).val();
				if(text=="Training") {
					$('#training_days').show();
				} else if (text=="Common Office Supplies") {
					$('#training_days').hide();
					var role="cat_get";
					$.ajax({
						url: 'php/end_user/mode_of_proc_webservice.php',
						type: 'POST',
						data: {
							tag: role,
							cat: text
						},
						success : function(response) {
							if(response=="no match") {
								$('#input_desc').hide();
								$('#text_input_desc').show();
								$('#items').html("");
							} else {
								var data=JSON.parse(response);
								$('#text_input_desc').hide();
								$('#input_desc').show();
								$('input[list=items]').on('keyup', function() {
								var role_unit="check_unit";
								var role_price="check_price";
								var item=$(this).val();
								console.log(item);
								var categ_name=$('#categories').val();
								$.ajax({
									url: "php/end_user/mode_of_proc_webservice.php",
									type: "post",
									data: {
										tag: role_unit,
										item_name: item,
										categ_name: categ_name,
									},
									success: function(response) {	
										if(response=="no match") {
											$('#input_unit').show();
											$('#input_text_unit').hide();
										} else {
											$('#input_unit').hide();
											$('#input_text_unit').show();
											$('#input_text_unit').val(response);
										}
									}
								});
								$.ajax({
									url: "php/end_user/mode_of_proc_webservice.php",
									type: "post",
									data: {
										tag: role_price,
										item_name: item,
										categ_name: categ_name,
									},
									success: function(response) {
										$('#input_est').val(response);
									}
								})
							});
								console.log(data);
								for (var key in data) {
									var items = data[key].items;
									var newTemp = items.replace(/"/g, '&quot;');
									console.log(items);
									document.getElementById("items").innerHTML += '<option value="' + newTemp + '">' +
																				newTemp + '</option>';
								}
							}
						}
					});
				} else {
					console.log("no match");
					$('#input_desc').hide();
					$('#input_text_unit').hide();
					$('#text_input_desc').show();
					$('#input_unit').show();
				}
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				var role="check_if_submittted";
				var id="<?php echo $_GET['eventid']; ?>";
					$.ajax({
						url: "php/end_user/mode_of_proc_webservice.php",
						type: "post",
						data: {
							tag: role,
							eventid: id
						},
						success: function(response) {
							console.log(response);
							if(response=="submitted") {
								$('button[data-role=add_item]').attr("disabled", true);
								$('#edit_title').attr("disabled", true);
							} else if(response=="not submitted") {
								$('button[data-role=add_item]').removeAttr("disabled");
								$('#edit_title').removeAttr("disabled");
							}
						}
					});
				$('#myTable').DataTable({
					"bSort" : false,
				});
				$('button[data-role=add_item]').on('click', function(){
					$('#input_desc').val("");
					$('#input_quan').val("");
					$('#input_est').val("");
					$('#categories').val("Choose....");
					$('#addItem').modal('toggle');
					$('#text_input_desc').show();
				});
				$('#add').click(function() {
					var desc;
					var unit;
					var quan=$('#input_quan').val();
					var est=$('#input_est').val();
					var category=$('#categories').val();
					if(category=="Choose....") {
						$('#categories').addClass("is-invalid");
					} else if(category=="Training") {
						var days=$('#days').val();
						est = est * days;
						desc = $('#text_input_desc').val() + "&nbsp;" + "&times;" + "&nbsp;" + days + "&nbsp;" + "days";
						unit=$('#input_unit').val();
					} else if(category=="Common Office Supplies") {
						desc=$('#input_desc').val();
						unit=$('#input_text_unit').val();
					} else {
						desc=$('#text_input_desc').val();
						unit=$('#input_unit').val();
					}
						$('#categories').removeClass("is-invalid");
						var proj_id=<?php echo $eventid ?>;
						var user_id=<?php echo $s_id ?>;
						var nf = Intl.NumberFormat();
						var role="insert_item";
						// console.log("EST in Training: " + est);
						$.ajax({
							url: 'php/end_user/mode_of_proc_webservice.php',
							type: 'POST',
							data: {
								tag: role,
								desc_toadd: desc,
								quan_toadd: quan,
								unit_toadd: unit,
								buds_toadd: est,
								eventid: proj_id,
								category: category,
								s_id: user_id
							},
							success : function(response) {
								console.log(response);
								$('#addItem').modal('toggle');
								var estimate="";
								var item_id="";
								var data;
								data = JSON.parse(response);
								estimate=data['est_bud'];
								item_id=data['item_id'];
								console.log(response);
								swal({
									title: "Success",
									text: "Item added successfully",
									type: "success",
									timer	: 1100,
									showConfirmButton: false
								})
								.then((value) => {
									location.reload();
								});
							}
						});
				});
				$('#save_title').click(function() {
					var role="save_title";
					var new_title=$('#get_title').val();
					var id=<?php echo $_GET['eventid']; ?>;
					$.ajax({
						url: "php/end_user/mode_of_proc_webservice.php",
						type: "post",
						data: {
							tag: role,
							get_title: new_title,
							eventid: id
						},
						success: function(response) {
							$('#editTitle').toggle('modal');
							swal({
								title: "Success!",
								text: "Project Title changed successfully.",
								type: "success",
								timer: 1100,
								showConfirmButton: false
							})
							.then((value) => {
								location.reload();
							});
						}
					});
				});
			});
		</script>
	</body>
</html>