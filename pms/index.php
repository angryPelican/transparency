<?php
	if(isset($_SESSION['s_user_type'])) {
		switch ($_SESSION['s_user_type']) {
			case '1':
			header("location:dashboard_sds.php");
				break;
			case '2':
			header("location:dashboard_so.php");
				break;
			default:
			header("location:dashboard_end_user.php");
				break;
		}
	}
?>
<!DOCTYPE html>
<html>
	<title>Project Procurement Plan</title>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php include('inc/header.php'); ?>
		<link rel="stylesheet" type="text/css" href="boots/login.css">
	</head>
	<body class="bod" onload="startTime()">
		<header>
			<nav class="navbar navbar-dark">
				<span class="navbar-brand"><img src="boots/sdo logo png.png" width="40" height="40"> PROCUREMENT MANAGEMENT SYSTEM</span>
				<form class="form-inline">
					<span class="navbar-brand text-white">v.1</span>
				</form>
			</nav>
		</header>
		<main role="main" class="container" id="container">
			<div class="form-group row">
				<div class="col-sm-6 col-md-6 col-xs-6" style="margin-top: 20%; padding-left: 10%; padding-right: 5%;">
					<div class="form-group">
						<h3 class="h4 text-white">Sign in here</h3>
					</div>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text " id="login_email"><i class="far fa-envelope"></i></span>
							</div>
							<input autocomplete="off" type="email" class="form-control form-control-sm" id="login_username" aria-describedby="emailHelp" placeholder="Email" />
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
									<span class="input-group-text " id="login_password_key"><i class="fas fa-key"></i></span>
								</div>
							<input type="password" class=" form-control form-control-sm" id="login_password" placeholder="Password">
						</div>
					</div>
					<div class="form-group">
						<button type="button" class='btn btn-default' id="submit"><i class="fas fa-arrow-right"></i> Login</button>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-xs-6" style="margin-top: 20%; padding-left: 10%; padding-right: 10%;">
					<div id="txt">
					</div>
				</div>
			</div>
		</main>
	</body>
</html>
<footer class="footer">
 <!-- Copyright -->
  <div class="container">
	<center class="text-white">© 2018 Copyright </center>
  </div>
  <!-- Copyright -->
</footer>
<script type="text/javascript">
	function startTime() {
	    var today = new Date();
	    var h = today.getHours() % 12;
	    var m = today.getMinutes();
	    var s = today.getSeconds();
	    var ampm = (today.getHours() >= 12) ? "PM" : "AM";
	    m = checkTime(m);
	    s = checkTime(s);
	    $('#txt').html(h + ":" + m + ":" + s + "&nbsp;" + ampm);
	    var t = setTimeout(startTime, 500);
	}
	function checkTime(i) {
	    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
	    return i;
	}
	$('#submit').click(function(){
		var email=$('input[type=email]').val();
		var password=$('#login_password').val();
		var role="login";
		if(email == "" && password == "") {
			swal({
				type: "error",
				title: "Login Failed",
				text: "Please provide user credentials.",
				timer: 1500,
				showConfirmButton: false
			})
			.then((value) => {
				swal.close();
			});
		} else {
			$.ajax({
				url: "login_webservice.php",
				type: "POST",
				data: {
					tag: role,
					login_username: email,
					login_password: password,
				}, 
				success: function(response) {
					console.log(response);
					if(response=="no data available") {
						swal({
							type: "error",
							title: "Login Failed",
							text: "User-email or Password is incorrect.",
							timer: 1500,
							showConfirmButton: false
						})
						.then((value) => {
							swal.close();
						});
					}
					else {
						swal({
							type: "success",
							title: "Login Successful!",
							text: "Please wait while redirecting to your page...",
							timer: 2000,
							showConfirmButton: false
						})
						.then((value) => {
							switch(response) {
								case "1":
									window.location="dashboard_sds.php";
								break;
								case "2":
									window.location="dashboard_so.php";
								break;
								default:
									window.location="dashboard_end_user.php";
								break;
							}	
						});
					}
				}
			});
		}
	});
</script>
