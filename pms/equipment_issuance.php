<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Issuances Dashboard</title>
		<meta charset="utf-8">
		<meta name = "viewport" content = "width=device-width, initial-scale=1">
			<?php
				require('links.php');
			?>
	</head>
	<?php
		include ('Connect.php'); 
		include('auth.php');
		require ('inc/navbar_transparency.php');
	?>
<body>
	<div class="container-fluid">
		<div class="col-sm-12">
			<table class="table" id="table_header" style="margin:0; border: none;">
				<tr style="border: none;">
					<td style="border: none;" width="15%" align="center"><img src="seal1.png" alt="Smiley face" width="100"></img></td>
					<td style="border: none;" width="70%" style="margin: 0; font-size: 0.8em;" align="center">REPUBLIC OF THE PHILIPPINES<br>
					<h5 style="margin: 0;">DEPARTMENT OF EDUCATION</h5>
					NATIONAL CAPITAL REGION<br>
					<h5 style="margin: 0;">SCHOOLS DIVISION OFFICE - MARIKINA CITY</h5>
					</td>
					<td style="border: none;" width="15%" align="center"><img src="deped_logo.png" alt="Smiley face" width="100"></img></td>
				</tr>
			</table>
			<h2><center>PROPERTY ACKNOWLEDGEMENT RECEIPT</center></h2>
			<table class="table table-bordered display table-sm">
				<thead>
					<tr>
						<td>Office:</td>
						<td colspan="2"><?php echo $_GET['office']; ?></td>
						<td colspan="2"><b>SDO Marikina City</b></td>
						<td></td>
					</tr>
					<tr>
						<td rowspan="2"><b>Qty</b></td>
						<td rowspan="2"><b>Unit</b></td>
						<td rowspan="2"><b>Description</b></td>
						<td><b>Property</b></td>
						<td><b>Date</b></td>
						<td rowspan="2"><b>Cost</b></td>
					<tr>
						<td><b>No.</b></td>
						<td><b>Acquired</b></td>
					</tr>
				</thead>
				<tbody>
					<?php
						if(empty($_GET['eventid'])) {
						$trans_id_sess=0;
						} else {
						$trans_id_sess=$_GET['eventid'];
						}
						$select_s="SELECT * FROM equipment_table WHERE trans_id=?";
						$sel_s_exec=$pdo->prepare($select_s);
						$sel_s_exec->execute([$trans_id_sess]);
						$count_trans=$sel_s_exec->rowCount();
						if($count_trans==0) {
						//do nothing
						} else if ($count_trans != 0) {
							while($row_s=$sel_s_exec->fetch(PDO::FETCH_ASSOC)) {
								$item_name=$row_s['item_desc'];
								$item_unit=$row_s['item_unit'];
								$item_qty=$row_s['item_qty'];
								$property_no=$row_s['property_no'];
								$date_acquired=$row_s['date_acquired'];
								$cost=$row_s['cost'];
								echo'<tr>
									<td>'.$item_qty.'</td>
									<td>'.$item_unit.'</td>
									<td>'.$item_name.'</td>
									<td>'.$property_no.'</td>
									<td>'.$date_acquired.'</td>
									<td>'.$cost.'</td>
								</tr>';
							}
						}
					?>
				</tbody>
				<tr>
					<td colspan="2">Received from: </td>
					<td></td>
					<td>Received by: </td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 30px;"></td>
					<td colspan="3" style="height: 30px;"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="3"><b>ANNA MARIE P. EXEQUIEL</b></td>
					<td colspan="3"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="3">Administrative Office IV/Supply Unit</td>
					<td colspan="3"></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="3"><?php echo date('F d,Y'); ?></td>
					<td colspan="3"><?php echo date('F d,Y'); ?></td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="3">Date</td>
					<td colspan="3">Date</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>