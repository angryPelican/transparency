<div class="panel with-nav-tabs panel-default">
	<div class="panel-heading">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1default" data-toggle="tab">For Approval</a></li>			
			<li><a href="#tab2default" data-toggle="tab">Approved</a></li>
		</ul>
	</div>	
	<div class="panel-body">
		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab1default">
				<table id="myTable" class="table table-striped display">
					<thead>
						<tr>
							<th>Project Title</th>
							<th>Date Created</th><!--DISPLAY ONLY, BASED ON LAST PROJECT EDIT-->
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><a href="">Food and Snacks for the ICT Capability Training</a></td>
							<td>December 10, 2017</td>
							<td><span class="label label-info">For approval</span></td>								
						</tr>		
					</tbody>
				</table>				
			</div>				
			<div class="tab-pane fade" id="tab2default">
				<table id="myTable" class="table table-striped display">
					<thead>
						<tr>
							<th>Project Title</th>
							<th>Date Created</th><!--DISPLAY ONLY, BASED ON LAST PROJECT EDIT-->
							<th>Status</th>
						</tr>
					</thead>
					<tbody>	
						<tr>
							<td><a href="">Leased Line Internet Subscription</a></td>
							<td>December 5, 2017</td>
							<td><span class="label label-success">Approved</span></td>								
						</tr>
					</tbody>
				</table>
			</div>
			<!--
			<div class="tab-pane fade" id="tab3default">
				<'?php include ('inc/table_rei_session.php'); ?>
			</div>-->
		</div>
	</div>
</div><!--END OF NAV-TABS-->	