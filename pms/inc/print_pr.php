<div class="container">

	<table class="table table-bordered">
		<!--1-->
		<tr>
			<td colspan="6" align="center">Purchase Request</td>
		</tr>
		<!--2-->
		<tr>
			<td colspan="6">Department: DepEd Marikina City</td>
		</tr>
		<!--3-->
		<tr>
			<td colspan="2">Office/Section:</td>
			<td colspan="2">P.R. NO: 12-2017-285DO</td>
			<td colspan="2">Date: 12/19/2017</td>
		</tr>
		<!--4-->
		<tr>
			<td colspan="2">Supply Unit</td>
			<td colspan="2">Responsibility Center Code:</td>
			<td colspan="2"></td>
		</tr>
		<!--5-->
		<tr>
			<td style= "width: 60pt;" align="center">Stock No.</td>
			<td style= "width: 70pt;" align="center">UNIT</td>
			<td style= "width: 150pt;"align="center">ITEM DESCRIPTION</td>
			<td style= "width: 70pt;" align="center">QUANTITY</td>
			<td style= "width: 90pt;"align="center">UNIT COST</td>
			<td style= "width: 90pt;"align="center">TOTAL COST</td>
		</tr>
		<!--6-->
		<tr>
			<td colspan="1">1</td>
			<td>unit</td>
			<td>Plaque Arcylic Glass 10"</td>
			<td>11</td>
			<td>₱ 1,500.00</td>
			<td>₱ 16,500.00</td>
		</tr>
		<!--7-->
		<tr>
			<td colspan="1">Purpose:</td>
			<td colspan="4">Project Title:</td>
			<td colspan="1" align="right">₱ 0,000.00</td>
			</tr>
		<!--8-->
		<tr>
			<td rowspan="3">
				Signature </br>
				Printed Name </br>
				Designation
			</td>
			<td>Requested by:</td>
			<td></td>
			<td colspan = "3">Approved by:</br>
				</br>
				</br>
			</td>					
		</tr>
	</table>
</div>