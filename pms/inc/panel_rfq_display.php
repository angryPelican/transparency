<div class="panel with-nav-tabs panel-default">
	<div class="panel-heading">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab1default" data-toggle="tab">Request for Quotation</a></li>
				<!--
				<li><a href="#tab2default" data-toggle="tab">Request for Proposal</a></li>
				<li><a href="#tab3default" data-toggle="tab">Request for Expression of Interest</a></li>
				<!--
				<li class="dropdown">
					<a href="#" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#tab4default" data-toggle="tab">Default 4</a></li>
						<li><a href="#tab5default" data-toggle="tab">Default 5</a></li>
					</ul>
				</li>-->
			</ul>
	</div>
	<div class="panel-body">
		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab1default">
				<?php include ('table_rfq_.php'); ?>
			</div>
			<!--
			<div class="tab-pane fade" id="tab2default">	
				<'?php include ('table_rfp_.php'); ?>
			</div>
			<div class="tab-pane fade" id="tab3default">
				<'?php include ('table_rei_.php'); ?>
			</div>-->
		</div>
	</div>
</div><!--END OF NAV-TABS-->	