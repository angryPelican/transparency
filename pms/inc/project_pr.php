<div class="panel panel-default">
	<div class="panel-heading">
		<!--PROJECT TITLE-->
		<h4 style="text-transform: uppercase;">PURCHASE REQUEST</h4>
	</div>
	<div class="panel-body table-responsive"  style="padding:0;">
		<table class="table table-striped table-bordered display" style="margin-bottom:0">
			<thead>
				<tr>
					<td colspan="2" rowspan="2">Office/Section<br /><b>ICT Unit</b></td>
					<td colspan="3">PR No. <b>12-2017-285DO</b></td><!--AUTO-GENERATED, AUTO INCREMENT-->
					<td colspan="3" rowspan="2">Date <br />
						<?php
							
							echo "<b>".date('F d, Y')."</b>";
						?>
					</td>
				</tr>
				<tr>
					<td colspan="3">Responsibility Center Code</td>
				</tr>							
			</tthead>
			<tbody>
				<tr>
					<th width="100">Stock No.</th>
					<th>Unit</th>
					<th colspan="2">Item Description</th>
					<th>Quantity</th>
					<th>Unit Cost</th>
					<th>Total Cost</th>
				</tr>
				<tr><!--FETCH RECORD FROM DBASE, LOOP-->
					<td>1</td><!--ROWSPAN VALUE DEPENDENT ON NUMBER OF ITEMS PER PROJECT-->
					<td>Lot</td>
					<td colspan="2">Leased Line, 8Mbps, 80% Reliability, Symmetric, Primary Router, 24/7 Technical support, 12 months subscription</td>
					<td>1</td><!--CONCATENATE QUANTITY AND UNIT-->								
					<td>300,000.00</td>
					<td>300,000.00</td>
				</tr>	
				<tr><!--FETCH RECORD FROM DBASE, LOOP-->
					<td></td><!--ROWSPAN VALUE DEPENDENT ON NUMBER OF ITEMS PER PROJECT-->
					<td></td>
					<td colspan="2">Primary router, optional WIFI</td>
					<td></td><!--CONCATENATE QUANTITY AND UNIT-->								
					<td></td>
					<td align="center"><a href="#"><i class="glyphicon glyphicon-pencil"></i></a></td>
				</tr>					
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2">Purpose</td>
					<td colspan="4"><b>Internet connection for DepEd office</b></td>
					<td><b>300,000.00</b></td>
				</tr>
			
			</tfoot>
		</table>
	</div>
	<div class="panel-footer clearfix" style="padding:8px 0;">
		<div class="col-sm-6">
			<h5><span class="label label-info">Approved</span></h5> 
		</div>
		<div class="col-sm-6">
			<div class="dropdown pull-right">					  											
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="#" data-toggle="modal" data-target="#addParticulars">Add Particulars</a></li><!--INSERTS A RECORD WITH ID CORRESPONDING WITH AN ITEM-->
					<li><a href="#">Download PR</a></li>
					<!--<li><a href="#">Generate PO</a></li>-->
					<li><a href="#addComment" data-toggle="modal">Comment</a></li>
				</ul>
			</div>							
		</div>
	</div>
</div>

<!-- Modal -->
<div id="addParticulars" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Particulars</h4>
      </div>
      <div class="modal-body">
        	<div class="form-group">
				<label>Description</label>
				<textarea class="form-control" rows="5" id="decription"></textarea>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Add</button>
      </div>
    </div>

  </div>
</div>