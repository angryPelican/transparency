<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bootstrap Example</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<?php
				require ('../inc/navbar_transparency.php');
			?>
			<h1>Project Procurement Plan</h1>
			<div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<!--PROJECT TITLE-->
							<h3 style="text-transform: uppercase;"><?php
							include('php/dynamic_title.php');
							?>
							</h3>
						</div>
						<div class="panel-body table-responsive">
							<table class="table table-striped table-bordered display">
								<thead>
									<tr>
										<th>Code</th>
										<th>General Description</th>
										<th>Qty/Size</th>
										<th>Estimated Budget</th>
										<th>Mode of Procurement</th>
										<th>Milestone of Activities</th>
										<th>Remarks</th>
									</tr>
								</thead>
								<?php
									include('php/end_user/view_items_end_user.php');
								?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									MILESTONES
								</div>
								<div class="panel-body">
									<table class="table table-striped table-bordered display">
										<thead>
											<tr>
												<th>Phases</th>
												<th>Approved</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Approved</td>
												<td><?php include('php/dynamic_approval_date.php'); ?></td>
											</tr>
											<tr>
												<td>Purchase Request</td>
												<td><?php include('php/get_pr_date.php'); ?></td>
											</tr>
											<tr>
												<td>Request for Quotation</td>
												<td><?php include('php/get_rfq_date.php'); ?></td>
											</tr>
											<tr>
												<td>Purchase Order</td>
												<td></td>
											</tr>
											<tr>
												<td>Delivered</td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									REMARKS
								</div>
								<div class="panel-body">
									<div class="list-group">
										<a href="#" class="list-group-item">
											<?php
							$get_id=$_GET['category'];
								$select_comment="SELECT * FROM project_table WHERE project_id='$get_id'";
									$res=mysqli_query($conn, $select_comment);
										while($row=mysqli_fetch_array($res)):
											$comment=$row['comment'];
										endwhile;
								if($comment != ""){
							echo '<i class="fa fa-twitter fa-fw"></i>'.$comment.'
							<span class="pull-right text-muted small"><em>RRegencia</em>
							</span>';
								} else if ($comment == ""){
									echo '<i class="fa fa-twitter fa-fw"></i>
							<span class="pull-right text-muted small"><em>RRegencia</em>
							</span>';
								}
							?>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</body>
		</html>