<div class="panel panel-default">
	<div class="panel-heading">
		<h4>PURCHASE ORDER</h4>
	</div>
	<div class="panel-body  table-responsive" style="padding:0;">
		<table class="table table-bordered display" style="margin:0;">
			<tr>
				<td width="110">Date</td>
				<td colspan="5"><b><?php echo date('F d, Y'); ?></b></td>
			</tr>
			<tr>
				<td>Quotation No</td>
				<td colspan="5"><b>DepEd-17-NCR-RFQ-153</b></td>
			</tr>
			<tr>
				<td>Total ABC</td>
				<td colspan="5"><b>Php300,000.00</b></td>
			</tr>
			<!--
			<tr>
				<td colspan="6">Detailed Estimate</td>
			</tr>
			<tr>
				<th width="110">Item No.</th>
				<th width="">Particulars</th>
				<th width="100">Quantity</th>
				<th width="100">Unit</th>
				<th width="100">Unit Cost (Php)</th>
				<th width="100">Total Cost</th>
			</tr>
			<tr>	
				<td>1</td>
				<td>Leased Line, 8Mbps, 80% Reliability, Symmetric, Primary Router, 24/7 Technical support, 12 months subscription</td>
				<td>1</td>
				<td>Lot</td>
				<td></td>
				<td></td>
			</tr>
			<tfoot>
				<td colspan="5" align="right">GRAND TOTAL</td>
				<td align="right">300,000.00
			</tfoot>
			-->
		</table>
	</div>
	<div class="panel-footer clearfix" style="padding:8px 0;">
		<div class="col-sm-6">
			<h5><span class="label label-info">Approved</span></h5> 
		</div>
		<div class="col-sm-6">
			<div class="dropdown pull-right">					  											
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="#">Add Item</a></li>
					<li><a href="#">Download PR</a></li>
					<!--<li><a href="#">Generate PO</a></li>-->
					<li><a href="#addComment" data-toggle="modal">Comment</a></li>
				</ul>
			</div>							
		</div>
	</div>
</div>