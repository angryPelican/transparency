<div class="panel panel-default">
	<div class="panel-heading">
		<!--PROJECT TITLE-->
		<h4 style="text-transform: uppercase;">PROJECT PROCUREMENT MANAGEMENT PLAN</h4>
	</div>
	<div class="panel-body table-responsive" style="padding:0;">
		<table class="table table-striped table-bordered display" style="margin:0;">
			<thead>
				<tr>
					<th width="100">Code</th>
					<th>General Description</th>
					<th>Qty/Size</th>
					<th>Estimated Budget</th>
					<th>Mode of Procurement</th>
					<th>Milestone of Activities</th>
					<th>Remarks</th>
				</tr>
			</thead>
			<?php
				include('view_items_so.php');
			?>
	<div class="panel-footer clearfix" style="padding:8px 0;">
		<div class="col-sm-6">
			<h5><span class="label label-info">Approved</span></h5> 
		</div>
		<div class="col-sm-6">
			<div class="dropdown pull-right">					  											
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Action
				<span class="caret"></span>
				</button>
				<?php
				include('check_purpose.php');
				?>
<div id="addPurpose" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Purpose</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Description</label>
					<form action="" method="post">
						<textarea class="form-control" name="purpose" rows="5"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" name="save" class="btn btn-success">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
				include('generate_ppmp.php');
				?>
		</div>
	</div>
</div>