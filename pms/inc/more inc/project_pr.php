<div class="panel panel-default">
	<div class="panel-heading">
		<!--PROJECT TITLE-->
		<h4 style="text-transform: uppercase;">PURCHASE REQUEST</h4>
	</div>
	<div class="panel-body table-responsive"  style="padding:0;">
		<table class="table table-striped table-bordered display" style="margin-bottom:0">
			<thead>
				<tr>
					<td colspan="2" rowspan="2">Office/Section<br /><b>ICT Unit</b></td>
					<td colspan="3">PR No. <b><?php include('get_pr.php');   echo "DepEd-"; echo date("y"); echo"-"; echo"NCR-PR-". $get_pr; ?></b></td><!--AUTO-GENERATED, AUTO INCREMENT-->
					<td colspan="3" rowspan="2">Date <br />
						<?php
							echo "<b>";
							include('get_dates.php');
							if($pr_date == '0000-00-00'){
							}
							else if ($pr_date != '0000-00-00'){
							echo date('F d, Y', strtotime($pr_date));
							}
							echo "</b>";
						?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><b><?php include('php/dynamic_title.php'); ?></b></td>
				</tr>							
			</thead>
			<tbody>
				<tr>
					<th width="100">Stock No.</th>
					<th>Unit</th>
					<th colspan="2">Item Description</th>
					<th>Quantity</th>
					<th>Unit Cost</th>
				</tr>
							<?php
			include('pr_table.php');
			?>				
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2"><b>Purpose</b></td>
					<td colspan="3"><?php include('get_purpose.php'); echo $purpose; ?></td>
					<td><b>Totat Cost: <?php include('get_total_cost.php'); ?></b></td>
				</tr>
			
			</tfoot>
		</table>
	</div>
	<div class="panel-footer clearfix" style="padding:8px 0;">
		<div class="col-sm-6">
			<h5><span class="label label-info">Approved</span></h5> 
		</div>
		<div class="col-sm-6">
			<div class="dropdown pull-right">					  											
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Action
				<span class="caret"></span>
				</button>
				<?php
					include('check_rfq.php');
				?>	
		</div>
	</div>
</div>
<?php
include('generate_rfq.php');
?>