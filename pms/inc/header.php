<meta charset="utf-8">
<meta name = "viewport" content = "width=device-width, initial-scale=1">

<!-- offline plugins -->

<!-- <link rel="stylesheet" type="text/css" href="fade-in.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" href="boots/flips/compiled/flipclock.css">
<script type="text/javascript" src="boots/js/jquery-3.3.1.min.js"></script> -->
<!-- bootstrap 4 datatable -->
<!-- <script type="text/javascript" src="boots/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="boots/datatable/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" type="text/css" href="boots/datatable/css/dataTables.bootstrap4.min.css"> -->
<!--Bootstrap 4.1.3-->
<!-- <script src="boots/popper/popper.min.js"></script>
<script src="boots/js/bootstrap.min.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="boots/css/bootstrap.min.css" crossorigin="anonymous"> -->
<!--other scripts-->
<!-- <script type="text/javascript" src="boots/crypto_js/rollups/md5.js"></script>
<script type="text/javascript" src="inc/date.js"></script>
<script src="boots/package/dist/sweetalert2.all.min.js"></script>
<link rel="stylesheet" type="text/css" href="boots/animate.css">
<link rel="stylesheet" type="text/css" href="boots/fontawesome/css/all.css"> -->

<!-- end of offline plugins -->

<!-- online plugins -->

<!-- bootstrap 4.1.3 -->
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<!-- bootstrap 4 datatable -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<!--other scripts-->
<link rel="stylesheet" type="text/css" href="css/fade-in.css" />
<link href="https://fonts.googleapis.com/css?family=Hind+Siliguri" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="boots/crypto_js/rollups/md5.js"></script>
<script type="text/javascript" src="inc/date.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.all.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<style>
	nav {
		font-size: 14px;
	}
	.dropdown-item, .dropdown-header {
		font-size: 14px !important;
	}
	.title {
		font-size: 16px;
	}
	.real_nav {
		-webkit-animation: slideInDown 0.7s; /* Safari 4+ */
		-moz-animation:    slideInDown 0.7s; /* Fx 5+ */
		-o-animation:      slideInDown 0.7s; /* Opera 12+ */
		animation:         slideInDown 0.7s; /* IE 10+, Fx 29+ */
	}
	.nav-drop {
		-webkit-animation: bounceIn 0.5s; /* Safari 4+ */
		-moz-animation:    bounceIn 0.5s; /* Fx 5+ */
		-o-animation:      bounceIn 0.5s; /* Opera 12+ */
		animation:         bounceIn 0.5s; /* IE 10+, Fx 29+ */
	}
	body {
		font-family: 'Hind Siliguri', sans-serif;
	}
	// Extra small devices (portrait phones, less than 576px)
	// No media query for `xs` since this is the default in Bootstrap



	// Small devices (landscape phones, 576px and up)
	@media (min-width: 576px) { ... }

	// Medium devices (tablets, 768px and up)
	@media (min-width: 768px) { ... }

	// Large devices (desktops, 992px and up)
	@media (min-width: 992px) { ... }

	// Extra large devices (large desktops, 1200px and up)
	@media (min-width: 1200px) { ... }

	</style>

<script type="text/javascript">
	$(document).ready( function () {
	    $('#otherProjects').DataTable();
	});
</script>

