<?php
include('Connect.php');
?>
<script type="text/javascript" src="boots/crypto_js/rollups/md5.js"></script>
<nav class="navbar navbar-expand-lg bg-dark text-white mb-1 pl-5 real_nav">
	<nav class="navbar-brand">
		<img src="../inc/seal1.png" width="35" height="35" class="d-inline-block" alt=""> <span class="title">PROCUREMENT MANAGEMENT SYSTEM</span>
	</nav>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto ">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Procurement</a>
				<div class="dropdown-menu nav-drop" aria-labelledby="navbarDropdown">
				<?php
					include('Connect.php');
					// GET THE USERNAME BASED ON USER ID
					$username = $_SESSION['s_username'];
					$query_select_user = "SELECT * FROM accounts WHERE user_name=?";
					$res = $pdo->prepare($query_select_user);
					if($res->execute([$username])) {
						$row_select = $res->fetch(PDO::FETCH_ASSOC);
						$get_type = $row_select['user_type'];
							//END OF QUERY
						switch($get_type){

							case '1':
								echo'<a class="dropdown-item" href="/transparency/pms/dashboard_sds.php">Procurement Projects</a>
										<a class="dropdown-item" href="/transparency/pms/app.php">APP</a>';
							break;

							case '2':
								echo'<a class="dropdown-item" href="/transparency/pms/dashboard_so.php">Procurement Projects</a>
										<a class="dropdown-item" href="/transparency/pms/app.php">APP</a>';
							break;

							case '3':
								echo'<a class="dropdown-item" href="/transparency/pms/dashboard_end_user.php">My Procurement Projects</a>
										<a class="dropdown-item" href="/transparency/pms/dashboard_sds.php">Procurement Projects</a>
										<a class="dropdown-item" href="/transparency/pms/app.php">APP</a>';
							break;

							case '4':
								echo'<a class="dropdown-item" href="/transparency/pms/dashboard_end_user.php">My Procurement Projects</a>
										<a class="dropdown-item" href="/transparency/pms/dashboard_so.php">Procurement Projects</a>
										<a class="dropdown-item" href="/transparency/pms/app.php">APP</a>';
							break;

							case '5':
								echo'<a class="dropdown-item" href="/transparency/pms/dashboard_end_user.php">My Procurement Projects</a>
										<a class="dropdown-item" href="/transparency/pms/dashboard_so.php">Procurement Projects</a>
										<a class="dropdown-item" href="/transparency/pms/app.php">APP</a>';
							break;

							default:
								echo'<a class="dropdown-item" href="/transparency/pms/dashboard_end_user.php">My Procurement Projects</a>
										<a class="dropdown-item" href="/transparency/pms/app.php">APP</a>
										<a class="dropdown-item" href="/transparency/pms/app.php">APP Reports</a>';
							break;
						}
					} else {
						echo $res->errorInfo();
					}
				?>
				
				</div>
			</li>
				<?php
					// GET THE USERNAME BASED ON USER ID
					$username = $_SESSION['s_username'];
					$query_select_user = "SELECT * FROM accounts WHERE user_name=?";
					$res = $pdo->prepare($query_select_user);
					if($res->execute([$username])) {
						$get_type = $row_select['user_type'];
						//END OF QUERY
						//SELECT ALL CATEGORIES
						$category_query="SELECT * FROM category_table";
						$res_category_query=$pdo->prepare($category_query);
						$res_category_query->execute();
						$category=array();
						$ajaxarr=array();
						while($row_category=$res_category_query->fetch(PDO::FETCH_ASSOC)) {
							$all_category=$row_category['category'];
							$all_code=$row_category['unique_code'];
							array_push($category, $all_category);
							array_push($ajaxarr, array('category'=>$all_category));
						}
						$val = json_encode($ajaxarr);
						switch($get_type){
							case '5':
							echo'<li class="nav-item dropdown" id="dd">
									<div class="dropdown-menu">
										<li class="nav-item dropdown" id="dd">
											<a style="" class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Inventory</a>
										<div class="dropdown-menu nav-drop">';
										foreach($category as $key => $value) {
											echo'<a id="issue_link" href="issuances_dashboard.php?category='.$value.'" data-role="'.$value.'" class="dropdown-item classifications">'.$value.'</a>';
										}
									echo'</div>
								</li>';
							break;
						}
					} else {
						echo $res->errorInfo();
					}
				?>
				<li class="nav-item">
					<a class="nav-link text-white" href="/transparency/pms/catalogue.php" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">Catalogue</a>
				</li>
				<li class="nav-item">
					
				</li>
				<script type="text/javascript">
				$(document).ready(function (){
					$('#issue_link').click(function(){
						localStorage.removeItem('activeTab');
					});
				var a = JSON.parse('<?php echo $val; ?>');
				for (var i = 0; i < a.length; i++) {
				if(a[i]['category'] == 'Advertising, Printing, Publication') {
						//to be removed
					$('a[data-role="Advertising, Printing, Publication"]').remove();
					$('a[data-role="Bonds/Traveling/Training"]').remove();
					$('a[data-role="Bonds"]').remove();
					$('a[data-role="Traveling/Representation Expenses"]').remove();
					$('a[data-role="In-Service Trainings"]').remove();
					$('a[data-role="Other Expenses"]').remove();
					$('a[data-role="Utilities"]').remove();
					$('a[data-role="Training"]').remove();
					$('a[data-role="Repair/Maintenance/Services"]').remove();
						//end of to be removed
					}
				}
				var admin=<?php echo $_SESSION['s_user_type']; ?>;
				if(admin=="6") { 
					$('#acc_settings').show();
					$('#drop_div').show();
					$('#manage_acc').show();
				} else if (admin!="6") {
					$('#acc_settings').hide();
					$('#drop_div').hide();
					$('#manage_acc').hide();
				}
				});
				</script>
			</div>
		</ul>
	<ul class="navbar-nav pull-right pr-5">
		<li class="nav-item dropdown">
			<?php
				$username=$_SESSION['s_username'];
				$user_type=$_SESSION['s_user_type'];
				echo '<meta http-equiv="refresh" content="9999;url=logoutadmin.php" />';
				$first_name=ucfirst($_SESSION['s_username']);
				echo '<a style="" class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b><i class="fas fa-user-circle"></i>&nbsp;'.$username.'</b></a>'
			?>
			<div class="dropdown-menu dropdown-menu-right dropdown-dark nav-drop" aria-labelledby="navbarDropdown1">
				<h7 class="dropdown-header" id="acc_settings">Account Settings</h7>
					<a href="home.php" class="dropdown-item" id="manage_acc"><i class="fas fa-address-book"></i> Manage Accounts</a>
					<div class="dropdown-divider" id="drop_div"></div>
				<h7 class="dropdown-header">Profile</h7>
					<a class="dropdown-item" href="#" data-toggle="modal" data-target="#userModal"><i class="fas fa-exchange-alt"></i> Change Password</a>
					<a class="dropdown-item" id="logout" href="logoutadmin.php"><i class="fas fa-sign-out-alt"></i> Logout</a>
				
				
			</div>
		</li>
	<script type="text/javascript">
		$('#logout').click(function(){
			localStorage.removeItem('req_fullname');
			localStorage.removeItem('req_position');
			localStorage.removeItem('req_unit');
			localStorage.removeItem('activeTab');
		});
	</script>
      <!--Change Password Modal -->
		<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header bg-primary">
						<h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-key"></i> Change User Password</h5>
						<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body text-dark">    
						<div class="form-group row">
							<div class="col">
								<?php
								$user_id=$_SESSION['s_id'];
								$select_user="SELECT * FROM accounts WHERE user_id=?";
								$select_user_exec=$pdo->prepare($select_user);
								$select_user_exec->execute([$user_id]);
								while($row_user=$select_user_exec->fetch(PDO::FETCH_ASSOC)) {
								$user_email=$row_user['user_email'];
								$user_password=$row_user['user_password'];
								}
								?>
								<div class="form-group">
									<label for="user_email"><b>Email</b></label>
									<input type="text" style="border: none;" name="user_email" class="form-control form-control-sm" value="<?php echo $user_email; ?>" disabled=""/>
								</div>
								<div class="form-group">
									<label for="def_pass"><b>Old Password</b></label>
									<input type="password" class="form-control form-control-sm" id="def_password" name="def_pass" value="" required/>
								</div>
								<div class="form-group">
									<label for="new_pass"><b>New Password</b></label>
									<input type="password" class="form-control form-control-sm" id="new_password" name="new_pass" value="" required/>
									<div class="valid-feedback">
										Password Matched!
									</div>
									<div class="invalid-feedback">
										New password do not match!
									</div>
								</div>
								<div class="form-group">
									<label for="re_pass"><b>Retype Password</b></label>
									<input type="password" class="form-control form-control-sm" id="re_pass" name="re_pass" value="" required/>
									<div class="valid-feedback">
										Password Matched!
									</div>
									<div class="invalid-feedback">
										New password do not match!
									</div>
								</div>
							</div>    
						</div>				
					</div>
					<div class="modal-footer">
						<div class="form-group pull-right m-0">
							<button class="btn btn-success" id="btn_submit">Save Changes</button>
						</div>
					</div>
				</div>
			</div>
		</div><!--END OF MODAL-->
	</ul>
</nav>

					  <script type="text/javascript">
					  $(document).ready(function() {
						$('#re_pass').on("keyup", function() {
							var re_pass=$(this).val();
							var new_pass=$('#new_password').val();

							  if(new_pass == re_pass){
								$(this).addClass("is-valid");
								$(this).removeClass("is-invalid");
							  } else {
								$(this).removeClass("is-valid");
								$(this).addClass("is-invalid");
							  }
							  if(re_pass == "") {
								$(this).removeClass("is-valid");
								$(this).removeClass("is-invalid");
							  }

						  });

						$('#new_password').on("keyup", function() {
							var new_pass=$(this).val();
							var re_pass=$('#re_pass').val();

							  if(new_pass == re_pass){
								$('#re_pass').addClass("is-valid");
								$('#re_pass').removeClass("is-invalid");
							  } else {
								$('#re_pass').removeClass("is-valid");
								$('#re_pass').addClass("is-invalid");
							  }
							  if(re_pass == "") {
								$('#re_pass').removeClass("is-valid");
								$('#re_pass').removeClass("is-invalid");
							  }

						  });

						$("#btn_submit").on("click", function(){
							  var re_pass=$('#re_pass').val();
							  var new_pass=$('#new_password').val();
							  var def_password=$('#def_password').val();
							  var md5_def_pass=CryptoJS.MD5(def_password);
							  var md5_re_pass=CryptoJS.MD5(re_pass);
							  var old_pass="<?php echo $user_password; ?>";
							  var user_id="<?php echo $_SESSION['s_id']; ?>";
							  var role="change_pass";
								if(md5_def_pass == old_pass){
									$.ajax({
									  url: '../pms/login_webservice.php',
									  type: 'POST',
									  data: {
									  tag: role,
									  new_password: re_pass,
									  user_id: user_id
									  },
									  success : function(response){
										swal({
										  title: "Password Changed",
										  text: "Once you click 'Ok', you'll be redirect to Login Page.",
										  icon: "success",
										})
										.then((ok) => {
										  window.location.href="../pms/logoutadmin.php";
										});
									  }
									});
								} else {
									swal({
									  title: "Oops!",
									  text: "Looks like your Old Password doesn't match.",
									  timer: 1100,
									  type: "error",
									  showConfirmButton: false,
									})
									.then((ok) => {
										swal.close();
									});
								}
							});
						})
					  </script>