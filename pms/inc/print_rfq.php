<div class="panel panel-default">
	<div class="panel-heading">
		<h4>PURCHASE ORDER</h4>
	</div>
	<div class="panel-body">
		<table class="table" style="font-size: 1.25em;">
			<tr>
				<td width="50"></td>
				<td>Date:</td>
				<td><b><i><?php echo date('F d, Y'); ?></i></b></td>
			</tr>
			<tr>
				<td width="50"></td>
				<td>Quotation No:</td>
				<td><b><i>DepEd-1-NCR-RFQ-153</i></b></td>
			</tr>
			<tr>	
				<td colspan="5"><b>To all Eligible Bidders</b></td>
			</tr>
			<tr>
				<td width="50">I.</td>
				<td colspan="4">Please quote your lowest price appraisal inclusive of VAT on the &quot;<b><u><i>LEASED LINE INTERNET SUBSCRIPTION</i></u></b>&quot;.
								This is subject to the Terms and Conditions of this RFQ. <b>Submit your quotation duly signed by your representative not later than
								<u><i>December 12, 2017</i></u></b>. For more information please call the BAC Secretariat at Telephone No. (02) 682-3989.
				</td>
			</tr>
			<tr>
				<td width="50">II.</td>
				<td colspan="4"><b>TOTAL APPROVED BUDGET FOR THE CONTRACT: Php300,000.00</b></td>
			</tr>
			<tr>
				<td width="50">II.</td>
				<td colspan="4"><b>SUMMARY OF WORKS</b></td>
			</tr>
		</table>
	</div>
</div>	