<?php
if (isset($_POST['ID'])) {
include "dbconnect/dbconfig.php";
 $ID = intval($_POST['ID']);
 $query = "SELECT * FROM public_bidding WHERE id='$ID'";
 $stmt = $db_con->prepare( $query );
 $stmt->execute(array(':ID'=>$ID));
 $rfq_row=$stmt->fetch(PDO::FETCH_ASSOC);
 extract($rfq_row);
 }
?>
<form  name="bidding_edit" action="../inc/table_bidding_edit_action.php" method="post" enctype="multipart/form-data">	
			<input type="hidden" name="id" value="<?php echo $rfq_row['id'];?>">

		  <div class="form-group">
			<label for="num">Reference No.</label>
			<input name="ref_no" type="" class="form-control" id="num" value="<?php echo $rfq_row['ref_no'];?>">
		  </div>		
		  <div class="form-group">
			<label for="desc">Description</label>
			<input name="desc" type="" class="form-control" id="desc" value="<?php echo $rfq_row['description'];?>">
		  </div>
		  <div class="form-group">
			<label for="budg">Approved Budget</label>
			<input name="budg" type="" class="form-control" id="budg" value="<?php echo $rfq_row['budget'];?>">
		  </div>
		  <div class="form-group">
			<label for="rem">Remarks</label>
			<select name="sel1" class="form-control" id="sel1">
				<option></option>
				<option <?php if ($rfq_row['remarks']=='Active'){echo 'selected';}?>>Active</option>
				<option <?php if ($rfq_row['remarks']=='Closed'){echo 'selected';}?>>Closed</option>
			</select>
		 </div>
<div class="checkbox">
			<label><input type="checkbox" onChange="document.getElementById('send').disabled = !this.checked;"> All the requirements prior to posting the ISQ have been complied with.</label>
		  </div>
		  
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" class="btn btn-default" id="send" disabled="disabled">Submit</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		</form>