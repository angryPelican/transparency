<div class="panel with-nav-tabs panel-default">
	<div class="panel-heading clearfix">
		<div class="panel-heading">
			<p class="text-right"><a href="create_project.php"><i class="glyphicon glyphicon-plus"></i> Add Project</a></p>
		</div>
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab1default" data-toggle="tab">For Approval</a></li>			
			<li><a href="#tab2default" data-toggle="tab">Approved</a></li>
			<li><a href="#tab3default" data-toggle="tab">Office PPMP</a></li>
		</ul>
	</div>	
	<div class="panel-body">
		<div class="tab-content">
			<div class="tab-pane fade in active" id="tab1default">
				<table id="approval" class="table table-striped display">
					<thead>
						<tr>
							<th>Project Title</th>
							<th>From</th>
							<th>Date Created</th><!--DISPLAY ONLY, BASED ON LAST PROJECT EDIT-->
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><a href="view_project_sds.php">Lunch and Snacks for the ICT Capability Training</a></td>
							<th>R. Regencia</th>
							<td>December 10, 2017</td>
							<td><span class="label label-info">For Approval</span></td>								
						</tr>		
					</tbody>
				</table>				
			</div>				
			<div class="tab-pane fade" id="tab2default">
				<table id="approved" class="table table-striped display">
					<thead>
						<tr>
							<th>Project Title</th>
							<th>From</th>
							<th>Date Created</th><!--DISPLAY ONLY, BASED ON LAST PROJECT EDIT-->
							<th>Status</th>
						</tr>
					</thead>
					<tbody>	
						<tr>
							<td><a href="">Leased Line Internet Subscription</a></td>
							<th>R. Regencia</th>
							<td>December 5, 2017</td>
							<td><span class="label label-success">Approved</span></td>								
						</tr>
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab3default">
				<table id="sdsTable" class="table table-striped display">
					<thead>
						<tr>
							<th>Project Title</th>
							<th>Date Created</th><!--DISPLAY ONLY, BASED ON LAST PROJECT EDIT-->
							<th align="center">Edit</th>
							<th>Status</th>
							<th align="center">Delete</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><a href="">Office Supplies for the SDS Office</a></td>
							<td>December 13, 2017</td>
							<td align="center"><i class="glyphicon glyphicon-pencil"></i></td>
							<td><span class="label label-default">Not submitted</span></td>								
							<td align="center"><i class="glyphicon glyphicon-trash"></i></td>
						</tr>
						<tr>
							<td><a href="">Food and Snacks for the SDS Meeting</a></td>
							<td>December 10, 2017</td>
							<td align="center"><i class="glyphicon glyphicon-pencil"></i></td>
							<td><span class="label label-default">Not submitted</span></td>								
							<td align="center"><i class="glyphicon glyphicon-trash"></i></td>
						</tr>		
						</tr>
					</tbody>
				</table>
			</div>			
			<!--
			<div class="tab-pane fade" id="tab3default">
				<'?php include ('inc/table_rei_session.php'); ?>
			</div>-->
		</div>
	</div>
</div><!--END OF NAV-TABS-->	