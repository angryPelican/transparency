<div class="panel panel-default">
	<div class="panel-heading">
		<!--PROJECT TITLE-->
		<h4 style="text-transform: uppercase;">PROJECT PROCUREMENT MANAGEMENT PLAN</h4>
	</div>
	<div class="panel-body table-responsive" style="padding:0;">
		<table class="table table-striped table-bordered display" style="margin:0;">
			<thead>
				<tr>
					<th width="100">Code</th>
					<th>General Description</th>
					<th>Qty/Size</th>
					<th>Estimated Budget</th>
					<th>Mode of Procurement</th>
					<th>Milestone of Activities</th>
					<th>Remarks</th>
				</tr>
			</thead>
			<tbody>
				<tr><!--FETCH RECORD FROM DBASE, LOOP-->
					<td rowspan="3"></td><!--ROWSPAN VALUE DEPENDENT ON NUMBER OF ITEMS PER PROJECT-->
					<td>Leased Line, 8Mbps, 80% Reliability, Symmetric, Primary Router, 24/7 Technical support, 12 months subscription</td>
					<td align="right">1 lot</td><!--CONCATENATE QUANTITY AND UNIT-->								
					<td align="right">300,000.00</td>
					<td rowspan="3">Small Value Procurement</td>
					<td rowspan="3">
						Pre-bid - December 18, 2017<br /><!--CONCATENATE MILESTONE AND DATE-->
						PhilGEPS posting - December 19, 2017
					</td>
					<td rowspan="2"></td>
				</tr>			
			</tbody>
			<tfoot>
				<td></td>
				<td></td>
				<td colspan-"3" align="right"><b>TOTAL</b></td>
				<td align="right"><b>300,000.00</b></td><!--DISPLAY COMPUTED TOTAL-->
				<td></td>
				<td></td>
				<td></td>
			</tfoot>
		</table>
	</div>
	<div class="panel-footer clearfix" style="padding:8px 0;">
		<div class="col-sm-6">
			<h5><span class="label label-info">Approved</span></h5> 
		</div>
		<div class="col-sm-6">
			<div class="dropdown pull-right">					  											
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<!--<li><a href="#">Generate PR</a></li>-->
					<li><a href="#">Download PPMP</a></li>
					<li><a href="#addComment" data-toggle="modal">Comment</a></li>
				</ul>
			</div>							
		</div>
	</div>
</div>