<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name = "viewport" content = "width=device-width, initial-scale=1">
	<link rel = "stylesheet" href = "boots/css/bootstrap.min.css" />
	<script type = "text/javascript" src = "boots/js/jquery-2.1.4.min.js"></script>
	<script type = "text/javascript" src = "boots/js/bootstrap.min.js"></script>
  
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">  
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type = "text/javascript" src = "execute_datatables.js"></script></head>
<body>
<form name = "myform" action = "" method = "post">
<div class="container">
	<?php
		require ('../inc/navbar_transparency.php');
	?>
	<h1><?php include('php/dynamic_title.php'); ?></h1>
	<div class="row">
		<div class="col-sm-12">
			<form action=""	 method="post" >
			<div class="panel panel-default">
				<div class="panel-heading">
				</div>
				<div class="panel-body table-responsive">
					<!--MODAL-->
					<p class="text-right"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addItem"><i class="glyphicon glyphicon-plus"></i> Add Item</button></p>
					<?php
						require('php/end_user/edit_ppmp_items.php');
						?>
					<br><div class="form-group">
					</div>
					<form action="" method="post">
						<label>Mode of Procurement: </label>
						<input type="text" id="id_of_pro" name="ppa" size="35" readonly="readonly" value="<?php 
						include('php/end_user/check_mode_of_procurement.php');
						?>"></input>
					<p>&nbsp;</p>
					<div class="well">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="col-sm-12">
								<?php
								include('php/dynamic_milestone.php');
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<p class="text-right"><button class="btn btn-success" name="save_mode"><i class="glyphicon glyphicon-floppy-save"></i> Save</button></p>
					</div>
					<?php
						include('php/end_user/save_procurement_on_edit_project.php');
						?>
					</form>
				</div>
			</div>
		</div>
	</div>
<div class="container">
	<!-- Modal -->
		<div id="addItem" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Item</h4>
					</div>
						<div class="modal-body">
							<form action="" method="post">
			<div class="form-group">
				<label>General Description</label>
				<input type="textarea" style="width:100%; height: 200px;" name = "gets" id="getsi"></input>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label>Quantity/Size:</label>						
						<input type="number" name = "quan" id = "quanti" ></input>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label>Unit:</label><br />						
						<select name="unit" style="width:100%;" id = "units">
							<option value = "pc">pc</option>
							<option value = "unit">unit</option>
							<option value = "set">set</option>
							<option value = "box">box</option>
							<option value = "pax">pax</option>
						</select>
					</div>
				</div>	
				<div class="col-sm-4">
					<div class="form-group">
						<label>Estimated Budget</label><br />						
						<input type="number" name = "buds" id = "est"></input>
					</div>
				</div>	
			</div>
      <div class="modal-footer">
        <button type = "submit" id = "insert_data" name = "ins" class = "btn btn-info btn-lg">+</button>
       </div>
	  </form>
	</div>
  </div>
</div>
</div>
		  <?php
			include("php/end_user/insert_item.php");
			?>
</div>
</body>
</html>