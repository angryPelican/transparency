<div class="panel panel-default">
	<div class="panel-heading">
		<h4>REQUEST FOR QUOTATION</h4>
	</div>
	<div class="panel-body table-responsive"  style="padding:0;">
		<table class="table table-bordered" style="margin:0;">
			<tr>
				<td width="110">Date:</td>
				<td><b><i><?php echo date('F d, Y'); ?></i></b></td>
			</tr>
			<tr>
				<td>Opening Date</td>
				<td><!--<input type="text" class="form-control" style="width: 50%">-->
					<div class="input-group date" style="width: 40%">
						<input type="text" class="form-control"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
					</div></input>
				</td>
			</tr>
			<tr>
				<td>QN:</td>
				<td><b><i>DepEd-1-NCR-RFQ-153</i></b></td>
			</tr>
		</table>
	</div>
	<div class="panel-footer clearfix" style="padding:8px 0;">
		<div class="col-sm-6">
			<h5><span class="label label-info">Approved</span></h5> 
		</div>
		<div class="col-sm-6">
			<div class="dropdown pull-right">					  											
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="#">Download RFQ</a></li>
					<!--<li><a href="#">Generate PO</a></li>-->
					<li><a href="#addComment" data-toggle="modal">Comment</a></li>
				</ul>
			</div>							
		</div>
	</div>	
</div>	

<script>
$document.ready{
	$('#sandbox-container .input-group.date').datepicker({
	});
}
</script>