<?php
ob_start();
include 'connectdb.php';
?>
<div class="card border-dark mb-4">
	<div class="card-header text-white bg-dark">
		<ul class="nav nav-pills" id="myTab">
			<li class="nav-item"><a class="nav-link link_for_approval tab_content text-white" href="#tab1default" data-toggle="tab">For Approval</a></li>
			<li class="nav-item"><a class="nav-link link_approved tab_content text-white" href="#tab2default" data-toggle="tab">Approved</a></li>
			<li class="nav-item"><a class="nav-link link_complete tab_content text-white" href="#tab3default" data-role="select_complete" data-toggle="tab">Completed</a></li>
		</ul>
	</div>
	<div class="card-body">
		<div class="tab-content">
			<div class="tab-pane fade" id="tab1default">
				<table id="approval" class="table table-striped display table-sm" style="width: 100%;">
					<thead>
						<tr>
							<th>Project Title</th>
							<th>Date Submitted</th>
							<th>From</th>
							<th>Total Estimated Budget</th>
						</tr>
					</thead>
					<tbody id="tabledark3">
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab2default">
				<table id="approved" class="table table-striped display table-sm" style="width: 100%;">
					<thead>
						<tr>
							<th>Project Title</th>
							<th>PPMP-AD</th>
							<th>PR No.</th>
							<th>RFQ No.</th>
							<th>PO No.</th>
							<th>EB</th>
						</tr>
					</thead>
					<tbody id="tabledark2">
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab3default">
				<table id="completed" class="table table-striped display table-sm" style="width: 100%;">
					<thead>
						<tr>
							<th>Project Title</th>
							<th>PPMP-AD</th>
							<th>PR No.</th>
							<th>RFQ No.</th>
							<th>PO No.</th>
							<th>EB</th>
							<th>AC</th>
						</tr>
					</thead>
					<tbody id="tabledark">
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php
	ob_end_flush();
?>
<script type="text/javascript">
$(document).ready(function() {

	$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
		localStorage.setItem('activeTab', $(e.target).attr('href'));
	});
	if(!localStorage.getItem('activeTab')) {
		localStorage.setItem('activeTab', "#tab1default");
	}
	var activeTab = localStorage.getItem('activeTab');
	if(activeTab) {
		$('#myTab a[href="' + activeTab + '"]').tab('show');
		if(activeTab == "#tab3default") {
			var role="select_complete";
			$.ajax({
				url: 'php/sds_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				},
				success : function(response){
					console.log(response);
					var data_json=JSON.parse(response);
					if ($.fn.DataTable.isDataTable("#completed")) {
						$('#completed').DataTable().clear().destroy();
					}
					$('#completed').DataTable({
					"aaData": data_json,
					});
				}
			});
		} else if (activeTab == "#tab2default"){
			var role="select_complete2";
			$.ajax({
				url: 'php/sds_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				},
				success : function(response){
					var data_json=JSON.parse(response);
					console.log(data_json);
					if ($.fn.DataTable.isDataTable("#approved")) {
						$('#approved').DataTable().clear().destroy();
					}
					$('#approved').DataTable({
					"aaData": data_json,
					});
				}
			});
		} else if(activeTab == "#tab1default") {
			var role="select_complete3";
			var username="<?php echo $_SESSION['s_username']; ?>";
			$.ajax({
				url: 'php/sds_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				s_username: username
				},
				success : function(response){
					var data_json=JSON.parse(response);
					if ($.fn.DataTable.isDataTable("#approval")) {
						$('#approval').DataTable().clear().destroy();
					}
					$('#approval').DataTable({
					"aaData": data_json,
					});
				}
			});
		}
	}

	$('.link_complete').on("click", function(){
		var role=$(this).data('role');
		$.ajax({
			url: 'php/sds_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			},
			success : function(response){
				console.log(response);
				var data_json=JSON.parse(response);
				if ($.fn.DataTable.isDataTable("#completed")) {
					$('#completed').DataTable().clear().destroy();
				}
				$('#completed').DataTable({
					"aaData": data_json,
				});
			}
		});
	});

	$('.link_approved').on("click", function(){
		var role="select_complete2";
		$.ajax({
			url: 'php/sds_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			},
			success : function(response){
				var data_json=JSON.parse(response);
				if ($.fn.DataTable.isDataTable("#approved")) {
					$('#approved').DataTable().clear().destroy();
				}
				$('#approved').DataTable({
				"aaData": data_json,
				});
			}
		});
	});

	$('.link_for_approval').on("click", function(){
		var role="select_complete3";
		var username="<?php echo $_SESSION['s_username']; ?>";
		$.ajax({
			url: 'php/sds_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			s_username: username
			},
			success : function(response){
				var data_json=JSON.parse(response);
				if ($.fn.DataTable.isDataTable("#approval")) {
					$('#approval').DataTable().clear().destroy();
				}
				$('#approval').DataTable({
					"aaData": data_json,
				});
			}
		});
	});

});
</script>