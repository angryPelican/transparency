	<tfoot>
		<tr>
			<td colspan="6" id="badge" style="border-right: 0;"></td>
			<td class="text-right" style="border-left: 0;">
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Action
					<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<form action="" method="post">
							<li id="to_generate"><button type="button" data-role="generate_rfq" data-toggle="modal" data-target="#rfq_modal" class="btn btn-link btn-generate-rfq">Generate RFQ</button></li>
							<li id="to_download"><a class="btn btn-link" href="download_pr.php?category=<?php echo $_GET['eventid']; ?>">Download PR</a></li>
							<li><a href="#" name="comment" class="btn btn-link comment_link" data-toggle="modal" data-target="#addCommentPR">Comment</a></li>
						</form>
					</ul>
				</div>
			</td>
		</tr>
	</tfoot>
</table>
<script type="text/javascript">
		var role="rfq_validate";
		var id="<?php echo $_GET['eventid']; ?>";
		var user_id="<?php echo $_SESSION['s_user_type']; ?>";
			$.ajax({
				url: "php/so_webservice.php",
				type: "post",
				data: {
					tag: role,
					eventid: id,
				},
				success: function(response) {
					var data=JSON.parse(response);
					console.log(data);
					for (var key in data){
						var pr_approved_date = data[key].pr_approved_date;
						var pr_date = data[key].pr_date;
						var rfq_date = data[key].rfq_date;
						switch(user_id){
							case '5':
								if(pr_approved_date != '0000-00-00') {
									document.getElementById('badge').innerHTML += '<span class="badge badge-success">Approved</span>';
									$('#to_generate').hide();
									$('#to_download').hide();
								} else if (pr_approved_date == '0000-00-00') {
									document.getElementById('badge').innerHTML += '<span class="badge badge-info">For Approval</span>';
									$('#to_generate').hide();
									$('#to_download').show();
								}
							break;
							case '4':
								if(pr_approved_date != '0000-00-00' && rfq_date == '0000-00-00') {
									document.getElementById('badge').innerHTML += '<span class="badge badge-success">Approved</span>';
									$('#to_generate').show();
								}
								if(pr_approved_date == '0000-00-00') {
									document.getElementById('badge').innerHTML += '<span class="badge badge-info">For Approval</span>';
									$('#to_generate').hide();
									$('#to_download').hide();
								}
								if(pr_approved_date != '0000-00-00' && rfq_date != '0000-00-00') {
									document.getElementById('badge').innerHTML += '<span class="badge badge-success">Approved</span>';
									$('#to_generate').hide();
									$('#to_download').hide();
								}
							break;
						}
					}

				}
			});
</script>
<?php
include('rfq_function.php')
?>
<script type="text/javascript">
	$(".btn-generate-rfq").click(function() {
			var role=$(this).data("role");
			var hidden_id="<?php echo $_GET['eventid']; ?>";
			var jqueryarray = <?php echo json_encode($a); ?>;
			var data = JSON.parse(jqueryarray);
			console.log(jqueryarray);
			for(var key in data) {
				var rfq_no=data[key].rfq_no;
				var category=data[key].categories;
			}
		$.ajax({
			url: 'php/so_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			project_id_tosend: hidden_id,
			rfq_tosend: rfq_no,
			category: category
			},
		success : function(response){
			console.log(response);
		swal ({
			title: "Success",
			text: "RFQ was generated!",
			type: "success",
			timer: 1100,
			showConfirmButton: false
		})
			.then((value) => {
				location.reload();
			});
		}
	});

});
</script>