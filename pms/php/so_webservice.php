<?php
include('connectdb.php');

$tag=$_POST['tag'];
switch ($tag) {
	case 'check_availability':
		$proj_id=$_POST['proj_id'];
		$check_abs="SELECT * FROM project_table WHERE project_id=?";
		$check_abs_exec=$pdo->prepare($check_abs);
		$check_abs_exec->execute([$proj_id]);
		$row=$check_abs_exec->fetch(PDO::FETCH_ASSOC);
		$abstract_date=$row['abstract_date'];
			if($abstract_date!='0000-00-00'){
				echo "eligible";
			} else if ($abstract_date=='0000-00-00') {
				echo "not eligible";
			}
		break;
	case 'rfq_validate':
		$get_id = $_POST['eventid'];
		$query = "SELECT * FROM project_table WHERE project_id=?";
		$stmt2=$pdo->prepare($query);
		$stmt2->execute([$get_id]);
		echo json_encode($stmt2->fetchall(PDO::FETCH_ASSOC));
		// while($row=$stmt2->fetch(PDO::FETCH_ASSOC)) {
		// 	$pr_approved_date = $row['pr_approved_date'];
		// 	$pr_date = $row['pr_date'];
		// 	$rfq_date = $row['rfq_date'];
		// }
		break;
	case 'comment_ppmp':
		# code...
		$get_id = $_POST['eventid'];
		$user = $_POST['s_username'];
		$module_input=$_POST['module'];
		$comment_value = $_POST['inputed_comment'];
		$update_comment = "INSERT INTO comment_table (project_id, module, comment, user_comment) VALUES (?,?,?,?)";
		$stmt4=$pdo->prepare($update_comment);
		if($stmt4->execute([$get_id,$module_input,$comment_value,$user])) {
			echo "yehey";
		} else {
			$stmt4->errorInfo();
		}
		break;
	case 'add_purpose':
		# code...
		$get_id = $_POST['eventid'];
		$get_purpose=$_POST['purpose'];
		$check_rows="SELECT * FROM purchase_request_table";
		$stmt1=$pdo->prepare($check_rows);
		if($stmt1->execute()) {
			echo "yehey1";
		} else {
			$stmt1->errorInfo();
		}
		$count=$stmt1->rowCount();
		if($count == 0) {
			$start_number = 54;
			$insert_pr="INSERT INTO purchase_request_table (project_id, pr_number) VALUES (?,?)";
			$stmt2=$pdo->prepare($insert_pr);
			if($stmt2->execute([$get_id,$start_number])) {
				echo "yehey2";
			}else {
				$stmt2->errorInfo();
			}
			$date_now=date('Y-m-d');
			$status=3;
			$update_pr_on_project_table="UPDATE project_table SET pr_no=?, pr_date=?, status=?, purpose=? WHERE project_id=?";
			$stmt3=$pdo->prepare($update_pr_on_project_table);
			if($stmt3->execute([$start_number,$date_now,$status,$get_purpose,$get_id])){
				echo"yehey3";
			} else {
				$stmt3->errorInfo();
			}
		}
		else if($count != 0){
			$getpr=0;
			$get_purpose=$_POST['purpose'];
			$select_last_pr_table="SELECT * FROM purchase_request_table ORDER BY pr_id DESC LIMIT 1";
						$stmt4=$pdo->prepare($select_last_pr_table);
						if($stmt4->execute()){
							echo"yehey4";
						}else {
							$stmt4->errorInfo();
						}
							while($raww=$stmt4->fetch(PDO::FETCH_ASSOC)):
									$pr_number=$raww['pr_number'];
									$pr_id=$raww['pr_id'];
									$getpr=$pr_number + 1;
							endwhile;
							
				$insert_pr="INSERT INTO purchase_request_table (project_id, pr_number) VALUES (?,?)";
					$stmt5=$pdo->prepare($insert_pr);
					if($stmt5->execute([$get_id,$getpr])){
						echo "yehey5";
					} else {
						$stmt5->errorInfo();
					}
			$date_now=date('Y-m-d');
			$status=3;
			$update_pr_on_project_table="UPDATE project_table SET pr_no=?, pr_date=?, status=?, purpose=? WHERE project_id=?";
					$stmt6=$pdo->prepare($update_pr_on_project_table);
					if($stmt6->execute([$getpr,$date_now,$status,$get_purpose,$get_id])){
						echo"yehey6";
					} else {
						$stmt6->errorInfo();
			}
		}
		break;
	
	case 'generate_rfq':
		# code...
		$rfq_no=$_POST['rfq_tosend'];
		$project_id=$_POST['project_id_tosend'];
		$cat=$_POST['category'];
		$arr_sum=array();
		$i=0;
		for ($i1=0; $i1 < count($cat); $i1++) { 
			$cat_array=$_POST['category'][$i1];
			$q1_sel="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
			$q1_exec=$pdo->prepare($q1_sel);
			if($q1_exec->execute([$project_id,$cat_array])){
				$sum=0;
				while($row=$q1_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_bud=$row['p_estimated_budget'];
					$p_title=$row['p_title_name'];
					$sum=$sum + $est_bud;
				}
				array_push($arr_sum, $sum);
			} else {
				echo $q1->errorInfo();
			}
		}
		if(empty($arr_sum)) {
			echo "none";
		} else if (!empty($arr_sum)) {
			echo json_encode($arr_sum);
		}

		$array_rfq=array();
		array_push($array_rfq, $rfq_no);
		$encoded=json_encode($rfq_no, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		$arr1=array();
			$count=count($rfq_no);

		for ($i=0; $i < $count; $i++) { 
			$category=$_POST['category'][$i];
			$rfq_no=$_POST['rfq_tosend'][$i];
			$project_id=$_POST['project_id_tosend'];
			$price=$arr_sum[$i];
			$insert_rfq_no="INSERT INTO rfq_table (project_id, rfq_number, category) VALUES (?,?,?)";
			$insert_exec=$pdo->prepare($insert_rfq_no);
			if($insert_exec->execute([$project_id,$rfq_no,$category])) {
				echo"gumana";
			} else {
				$insert_exec->errorInfo();
			}
			$insert_rfq_fund="INSERT INTO fund_utilization_table (project_id,project_title,rfq_number,rfq_amount,category) VALUES (?,?,?,?,?)";
			$fund_exec=$pdo->prepare($insert_rfq_fund);
			if($fund_exec->execute([$project_id,$p_title,$rfq_no,$price,$category])) {
				echo"gumana";
			} else {
				$insert_exec->errorInfo();
			}
		}
		$date_now=date('y');

		$select_proj_rfq="SELECT * FROM project_table WHERE project_id=?";
		$sel_proj_exec=$pdo->prepare($select_proj_rfq);
		$sel_proj_exec->execute([$project_id]);
			$row_sel_proj=$sel_proj_exec->fetch(PDO::FETCH_ASSOC);
			$rfq_fromcol=$row_sel_proj['rfq_no'];

		for ($i_c=0; $i_c < $count; $i_c++) { 
			# code...
			$pp=$_POST['rfq_tosend'][$i_c];
			$rfq_fromcol .= "DepEd-$date_now-NCR-RFQ-$pp\n";
		}

		echo $rfq_fromcol;
		$status=5;
		$date_rfq=date('Y-m-d');
		$update_project="UPDATE project_table SET rfq_no=?, status=?, rfq_date=? WHERE project_id=?";
		$update_proj_exec=$pdo->prepare($update_project);
		$update_proj_exec->execute([$rfq_fromcol,$status,$date_rfq,$project_id]);


		echo json_encode($rfq_no);
		break;

	case 'set_opening_date':
		# code...
		echo"asdasd";
		$get = $_POST['eventid'];
		$open=$_POST['pol'];
		$category=$_POST['cats'];
		$date='0000-00-00';
		$query_select="SELECT * FROM rfq_table WHERE opening_date=? AND project_id=? AND category=?";
		$query_exec=$pdo->prepare($query_select);
		$query_exec->execute([$date,$get,$category]);
		$cnt_row=$query_exec->rowCount();
		if($cnt_row!=0){
			$save_date="UPDATE rfq_table SET opening_date=? WHERE project_id=? AND category=?";
		$stmt3_rfq=$pdo->prepare($save_date);
			if ($stmt3_rfq->execute([$open,$get,$category])) {
		    	echo "fasdsa";
			} else {
		   		$stmt3->errorInfo();
			}
		} else if ($cnt_row==0) {
			$save_date="UPDATE rfq_table SET opening_date=? WHERE project_id=? AND category=?";
		$stmt3_rfq=$pdo->prepare($save_date);
			if ($stmt3_rfq->execute([$open,$get,$category])) {
		    	echo "fasdsa";
			} else {
		   		$stmt3->errorInfo();
			}
		}
		break;
	case 'select_approved':
		//SELECT APPROVED PROJECTS
			$null_status=0;
			$var2 = '0000-00-00';
			$querytitle = "SELECT * FROM project_table WHERE approval_date<>? AND delivery_status=? ORDER BY approval_date DESC";
			$stmt=$pdo->prepare($querytitle);
			$stmt->execute([$var2, $null_status]);
			$cnt_row=$stmt->rowCount();
			$sum = 0;
			$i = 0;
			$bigonearray=array();
			if($cnt_row!=0){
				while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
					$nestedarray=array();
					$project_id=$row['project_id'];
					$pr=$row['pr_no'];
					$po=$row['po_no'];
					$rfq=$row['rfq_no'];
					$title=$row['project_title'];
					$formatted=number_format($row['total_estimated_budget'], 2);
					$approval_date=$row['approval_date'];
					$year=date("y");
					if($pr=="") {
						$displayed_pr="";
					} else {
						$displayed_pr="01-" . $year . "-" . $pr . "-DO";	
					}
					if($rfq=="") {
						$displayed_rfq="";
					} else {
						$displayed_rfq=$rfq;
					}
					if($po=="") {
						$displayed_po="";
					} else {
						$displayed_po=$po;
					}
						$nestedarray[]="<a href='view_project_so.php?eventid=".$project_id."'>$title</a>";
						$nestedarray[]=$approval_date;
						$nestedarray[]=$displayed_pr;
						$nestedarray[]=$displayed_rfq;
						$nestedarray[]=$displayed_po;
						$nestedarray[]=$formatted;
					$bigonearray[]=$nestedarray;
				}
				echo json_encode($bigonearray);
			} else if($cnt_row==0){
				echo "null";
			}
		break;

	case 'select_complete':
		# code...
		$status=10;
		$sel_query_complete="SELECT * FROM project_table WHERE delivery_status=? ORDER BY approval_date";
		$executor=$pdo->prepare($sel_query_complete);
		$executor->execute([$status]);
		$cnt_row=$executor->rowCount();
		if($cnt_row!=0){
			while($row=$executor->fetch(PDO::FETCH_ASSOC)) {
				$nestedarray=array();
				$project_id=$row['project_id'];
				$pr=$row['pr_no'];
				$po=$row['po_no'];
				$rfq=$row['rfq_no'];
				$title=$row['project_title'];
				$formatted=number_format($row['total_estimated_budget'], 2);
				$formatted_ac=number_format($row['total_actual_cost'], 2);
				$approval_date=$row['approval_date'];
				$year=date("y");
				if($pr=="") {
					$displayed_pr="";
				} else {
					$displayed_pr="01-" . $year . "-" . $pr . "-DO";	
				}
				if($rfq=="") {
					$displayed_rfq="";
				} else {
					$displayed_rfq=$rfq;
				}
				if($po=="") {
					$displayed_po="";
				} else {
					$displayed_po=$po;
				}
					$nestedarray[]="<a href='view_project_so.php?eventid=".$project_id."'>$title</a>";
					$nestedarray[]=$approval_date;
					$nestedarray[]=$displayed_pr;
					$nestedarray[]=$displayed_rfq;
					$nestedarray[]=$displayed_po;
					$nestedarray[]=$formatted;
					$nestedarray[]=$formatted_ac;
				$bigonearray[]=$nestedarray;
			}
			echo json_encode($bigonearray);
		} else {
			echo "null";
		}
		break;

	case 'add_quote':
		# code...

		$get_id=$_POST['proj_id'];
		$cats=$_POST['category_abs'];
		$quote_data=$_POST['stringified_json'];
		$arr_p1=$_POST['supp1'];
		$arr_p2=$_POST['supp2'];
		$arr_p3=$_POST['supp3'];
		$query_sel="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
		$query_sel_exec=$pdo->prepare($query_sel);
		$query_sel_exec->execute([$get_id, $cats]);
		$row_sel=$query_sel_exec->rowCount();
		if(empty($row_sel)) {
			$ins_quote="INSERT INTO abstract_table (project_id, category, supplier_data) VALUES (?,?,?)";
				$ins_quote_exec=$pdo->prepare($ins_quote);
				if($ins_quote_exec->execute([$get_id, $cats, $quote_data])) {
					$sel_items="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
					$sel_items_exec=$pdo->prepare($sel_items);
					$sel_items_exec->execute([$get_id, $cats]);
					$cnt_items=$sel_items_exec->rowCount();
					$xx=0;
					$arr1=array();
					$arr2=array();
					$arr3=array();
					while($row=$sel_items_exec->fetch(PDO::FETCH_ASSOC)) {
						$items=$row['p_description'];
						$quantity=$row['p_quantity_size'];
						$check_rows="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
						$result_supp=$pdo->prepare($check_rows);
						$result_supp->execute([$get_id, $cats]);
						$row_abs=$result_supp->fetchall(PDO::FETCH_ASSOC);
						$json_decode=json_decode($row_abs[0]['supplier_data'], true);
						$q1="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
							$q1_exec=$pdo->prepare($q1);
							$q1_exec->execute([$get_id, $cats]);
							$cnt_row=$q1_exec->rowCount();
							$row_q1=$q1_exec->fetch(PDO::FETCH_ASSOC);
							$item_id=$row_q1['item_id'];
							$low=0;
							for ($i=0; $i < $cnt_row; $i++) {
							$prices=$json_decode[0]['supp_price'][$i];
							array_push($arr1, $prices);
							$prices2=$json_decode[1]['supp_price'][$i];
							array_push($arr2, $prices2);
							$prices3=$json_decode[2]['supp_price'][$i];
							array_push($arr3, $prices3);
							}// for loop
							$sk=$xx++;
							$multiplied1[]=$arr1[$sk] * $quantity;
							$multiplied2[]=$arr2[$sk] * $quantity;
							$multiplied3[]=$arr3[$sk] * $quantity;
							$added1=array_sum($multiplied1);
							$added2=array_sum($multiplied2);
							$added3=array_sum($multiplied3);
					}
					$finalbig[]=$added1;
					$finalbig[]=$added2;
					$finalbig[]=$added3;
					$mins=min(array_filter($finalbig));
					$ins_quote="UPDATE abstract_table SET supplier_data=?, total_actual_price=? WHERE project_id=? AND category=?";
					$ins_quote_exec=$pdo->prepare($ins_quote);
					if($ins_quote_exec->execute([$quote_data, $mins, $get_id, $cats])) {
						echo"pwede1";
				}
				$date_now=date('Y-m-d');
				$update_items_abs="UPDATE items_table SET abstract_date=? WHERE p_title_handler=? AND classification=?";
				$updt_exec=$pdo->prepare($update_items_abs);
				$updt_exec->execute([$date_now,$get_id,$cats]);
					echo"success: on updt_exec";
				} else {
					$ins_quote_exec->errorInfo();
				}
			} else {
				$sel_items="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
					$sel_items_exec=$pdo->prepare($sel_items);
					$sel_items_exec->execute([$get_id, $cats]);
					$cnt_items=$sel_items_exec->rowCount();
					$xx=0;
					$arr1=array();
					$arr2=array();
					$arr3=array();
					while($row=$sel_items_exec->fetch(PDO::FETCH_ASSOC)) {
						$items=$row['p_description'];
						$quantity=$row['p_quantity_size'];
						$check_rows="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
						$result_supp=$pdo->prepare($check_rows);
						$result_supp->execute([$get_id, $cats]);
						$row_abs=$result_supp->fetchall(PDO::FETCH_ASSOC);
						$json_decode=json_decode($row_abs[0]['supplier_data'], true);
						$q1="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
							$q1_exec=$pdo->prepare($q1);
							$q1_exec->execute([$get_id, $cats]);
							$cnt_row=$q1_exec->rowCount();
							$low=0;
							for ($i=0; $i < $cnt_row; $i++) {
							$prices=$json_decode[0]['supp_price'][$i];
							array_push($arr1, $prices);
							$prices2=$json_decode[1]['supp_price'][$i];
							array_push($arr2, $prices2);
							$prices3=$json_decode[2]['supp_price'][$i];
							array_push($arr3, $prices3);
							}// for loop
							$sk=$xx++;
							$multiplied1[]=$arr1[$sk] * $quantity;
							$multiplied2[]=$arr2[$sk] * $quantity;
							$multiplied3[]=$arr3[$sk] * $quantity;
							$added1=array_sum($multiplied1);
							$added2=array_sum($multiplied2);
							$added3=array_sum($multiplied3);
					}
					$finalbig[]=$added1;
					$finalbig[]=$added2;
					$finalbig[]=$added3;
					$mins=min(array_filter($finalbig));
				$ins_quote="UPDATE abstract_table SET supplier_data=?, total_actual_price=? WHERE project_id=? AND category=?";
				$ins_quote_exec=$pdo->prepare($ins_quote);
				if($ins_quote_exec->execute([$quote_data, $mins, $get_id, $cats])) {
					echo"pwede2";
				$date_now=date('Y-m-d');
				$update_items_abs="UPDATE items_table SET abstract_date=? WHERE p_title_handler=? AND classification=?";
				$updt_exec=$pdo->prepare($update_items_abs);
				$updt_exec->execute([$date_now,$get_id,$cats]);
				} else {
					$ins_quote_exec->errorInfo();
				}
			}
				//insert to inventory		
				$no_date='0000-00-00';
				$q1_sel_items="SELECT * FROM items_table WHERE p_title_handler=? AND classification=? AND abstract_date<>?";
					$q1_sel_item_exec=$pdo->prepare($q1_sel_items);
					if($q1_sel_item_exec->execute([$get_id,$cats,$no_date])) {
						echo "success: no errors found";
						$cnt_item=$q1_sel_item_exec->rowCount();
						echo $cnt_item;
						$xx=0;
						$array_item_id=array();
						$array_item_name=array();
						$array_quantity=array();
						$array_categories=array();
						$array_unit=array();
						$array_user_id=array();
						while($row_categ=$q1_sel_item_exec->fetch(PDO::FETCH_ASSOC)) {
							$categories=$row_categ['classification'];
							$item_name=$row_categ['p_description'];
							$item_id=$row_categ['item_id'];
							
							$item_quantity=$row_categ['p_quantity_size'];
							$item_unit=$row_categ['p_unit'];
							$user_id=$row_categ['user_id'];
							array_push($array_item_id, $item_id);
							array_push($array_item_name, $item_name);
							array_push($array_quantity, $item_quantity);
							array_push($array_unit, $item_unit);
							array_push($array_user_id, $user_id);
							array_push($array_categories, $categories);
							$check_quote="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
							$check_exec=$pdo->prepare($check_quote);
							if($check_exec->execute([$get_id,$cats])){
								echo"success on check_exec";
								$row_cnt_check=$check_exec->rowCount();
							}
						}// end of while
					} else {
						echo"error on checkexec: " . $check_exec->rowCount();
					}//end of if
						$check_inv="SELECT * FROM inventory_table WHERE project_id=? AND category=?";
							$check_inv_exec=$pdo->prepare($check_inv);
							if($check_inv_exec->execute([$get_id,$cats])) {
								echo "success on check inventory";
								echo"<script>console.log('" . json_encode($array_item_id) . "');</script>";
								echo"<script>console.log('" . json_encode($array_item_name) . "');</script>";
								echo"<script>console.log('" . json_encode($array_quantity) . "');</script>";
								echo"<script>console.log('" . json_encode($array_unit) . "');</script>";
								echo"<script>console.log('" . json_encode($array_user_id) . "');</script>";
								echo"<script>console.log('" . json_encode($array_categories) . "');</script>";
								$cnt_inv=$check_inv_exec->rowCount();
							} else {
								echo $check_inv_exec->errorInfo();
						}
						if(empty($cnt_inv)) {
							for ($key=0; $key < $cnt_item; $key++) {
								$arr_id=$array_user_id[$key];
								$arr_item_id=$array_item_id[$key];
								$arr_cats=$array_categories[$key];
								$arr_item=$array_item_name[$key];
								$arr_quan=$array_quantity[$key];
								$arr_unit=$array_unit[$key];
								$q2_insert="INSERT INTO inventory_table (project_id,user_id,item_id,category,i_description,i_quantity_size,i_unit) VALUES (?,?,?,?,?,?,?)";
								$q2_exec_insert=$pdo->prepare($q2_insert);
								if($q2_exec_insert->execute([$get_id,$arr_id,$arr_item_id,$arr_cats,$arr_item,$arr_quan,$arr_unit])) {
									echo "success on q2: no errors found";
								} else {
									echo"error on q2: " . $q2_exec_insert->errorInfo();
								}
							}
							$getwinner="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
							$getwinner_exec=$pdo->prepare($getwinner);
							$getwinner_exec->execute([$get_id, $cats]);
							$row_winner=$getwinner_exec->fetchall(PDO::FETCH_ASSOC);
							$decode_first=json_decode($row_winner[0]['supplier_data'], true);
							$bigone1=array();
							$arr33=array();
							$arr22=array();
							$arr11=array();
							for ($i=0; $i < $cnt_item; $i++) {
								$prices1=$decode_first[0]['supp_price'][$i];
								$prices2=$decode_first[1]['supp_price'][$i];
								$prices3=$decode_first[2]['supp_price'][$i];
								array_push($arr11, $prices1);
								array_push($arr22, $prices2);
								array_push($arr33, $prices3);
							}
							for ($sk=0; $sk < $cnt_item; $sk++) {
								$multiplied11[]=$arr11[$sk] * $array_quantity[$sk];
						        $multiplied22[]=$arr22[$sk] * $array_quantity[$sk];
						        $multiplied33[]=$arr33[$sk] * $array_quantity[$sk];
							}  
					           	$sum1=array_sum($multiplied11);
						        $sum2=array_sum($multiplied22);
						        $sum3=array_sum($multiplied33);
						        array_push($bigone1, $sum1);
						        array_push($bigone1, $sum2);
						        array_push($bigone1, $sum3);
						        $mins=min(array_filter($bigone1));

							if($mins==$sum1) {
								for ($x=0; $x < count($arr11); $x++) {
									$price_final_list=$arr11[$x];
									$item_ids=$array_item_id[$x];
									$q3_update = "UPDATE inventory_table SET unit_price=? WHERE item_id=? AND category=?";
									$q3_exec=$pdo->prepare($q3_update);
									if($q3_exec->execute([$price_final_list,$item_ids,$cats])) {
										echo"success: no errors found on q3 on sum1";
									} else {
										echo "error: " . $q3_exec->errorInfo();
									}
								}
							}
							if($mins==$sum2) {
								for ($x=0; $x < count($arr22); $x++) {
									$price_final_list=$arr22[$x];
									$item_ids=$array_item_id[$x];
									$q3_update = "UPDATE inventory_table SET unit_price=? WHERE item_id=? AND category=?";
									$q3_exec=$pdo->prepare($q3_update);
									if($q3_exec->execute([$price_final_list,$item_ids,$cats])) {
										echo"success: no errors found on q3 on sum2";
									} else {
										echo "error: " . $q3_exec->errorInfo();
									}
								}
							}
							if($mins==$sum3) {
								for ($x=0; $x < count($arr33); $x++) {
									$price_final_list=$arr33[$x];
									$item_ids=$array_item_id[$x];
									$q3_update = "UPDATE inventory_table SET unit_price=? WHERE item_id=? AND category=?";
									$q3_exec=$pdo->prepare($q3_update);
									if($q3_exec->execute([$price_final_list,$item_ids,$cats])) {
										echo"success: no errors found on q3 on sum3";
									} else {
										echo "error: " . $q3_exec->errorInfo();
									}
								}
							}
						} else if (!empty($cnt_inv)) {
							$getwinner="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
							$getwinner_exec=$pdo->prepare($getwinner);
							$getwinner_exec->execute([$get_id, $cats]);
							$row_winner=$getwinner_exec->fetchall(PDO::FETCH_ASSOC);
							$decode_first=json_decode($row_winner[0]['supplier_data'], true);
							$bigone1=array();
							$arr33=array();
							$arr22=array();
							$arr11=array();
							for ($i=0; $i < $cnt_item; $i++) {
								$prices1=$decode_first[0]['supp_price'][$i];
								$prices2=$decode_first[1]['supp_price'][$i];
								$prices3=$decode_first[2]['supp_price'][$i];
								array_push($arr11, $prices1);
								array_push($arr22, $prices2);
								array_push($arr33, $prices3);
							}
							for ($sk=0; $sk < $cnt_item; $sk++) {
								$multiplied11[]=$arr11[$sk] * $array_quantity[$sk];
						        $multiplied22[]=$arr22[$sk] * $array_quantity[$sk];
						        $multiplied33[]=$arr33[$sk] * $array_quantity[$sk];
							}  
					           	$sum1=array_sum($multiplied11);
						        $sum2=array_sum($multiplied22);
						        $sum3=array_sum($multiplied33);
						        array_push($bigone1, $sum1);
						        array_push($bigone1, $sum2);
						        array_push($bigone1, $sum3);
						        $mins=min(array_filter($bigone1));

							if($mins==$sum1) {
								for ($x=0; $x < count($arr11); $x++) {
									$price_final_list=$arr11[$x];
									$item_ids=$array_item_id[$x];
									$q3_update = "UPDATE inventory_table SET unit_price=? WHERE item_id=? AND category=?";
									$q3_exec=$pdo->prepare($q3_update);
									if($q3_exec->execute([$price_final_list,$item_ids,$cats])) {
										echo"success: no errors found on q3 on sum1 update";
									} else {
										echo "error: " . $q3_exec->errorInfo();
									}
								}
							}
							if($mins==$sum2) {
								for ($x=0; $x < count($arr22); $x++) {
									$price_final_list=$arr22[$x];
									$item_ids=$array_item_id[$x];
									$q3_update = "UPDATE inventory_table SET unit_price=? WHERE item_id=? AND category=?";
									$q3_exec=$pdo->prepare($q3_update);
									if($q3_exec->execute([$price_final_list,$item_ids,$cats])) {
										echo"success: no errors found on q3 on sum2 update";
									} else {
										echo "error: " . $q3_exec->errorInfo();
									}
								}
							}
							if($mins==$sum3) {
								for ($x=0; $x < count($arr33); $x++) {
									$price_final_list=$arr33[$x];
									$item_ids=$array_item_id[$x];
									$q3_update = "UPDATE inventory_table SET unit_price=? WHERE item_id=? AND category=?";
									$q3_exec=$pdo->prepare($q3_update);
									if($q3_exec->execute([$price_final_list,$item_ids,$cats])) {
										echo"success: no errors found on q3 on sum3 update";
									} else {
										echo "error: " . $q3_exec->errorInfo();
									}
								}
							}
						}
		break;
	case 'get_abstract':
		# code...
			$get_id=$_POST['proj_id'];
			$sel_abstract="SELECT * FROM abstract_table WHERE project_id=?";
			$sel_abs_exec=$pdo->prepare($sel_abstract);
			$sel_abs_exec->execute([$get_id]);
			$arr1=array();
				while ($row_abs=$sel_abs_exec->fetch(PDO::FETCH_ASSOC)) {
					echo $row_abs['supplier_data'];
				}
		break;
	case 'sel_existing_tab':
		# code...
			$get_cat=$_POST['hidden_cat'];
			$get_id=$_POST['proj_id'];
			$sel_existing_abs="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
			$sel_exist_exec=$pdo->prepare($sel_existing_abs);
			$sel_exist_exec->execute([$get_id, $get_cat]);
			$row_count=$sel_exist_exec->rowCount();
				if(empty($row_count)) {
					echo "none";
					// $outer_array=array();
					// $inner_array=array();
						// $inner_array[]=""
				} else {
					$abs_row=$sel_exist_exec->fetchall(PDO::FETCH_ASSOC);
					$x1=json_encode($abs_row[0]['supplier_data'], true);
					echo json_decode($x1);
				}
		break;
	case 'sel_items_abs':
		# code...
		$get_id=$_POST['proj_id'];
		$hidden_cat=$_POST['hidden_cat'];
		$query="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
		$query_exec=$pdo->prepare($query);
		$query_exec->execute([$get_id, $hidden_cat]);
		$cnt_row=$query_exec->rowCount();
			if(empty($cnt_row)){
				echo"none";
			} else {
				$row_abs=$query_exec->fetchall(PDO::FETCH_ASSOC);
				$x1=json_encode($row_abs[0]['supplier_data'], true);
				echo json_decode($x1);
			}
		break;
	case 'sel_items_by_cat':
		$get_id=$_POST['proj_id'];
		$get_cat=$_POST['category'];
		$select_items="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
			$sel_items_exec=$pdo->prepare($select_items);
			$sel_items_exec->execute([$get_id, $get_cat]);
		if($sel_items_exec->rowCount() == 0) {
			echo "null";
		}
		if($sel_items_exec->rowCount() != 0) {
			echo json_encode($sel_items_exec->fetchall(PDO::FETCH_ASSOC));
		}
		break;
	case 'select_dl_abs':
		# code...
		$get_id=$_POST['proj_id'];
		$sel_query="SELECT * FROM items_table WHERE p_title_handler=? GROUP BY classification";
		$sel_query_exec=$pdo->prepare($sel_query);
		$sel_query_exec->execute([$get_id]);
			echo json_encode($sel_query_exec->fetchall(PDO::FETCH_ASSOC));
		break;
	case 'generate_po':
		# code...
		$get_id=$_POST['proj_id'];
		$common="Common Office Supplies";
		$q1="SELECT * FROM items_table WHERE p_title_handler=? AND classification<>? GROUP BY classification";
		$q1_exec=$pdo->prepare($q1);
		$q1_exec->execute([$get_id,$common]);
		$count=$q1_exec->rowCount();
		$cats_array=array();
		while($row=$q1_exec->fetch(PDO::FETCH_ASSOC)){
			$cats=$row['classification'];
			$cats_array[]=$cats;
		}
		$q1_sel_po="SELECT * FROM po_table ORDER BY po_id DESC LIMIT 1";
		$q1_sel_po_exec=$pdo->prepare($q1_sel_po);
		if($q1_sel_po_exec->execute()) {
			$cnt_po=$q1_sel_po_exec->rowCount();
			if($cnt_po!=0) {
				while($row=$q1_sel_po_exec->fetch(PDO::FETCH_ASSOC)) {
					$starting=$row['po_no'];
				}
			} else {
				$starting=50;
			}
		}

		$q1_sel_abs="SELECT * FROM abstract_table WHERE category<>? AND project_id=?";
		$q1_abs_exec=$pdo->prepare($q1_sel_abs);
		if($q1_abs_exec->execute([$common,$get_id])) {
			$arr_price=array();
			while ($row_abs1=$q1_abs_exec->fetch(PDO::FETCH_ASSOC)) {
				$total_actual_price=$row_abs1['total_actual_price'];
				array_push($arr_price, $total_actual_price);
			}
		} else {
			$q1_abs_exec->errorInfo();
		}
		$q_fund="SELECT * FROM fund_utilization_table WHERE project_id=?";
		$q_fund_exec=$pdo->prepare($q_fund);
		if($q_fund_exec->execute([$get_id])) {
			$arr_fundid=array();
			$rfq_amount_array=array();
			while($row_fund=$q_fund_exec->fetch(PDO::FETCH_ASSOC)) {
				$fund_id=$row_fund['fund_id'];
				$rfq_amount=$row_fund['rfq_amount'];
				array_push($arr_fundid, $fund_id);
				array_push($rfq_amount_array, $rfq_amount);
			}
		} else {
			$q_fund_exec->errorInfo();
		}

		$date_now=date('Y-m-d');
			for ($i=0; $i < $count; $i++) {
				$catsu=$cats_array[$i];
				$xx=$starting++;
				$update="INSERT INTO po_table (project_id, po_no, po_date, category) VALUES (?,?,?,?)";
				$update_exec=$pdo->prepare($update);
				$update_exec->execute([$get_id,$xx,$date_now,$catsu]);
			}

		$sel_po="SELECT * FROM po_table WHERE project_id=?";
		$exec_sel_po=$pdo->prepare($sel_po);

		if($exec_sel_po->execute([$get_id])) {
			$cnt_po_row=$exec_sel_po->rowCount();
			$po_array=array();
			if(empty($cnt_po_row)) {
				$po_no=50;
				for ($po=0; $po < count($arr_fundid); $po++) { 
					array_push($po_array, $po_no++);                                                     
				}
			} else if (!empty($cnt_po_row)) {
				while($po_row=$exec_sel_po->fetch(PDO::FETCH_ASSOC)) {
					$po_no=$po_row['po_no'];
					array_push($po_array, $po_no);
				}
			}
		}

		for ($i_fund=0; $i_fund < count($arr_fundid); $i_fund++) {
			$difference=$rfq_amount_array[$i_fund] - $arr_price[$i_fund];
			$po_price=$arr_price[$i_fund];
			$rfq_price=$rfq_amount_array[$i_fund];
			$id=$arr_fundid[$i_fund];
			$po_number=$po_array[$i_fund];
			echo $cats_array[$i_fund];
			$sel_cat="SELECT * FROM category_table WHERE category=?";
			$exec_sel_cat=$pdo->prepare($sel_cat);
			if($exec_sel_cat->execute([$cats_array[$i_fund]])) {
				while($row_cat=$exec_sel_cat->fetch(PDO::FETCH_ASSOC)) {
					$total_po=$row_cat['total_po_amount'];
					$app_budget=$row_cat['app_budget'];
				}
			} else {
				$exec_sel_cat->errorInfo();
			}
			$total_po_amount=$total_po + $po_price;
			$diff=$app_budget - $po_price;
			$cat_up_po="UPDATE category_table SET total_po_amount=?, app_balance=? WHERE category=?";
			$exec_cat_po=$pdo->prepare($cat_up_po);
			if($exec_cat_po->execute([$total_po_amount,$diff,$cats_array[$i_fund]])) {
				echo"pumasok sa category";
			} else {
				$exec_cat_po->errorInfo();
			}
			$q_fund_update="UPDATE fund_utilization_table SET po_number=?, po_amount=?, difference=? WHERE project_id=? AND fund_id=?";
			$exec_qfund=$pdo->prepare($q_fund_update);
			if($exec_qfund->execute([$po_number,$po_price,$difference,$get_id,$id])) {
				echo "success";
			} else {
				$exec_qfund->errorInfo();
			}
		}
		$q_po="SELECT * FROM po_table WHERE project_id=?";
		$q_po_exec=$pdo->prepare($q_po);
		$q_po_exec->execute([$get_id]);
		$arr_po=array();
		while($q_po_row=$q_po_exec->fetch(PDO::FETCH_ASSOC)){
			$po_no=$q_po_row['po_no'];
			$arr_po[]=$po_no;
		}
		$q2="SELECT * FROM project_table WHERE project_id=?";
		$q2_exec=$pdo->prepare($q2);
		$q2_exec->execute([$get_id]);
		$row_q2=$q2_exec->fetch(PDO::FETCH_ASSOC);
		$po_toinsert=$row_q2['po_no'];
		$y=date("y");
			for ($po=0; $po < $count; $po++) {
				$xx=$arr_po[$po];
				$po_toinsert .= "01-". $y  ."-". $xx ."-DO\n";
			}
		$date_rfq=date('Y-m-d');
		$status=8;
		$update_project="UPDATE project_table SET status=?, po_no=?, po_date=? WHERE project_id=?";
		$update_proj_exec=$pdo->prepare($update_project);
		if($update_proj_exec->execute([$status,$po_toinsert,$date_rfq,$get_id])){
			echo"yehey";
		} else {
			$update_proj_exec->errorInfo();
		}
		break;
	case 'validate_abs':
		$nodate='0000-00-00';
		$get_id=$_POST['eventid'];
		$sel_item_abs="SELECT * FROM project_table WHERE project_id=?";
		$sel_item_exec=$pdo->prepare($sel_item_abs);
		$sel_item_exec->execute([$get_id]);
		$row=$sel_item_exec->fetch(PDO::FETCH_ASSOC);
			if($row['abstract_date']=='0000-00-00'){
				echo "null";
			} else {
				echo $row['abstract_date'];
			}
		break;
	case 'del_full':
		# code...
	$date_full=$_POST['date_full'];
	$get_id=$_POST['id'];
		$update_del="UPDATE items_table SET delivery_date=? WHERE p_title_handler=?";
			$del_executor=$pdo->prepare($update_del);
			$del_executor->execute([$date_full,$get_id]);
				$status=10;
				$update_stat_query="UPDATE project_table SET delivery_status=? WHERE project_id=?";
				$execute_update_stat=$pdo->prepare($update_stat_query);
				$execute_update_stat->execute([$status,$get_id]);

		$milestone_delivery="UPDATE milestone_table SET delivery_date=? WHERE milestone_handler=?";
		$execute_delivery=$pdo->prepare($milestone_delivery);
		if($execute_delivery->execute([$date_full,$get_id])){
			echo"execute_delivery : success";
		} else {
			echo $execute_delivery->errorInfo();
		}
			//update total_actual_cost
			$q_actual_cost="SELECT * FROM inventory_table WHERE project_id=?";
			$q_actual_cost_exec=$pdo->prepare($q_actual_cost);
			$q_actual_cost_exec->execute([$get_id]);
			$sum=0;
			while($row_act=$q_actual_cost_exec->fetch(PDO::FETCH_ASSOC)) {
				$unit_price=$row_act['unit_price'] * $row_act['i_quantity_size'];
				$total=$sum + $unit_price;
			}
			$status=10;
			$update_stat_query="UPDATE project_table SET delivery_status=?, total_actual_cost=? WHERE project_id=?";
			$execute_update_stat=$pdo->prepare($update_stat_query);
			$execute_update_stat->execute([$status,$total,$get_id]);
				//UPDATE QUERY FOR INVENTORY
				$x1="Advertising, Printing, Publication";
				$x2="Bonds/Traveling/Training";
				$x3="Bonds";
				$x4="Traveling/Representation Expenses";
				$x5="In-Service Trainings";
				$x6="Other Expenses";
				$x7="Utilities";
				$x8="Training";
				$x9="Repair/Maintenance/Services";
				$q1_select_first="SELECT * FROM inventory_table WHERE project_id=? AND category<>? AND category<>? AND category<>? AND category<>? AND category<>? AND category<>? AND category<>? AND category<>? AND category<>?";
				$q1_sel_exec=$pdo->prepare($q1_select_first);
				if($q1_sel_exec->execute([$get_id,$x1,$x2,$x3,$x4,$x5,$x6,$x7,$x8,$x9])) {
					$cnt_row_item=$q1_sel_exec->rowCount();
					$array_item_id=array();
					$array_category=array();
					while($row_item=$q1_sel_exec->fetch(PDO::FETCH_ASSOC)) {
						array_push($array_item_id, $row_item['item_id']);
						array_push($array_category, $row_item['category']);
					}
					$status=1;
					if($cnt_row_item!=0) {
						for ($i=0; $i < count($array_item_id); $i++) {
							$arr_item=$array_item_id[$i];
							$arr_cat=$array_category[$i];
							$q1_update_inv="UPDATE inventory_table SET status=? WHERE project_id=? AND category=? AND item_id=?";
							$q1_stmt=$pdo->prepare($q1_update_inv);
							if($q1_stmt->execute([$status,$get_id,$arr_cat,$arr_item])) {
								echo"q1_update: success";
							} else {
								echo $q1_stmt->errorInfo();
							}
						}
					}
				} else {
					echo $q1_sel_exec->errorInfo();
				}
				
		break;
		case 'del_exec':
			$get_id=$_POST['id'];
			$count=count($_POST['actual']);
			for($i=0; $i < $count; $i++) {
				$dates=$_POST['actual'][$i];
				$items=$_POST['item_id'][$i];
				$delivery_query="UPDATE items_table SET delivery_date=? WHERE item_id=?";
					$execute_del=$pdo->prepare($delivery_query);
					if($execute_del->execute([$dates,$items])) {
						echo "yehey";
					} else {
						$execute_del->errorInfo();
					}
			}
			$last_date=$_POST['last_del_date'];
			echo $last_date;
			$milestone_delivery="UPDATE milestone_table SET delivery_date=? WHERE milestone_handler=?";
				$execute_delivery=$pdo->prepare($milestone_delivery);
				if($execute_delivery->execute([$last_date,$get_id])) {
					echo"yehey";
				} else {
					$execute_del->errorInfo();
				}
			$null_date="0000-00-00";
			$select_items="SELECT * FROM items_table WHERE p_title_handler=? AND delivery_date=? ORDER BY item_id DESC LIMIT 1";
				$execute_select_item=$pdo->prepare($select_items);
				$execute_select_item->execute([$get_id,$null_date]);
				$row_select_item=$execute_select_item->fetch(PDO::FETCH_ASSOC);
				$last_date_delivery=$row_select_item['delivery_date'];
					if($last_date_delivery == '0000-00-00') {
						//update total_actual_cost
						$q_actual_cost="SELECT * FROM inventory_table WHERE project_id=?";
						$q_actual_cost_exec=$pdo->prepare($q_actual_cost);
						$q_actual_cost_exec->execute([$get_id]);
						$sum=0;
						while($row_act=$q_actual_cost_exec->fetch(PDO::FETCH_ASSOC)) {
							$unit_price=$row_act['unit_price'] * $row_act['i_quantity_size'];
							$total=$sum + $unit_price;
						}
						$status=11;
						$update_stat_query="UPDATE project_table SET delivery_status=?, total_actual_cost=? WHERE project_id=?";
						$execute_update_stat=$pdo->prepare($update_stat_query);
						$execute_update_stat->execute([$status,$total,$get_id]);
					} else if ($last_date_delivery != '0000-00-00') {
						//update total_actual_cost
						$q_actual_cost="SELECT * FROM inventory_table WHERE project_id=?";
						$q_actual_cost_exec=$pdo->prepare($q_actual_cost);
						$q_actual_cost_exec->execute([$get_id]);
						$sum=0;
						while($row_act=$q_actual_cost_exec->fetch(PDO::FETCH_ASSOC)) {
							$unit_price=$row_act['unit_price'];
							$total=$sum + $unit_price;
						}
						$status=10;
						$update_stat_query="UPDATE project_table SET delivery_status=?, total_actual_cost=? WHERE project_id=?";
						$execute_update_stat=$pdo->prepare($update_stat_query);
						$execute_update_stat->execute([$status,$total,$get_id]);
						//UPDATE QUERY FOR INVENTORY
						$x1="Advertising, Printing, Publication";
						$x2="Bonds/Traveling/Training";
						$x3="Bonds";
						$x4="Traveling/Representation Expenses";
						$x5="In-Service Trainings";
						$x6="Other Expenses";
						$x7="Utilities";
						$x8="Training";
						$x9="Repair/Maintenance/Services";
						$q1_select_first="SELECT * FROM inventory_table WHERE project_id=? AND category<>? AND category<>? AND category<>? AND category<>? AND category<>? AND category<>? AND category<>? AND category<>? AND category<>?";
						$q1_sel_exec=$pdo->prepare($q1_select_first);
						if($q1_sel_exec->execute([$get_id,$x1,$x2,$x3,$x4,$x5,$x6,$x7,$x8,$x9])) {
							$cnt_row_item=$q1_sel_exec->rowCount();
							$array_item_id=array();
							$array_category=array();
							while($row_item=$q1_sel_exec->fetch(PDO::FETCH_ASSOC)) {
								array_push($array_item_id, $row_item['item_id']);
								array_push($array_category, $row_item['category']);
							}
							$status=1;
							if($cnt_row_item!=0) {
								for ($i=0; $i < count($array_item_id); $i++) {
									$arr_item=$array_item_id[$i];
									$arr_cat=$array_category[$i];
									$q1_update_inv="UPDATE inventory_table SET status=? WHERE project_id=? AND category=? AND item_id=?";
									$q1_stmt=$pdo->prepare($q1_update_inv);
									if($q1_stmt->execute([$status,$get_id,$arr_cat,$arr_item])) {
										echo"q1_update: success";
									} else {
										echo $q1_stmt->errorInfo();
									}
								}
							}
						} else {
							echo $q1_sel_exec->errorInfo();
						}
					}
			break;

}		
?>