<h5 style="text-transform: uppercase;">ABSTRACT OF QUOTATION</h5>
<style type="text/css">
	.modal-lg {
    max-width: 65% !important;
}
input[type="number"]::-webkit-outer-spin-button,
input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input[type="number"] {
    -moz-appearance: textfield;
}
</style>
<table class="table table-striped table-bordered display mb-4" id="table_aoq">
	<thead>
		<tr>
			<th width="50%">Categories</th>
			<th width="20%">Supplier</th>
			<th>Amount</th><!--AUTO-GENERATED, AUTO INCREMENT-->
			<th>Download</th>
		</tr>
	</thead>
	<tbody id="tb_dark">
		<?php
			include('php/connectdb.php');
			$get_id=$_GET['eventid'];
			$common="Common Office Supplies";
			$sel_items="SELECT * FROM items_table WHERE p_title_handler=? AND classification<>? GROUP BY classification";
			$sel_exec=$pdo->prepare($sel_items);
			$sel_exec->execute([$get_id,$common]);
			$i=0;
			$cc=0;
			$arr_c=array();
			
			$merger=array();
				while($row=$sel_exec->fetch(PDO::FETCH_ASSOC)) {
					$categories=$row['classification'];
					array_push($arr_c, $categories);
					$count_cat=count($arr_c);
					echo"<tr>
							<td><a href='#' data-toggle='modal' id='abs_exec' data-cat='$categories' data-target='#modal_quote'>$categories</a></td>";
				for ($i_c=0; $i_c < $count_cat; $i_c++) {
					$sel_abs="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
					$sel_abs_exec=$pdo->prepare($sel_abs);
					$sel_abs_exec->execute([$get_id,$categories]);
				}		
					$rowcount=$sel_abs_exec->rowCount();
					$i=0;
						if($rowcount==0) {
							echo"<td></td>
									<td></td>
									<td></td>
								</tr>";
						} else if($rowcount!=0) {
								$row_abs=$sel_abs_exec->fetchall(PDO::FETCH_ASSOC);
								$json_decode=json_decode($row_abs[0]['supplier_data'], true);
								$total=$row_abs[0]['total_actual_price'];
							$q_sel_items="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
							$q_exec=$pdo->prepare($q_sel_items);
							$q_exec->execute([$get_id, $categories]);
							$cnt_row_items=$q_exec->rowCount();
							while($rs_quan=$q_exec->fetch(PDO::FETCH_ASSOC)) {
								$arr_q=array();
								$quantity=$rs_quan['p_quantity_size'];
								array_push($arr_q, $quantity);
								$json_encode=json_decode(json_encode(array_values($json_decode)), true);
								$arr1=array();
								$arr2=array();
								$arr3=array();
									for ($i=0; $i < $cnt_row_items; $i++) {
										$arr_winner=array();
										$arr_names=array();
										$prices1=$json_encode[0]['supp_price'][$i];
										$prices2=$json_encode[1]['supp_price'][$i];
										$prices3=$json_encode[2]['supp_price'][$i];
										$names1=$json_encode[0]['supp_name'];
										$names2=$json_encode[1]['supp_name'];
										$names3=$json_encode[2]['supp_name'];
										$arr1[]=$prices1;
										$arr2[]=$prices2;
										$arr3[]=$prices3;
										$summed1=array_sum($arr1);
										$summed2=array_sum($arr2);
										$summed3=array_sum($arr3);
										$arr_names[$names1]=$summed1;
										$arr_names[$names2]=$summed2;
										$arr_names[$names3]=$summed3;
									}
								}
								$xxsupp=array_keys($arr_names, min(array_filter($arr_names)));
								$total=number_format($total, 2);
										echo'<td>'.$xxsupp[0].'</td>
										<td align="right">'.$total.'</td>';
										
								switch ($_SESSION['s_user_type']) {
									case '4':
										echo'<td align="center"><a class="btn btn-primary" href="download_abstract.php?eventid='.$get_id.'&category='.$categories.'"><i class="fas fa-download"></i></a></td>';
										break;
									
									default:
										echo'<td align="center"><a class="btn btn-primary btn disabled" href="download_abstract.php?eventid='.$get_id.'&category='.$categories.'"><i class="fas fa-download"></i></a></td>';
										break;
								}
										

									echo"</tr>";
							}
						}
						
		?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3" style="border-right: none;" id="td_dark"></td>
			<td style="border-left: none;" align="right">
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
						<?php
							switch ($_SESSION['s_user_type']) {
								case '5':
									echo'<li><a class="btn btn-link" href="#" id="generate_po">Generate PO</a></li>';
									break;
								case '4':
									echo'<li><a class="btn btn-link sr-only" href="#" id="generate_po">Generate PO</a></li>';
									break;
							}
						?>
						<li><a class="btn btn-link" href="#" data-target='#addCommentABSTRACT' data-toggle='modal'>Comment</a></li>
					</ul>
				</div>
			</td>
		</tr>
	</tfoot>
</table>
<script type="text/javascript">
	var role="validate_abs";
	var id="<?php echo $_GET['eventid']; ?>";
	$.ajax({
		url: "php/so_webservice.php",
		type: "post",
		data: {
			tag: role,
			eventid: id,
		},
		success: function(response){
			console.log(response);
			if(response=="null"){
				document.getElementById('td_dark').innerHTML += "<span class='badge badge-info'>For Approval</span>";
				$("#generate_po").hide();
			} else if (response!="null") {
				document.getElementById('td_dark').innerHTML += "<span class='badge badge-success'>Approved</span>";
				$("#generate_po").show();
			}
		}
	});
	$('#generate_po').click(function(){
		var role="generate_po";
		var id="<?php echo $_GET['eventid']; ?>";
			$.ajax({
				url: "php/so_webservice.php",
				type: "POST",
				data: {
					tag: role,
					proj_id: id,
				},
				success: function(response) {
					// console.log(response);
					swal({
						title: "Success!",
						text: "PO Generated Successfully",
						type: "success",
						timer: 1100,
						showConfirmButton: false,
					})
					.then((result) => {
						location.reload();
					});
				}
			});
	});
	$('a[id="abs_exec"]').click(function(){
		var cat=$(this).data('cat');
			$('#dynamic_cat').text(cat);
			$('#hidden_cat').val(cat);
			$("#tbod-dark").html("");
	});
</script>

<!-- Comment Modal -->
<div id="addCommentABSTRACT" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="background: #0652DD; color: white;">
				<h5 class="modal-title">Add Comment</h5>
				<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<form action="" method="post" id="formcomment">
					<div class="form-group">
						<label>Comment </label>
						<textarea class="form-control" id="comment_text_aoq" form="formcomment" name="inputed_comment" rows="4" ></textarea>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" name="comment" data-role="comment_ppmp" form="formcomment" class="btn btn-default btn-default btn-comment-abstract" style="background: #0652DD; color: white;">Add</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.btn-comment-abstract').click(function(){
		var section="AOQ";
		var role=$(this).data("role");
		var hidden_id="<?php echo $_GET['eventid']; ?>";
		var username="<?php echo $_SESSION['s_username']; ?>";
		var comment_input=$('#comment_text_aoq').val();
		$('#addCommentABSTRACT').modal('toggle');
			$.ajax({
				url: 'php/so_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				module: section,
				inputed_comment: comment_input,
				s_username: username,
				eventid: hidden_id
				},
				success : function(response){
					// console.log(response);
					swal({
						title: "Success",
						text: "You Successfully commented!",
						type: "success",
						timer: 1100,
						showConfirmButton: false
					})
					.then((value) => {
						location.reload();
					});
				}
			});
	});
</script>
<!-- Modal -->
<div class="modal fade" id="modal_quote" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-dark text-white">
				<h5 class="modal-title" id="exampleModalLongTitle">Add Quotations - <input type="hidden" value="" name="hidden_cat" id="hidden_cat"><span id="dynamic_cat"></span></h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="the-form">
					<table class="table table-bordered table-striped table-sm" style="margin: 0; table-layout: auto; width: 100%;">
						<thead id="thead_dark">
							<tr>
								<th width='40%' rowspan='2' style='vertical-align: middle;'>Items</th>
								<th width='60%' colspan='3' style='text-align: center;'>Suppliers</th>
							</tr>
							<tr>
								<th><input data-reset="0" class='form-control form-inline' type='text' placeholder='Type Supplier Here ..' id='supp_1' required>
									<div class='custom-control custom-checkbox my-1 mr-sm-2' data-unique="0">
										<input type='checkbox' class='custom-control-input' id='inline1' data-unique="0">
										<label class='custom-control-label' for='inline1'>DNMS(s)</label>
									</div>
								</th>
								<th><input data-reset="1" class='form-control' type='text' placeholder='Type Supplier Here ..' id='supp_2' required>
									<div class='custom-control custom-checkbox my-1 mr-sm-2' data-unique="1">
										<input type='checkbox' class='custom-control-input' id='inline2' data-unique="1">
										<label class='custom-control-label' for='inline2'>DNMS(s)</label>
									</div>
								</th>
								<th><input data-reset="2" class='form-control' type='text' placeholder='Type Supplier Here ..' id='supp_3' required>
									<div class='custom-control custom-checkbox my-1 mr-sm-2' data-unique="2">
										<input type='checkbox' class='custom-control-input' id='inline3' data-unique="2">
										<label class='custom-control-label' for='inline3'>DNMS(s)</label>
									</div>
								</th>
							</tr>
							<script type="text/javascript">
								$(document).ready(function(){
									$('#modal_quote').on('show.bs.modal', function (e) {
										var role="sel_existing_tab";
										var get_cat=$('#hidden_cat').val();
										var get_id="<?php echo $_GET['eventid']; ?>";
										$.ajax({
											url: "php/so_webservice.php",
											type: 'POST',
											data: {
												tag: role,
												proj_id: get_id,
												hidden_cat: get_cat
											}, 
											success: function(response){	
												console.log(response);
												if(response=="none") {
													$('#supp_1').val("");
													$('#inline1').attr('checked', false);
													$('#supp_2').val("");
													$('#inline2').attr('checked', false);
													$('#supp_3').val("");
													$('#inline3').attr('checked', false);
													$('#inline1').click(function() {
														if($(this).is(":checked")){
															$('input[name="supp_p1"]').each(function() {
																$(this).attr('disabled', true);
															});
														} else {
															$("#supp_1").removeAttr('disabled', true);
															$('input[name="supp_p1"]').each(function() {
																	$(this).removeAttr('disabled', true);
																});
														}
													});
													$('#inline2').click(function() {
														if($(this).is(":checked")){
															
															$('input[name="supp_p2"]').each(function() {
																$(this).attr('disabled', true);
															});
														} else {
															$("#supp_2").removeAttr('disabled', true);
															$('input[name="supp_p2"]').each(function() {
																$(this).removeAttr('disabled', true);
															});
														}
													});
													$('#inline3').click(function() {
														if($(this).is(":checked")){
															
															$('input[name="supp_p3"]').each(function() {
																$(this).attr('disabled', true);
															});
														} else {
															$("#supp_3").removeAttr('disabled', true);
															$('input[name="supp_p3"]').each(function() {
																$(this).removeAttr('disabled', true);
															});
														}
													});
												} else {
													var data=JSON.parse(response);
													for(var key in data){
														var suppliers=data[key].supp_name;
														if(suppliers=="no data available in json"){
															$('input[data-reset=' + key + ']').attr('disabled', true);
															$('input[data-unique=' + key + ']').prop('checked', true);
														} else {
															$('input[data-reset=' + key + ']').removeAttr('disabled', true);
															$('input[data-unique' + key + ']').prop('checked', false);
														}
														$('input[data-reset=' + key + ']').val(suppliers);
													}
													$('#inline1').click(function() {
														if($(this).is(":checked")){
															$('input[name="supp_p1"]').each(function() {
																$(this).attr('disabled', true);
															});
														} else {
															$("#supp_1").removeAttr('disabled', true);
															$('input[name="supp_p1"]').each(function() {
																	$(this).removeAttr('disabled', true);
																});
														}
													});
													$('#inline2').click(function() {
														if($(this).is(":checked")){
															
															$('input[name="supp_p2"]').each(function() {
																$(this).attr('disabled', true);
															});
														} else {
															$("#supp_2").removeAttr('disabled', true);
															$('input[name="supp_p2"]').each(function() {
																$(this).removeAttr('disabled', true);
															});
														}
													});
													$('#inline3').click(function() {
														if($(this).is(":checked")){
															
															$('input[name="supp_p3"]').each(function() {
																$(this).attr('disabled', true);
															});
														} else {
															$("#supp_3").removeAttr('disabled', true);
															$('input[name="supp_p3"]').each(function() {
																$(this).removeAttr('disabled', true);
															});
														}
													});
												}
											}
										});
									});
								});
							</script>
						</thead>
						<tbody id="tbod-dark">
							<script type="text/javascript">
								$('#modal_quote').on('show.bs.modal', function (e) {
									$('#tbod-dark').html("");
									var role="sel_items_by_cat";
									var id="<?php echo $_GET['eventid']; ?>";
									var categ=$('#hidden_cat').val();
										$.ajax({
											url: "php/so_webservice.php",
											type: "POST",
											data: {
												tag: role,
												proj_id: id,
												category: categ,
											}, success: function(response){
												var data=JSON.parse(response);
												console.log(response);
												for (var key in data) {	
													var items=data[key].p_description;
													var item_id=data[key].item_id;
													var quantity=data[key].p_quantity_size;
													document.getElementById("tbod-dark").innerHTML += "<tr id='"+ items +"' data-items='"+ items +"'><td><input type='hidden' data-id='"+ item_id +"' id='item_name' value='"+ items +"'>"+ items + "<small> x " + quantity +"</small></td><td><input class='form-control' type='number' data-item='"+ items +"' id='star' step='any' style='text-align:right;' name='supp_p1' required></td><td><input class='form-control' type='number' data-item='"+ items +"' id='star' step='any' data-item='"+ items +"' style='text-align:right;' name='supp_p2' required></td><td><input class='form-control' type='number' data-item='"+ items +"' id='star' step='any' data-item='"+ items +"' style='text-align:right;' name='supp_p3' required></td></tr>";
												}
											}
										});
								});
							</script>
							<script type="text/javascript">
								$(document).ready(function(){
									$('#modal_quote').on('show.bs.modal', function (e) {
										var get_cat=$('#hidden_cat').val();
										var role="sel_items_abs";
										var get_id="<?php echo $_GET['eventid']; ?>";
										$.ajax({
											url: "php/so_webservice.php",
											type: "POST",
											data: {
												tag: role,
												hidden_cat: get_cat,
												proj_id: get_id
											}, success: function(response) {
												if(response=="none") {

												} else {
													var data=JSON.parse(response);
													// console.log(data[2].supp_price);
													var prices1=data[0].supp_price;
													// console.log(prices1);
													var prices2=data[1].supp_price;
													var prices3=data[2].supp_price;
													var ix1 = 0;
													var ix2 = 0;
													var ix3 = 0;
													$('input[name="supp_p1"]').each(function() {
														ix1++;
															for (var i = 0; i < ix1; i++) {
																if(prices1[i]==0){
																	prices1[i]="";
																	$(this).attr("disabled", true);
																} else {
															// alert(prices[i]);
																	$(this).val(prices1[i]);
																}
														}
													});
													$('input[name="supp_p2"]').each(function() {
														ix2++;
															for (var i = 0; i < ix2; i++) {
															if(prices2[i]==0) {
																prices2[i]="";
																	$(this).attr("disabled", true);
																} else {
															// alert(prices[i]);
																	$(this).val(prices2[i]);
																}
														}
													});
													$('input[name="supp_p3"]').each(function() {
														ix3++;
															for (var i = 0; i < ix3; i++) {
															if(prices3[i]==0){
																prices3[i]="";
																	$(this).attr("disabled", true);
																} else {
															// alert(prices[i]);
																	$(this).val(prices3[i]);
																}
														}
													});
												}
											}
										});
									});
								});
							</script>
						</tbody>
					</table>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" id="btn_save_abs" form="the-form" class="btn btn-primary">Save changes</button>
				<script type="text/javascript">
					$('#btn_save_abs').click(function() {
						if($("#the-form")[0].checkValidity()) {
							var role="add_quote";
							var hidden_id="<?php echo $_GET['eventid']; ?>";
							var child=$('#item_id').data('item');
							var item_id1=new Array();
							$('input[id="item_name"]').each(function(){
								var dd1 = $(this).data('id');
								item_id1.push(dd1);
							});
							var arr_suppfinal1= new Array
							var arr_price1 = new Array();
							var arr_price2 = new Array();
							var arr_price3 = new Array();
							var arr_supp1  = new Array();
							var arr_supp2  = new Array();
							var arr_supp3  = new Array();
							var supp_name1 = new Array();
							var supp_name2 = new Array();
							var supp_name3 = new Array();
							var cat_hidden=$('#dynamic_cat').text();
								$('input[name="supp_p1"]').each(function(){
									for (var i = 0; i < $(this).length; i++) {
										var xx1 = $(this).val();
										var dd1 = $(this).data('item').replace(/\"/g, '\\"');
											if($('#inline1').is(":checked")){

											} else {
												arr_supp1.push(dd1);
												arr_price1.push(xx1);
											}
									}
								});
								$('input[name="supp_p2"]').each(function(){
									for (var i = 0; i < $(this).length; i++) {
										var xx2 = $(this).val();
										var dd2 = $(this).data('item').replace(/\"/g, '\\"');
											if($('#inline2').is(":checked")){

											} else {
												arr_supp2.push(dd2);
												arr_price2.push(xx2);
											}
									}
								});
								$('input[name="supp_p3"]').each(function(){
									for (var i = 0; i < $(this).length; i++) {
										var xx3 = $(this).val();
										var dd3 = $(this).data('item').replace(/\"/g, '\\"');
											if ($('#inline3').is(":checked")) {

											} else {
												arr_supp3.push(dd3);
												arr_price3.push(xx3);
											}
									}
								});
								var zero=0;
								if($('#inline1').is(":checked")){
									supp_name1.push("no data available in json");
									arr_supp1.push("no data available in json");
									$('input[name="supp_p1"]').each(function(){
										arr_price1.push(zero);
									});
								} else {
									supp_name1.push($('#supp_1').val());
								}
								if($('#inline2').is(":checked")){
									supp_name2.push("no data available in json");
									arr_supp2.push("no data available in json");
									$('input[name="supp_p2"]').each(function(){
										arr_price2.push(zero);
									});
								} else {
									supp_name2.push($('#supp_2').val());			
								}
								if($('#inline3').is(":checked")){
									supp_name3.push("no data available in json");
									arr_supp3.push("no data available in json");
									$('input[name="supp_p3"]').each(function(){
										arr_price3.push(zero);
									});
								} else {
									supp_name3.push($('#supp_3').val());
								}
								var encode=JSON.stringify('[{"supp_name":' + '"' + supp_name1 + '"' + ',' + '"supp_data":' + '["' + arr_supp1 + '"]' + ',' + '"supp_price":' + '[' + arr_price1 + ']' + '},' + '{"supp_name":' + '"' + supp_name2 + '"' + ',' + '"supp_data":' + '["' + arr_supp2 + '"]' + ',' + '"supp_price":' + '[' + arr_price2 + ']' + '},' + '{"supp_name":' + '"' + supp_name3 + '"' + ',' + '"supp_data":' + '["' + arr_supp3 + '"]' + ',' + '"supp_price":' + '[' + arr_price3 + ']' + '}]');
								var obj2 = JSON.parse(encode);
								// console.log(obj2);
									$.ajax({
										url: 'php/so_webservice.php',
										type: 'POST',
										data: {
											tag: role,
											proj_id: hidden_id,
											stringified_json: obj2,
											category_abs: cat_hidden,
											supp1: arr_price1,
											supp2: arr_price2,
											supp3: arr_price3,
										},
										success: function(response) {
											$('#modal_quote').toggle('modal');
											swal({
												title: "Yay!",
												text: "Successfully added quotation.",
												type: "success",
												showConfirmButton: false,
												timer: 1100
											})
											.then((value) => {
												location.reload();
											});
										}
									});
				    	} else {
				        	$("#the-form")[0].reportValidity();
				    	}	
					});
				</script>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
		$(document).ready(function(){
			var user_type="<?php echo $_SESSION['s_user_type']; ?>";
			console.log(user_type);
			if(user_type==5){
				$('a[id=abs_exec]').css("pointer-events", "none");
				$('a[id=abs_exec]').css("cursor", "default");
			} else if (user_type==4) {
				// do nothing
			} else {
				$('a[id=abs_exec]').css("pointer-events", "none");
				$('a[id=abs_exec]').css("cursor", "default");
			}
		});
		// $('#modal_quote').on('shown.bs.modal', function (e) {
			// var role="check_availability";
			// $.ajax({
			// 	url: "php/so_webservice.php",
			// 	type: "post",
			// 	data: {
			// 		tag: role,
			// 		proj_id: id,
			// 	},
			// 	success: function(response) {
			// 		if(response=="not eligible"){
			// 			$('#btn_save_abs').attr("disabled", true);
			// 		} else if (response!="eligible") {
			// 			$('#btn_save_abs').removeAttr("disabled");
			// 		}
			// 	}
			// });
		// });
</script>
