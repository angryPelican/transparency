<?php
ob_start();
include('connectdb.php');
?>
<div class="card border-dark">
	<div class="card-header bg-dark">
		<ul class="nav nav-pills" id="myTab">
			<li class="nav-item"><a class="active nav-link link_approved text-white" href="#tab1default" data-toggle="tab">Approved</a></li>
			<li class="nav-item"><a class="nav-link link_complete text-white" href="#tab2default" data-role="select_complete" data-toggle="tab">Completed</a></li>
		</ul>
	</div>
	<div class="card-body table-responsive">
		<div class="tab-content">
			<div class="tab-pane fade active show" id="tab1default">
				<table id="approved" class="table table-striped display" style="table-layout: auto; width: 100%;">
					<thead>
						<tr>
							<th width="30%">Project Title</th>
							<th width="10%">PPMP-AD</th>
							<th width="10%">PR No.</th>
							<th width="20%">RFQ No.</th>
							<th width="15%">PO No.</th>
							<th width="15%">EB</th>
						</tr>
					</thead>
					<tbody id="tabledark2">
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab2default">
				<table id="completed" class="table table-striped display" style="table-layout: auto; width: 100%;">
					<thead>
						<tr>
							<th width="30%">Project Title</th>
							<th width="10%">PPMP-AD</th>
							<th width="10%">PR No.</th>
							<th width="20%">RFQ No.</th>
							<th width="10%">PO No.</th>
							<th width="10%">EB</th>
							<th width="10%">AC</th>
						</tr>
					</thead>
					<tbody id="tabledark">
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
var d = new Date();
var n = d.getFullYear();
	$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
		localStorage.setItem('activeTab', $(e.target).attr('href'));
	});
	if(!localStorage.getItem('activeTab')) {
		localStorage.setItem('activeTab', "#tab1default");
	}
	var activeTab = localStorage.getItem('activeTab');
	if(activeTab) {
		$('#myTab a[href="' + activeTab + '"]').tab('show');

		if(activeTab=="#tab1default") {
			var role="select_approved";
			$('#tabledark2').html("");
			$.ajax({
				url: 'php/so_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				},
				success : function(response) {
					console.log(response);
					var data_json=JSON.parse(response);
					if ($.fn.DataTable.isDataTable("#approved")) {
						$('#approved').DataTable().clear().destroy();
					}
					$('#approved').DataTable({
					"aaData": data_json,
					});
				}
			});
		} else if (activeTab=="#tab2default") {
			var role="select_complete";
			$('#tabledark').html("");
			$.ajax({
				url: 'php/so_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				},
				success : function(response) {
					var data_json=JSON.parse(response);
					if (data_json=="null"){
						if ($.fn.DataTable.isDataTable("#completed")) {
							$('#completed').DataTable().clear().destroy();
						}
					} else {
						console.log(response);
						if ($.fn.DataTable.isDataTable("#completed")) {
							$('#completed').DataTable().clear().destroy();
						}
						$('#completed').DataTable({
							"aaData": data_json,
						});
					}
				}
			});
		}

	}

	$('.link_complete').click(function() {
		var role=$(this).data('role');
		$('#tabledark').html("");
		$.ajax({
			url: 'php/so_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			},
			success : function(response) {
				var data_json=JSON.parse(response);
				if (data_json=="null"){
					if ($.fn.DataTable.isDataTable("#completed")) {
						$('#completed').DataTable().clear().destroy();
					}
				} else {
					console.log(response);
					if ($.fn.DataTable.isDataTable("#completed")) {
						$('#completed').DataTable().clear().destroy();
					}
					$('#completed').DataTable({
						"aaData": data_json,
					});
				}
			}
		});
	});

	$('.link_approved').click(function(){
		var role="select_approved";
		$('#tabledark2').html("");
		$.ajax({
			url: 'php/so_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			},
			success : function(response){
				console.log(response);
				var data_json=JSON.parse(response);
					if ($.fn.DataTable.isDataTable("#approved")) {
						$('#approved').DataTable().clear().destroy();
					}
					$('#approved').DataTable({
					"aaData": data_json,
					});
			}
		});
	});

});
</script>
