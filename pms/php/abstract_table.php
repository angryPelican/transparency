<?php
include 'connectdb.php';
$get_id = $_GET['eventid'];
$select_item = "SELECT * FROM items_table WHERE p_title_handler=?";
$select_item_pdo = $pdo->prepare($select_item);
$select_item_pdo->execute([$get_id]);
while ($row = $select_item_pdo->fetch(PDO::FETCH_ASSOC)) {
$item_id = $row['item_id'];
$item_description = $row['p_description'];
$item_quantity = $row['p_quantity_size'];
$item_unit = $row['p_unit'];
echo "
<form action='' method='post' id='formname'>
  <tr>
    <td><pre class='pre_style'>$item_description</pre></td>
    <td><pre class='pre_style'>$item_unit</pre></td>
    <td><pre class='pre_style'>$item_quantity</pre></td>
    <td><input type='number' class='form-control' form='formname' name='actual[]' style='width: 100%; margin: 0 auto;' ></input></td>
    <input type='hidden' form='formname' name='get_item[]' value='$item_id'></input>
  </tr>
</form>";
}
if (isset($_POST['saves'])) {
$specification = $_POST['specification'];
$start_checking = "SELECT * FROM abstract_table WHERE project_id=?";
$start_checking_pdo = $pdo->prepare($start_checking);
if ($start_checking_pdo->execute([$get_id])) {
$count_rows = $start_checking_pdo->rowCount();
/* IF 1 ROW IS EXISTING AND SPECIFICATION IS NOT CHECKED */
if ($count_rows == 0 && $specification == 0) {
$all_ok = true;
$number = 1;
$count = count($_POST['actual']);
for ($i = 0; $i < $count; $i++) {
$easy = "ex[$i]" . $_POST['actual'][$i];
if ($easy === "ex[$i]") {
echo "<script>
alert('Missing field');
</script>";
$easy = null;
$all_ok = false;
return;
}
}
if ($all_ok == true) {
$count = count($_POST['actual']);
for ($i = 0; $i < $count; $i++) {
$easy = $_POST['actual'][$i];
$id = $_POST['get_item'][$i];
/* UPDATE ACTUAL PRICES ON ITEMS TABLE */
$sql = "UPDATE items_table SET p_actual_price_1=? WHERE item_id =?";
$sql_pdo = $pdo->prepare($sql);
$sql_pdo->execute([$easy, $id]);
}
/* INSERT SUPPLIER CREDENTIALS */
$supplier = $_POST['supplier'];
$insert_supplier = "INSERT INTO abstract_table (project_id, name_of_supplier, supplier_number) VALUES (?,?,?)";
$insert_supplier_pdo = $pdo->prepare($insert_supplier);
$insert_supplier_pdo->execute([$get_id, $supplier, $number]);
/* SELECT ITEMS TABLE TO ADD AND TOTAL ACTUAL PRICE */
$total = 0;
$total1 = 0;
$select_items = "SELECT * FROM items_table WHERE p_title_handler=?";
$select_items_pdo = $pdo->prepare($select_items);
$select_items_pdo->execute([$get_id]);
while ($row_items = $select_items_pdo->fetch(PDO::FETCH_ASSOC)):
$actual = $row_items['p_actual_price_1'];
$p_quantity_size = $row_items['p_quantity_size'];
$total1 = $actual * $p_quantity_size;
$total = $total + $total1;
endwhile;
/* SELECT ABSTRACT TABLE TO GET THE SUPPLIER NUMBER */
$select_abstract = "SELECT * FROM abstract_table WHERE project_id=?";
$select_abstract_pdo = $pdo->prepare($select_abstract);
$select_abstract_pdo->execute([$get_id]);
while ($row_abstract = $select_abstract_pdo->fetch(PDO::FETCH_ASSOC)):
$supplier_number = $row_abstract['supplier_number'];
endwhile;
/* UPDATE TOTAL ACTUAL PRICE ON SUPPLIER TABLE */
$update_abstract = "UPDATE abstract_table SET total_actual_price=? WHERE supplier_number=? AND project_id=?";
$update_abstract_pdo = $pdo->prepare($update_abstract);
$update_abstract_pdo->execute([$total, $supplier_number, $get_id]);
echo "<script>
alert('successfully created first supplier that meets specification');
</script>";
echo "<meta http-equiv='refresh' content='0'>";
}
}
}
/* IF 1 ROW IS EXISTING AND SPECIFICATION IS CHECKED */
if ($count_rows == 0 && $specification == 1) {
$all_ok = true;
$number = 1;
$number1 = 1;
$count = count($_POST['actual']);
for ($i = 0; $i < $count; $i++) {
$easy = $_POST['actual'][$i];
$id = $_POST['get_item'][$i];
if ($easy === "") {
$easy = 0;
}
echo "<script>alert($easy)</script>";
$sql = "UPDATE items_table SET p_actual_price_1=? WHERE item_id =?";
$sql_pdo = $pdo->prepare($sql);
$sql_pdo->execute([$easy, $id]);
}
/* INSERT SUPPLIER CREDENTIALS */
$supplier = $_POST['supplier'];
$insert_supplier = "INSERT INTO abstract_table (project_id, name_of_supplier, supplier_number, not_meet_specification) VALUES (?,?,?,?)";
$insert_supplier_pdo = $pdo->prepare($insert_supplier);
$insert_supplier_pdo->execute([$get_id, $supplier, $number, $number1]);
/* SELECT ITEMS TABLE TO ADD AND TOTAL ACTUAL PRICE */
$total = 0;
$total1 = 0;
$select_items = "SELECT * FROM items_table WHERE p_title_handler=?";
$select_items_pdo = $pdo->prepare($select_items);
$select_items_pdo->execute([$get_id]);
while ($row_items = $select_items_pdo->fetch(PDO::FETCH_ASSOC)):
$actual = $row_items['p_actual_price_1'];
$p_quantity_size = $row_items['p_quantity_size'];
$total1 = $actual * $p_quantity_size;
$total = $total + $total1;
endwhile;
/* SELECT ABSTRACT TABLE TO GET THE SUPPLIER NUMBER */
$select_abstract = "SELECT * FROM abstract_table WHERE project_id=?";
$select_abstract_pdo = $pdo->prepare($select_abstract);
$select_abstract_pdo->execute([$get_id]);
while ($row_abstract = $select_abstract_pdo->fetch(PDO::FETCH_ASSOC)):
$supplier_number = $row_abstract['supplier_number'];
endwhile;
/* UPDATE TOTAL ACTUAL PRICE ON SUPPLIER TABLE */
$update_abstract = "UPDATE abstract_table SET total_actual_price=? WHERE supplier_number=? AND project_id=?";
$update_abstract_pdo = $pdo->prepare($update_abstract);
$update_abstract_pdo->execute([$total, $supplier_number, $get_id]);
echo "<script>
alert('successfully created first supplier that doesn't meet specification');
</script>";
echo "<meta http-equiv='refresh' content='0'>";
$all_ok = false;
return;
}
/* IF 2 ROW IS EXISTING AND SPECIFICATION IS NOT CHECKED */
if ($count_rows == 1 && $specification != 1) {
$number = 2;
$all_ok = true;
$count = count($_POST['actual']);
for ($i = 0; $i < $count; $i++) {
$easy = "ex[$i]" . $_POST['actual'][$i];
if ($easy === "ex[$i]") {
echo "<script>
alert('Missing field');
</script>";
$easy = null;
$all_ok = false;
return;
}
}
if ($all_ok == true) {
$count = count($_POST['actual']);
for ($i = 0; $i < $count; $i++) {
$easy = $_POST['actual'][$i];
$id = $_POST['get_item'][$i];
/* UPDATE ACTUAL PRICES ON ITEMS TABLE */
$sql = "UPDATE items_table SET p_actual_price_2=? WHERE item_id = ?";
$key6 = $pdo->prepare($sql);
$key6->execute([$easy, $id]);
}
/* INSERT SUPPLIER CREDENTIALS */
$supplier = $_POST['supplier'];
$insert_supplier = "INSERT INTO abstract_table (project_id, name_of_supplier, supplier_number) VALUES (?,?,?)";
$key7 = $pdo->prepare($insert_supplier);
$key7->execute([$get_id, $supplier, $number]);
/* SELECT ITEMS TABLE TO ADD AND TOTAL ACTUAL PRICE */
$total = 0;
$total1 = 0;
$select_items = "SELECT * FROM items_table WHERE p_title_handler=?";
$key8 = $pdo->prepare($select_items);
$key8->execute([$get_id]);
while ($row_items = $key8->fetch(PDO::FETCH_ASSOC)):
$actual = $row_items['p_actual_price_2'];
$p_quantity_size = $row_items['p_quantity_size'];
$total1 = $actual * $p_quantity_size;
$total = $total + $total1;
endwhile;
/* SELECT ABSTRACT TABLE TO GET THE SUPPLIER NUMBER */
$select_abstract = "SELECT * FROM abstract_table WHERE project_id=?";
$key9 = $pdo->prepare($select_abstract);
$key9->execute([$get_id]);
while ($row_abstract = $key9->fetch(PDO::FETCH_ASSOC)):
$supplier_number = $row_abstract['supplier_number'];
endwhile;
/* UPDATE TOTAL ACTUAL PRICE ON SUPPLIER TABLE */
$update_abstract = "UPDATE abstract_table SET total_actual_price=? WHERE supplier_number=? AND project_id=?";
$key10 = $pdo->prepare($update_abstract);
$key10->execute([$total, $supplier_number, $get_id]);
echo "<script>
alert('successfully created second supplier that meets specification');
</script>";
echo "<meta http-equiv='refresh' content='0'>";
}
}
/* IF 2 ROW IS EXISTING AND SPECIFICATION IS CHECKED */
else if ($count_rows == 1 && $specification == 1) {
$all_ok = true;
$number = 2;
$number1 = 1;
$count = count($_POST['actual']);
for ($i = 0; $i < $count; $i++) {
$easy = $_POST['actual'][$i];
$id = $_POST['get_item'][$i];
if ($easy === "") {
$easy = 0;
}
$sql = "UPDATE items_table SET p_actual_price_2=? WHERE item_id =?";
$sql_pdo = $pdo->prepare($sql);
$sql_pdo->execute([$easy, $id]);
}
/* INSERT SUPPLIER CREDENTIALS */
$supplier = $_POST['supplier'];
$insert_supplier = "INSERT INTO abstract_table (project_id, name_of_supplier, supplier_number, not_meet_specification) VALUES (?,?,?,?)";
$insert_supplier_pdo = $pdo->prepare($insert_supplier);
$insert_supplier_pdo->execute([$get_id, $supplier, $number, $number1]);
/* SELECT ITEMS TABLE TO ADD AND TOTAL ACTUAL PRICE */
$total = 0;
$total1 = 0;
$select_items = "SELECT * FROM items_table WHERE p_title_handler=?";
$select_items_pdo = $pdo->prepare($select_items);
$select_items_pdo->execute([$get_id]);
while ($row_items = $select_items_pdo->fetch(PDO::FETCH_ASSOC)):
$actual = $row_items['p_actual_price_2'];
$p_quantity_size = $row_items['p_quantity_size'];
$total1 = $actual * $p_quantity_size;
$total = $total + $total1;
endwhile;
/* SELECT ABSTRACT TABLE TO GET THE SUPPLIER NUMBER */
$select_abstract = "SELECT * FROM abstract_table WHERE project_id=?";
$select_abstract_pdo = $pdo->prepare($select_abstract);
$select_abstract_pdo->execute([$get_id]);
while ($row_abstract = $select_abstract_pdo->fetch(PDO::FETCH_ASSOC)):
$supplier_number = $row_abstract['supplier_number'];
endwhile;
/* UPDATE TOTAL ACTUAL PRICE ON SUPPLIER TABLE */
$update_abstract = "UPDATE abstract_table SET total_actual_price=? WHERE supplier_number=? AND project_id=?";
$update_abstract_pdo = $pdo->prepare($update_abstract);
$update_abstract_pdo->execute([$total, $supplier_number, $get_id]);
echo "<script>
alert('successfully created first supplier that doesn't meet specification');
</script>";
echo "<meta http-equiv='refresh' content='0'>";
$all_ok = false;
return;
}
/* IF ROW 3 IS EXISTING AND SPECIFICATION IS NOT CHECKED */
else if ($count_rows == 2 && $specification != 1) {
$number = 3;
$all_ok = true;
$count = count($_POST['actual']);
for ($i = 0; $i < $count; $i++) {
$easy = "ex[$i]" . $_POST['actual'][$i];
if ($easy === "ex[$i]") {
echo "<script>
alert('Missing field');
</script>";
$easy = null;
$all_ok = false;
return;
}
}
if ($all_ok == true) {
$count = count($_POST['actual']);
for ($i = 0; $i < $count; $i++) {
$easy = $_POST['actual'][$i];
$id = $_POST['get_item'][$i];
/* UPDATE ACTUAL PRICES ON ITEMS TABLE */
$sql = "UPDATE items_table SET p_actual_price_3=? WHERE item_id=?";
$handler5 = $pdo->prepare($sql);
$handler5->execute([$easy, $id]);
}
/* INSERT SUPPLIER CREDENTIALS ON ROW 3 */
$supplier = $_POST['supplier'];
$insert_supplier = "INSERT INTO abstract_table (project_id, name_of_supplier, supplier_number) VALUES (?,?,?)";
$insert_supplier_pdo = $pdo->prepare($insert_supplier);
$insert_supplier_pdo->execute([$get_id, $supplier, $number]);
/* SELECT ITEMS TABLE TO ADD AND TOTAL ACTUAL PRICE ON ROW 3 */
$total = 0;
$total1 = 0;
$select_items = "SELECT * FROM items_table WHERE p_title_handler=?";
$select_items_pdo = $pdo->prepare($select_items);
$select_items_pdo->execute([$get_id]);
while ($row_items = $select_items_pdo->fetch(PDO::FETCH_ASSOC)):
$actual = $row_items['p_actual_price_3'];
$p_quantity_size = $row_items['p_quantity_size'];
$total1 = $actual * $p_quantity_size;
$total = $total + $total1;
endwhile;
/* SELECT ABSTRACT TABLE TO GET THE SUPPLIER NUMBER ON ROW 3 */
$select_abstract = "SELECT * FROM abstract_table WHERE project_id=?";
$result_abstract = $pdo->prepare($select_abstract);
$result_abstract->execute([$get_id]);
while ($row_abstract = $result_abstract->fetch(PDO::FETCH_ASSOC)):
$supplier_number = $row_abstract['supplier_number'];
endwhile;
/* UPDATE TOTAL ACTUAL PRICE ON SUPPLIER TABLE ON ROW 3 */
$update_abstract = "UPDATE abstract_table SET total_actual_price=? WHERE supplier_number=? AND project_id=?";
$update_abstract_result = $pdo->prepare($update_abstract);
$update_abstract_result->execute([$total, $supplier_number, $get_id]);
echo "<script>
alert('successfully created third supplier that meets specification');
</script>";
echo "<meta http-equiv='refresh' content='0'>";
}
}
/* IF 3 ROW IS EXISTING AND SPECIFICATION IS CHECKED */
else if ($count_rows == 2 && $specification == 1) {
$all_ok = true;
$number = 3;
$number1 = 1;
$count = count($_POST['actual']);
for ($i = 0; $i < $count; $i++) {
$easy = $_POST['actual'][$i];
$id = $_POST['get_item'][$i];
if ($easy === "") {
$easy = 0;
}
$sql = "UPDATE items_table SET p_actual_price_3=? WHERE item_id =?";
$sql_pdo = $pdo->prepare($sql);
$sql_pdo->execute([$easy, $id]);
}
/* INSERT SUPPLIER CREDENTIALS */
$supplier = $_POST['supplier'];
$insert_supplier = "INSERT INTO abstract_table (project_id, name_of_supplier, supplier_number, not_meet_specification) VALUES (?,?,?,?)";
$insert_supplier_pdo = $pdo->prepare($insert_supplier);
$insert_supplier_pdo->execute([$get_id, $supplier, $number, $number1]);
/* SELECT ITEMS TABLE TO ADD AND TOTAL ACTUAL PRICE */
$total = 0;
$total1 = 0;
$select_items = "SELECT * FROM items_table WHERE p_title_handler=?";
$select_items_pdo = $pdo->prepare($select_items);
$select_items_pdo->execute([$get_id]);
while ($row_items = $select_items_pdo->fetch(PDO::FETCH_ASSOC)):
$actual = $row_items['p_actual_price_3'];
$p_quantity_size = $row_items['p_quantity_size'];
$total1 = $actual * $p_quantity_size;
$total = $total + $total1;
endwhile;
/* SELECT ABSTRACT TABLE TO GET THE SUPPLIER NUMBER */
$select_abstract = "SELECT * FROM abstract_table WHERE project_id=?";
$select_abstract_pdo = $pdo->prepare($select_abstract);
$select_abstract_pdo->execute([$get_id]);
while ($row_abstract = $select_abstract_pdo->fetch(PDO::FETCH_ASSOC)):
$supplier_number = $row_abstract['supplier_number'];
endwhile;
/* UPDATE TOTAL ACTUAL PRICE ON SUPPLIER TABLE */
$update_abstract = "UPDATE abstract_table SET total_actual_price=? WHERE supplier_number=? AND project_id=?";
$update_abstract_pdo = $pdo->prepare($update_abstract);
$update_abstract_pdo->execute([$total, $supplier_number, $get_id]);
echo "<script>
alert('successfully created first supplier that doesn't meet specification');
</script>";
echo "<meta http-equiv='refresh' content='0'>";
$all_ok = false;
return;
}
if ($count_rows == 3) {
echo "<script>alert('MAXIMUM NO. OF SUPPLIER IS 3');</script>";
return;
}
}