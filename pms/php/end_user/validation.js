//ADDING ITEM FUNCTION
$('#addItem').on('show.bs.modal', function (e) {
	$('#training_days').hide();
})
$('#categories').on('change', function () {
	var category=$(this).val();
	if(category=="Training") {
		$('#training_days').show();
	} else {
		$('#training_days').hide();
	}
	});
$('#addItem').on('show.bs.modal', function (e) {
	$('#input_desc').hide();
	$('#input_text_unit').hide();
});
$('#categories').on('change', function(){
	var text=$(this).val();
	var role="cat_get";
	$.ajax({
		url: 'php/end_user/mode_of_proc_webservice.php',
		type: 'POST',
		data: {
			tag: role,
			cat: text
		},
		success : function(response) {
			var data=JSON.parse(response);
			if(data.length<=0) {
				$('#input_desc').hide();
				$('#text_input_desc').show();
				$('#items').html("");
			} else {
				$('#text_input_desc').hide();
				$('#input_desc').show();
				$('input[list=items]').on('keyup', function() {
				var role_unit="check_unit";
				var role_price="check_price";
				var item=$(this).val();
				var categ_name=$('#categories').val();
				$.ajax({
					url: "php/end_user/mode_of_proc_webservice.php",
					type: "post",
					data: {
						tag: role_unit,
						item_name: item,
						categ_name: categ_name,
					},
					success: function(response) {
						if(response=="no match") {
							$('#input_unit').show();
							$('#input_text_unit').hide();
						} else {
							$('#input_unit').hide();
							$('#input_text_unit').show();
							$('#input_text_unit').val(response);
						}
					}
				});
				$.ajax({
					url: "php/end_user/mode_of_proc_webservice.php",
					type: "post",
					data: {
						tag: role_price,
						item_name: item,
						categ_name: categ_name,
					},
					success: function(response) {
						$('#input_est').val(response);
					}
				})
			});
				console.log(data);
				for (var key in data) {
					var items = data[key].items;
					var newTemp = items.replace(/"/g, '&quot;');
					console.log(items);
					document.getElementById("items").innerHTML += '<option value="' + newTemp + '">' +
																newTemp + '</option>';
				}
			}
		}
	});	
});