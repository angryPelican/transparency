<table id="myTable" class="table table-striped display table-sm">
	<thead>
		<tr>
			<th width="350">Item Description</th>
			<th width="30">Quantity</th>
			<th width="30">Unit</th>
			<th width="140">Estimated Budget</th>
			<th width="30">Actions</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$selected_eventid_edit = $_GET['eventid'];
		$query1 = "SELECT * FROM items_table WHERE p_title_handler=? ORDER BY item_id DESC";
		$stmt=$pdo->prepare($query1);
		$stmt->execute([$selected_eventid_edit]);
		$sum=0;
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))	{
				$p_id = $row['item_id'];
				$i_desc = $row['p_description'];
				$i_quantity = $row['p_quantity_size'];
				$i_unit = $row['p_unit'];
				$i_estimate = $row['p_estimated_budget'];
				$formatted_estimate=number_format($i_estimate, 2);
				$sum = $sum + $row['p_estimated_budget'];
				$code=$row['code'];
				$classification=$row['classification'];
				echo '<tr id="'.$p_id.'">
						<form action="" method="post">
							<input type="hidden" value="'.$classification.'" id="category">
							<input type="hidden" value="'.$code.'" id="code">
			 				<td data-target="description">'.$i_desc. '</td>
							<td data-target="quantity" align="center">'.$i_quantity. '</td>
							<td data-target="unit" align="center">'.$i_unit. '</td>
							<td data-target="estimate" align="right">'.$formatted_estimate. '</td>
							<td align="center">
							<a href="#" data-estimate="'.$i_estimate.'" data-id="'.$p_id.'" data-role="update" class="btn btn-primary" data-toggle="modal" data-target="#editItem"><i class="fas fa-edit"></i></a>
							<a href="#" data-id="'.$p_id.'" data-role="delete" class="btn btn-danger" ><i class="far fa-trash-alt"></i></a>
							</td>
						</form>
					</tr>';
			}
	?>
	</tbody>
	<tfoot>
		<?php
			get_total_amount();
		?>
	</tfoot>
</table>
<!--EDIT MODAL -->
<div id="editItem" class="modal fade" role="dialog" data-backdrop="static"  data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white"><i class="fas fa-edit"></i> PMS | Edit Item </h5>
				<button type="button" class="close" style="color: white;" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label><b>Classification</b></label>
						<select class='form-control form-control-sm' id='categories1'>
							<option></option>
							<?php
								get_categories();
							?>
						</select>
				</div>
						<div class="form-group">
							<label><b>General Description:</b></label>
							<textarea rows="4" cols="50" class="form-control form-control-sm" name="getdesc" id="getsi1"></textarea>
						</div>
						<div class="form-group row">
							<div class="col-sm-4">
								<div class="form-group">
									<label><b>Quantity/Size:</b></label>
									<input type="number" class="form-control form-control-sm" id="quanti1">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label><b>Unit:</b></label><br>
									<select name="getunit" class="form-control form-control-sm" style="width:100%;" id = "units1">
										<option value = "pc">pc</option>
										<option value = "unit">unit</option>
										<option value = "set">set</option>
										<option value = "box">box</option>
										<option value = "pax">pax</option>
										<option value = "bottle">bottle</option>
										<option value = "can">can</option>
										<option value = "bottle">gallon</option>
									</select>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label><b>Unit Cost</b></label><br>
									<input type="number" name = "getbuds" class="form-control form-control-sm" id = "est1">
								</div>
							</div>	
						</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="hidden_id">
				<a href="#" id="save" class="btn btn-default" style="background: #0652DD; color: white;">Save</a>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	var role="check_if_submittted";
	var id="<?php echo $_GET['eventid']; ?>";
	$.ajax({
		url: "php/end_user/mode_of_proc_webservice.php",
		type: "post",
		data: {
			tag: role,
			eventid: id
		},
		success: function(response) {
			console.log(response);
			if(response=="submitted") {
				$('a[data-role=update]').addClass("btn disabled");
				$('a[data-role=delete]').addClass("btn disabled");
			} else if(response=="not submitted") {
				$('a[data-role=update]').removeClass("btn disabled");
				$('a[data-role=delete]').removeClass("btn disabled");
			}
		}
	});
});
$(document).on('click', 'a[data-role=update]', function(){
	var id = $(this).data('id');
	var description = $('#'+id).children('td[data-target=description]').text();
	var quantity = $('#'+id).children('td[data-target=quantity]').text();
	var unit = $('#'+id).children('td[data-target=unit]').text();
	var category=$('#'+id).children('#category').val();
	var code=$('#code').val();
	var estimate = $(this).data('estimate');
	var final_form=estimate / quantity;
	$('#getsi1').val(description);
	$('#quanti1').val(quantity);
	$('#units1').val(unit);
	$('#est1').val(final_form);
	$('#hidden_id').val(id);
	$('#categories1').val(category)
	$('#editItem').modal('toggle');
});
$('#save').click(function(){
	var id = $('#hidden_id').val();
	var desc = $('#getsi1').val();
	var quan = $('#quanti1').val();
	var unit = $('#units1').val();
	var est = $('#est1').val();
	var cat = $('#categories1').val();
	var nf = Intl.NumberFormat();
	var role="update_item";
	var project_id='<?php echo $selected_eventid_edit; ?>';
	console.log(project_id);
	$.ajax({
		url: 'php/end_user/mode_of_proc_webservice.php',
		type: 'POST',
		data: {
			tag: role,
			cat_tosend: cat,
			desc_tosend: desc,
			quan_tosend: quan,
			unit_tosend: unit,
			est_tosend: est,
			id_tosend: id,
			proj_id_tosend: project_id
		},
		success : function(response){
			$('#editItem').modal('toggle');
			swal({
				title: "Success!",
				text: "Item was updated successfully.",
				type: "success",
				timer: 1100,
				showConfirmButton: false
			})
			.then((value) => {
				location.reload();
			});
		}
	});
});
$(document).on('click', 'a[data-role=delete]', function(){
	var item_id=$(this).data('id');
	var project_id='<?php echo $selected_eventid_edit; ?>';
	var tr_todelete = $(this).closest('tr');
	var role="delete_item";
	swal({
		title: "Are you sure?",
		text: "Once deleted, you will not be able to recover this item",
		type: "warning",
		showCancelButton: true,
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, delete it!'
	})
	.then((result) => {
		if (result.value) {
			$.ajax({
				url: 'php/end_user/mode_of_proc_webservice.php',
				type: 'POST',
				data: {
					tag: role,
					id_todelete: item_id,
					id_toupdate: project_id
				},
				success : function(response) {
					swal("Your item has been deleted!", {
					type: "success",
					timer: 1100,
					showConfirmButton: false
					})
					.then((result) => {
						tr_todelete.find('td').fadeOut(300,function(){ 
							tr_todelete.remove();                    
						});
					});
				}
			});
		} else {
			swal({
				text: "Your item is safe!", 
				type: "success",
				timer: 1100,
				showConfirmButton: false
			})
			.then((result) => {

			});
		}
	});
});
</script>