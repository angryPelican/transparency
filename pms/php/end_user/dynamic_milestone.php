<?php
include('php/connectdb.php');
$titlehandle1 = $_GET['eventid'];
echo'<input type="hidden" class="hidden_id" value="'.$titlehandle1.'">';
// SHOPPING MILESTONE ARRAY
$shop=array('Shopping', 'Shopping', 'Shopping', 'Shopping');
$small=array('Purchase Request', 'Request for Quotation', 'Abstract of Quotation', 'Purchase Order');
// SHOPPING MILESTONE END
// QUERY FOR SHOWING MILESTONES
$query = "SELECT * FROM milestone_table WHERE milestone_handler=?";
$stmt=$pdo->prepare($query);
$stmt->execute([$titlehandle1]);
$datenow = date('Y-m-d');
	while($row=$stmt->fetch(PDO::FETCH_ASSOC))	{
		$mile1 = $row['approval_of_request'];
		$mile2 = $row['philgeps_posting'];
		$mile3 = $row['pre_bid'];
		$mile4 = $row['opening_of_bid'];
		$mile5 = $row['post_qualification'];
		if($mile1=='0000-00-00') {
			$mile1="";
		}
		if($mile2=='0000-00-00') {
			$mile2="";
		}
		if($mile3=='0000-00-00') {
			$mile3="";
		}
		if($mile4=='0000-00-00') {
			$mile4="";
		}
		if($mile5=='0000-00-00') {
			$mile5="";
		}
	}
$sel_for_source="SELECT * FROM project_table WHERE project_id=?";
$selforsource_exec=$pdo->prepare($sel_for_source);
$selforsource_exec->execute([$titlehandle1]);
$row_forsource=$selforsource_exec->fetch(PDO::FETCH_ASSOC);
$sor=$row_forsource['source_of_fund'];
if (empty($sor)) {
	$sor="";
}
?>
<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label><b>Source of Fund</b></label>
			<input type="text" id="fund" class="form-control form-control-sm" value="<?php echo $sor; ?>" />
		</div>
		<div class="form-group">
			<label><b>Mode of Procurement </b></label>
			<select class="form-control form-control-sm" name="mode_of_procurement_selection" id="mode_of_proc" form="sss_mode">
				<option value="Small Value Procurement">Small Value Procurement</option>
				<option value="Shopping">Shopping</option>
			</select>
		</div>
	</div>
	<div class="col-sm-8">
		<table id = "table_svp" class="table table-striped table-bordered display">
			<thead>
				<tr>
					<th class="th_milestone">Milestone</th>
					<th class="th_date form-inline">
						<div class="col-sm-3" style="margin: 0; padding: 0;">
							Target Date
						</div>
						<div class="col-sm-5" >
							<div class="form-check">
								<input type="checkbox" class="form-check-input" id="check1">
								<label class="form-check-label" for="check1">Advanced</label>
							</div>
						</div>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><label for="date"><b>Implementation Date</b></label></td>
					<td><input type="date" id="date1" class="form-control form-control-sm polis is-valid" data-mile="approval_of_request" data-role="save_mile_date" name="time11" value="<?php echo $mile1; ?>" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
					<div class="invalid-feedback">
						Your date is invalid. Please choose Weekdays [ex: Monday,Tuesday]
					</div>
					<div class="valid-feedback">
						Note: You can only choose Weekdays to avoid conflict of dates.
					</div>
					</td>
				</tr>
				<tr>
					<td><label for="date"><b>Purchase Order</b></label></td>
					<td><input date-custom="date" type="date" id="date2" class="form-control form-control-sm poli" data-mile="philgeps_posting" name="time22" value="<?php echo $mile2; ?>" disabled="true">
					<div class="invalid-feedback">
						Your date is invalid. Please choose Weekdays [ex: Monday,Tuesday]
					</div>
					<div class="valid-feedback">
						Note: You can only choose Weekdays to avoid conflict of dates.
					</div>
					</td>
				</tr>
				<tr>
					<td><label for="date"><b>Abstract of Quotation</b></label></td>
					<td><input date-custom="date" type="date" data-mile="pre_bid" id="date3"  class="form-control form-control-sm poli" value="<?php echo $mile3; ?>" disabled="true">
					<div class="invalid-feedback">
						Your date is invalid. Please choose Weekdays [ex: Monday,Tuesday]
					</div>
					<div class="valid-feedback">
						Note: You can only choose Weekdays to avoid conflict of dates.
					</div>
					</td>
				</tr>
				<tr>
					<td><label for="date"><b>Request for Quotation</b></label></td>
					<td><input date-custom="date" type="date" id="date4" data-mile="opening_of_bid" class="form-control form-control-sm poli" value="<?php echo $mile4; ?>" disabled="true">
					<div class="invalid-feedback">
						Your date is invalid. Please choose Weekdays [ex: Monday,Tuesday]
					</div>
					<div class="valid-feedback">
						Note: You can only choose Weekdays to avoid conflict of dates.
					</div>
					</td>
				</tr>
				<tr>
					<td><label for="date"><b>Purchase Request</b></label></td>
					<td><input date-custom="date" type="date" id="date5" data-mile="post_qualification" class="form-control form-control-sm poli" value="<?php echo $mile5; ?>" disabled="true">
					<div class="invalid-feedback">
						Your date is invalid. Please choose Weekdays [ex: Monday,Tuesday]
					</div>
					<div class="valid-feedback">
						Note: You can only choose Weekdays to avoid conflict of dates.
					</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="container">
	<form action="" method="post" id="sss_mode">
	</form>
	<div class="text-right">
		<div class="btn-group">
			<button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			Action
			</button>
			<div class="dropdown-menu">
				<?php
					check_stats();
				?>
				<a class="dropdown-item" href="download.php?eventid=<?php echo $titlehandle1; ?>">Download</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function () {

	var role="check_if_submittted";
	var id="<?php echo $_GET['eventid']; ?>";
	$.ajax({
		url: "php/end_user/mode_of_proc_webservice.php",
		type: "post",
		data: {
			tag: role,
			eventid: id
		},
		success: function(response) {
			console.log(response);
			if(response=="submitted") {
				$('#date1').attr("disabled", true);
				$('#fund').attr("disabled", true);
				$('#mode_of_proc').attr("disabled", true);
				$('#check1').attr("disabled", true);
			} else if(response=="not submitted") {
				$('#date1').removeAttr("disabled");
				$('#fund').removeAttr("disabled");
				$('#mode_of_proc').removeAttr("disabled");
				$('#check1').removeAttr("disabled");
			}
		}
	});

	$('#check1').click(function(){
		if($(this).is(':checked')) {
			$(this).attr("checked", "checked");
			$('#savecustomdate').show();
			$('#date1').attr("date-custom", "date");
			$('#date2').removeAttr("disabled");
			$('#date3').removeAttr("disabled");
			$('#date4').removeAttr("disabled");
			$('#date5').removeAttr("disabled");

			$('input[date-custom=date]').on('change', function() {
				var id="<?php echo $_GET['eventid']; ?>";
				var role="set_custom_date";
				var column=$(this).data('mile');
				var date = $(this).val().split("-");
				day = date[1];
				month = date[0];
				year = date[2];
				var format_date = month + "/" + day + "/" + year;
				var coco = new Date(format_date);
				var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
				var log=(days[coco.getDay()]);
				if(log === "Saturday")	{
					$(this).addClass("is-invalid");
					$(this).removeClass("is-valid");
				}
				if(log === "Sunday") {
					$(this).addClass("is-invalid");
					$(this).removeClass("is-valid");
				} else if (log === "Monday" || log === "Tuesday" || log === "Wednesday" || log === "Thursday" || log === "Friday") {
					$(this).removeClass("is-invalid");
					console.log("Column: " + column);
					console.log("Date: " + format_date);
					$.ajax({
						url: 'php/end_user/mode_of_proc_webservice.php',
						type: 'POST',
						data: {
							tag: role,
							date_identifier: column,
							date_handler: format_date,
							proj_id: id
						},
						success: function(response){
							location.reload();
						}
					});
				}
			});

		} else {
			$(this).removeAttr("checked");
			$('#savecustomdate').hide();
			$('#date11').removeAttr("date-custom");
			$('#date2').attr("disabled", true);
			$('#date3').attr("disabled", true);
			$('#date4').attr("disabled", true);
			$('#date5').attr("disabled", true);
		}
	});

		var date_input = document.getElementById('date1');
		$("#date1").on('change', function(){
			var hidden_id = $('.hidden_id').val();
			var date_exec = $(date_input).data('role');
			var date = $(this).val().split("-");
			//console.log(date, $(this).val())
			day = date[1];
			month = date[0];
			year = date[2];
			var format_date = month + "/" + day + "/" + year;
			var coco = new Date(format_date);
			var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
			var log=(days[coco.getDay()]);
			if(log === "Saturday")	{
				$(this).addClass("is-invalid");
				$(this).removeClass("is-valid");
				$('#savecustomdate').attr("disabled", true);
			}
			if(log === "Sunday") {
				$(this).addClass("is-invalid");
				$(this).removeClass("is-valid");
				$('#savecustomdate').attr("disabled", true);
			} else if(log === "Monday" || log === "Tuesday" || log === "Wednesday" || log === "Thursday" || log === "Friday") {
				$(this).removeClass("is-invalid");
				$('#savecustomdate').removeAttr("disabled", true);
				if($('#check1').is(":checked")){
					// do nothing
				} else {
					$.ajax({
						url: 'php/end_user/mode_of_proc_webservice.php',
						type: 'POST',
						data: {
							tag: date_exec,
							time11: format_date,
							hidden_id: hidden_id,
						},
						success : function(response){
							// console.log(response);
							var data = JSON.parse(response);
							var date1=data['mile1'];
							var date2=data['mile2'];
							var date3=data['mile3'];
							var date4=data['mile4'];
							var date5=data['mile5'];
							$('#date1').val(date1);
							$('#date2').val(date2);
							$('#date3').val(date3);
							$('#date4').val(date4);
							$('#date5').val(date5);
							location.reload();
						}
					});
				}
			}
		});//date onchange

	$('#fund').focusout(function(){
		var hidden_id="<?php echo $_GET['eventid']; ?>";
		var fund_text=$('#fund').val();
		var tag_text="fund_add";

		$.ajax({
			url: 'php/end_user/mode_of_proc_webservice.php',
			type: 'POST',
			data: {
				tag: tag_text,
				eventid: hidden_id,
				fund_tosend: fund_text
			},
			success : function(response){
				console.log(response);
			}
		});
	});

var mode_procurement=document.getElementById("mode_of_proc");
mode_procurement.onchange = function(){
var mode_of_proc = $('#mode_of_proc').val();
var hidden_id = $('.hidden_id').val();
var button_exec = "update_mode";
$.ajax({
url: 'php/end_user/mode_of_proc_webservice.php',
type: 'POST',
data: {
tag: button_exec,
eventid: hidden_id,
mode_of_procurement_selection: mode_of_proc
},
success : function(response){

		}
	});
}

$(document).on('click', 'button[data-role=set_submit_date]', function(){
	var date= "<?php echo date('Y-m-d'); ?>";
	var hidden_id = $('.hidden_id').val();
	var btn_exec = $('.btn_submit').data('role');

$.ajax({
url: 'php/end_user/mode_of_proc_webservice.php',
type: 'POST',
data: {
tag: btn_exec,
eventid: hidden_id,
date_now: date
},
success : function(response){
//console.log(response);
	swal({
		title: "Congrats!",
		text: "Project Submitted!",
		type: "success",
		timer: 1100,
		showConfirmButton: false
	})
	.then((value) => {
		$('.btn_submit').addClass("sr-only");
		location.reload();
	})
}
});

});

}); // document ready closing
</script>