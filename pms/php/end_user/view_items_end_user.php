<?php
	include('connectdb.php');
	$get_id = $_GET['eventid'];
	//FIRST QUERY
		$first_query="SELECT * FROM items_table WHERE p_title_handler=?";
		$key1=$pdo->prepare($first_query);
		$key1->execute([$get_id]);
		$row_count=$key1->fetchColumn();
	//SECOND QUERY
		$second_query = "SELECT * FROM items_table WHERE p_title_handler=?";
		$key2=$pdo->prepare($second_query);
		$key2->execute([$get_id]);
		$count=$key2->rowCount();
		$i = 0;
		$i1 = 0;
		$i2 = 0;
	//loop each items
		while($row1=$key2->fetch(PDO::FETCH_ASSOC)) {
			$code = $row1['code'];
			$sx = $row1['p_title_name'];
			$s1 = $row1['p_description'];
			$s2 = $row1['p_quantity_size'];
			$s3 = $row1['p_unit'];
			$s4 = $row1['p_estimated_budget'];
			$s5 = $row1['classification'];
			$formattedNum = number_format($s4, 2);

?>
	<!-- table row -->
	<tr>

<?php
	//Code Column Rowspan
		if($i1 == 0) {
			echo'<td rowspan='.$count.'>'.$code.'</td>';
			$i1=1;
		}
?>

		<td><?php echo $s1; ?></td>
		<td><?php echo $s2 . "-" . $s3 ?></td>
		<td align='right'><?php echo $formattedNum; ?></td>

<?php	
	//MODE OF PROCUREMENT QUERY
		$querymile = "SELECT * FROM project_table JOIN milestone_table ON project_table.project_id = milestone_table.milestone_handler WHERE project_id=?";
		$key3=$pdo->prepare($querymile);
		$key3->execute([$get_id]);
			while($row_get=$key3->fetch(PDO::FETCH_ASSOC)) {
				$mode = $row_get['mode_of_procurement'];
				$fund = $row_get['source_of_fund'];
				$milestone_check1 = $row_get['approval_of_request'];
				$milestone_check2 = $row_get['philgeps_posting'];
				$milestone_check3 = $row_get['pre_bid'];
				$milestone_check4 = $row_get['opening_of_bid'];
				$milestone_check5 = $row_get['post_qualification'];
	//Code Column Rowspan
	if($i == 0) {
?>

		<td rowspan='<?php echo $count; ?>'><? echo $mode; ?></td>
		<td rowspan='$count'>
			<ol>

<?php
		if($milestone_check1 != '0000-00-00'){
			$formattedDate5=date('F d, Y', strtotime($milestone_check1));
				echo"<li style='color: black;'>PR - $formattedDate5</li>";
			}
			else if($milestone_check1 == '0000-00-00'){
			}
			if($milestone_check2 != '0000-00-00'){
			$formattedDate4=date('F d, Y', strtotime($milestone_check2));
				echo"<li style='color: black;'>RFQ - $formattedDate4</li>";
			}
			else if($milestone_check2 == '0000-00-00'){
			}
			if($milestone_check3 != '0000-00-00'){
			$formattedDate3=date('F d, Y', strtotime($milestone_check3));
				echo"<li style='color: black;'>AOQ - $formattedDate3</li>";
			}
			else if($milestone_check3 == '0000-00-00'){
			}
			if($milestone_check4 != '0000-00-00'){
			$formattedDate2=date('F d, Y', strtotime($milestone_check4));
				echo"<li style='color: black;'>PO - $formattedDate2</li>";
			}
			else if($milestone_check4 == '0000-00-00'){
			}
			if($milestone_check5 != '0000-00-00'){
			$formattedDate1=date('F d, Y', strtotime($milestone_check5));
				echo"<li style='color: black;'>ID - $formattedDate1</li>";
			}
			else if($milestone_check5 == '0000-00-00'){
			}
?>

			</ol>
		</td>
		<td rowspan='<?php echo $count; ?>'><?php echo $fund; ?></td>
		
<?php
			$i=1;
	}
?>
	</tr>
<?php 
			}
		}
?>