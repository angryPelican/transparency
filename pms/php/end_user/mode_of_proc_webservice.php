<?php
include('connectdb.php');
$tag=$_POST['tag'];
	switch($tag) {
		case 'submit_ppmp':
			$get_id_hidden=$_POST['id'];
			$date = date('Y-m-d');
			$query_status = "UPDATE project_table SET submit_date=? WHERE project_id=?";
			$key6=$pdo->prepare($query_status);
			if($key6->execute([$date, $get_id_hidden])){
				echo"gumana";
			} else {
				$key6->errorInfo();
			}
		break;
		case 'view_project_action':
			// EDIT, SUBMIT PPMP, DOWNLOAD
			$get_id=$_POST['eventid'];
			$query_check_status = "SELECT * FROM project_table WHERE project_id=?";
			$key4=$pdo->prepare($query_check_status);
			$key4->execute([$get_id]);
			$outer_array=array();
			while($row_get_status=$key4->fetch(PDO::FETCH_ASSOC)) {
				$arr1['submit_date']=$row_get_status['submit_date'];
				$arr1['approval_date']=$row_get_status['approval_date'];
				$arr1['pr_approved_date']=$row_get_status['pr_approved_date'];
			}
			$outer_array[]=$arr1;
			echo json_encode($outer_array);
		break;
		case 'save_title':
			$get_id = $_POST['eventid'];
			$get_title = $_POST['get_title'];
			$update_title = "UPDATE project_table SET project_title=? WHERE project_id=?";
			$key2 = $pdo->prepare($update_title);
			$key2->execute([$get_title, $get_id]);
			$update_item = "UPDATE items_table SET p_title_name=? WHERE p_title_handler=?";
			$key3 = $pdo->prepare($update_item);
			$key3->execute([$get_title, $get_id]);
			$update_mile = "UPDATE milestone_table SET milestone_title_handler=? WHERE milestone_handler=?";
			$key3 = $pdo->prepare($update_mile);
			$key3->execute([$get_title, $get_id]);
			break;
		case 'check_if_submittted':
			$proj_id=$_POST['eventid'];
			$q1="SELECT * FROM project_table WHERE project_id=?";
			$q1_exec=$pdo->prepare($q1);
			if($q1_exec->execute([$proj_id])) {
				while($row=$q1_exec->fetch(PDO::FETCH_ASSOC)) {
					$date_created=$row['submit_date'];
					if($date_created == '0000-00-00') {
						echo "not_submitted";
					} else if ($date_created != '0000-00-00') {
						echo "submitted";
					}
				}
			} else {
				echo $q1_exec->errorInfo();
			}

			break;
		case 'select_projects':
			$outer_array=array();
			$user_id=$_POST['user_id'];
			$q1="SELECT * FROM project_table WHERE user_id=?";
			$q1_exec=$pdo->prepare($q1);
			if($q1_exec->execute([$user_id])){
				while($row=$q1_exec->fetch(PDO::FETCH_ASSOC)) {
					$inner_array=array();
					$id_catcher=$row['project_id'];
					$title=$row['project_title'];
					$date_created=date('F d, Y', strtotime($row['date_created']));
					$inner_array[]='<a href="view_project_end_user.php?eventid=' .$id_catcher. '">' .$title. '</a>';
					$inner_array[]=$date_created;
					$outer_array[]=$inner_array;
				}
				echo json_encode($outer_array);
			} else {
				echo "Datatabase Error : " . $q1_exec->errorInfo();
			}
			break;
		case 'check_unit':
			$item_name=$_POST['item_name'];
			$q1="SELECT * FROM office_supplies_table WHERE items=?";
			$q1_exec=$pdo->prepare($q1);
			if($q1_exec->execute([$item_name])) {
				$row_q1=$q1_exec->fetch(PDO::FETCH_ASSOC);
				$cnt_row=$q1_exec->rowCount();
				$unit=$row_q1['item_unit'];
				if(empty($unit)) { 
					echo "no match"; 
				} else if(!empty($unit)) {
					echo $unit;
				}
			} else {
				echo"error";
			}
			break;
		case 'check_price':
			$item_name=$_POST['item_name'];
			$q1="SELECT * FROM office_supplies_table WHERE items=?";
			$q1_exec=$pdo->prepare($q1);
			if($q1_exec->execute([$item_name])) {
				$row_q1=$q1_exec->fetch(PDO::FETCH_ASSOC);
				$qty=$row_q1['mt_unit_cost'];
				if(empty($qty)) {
					// do nothing
				} else {
					echo $qty;
				}
			} else {
				echo"error";
			}
			break;
		case 'delete_proj':
			$delete_id_handler=$_POST['delete_id'];
			//DELETE MILESTONE FROM MILESTONE TABLE
			$query_delete_title = "DELETE FROM project_table WHERE project_id=?";
			$stmt=$pdo->prepare($query_delete_title);
			$stmt->execute([$delete_id_handler]);
			//DELETE MILESTONE FROM MILESTONE TABLE
			$query_delete_milestone = "DELETE FROM milestone_table WHERE milestone_handler=?";
			$stmt=$pdo->prepare($query_delete_milestone);
			$stmt->execute([$delete_id_handler]);
			//DELETE ITEMS FROM ITEMS TABLE
			$query_delete_ppmp = "DELETE FROM items_table WHERE p_title_handler=?";
			$stmt=$pdo->prepare($query_delete_ppmp);
			$stmt->execute([$delete_id_handler]);
			break;

		case 'set_custom_date':
			$get_id=$_POST['proj_id'];
			$x1=$_POST['date_handler'];
			$x2=$_POST['date_identifier'];
			$q1="UPDATE milestone_table SET ".$x2."=? WHERE milestone_handler=?";
			$q1_exec=$pdo->prepare($q1);
			if($q1_exec->execute([$x1,$get_id])){
				echo"success";
			} else {
				// echo $q1_exec->errorInfo();
				echo $q1;
			}
			break;

		case 'update_mode':
			//UPDATE MODE OF PROCUREMENT FUNCTION
			$title_id = $_POST['eventid'];
			$mode_of_procurement_input = $_POST['mode_of_procurement_selection'];
				$update_mode_query="UPDATE project_table SET mode_of_procurement='$mode_of_procurement_input' WHERE project_id='$title_id'";
				$sql_update_mode=$pdo->prepare($update_mode_query);
				if($sql_update_mode->execute()) {
					//echo"updated";
				} else {
					//$sql_update_mode->errorInfo();
				}
		break;

		case 'save_mile_date':
			//PR MILESTONE
			$titlehandle1=$_POST['hidden_id'];
			$time1=$_POST['time11'];
			$time1_formatted = date('l', strtotime($time1));
			//RFQ MILESTONE
			$time2 = date('Y-m-d', strtotime($time1 . ' - 3 days'));
			$time2_formatted = date('l', strtotime($time2));
				if($time2_formatted == 'Saturday') {
				$time2_real=date('Y-m-d', strtotime($time2 . ' - 1 days'));
				}
				if($time2_formatted == 'Sunday') {
				$time2_real=date('Y-m-d', strtotime($time2 . ' - 2 days'));
				}
			//MONDAY TO FRIDAY
				if($time2_formatted == 'Monday' || $time2_formatted == 'Tuesday' || $time2_formatted == 'Wednesday' || $time2_formatted == 'Thursday' || $time2_formatted == 'Friday') {
				$time2_real=date('Y-m-d', strtotime($time2));
				}
			//AOQ MILESTONE
			$time3 = date('Y-m-d', strtotime($time2_real . ' - 7 days'));
			$time3_formatted=date('l', strtotime($time3));
				if($time3_formatted == 'Saturday') {
				$time3_real=date('Y-m-d', strtotime($time3 . ' - 1 days'));
				}
				if($time3_formatted == 'Sunday') {
				$time3_real=date('Y-m-d', strtotime($time3 . ' - 2 days'));
				}
			//MONDAY TO FRIDAY
				if($time3_formatted == 'Monday' || $time3_formatted == 'Tuesday' || $time3_formatted == 'Wednesday' || $time3_formatted == 'Thursday' || $time3_formatted == 'Friday'){
				$time3_real=date('Y-m-d', strtotime($time3));
				}
			//PO MILESTONE
			$time4 = date('Y-m-d', strtotime($time3_real . ' - 3 days'));
			$time4_formatted=date('l', strtotime($time4));
				if($time4_formatted == 'Saturday') {
				$time4_real=date('Y-m-d', strtotime($time4 . ' - 1 days'));
				}
				if($time4_formatted == 'Sunday') {
				$time4_real=date('Y-m-d', strtotime($time4 . ' - 2 days'));
				}
			//MONDAY TO FRIDAY
				if($time4_formatted == 'Monday' || $time4_formatted == 'Tuesday' || $time4_formatted == 'Wednesday' || $time4_formatted == 'Thursday' || $time4_formatted == 'Friday'){
				$time4_real=date('Y-m-d', strtotime($time4));
				}
			//DELIVERY MILESTONE
			$time5 = date('Y-m-d', strtotime($time4_real . ' - 3 days'));
			$time5_formatted=date('l', strtotime($time5));
			$time5_day = date('d', strtotime($time5));
			$q1="SELECT * FROM project_table WHERE project_id=?";
			$q1_exec=$pdo->prepare($q1);
			$q1_exec->execute([$titlehandle1]);
			$row=$q1_exec->fetch(PDO::FETCH_ASSOC);
			$date_created=date('Y-m-d', strtotime($row['date_created']));
			$date_created_day=date('d', strtotime($row['date_created']));
			if( $time5_day < $date_created_day ) {
				$time5 = date('Y-m-d', strtotime($date_created . ' + 1 days'));
				$time5_formatted=date('l', strtotime($time5));
				if($time5_formatted == 'Saturday') {
					$time5_real=date('Y-m-d', strtotime($time5 . ' + 1 days'));
				} else if($time5_formatted == 'Sunday') {
					$time5_real=date('Y-m-d', strtotime($time5 . ' + 2 days'));
				} else {
					$time5_real=date('Y-m-d', strtotime($time5));
				}
			} else {
				//MONDAY TO FRIDAY
				if($time5_formatted == 'Monday' || $time5_formatted == 'Tuesday' || $time5_formatted == 'Wednesday' || $time5_formatted == 'Thursday' || $time5_formatted == 'Friday'){
					$time5_real=date('Y-m-d', strtotime($time5));
				}	
			}
			$query="UPDATE milestone_table SET approval_of_request='$time1', philgeps_posting='$time2_real', pre_bid='$time3_real', opening_of_bid='$time4_real', post_qualification='$time5_real' WHERE milestone_handler='$titlehandle1'";
			$exec=$pdo->prepare($query);

			if($exec->execute()) {
				//"updated";
				} else {
				//$exec->errorInfo();
			}
			$arr = array();
			$sel_mile="SELECT * FROM milestone_table WHERE milestone_handler=?";
				$exec_sel_mile=$pdo->prepare($sel_mile);
				$exec_sel_mile->execute([$titlehandle1]);
				while($row=$exec_sel_mile->fetch(PDO::FETCH_ASSOC)){
					$arr['mile1']=$row['approval_of_request'];
					$arr['mile2']=$row['philgeps_posting'];
					$arr['mile3']=$row['pre_bid'];
					$arr['mile4']=$row['opening_of_bid'];
					$arr['mile5']=$row['post_qualification'];
				}
			echo json_encode($arr);	

		break;

		case 'set_submit_date':
			$get_id = $_POST['eventid'];
			$date = $_POST['date_now'];
				$query_status = "UPDATE project_table SET submit_date='$date' WHERE project_id='$get_id'";
				$key6=$pdo->prepare($query_status);
					if($key6->execute()) {
						//echo "yehey";
					} else {
						$key6->errorInfo();
					}
		break;

		case 'fund_add':
			$get_id = $_POST['eventid'];
			$fund_input=$_POST['fund_tosend'];
	
				$query_add_remarks="UPDATE project_table SET source_of_fund=? WHERE project_id=?";
				$exec_query_remarks=$pdo->prepare($query_add_remarks);
				if($exec_query_remarks->execute([$fund_input,$get_id])) {
					echo"yehey";
				} else {
					$exec_query_remarks->errorInfo();
				}
		break;

		case 'insert_item':
			$get_user_id=$_POST['s_id'];
			$title_id=$_POST['eventid'];
			$title_category=$_POST['category'];
			//SELECT CODE FROM CATEGORY
			$sel_cat="SELECT * FROM category_table WHERE category=?";
			$sel_cat_exec=$pdo->prepare($sel_cat);
			$sel_cat_exec->execute([$title_category]);
			while($row_cat=$sel_cat_exec->fetch(PDO::FETCH_ASSOC)){
				$code=$row_cat['code']; 
			}
			$sums = 0;
				$queryselect = "SELECT * FROM project_table WHERE project_id='$title_id'";
				$stmt=$pdo->prepare($queryselect);
				$stmt->execute();
				while($rowget = $stmt->fetch()) {
				$id_catcher=$rowget['project_id'];
				$project_title=$rowget['project_title'];
				}
			$arr1=array();
			$desc = $_POST['desc_toadd'];
			$quant = $_POST['quan_toadd'];
			$unit = $_POST['unit_toadd'];
			$est = $_POST['buds_toadd'];
			$final_est = $quant * $est;
			$arr1["item_description"]=$desc;
			$arr1["item_quantity"]=$quant;
			$arr1["item_unit"]=$unit;
			$arr1["item_est"]=$est;
			$query_ins = "INSERT INTO items_table (p_title_name, code, p_title_handler, p_description, p_quantity_size, p_unit, p_estimated_budget, classification, user_id) VALUES (?,?,?,?,?,?,?,?,?)";
			$stmt1=$pdo->prepare($query_ins);
			if($stmt1->execute([$project_title,$code,$id_catcher,$desc,$quant,$unit,$final_est,$title_category,$get_user_id])) {
				echo json_encode($arr1);
				//SELECT ITEM FROM THE LAST ROW
				$query_last_row="SELECT * FROM items_table WHERE p_title_handler=? ORDER BY item_id DESC LIMIT 1";
				$exec_lr=$pdo->prepare($query_last_row);
				$exec_lr->execute([$title_id]);
				$arr=array();
				while($l_row=$exec_lr->fetch(PDO::FETCH_ASSOC)) {
					$arr["est_bud"]=$l_row['p_estimated_budget'];
					$arr["item_id"]=$l_row['item_id'];
				}
				//echo json_encode($arr);
				// SELECT ITEM BASED ON PROJECT TITLE
				$query_select = "SELECT * FROM items_table WHERE p_title_handler='$title_id'";
				$stmt2=$pdo->prepare($query_select);
				$stmt2->execute();
				while($row_select=$stmt2->fetch(PDO::FETCH_ASSOC)){
				$sums = $sums + $row_select['p_estimated_budget'];
				}
				$formattedNum=number_format($sums, 2);
				//UPDATE TOTAL ESTIMATED BUDGET IN PROJECT
				$query_total = "UPDATE project_table SET total_estimated_budget='$sums' WHERE project_id='$title_id'";
				$stmt=$pdo->prepare($query_total);
				$stmt->execute();
			} else {
				$stmt1->errorInfo();
			}
		break;

		case 'update_item':
			$id = $_POST['proj_id_tosend'];
			$base = $_POST['id_tosend'];
			$desc1 = $_POST['desc_tosend'];
			$quan1 = $_POST['quan_tosend'];
			$unit1 = $_POST['unit_tosend'];
			$est1 = $_POST['est_tosend'];
			$cat_get = $_POST['cat_tosend'];
			$get_cat="SELECT * FROM category_table WHERE category=?";
			$get_cat_exec=$pdo->prepare($get_cat);
			$get_cat_exec->execute([$cat_get]);
			while($row_get_cat=$get_cat_exec->fetch(PDO::FETCH_ASSOC)) {
				$code=$row_get_cat['code'];
			}
			$final_comp=$quan1 * $est1;
				$query8 = "UPDATE items_table SET classification='$cat_get', code='$code', p_description='$desc1', p_quantity_size='$quan1', p_unit='$unit1', p_estimated_budget='$final_comp' WHERE item_id='$base'";
				$stmt=$pdo->prepare($query8);
		        if($stmt->execute()) {
		        	//echo "data updated";
		        } else {
		        	//echo "failed";
		        }
        		$query6 = "SELECT * FROM items_table WHERE p_title_handler='$id'";
				$query6_key=$pdo->prepare($query6);
		        $query6_key->execute();
		        $sum=0;
		        while($row=$query6_key->fetch(PDO::FETCH_ASSOC)) {
		        	$sum = $sum + $row['p_estimated_budget'];
		        }
				$query7= "UPDATE project_table SET total_estimated_budget='$sum' WHERE project_id='$id'";
				$query7_key=$pdo->prepare($query7);
				$query7_key->execute();
		break;

		case 'delete_item':
			$delete = $_POST['id_todelete'];
			$project_id = $_POST['id_toupdate'];
				$query5 = "DELETE FROM items_table WHERE item_id='$delete'";
				$stmt=$pdo->prepare($query5);
				$stmt->execute();
				$query6 = "SELECT * FROM items_table WHERE p_title_handler='$project_id'";
				$query6_key=$pdo->prepare($query6);
				$query6_key->execute();
				$sum=0;
				while($row=$query6_key->fetch(PDO::FETCH_ASSOC)){
				$sum = $sum + $row['p_estimated_budget'];
				}
				$query7= "UPDATE project_table SET total_estimated_budget='$sum' WHERE project_id='$project_id'";
				$query7_key=$pdo->prepare($query7);
				$query7_key->execute();
		break;
		case 'cat_get':
			# code...
			$arr1=array();
			$dynamic_table=$_POST['cat'];
			if($dynamic_table=="Common Office Supplies") {
				$select_i="SELECT * FROM office_supplies_table";
					$select_i_exec=$pdo->prepare($select_i);
					if($select_i_exec->execute()) {
						echo json_encode($select_i_exec->fetchall(PDO::FETCH_ASSOC));
					} else {
						echo $select_i_exec->errorInfo();
					}
			} else {
				echo "no match";
			}
			break;
}
?>