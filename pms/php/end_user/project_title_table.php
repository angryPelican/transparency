<table id="myTable" class="table table-striped table-sm d-board-enduser">
	<thead>
		<tr>
			<th width="500">Project Title</th>
			<th>Date Created</th>
			<th>Estimated Budget</th>
			<th>Status</th><!--DISPLAY ONLY, BASED ON LAST PROJECT EDIT-->
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php
			include('Connect.php');
			$username=$_SESSION['s_username'];
			$user_id=$_SESSION['s_id'];
			// GET THE USERNAME BASED ON USER ID
			$username = $_SESSION['s_username'];
			$query_select_user = "SELECT * FROM accounts WHERE user_id=?";
			$stmt=$pdo->prepare($query_select_user);
			if($stmt->execute([$user_id])) {
				while($row_select = $stmt->fetch())	{
					$get_type=$row_select['user_type'];
					$get_id=$row_select['user_id'];
				}
			} else {
				$stmt->errorInfo();
			}
			//END OF QUERY
			$querytitle = "SELECT * FROM project_table WHERE user_id=? ORDER BY date_created DESC";
			$stmt=$pdo->prepare($querytitle);
			if($stmt->execute([$get_id])) {
				while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$id_catcher=$row['project_id'];
					$title=$row['project_title'];
					$pr_date=$row['pr_date'];
					$approval_date=$row['approval_date'];
					$pr_approved_date=$row['pr_approved_date'];
					$date_created=$row['date_created'];
					$format_date=date('F d, Y', strtotime($date_created));
					$submit_date=$row['submit_date'];
					$total=$row['total_estimated_budget'];
					$delivery_status=$row['delivery_status'];
					$po_approved_date=$row['po_approved_date'];
					$status=$row['status'];
					$formattedNum = number_format($total, 2);
		?>

			<tr>
				<td><a href="view_project_end_user.php?eventid=<?php echo $id_catcher; ?>"><?php echo $title; ?></a></td>
				<td align="right"><?php echo $format_date; ?></td>
				<td align="right"><?php echo $formattedNum; ?></td>

		<?php
				if($submit_date == "0000-00-00"){
				echo'<td><span class="badge badge-pill badge-secondary">Not Submitted</span></td>';
				} else if ($submit_date != "0000-00-00" && $status == 0) {
				echo'<td><span class="badge badge-pill badge-success">Submitted</span></td>';
				} else if ($status==2) {
				echo'<td><span class="badge badge-pill badge-success">Approved PPMP</span></td>';
				} else if ($status==3) {
				echo'<td><span class="badge badge-pill badge-success">PR Created</span></td>';
				} else if ($status==4) {
				echo'<td><span class="badge badge-pill badge-success">PR Approved</span></td>';
				} else if ($status==5) {
				echo'<td><span class="badge badge-pill badge-success">RFQ Created</span></td>';
				} else if ($status==6) {
				echo'<td><span class="badge badge-pill badge-success">AOQ Created</span></td>';
				} else if ($status==7) {
				echo'<td><span class="badge badge-pill badge-success">AOQ Approved</span></td>';
				} else if ($status==8) {
				echo'<td><span class="badge badge-pill badge-success">PO Created</span></td>';
				} else if ($status==9 && $delivery_status == 0) {
				echo'<td><span class="badge badge-pill badge-success">PO Approved</span></td>';
				} else if ($status == 9 && $delivery_status == 10) {
				echo'<td><span class="badge badge-pill badge-success">Completed</span></td>';
				} else if ($status == 9 && $delivery_status == 11) {
				echo'<td><span class="badge badge-pill badge-success">Partial Delivery</span></td>';
				}
					if($pr_date == '0000-00-00') {
					echo'<td align="center">
						<button name="title_delete" id="btn_delete" data-id="'.$id_catcher.'" class="btn btn-outline-danger"><i class="far fa-trash-alt"></i></button>
					</td>';
					}
					else if ($pr_date != '0000-00-00') {
					echo'<td align="center">
						<button name="title_delete" id="btn_delete" data-id="'.$id_catcher.'" class="btn btn-outline-danger"><i class="far fa-trash-alt"></i></button>
					</td>';
					}
			echo'</tr>';
				}
			}
		?>
	</tbody>
</table>
<script>
	$('button[id=btn_delete]').click(function(){
		var item_id=$(this).data('id');
		var role="delete_proj";
		swal({
			title: "Are you sure?",
			text: "Once deleted, you will not be able to recover this item",
			type: "warning",
			showCancelButton: true,
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
			})
			.then((result) => {

				if (result.value) {
					$.ajax({
						url: "php/end_user/mode_of_proc_webservice.php",
						type: "post",
						data: {
							delete_id: item_id,
							tag: role,
						},
						success: function(response) {
							swal("Your project has been deleted!", {
							type: "success",
							timer: 1100,
							showConfirmButton: false
							})
							.then((result) => {
								location.reload();
							});
						}
					});
				} else {
					swal({
						text: "Your item is safe!",
						type: "success",
						timer: 1100,
						showConfirmButton: false
					})
					.then((result) => {
						location.reload();
					});
				}
			});
	});
</script>