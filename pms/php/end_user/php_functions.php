<?php
	include('connectdb.php');
	function get_categories() {
		global $pdo;
		$category=array();
		$select_cat="SELECT * FROM category_table";
		$query=$pdo->prepare($select_cat);
		$query->execute();
			while($row=$query->fetch(PDO::FETCH_ASSOC)) {
				$get_category=$row['category'];
				array_push($category, $get_category);
			}
			foreach($category as $key => $value) {
				echo"<option value='$value'>$value</option>";
			}
		}
	function get_project_title() {
		global $pdo;
		$get_id=$_GET['eventid'];
			$select_project="SELECT * FROM project_table WHERE project_id=?";
			$stmt=$pdo->prepare($select_project);
			$stmt->execute([$get_id]);
				while ($row = $stmt->fetch()):
					$project_title = $row['project_title'];
				endwhile;
			echo $project_title;
	}
	function edit_project_title() {
		global $pdo;
		$title_id=$_GET['eventid'];
		$select_title="SELECT * FROM project_table WHERE project_id=?";
		$key1=$pdo->prepare($select_title);
		$key1->execute([$title_id]);
			while($row=$key1->fetch(PDO::FETCH_ASSOC)):
				$title_name=$row['project_title'];
			endwhile;
		if ($title_name=="") {
			echo '<textarea form="form_get" class="form-control form-control-sm" id="get_title"></textarea>';
		} else if ($title_name!="") {
			echo '<textarea form="form_get" class="form-control form-control-sm" id="get_title">'.$title_name.'</textarea>';
		}
	}
	function get_total_amount() {
		global $pdo;
		$get_id=$_GET['eventid'];
		$query1="SELECT * FROM project_table WHERE project_id=?";
		$stmt=$pdo->prepare($query1);
		if($stmt->execute([$get_id])) {
			$row=$stmt->fetch(PDO::FETCH_ASSOC);
			$formattedNum=number_format($row['total_estimated_budget'], 2);
			echo"<tr>
					<td colspan='5'><b>Total Estimated Budget: $formattedNum</b></td>
				</tr>";
		} else {
			$stmt->errorInfo();
		}
	}
	function check_stats() {
		global $pdo;
		$titlehandle1=$_GET['eventid'];
		$query_select_item="SELECT * FROM items_table WHERE p_title_handler=?";
		$exec_select_item=$pdo->prepare($query_select_item);
		$exec_select_item->execute([$titlehandle1]);
		$count_item=$exec_select_item->rowCount();
		if($count_item == 0)	{
    			echo '<a class="dropdown-item disabled="true"" href="#">Submit</a>';
    		}
    	else if($count_item != 0){
		$query_select_project="SELECT * FROM project_table WHERE project_id=?";
		$exec_select_project=$pdo->prepare($query_select_project);
		$exec_select_project->execute([$titlehandle1]);

			while($row_select_project=$exec_select_project->fetch(PDO::FETCH_ASSOC)){
				$date_submit=$row_select_project['submit_date'];
			}
				if($date_submit == '0000-00-00') {
    		echo '<button type="button" name="btn_submit" data-role="set_submit_date" class="dropdown-item btn_submit">Submit</button>';
    		} else if($date_submit != '0000-00-00') {
    			echo '<button type="button" name="btn_submit" data-role="set_submit_date" class="dropdown-item btn_submit sr-only">Submit</button>';
    		}
    	}
	}
?>
<?php
	include('modals.php');
?>