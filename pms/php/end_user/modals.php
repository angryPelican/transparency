<!-- EDIT PROJECT TITLE MODAL -->
	<div id="editTitle" class="modal fade" role="dialog" data-backdrop="static"  data-keyboard="false">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class ="modal-header bg-primary">
					<h5 class="modal-title text-white"><i class="fas fa-edit"></i> PMS | Edit Project Title</h5>
					<button type = "button" class = "close text-white" data-dismiss = "modal">&times;</button>
				</div>
				<form action="" method="post" id="form_get">
					<div class="modal-body">
						<div class="form-group">
							<label for="project_title"><b>Project Title:</b></label>
							<?php
								edit_project_title();
							?>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="save_title" class = "btn btn-primary">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<!-- MODAL FOR ADDING ITEM -->
<div id="addItem" class="modal fade" role="dialog" data-backdrop="static"  data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white"><i class="fas fa-edit"></i> PMS | Add item</h5>
				<button type = "button" class = "close text-white" data-dismiss = "modal">&times;</button>
			</div>
			<div class="modal-body">
				<form action="" method="post" id="additem_form"></form>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="categories"><b>Classification</b></label>
							<select class='custom-select mr-sm-2 form-control-sm' id='categories' required>
								<option value="Choose...." selected>Choose....</option>
								<?php
									get_categories();
								?>
							</select>
							<div class="invalid-feedback">
								Pick a Category first when adding an item on your project.
							</div>
						</div>
					</div>
					<div class="col-sm-6" id="training_days">
						<div class="form-group">
							<label><b>No. of Days</b></label>
							<input type="number" id="days" class="form-control form-control-sm" placeholder="Place no. of days here .." />
							<script type="text/javascript">
							</script>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label><b>General Description </b></label>
					<input type="text" list="items" autocomplete="off" class="form-control form-control-sm" name="getdesc" id="input_desc" form="additem_form">
					<datalist id="items" class="item_drop">
					</datalist>
					<textarea id="text_input_desc" class="form-control form-control-sm" rows="4"></textarea>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label><b>Quantity/Size </b></label>
							<input type="number" class="form-control form-control-sm" name="getquan" id = "input_quan" form="additem_form" />
						</div>
						<script type="text/javascript">
						</script>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label><b>Unit </b></label><br />
							<select name="getunit" class="form-control form-control-sm" style="width:100%;" id="input_unit" form="additem_form">
								<option selected>Choose...</option>
								<option value = "pc">pc</option>
								<option value = "unit">unit</option>
								<option value = "set">set</option>
								<option value = "box">box</option>
								<option value = "pax">pax</option>
								<option value = "bottle">bottle</option>
								<option value = "can">can</option>
								<option value = "bottle">gallon</option>
							</select>
							<input type="text" class="form-control form-control-sm" id="input_text_unit" value="" />
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label><b>Unit Price</b></label><br />
							<input type="number" name = "getbuds" class="form-control form-control-sm" id = "input_est" form="additem_form" />
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="hidden_id" form="additem_form">
				<a href="#" id="add" class="btn btn-primary">Add</a>
			</div>
		</div>
	</div>
</div>

