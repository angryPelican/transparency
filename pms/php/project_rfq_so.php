<h5>REQUEST FOR QUOTATION</h5>
<table class="table table-bordered mb-4" id="table_rfq">
	<form action="" method="post" id="form1" style="table-layout: fixed; width: 100%;">
		<tr>
			<td width="30%" colspan="2">Date Created: <b><i><?php
				$get_id = $_GET['eventid'];
				$get_date="SELECT * FROM project_table WHERE project_id=?";
				$res=$pdo->prepare($get_date);
				$res->execute([$get_id]);
				while($row=$res->fetch(PDO::FETCH_ASSOC)):
				$rfq_date=$row['rfq_date'];
				endwhile;
				if($rfq_date != '0000-00-00'){
				echo date('F d, Y', strtotime($rfq_date));
				}else if($rfq_date = '0000-00-00'){
				echo "";
				}
			?></i></b></td>
			<td width='5%'>Opening Date</td>
			<td width='1%'>Action</td>
		</tr>
			<tr width="20%">
				<input type="hidden" class="hidden_id" value="<?php echo $get_id; ?>">
				<input type="hidden" class="hidden_tag" value="set_opening_date">
			</td>
		</tr>
		<?php
			$sel_rfq="SELECT * FROM rfq_table WHERE project_id=?";
			$sel_rfq_exec=$pdo->prepare($sel_rfq);
			$sel_rfq_exec->execute([$get_id]);
			$row_count=$sel_rfq_exec->rowCount();
			$year=date("y");
				while($row_rfq=$sel_rfq_exec->fetch(PDO::FETCH_ASSOC)) {
					$rfq_no=$row_rfq['rfq_number'];
					$categ=$row_rfq['category'];
					$open_date=$row_rfq['opening_date'];

					switch ($_SESSION['s_user_type']) {
						case '4':					
							echo"<tr>
								<td><p id='cats'>$categ</p></td>
								<td>DepEd-$year-NCR-QN-$rfq_no</td>
								<td><input type='date' id='pol' data-categ='$categ' data-id='$rfq_no' data-role='set_opening_date' class='form-control is-valid datepicker' value='$open_date'>
									<div class='invalid-feedback'>
										Your date is invalid. Please choose Weekdays [ex: Monday,Tuesday]
									</div>
									<div class='valid-feedback'>
										Note: You can only choose Weekdays to avoid conflict of dates.
									</div>
							</td>";
								if($open_date!='0000-00-00'){
									echo"<td align='center'><a class='btn btn-primary' id='dl1' href='download_rfq.php?category=$get_id&class=$categ'><i class='fas fa-download'></i></a></td>";
								} else if($open_date=='0000-00-00'){
									echo"<td align='center'><a class='btn btn-primary form-control disabled' id='dl1' role='button' aria-disabled='true' href='download_rfq.php?category=$get_id&class=$categ'><i class='fas fa-download'></i></a></td>";
								}
						echo"</tr>";
							break;
						case '5':
							echo"<tr>
							<td><p id='cats'>$categ</p></td>
							<td>DepEd-$year-NCR-QN-$rfq_no</td>
							<td><input type='date' id='pol' data-categ='$categ' data-id='$rfq_no' data-role='set_opening_date' class='form-control is-valid datepicker' value='$open_date' disabled>
								<div class='invalid-feedback'>
									Your date is invalid. Please choose Weekdays [ex: Monday,Tuesday]
								</div>
								<div class='valid-feedback'>
									Note: You can only choose Weekdays to avoid conflict of dates.
								</div>
							</td>";
							if($open_date!='0000-00-00'){
								echo"<td align='center'><a class='btn btn-primary btn disabled' id='dl1' href='download_rfq.php?category=$get_id&class=$categ' disabled><i class='fas fa-download'></i></a></td>";
							} else if($open_date=='0000-00-00'){
								echo"<td align='center'><a class='btn btn-primary form-control btn disabled' id='dl1' role='button' aria-disabled='true' href='download_rfq.php?category=$get_id&class=$categ'><i class='fas fa-download'></i></a></td>";
							}
							break;
					}

				}
		?>
	</form>
</div>
<tfoot>
<tr>
	<td style="border-right: 0;">
		<span class="badge badge-success">Approved</span>
	</td>
			<td style="border-right: none; border-left: none;"></td>
			<td style="border-right: none; border-left: none;"></td>
			<td class="text-right" style="border-left: none;">
				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Action
					<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li><a href="#" name="comment" class="btn btn-link" data-toggle="modal" data-target="#addCommentRFQ">Comment</a></li>
					</ul>
				</div>
			</td>
		</tr>
	</tfoot>
</table>
<!-- Modal -->
	<div id="addCommentRFQ" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="background: #0652DD; color: white;">
					<h5 class="modal-title">Add Comment</h5>
					<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form action="" method="post" id="formcomment">
						<div class="form-group">
							<label>Comment</label>
							<textarea class="form-control" id="comment_text_rfq" form="formcomment" name="inputed_comment" rows="5" ></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" name="comment" data-role="comment_ppmp" form="formcomment" class="btn btn-default btn-default btn-comment" style="background: #0652DD; color: white;">Add</button>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
	$('input[id="pol"]').on('change',function(){
		var data_id=$(this).data('id');
		var initialcateg=$(this).data('categ');
		var date_exec=$('input[data-id="'+ data_id + '"]').val();
		var date_input=$(this).val();
		var categ=$('input[data-categ="'+ initialcateg + '"]').data('categ');
		console.log(categ);
		var hidden_id=$('.hidden_id').val();
		console.log(hidden_id);
		var hidden_tag=$(this).data('role');
		console.log(hidden_tag);
		var date = $('#pol').val().split("-");
		day = date[1];
		month = date[0];
		year = date[2];
		var format_date = month + "/" + day + "/" + year;
		var coco = new Date(format_date);
		var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		var log=(days[coco.getDay()]);
		console.log(log);
		if(log === "Saturday")	{
			$('input[data-id="'+ data_id + '"]').addClass("is-invalid");
			$('input[data-id="'+ data_id + '"]').removeClass("is-valid");
		}
		if(log === "Sunday") {

			$('input[data-id="'+ data_id + '"]').addClass("is-invalid");
			$('input[data-id="'+ data_id + '"]').removeClass("is-valid");

		} else if(log === "Monday" || log === "Tuesday" || log === "Wednesday" || log === "Thursday" || log === "Friday") {
			$('input[data-id="'+ data_id + '"]').removeClass("is-invalid");
			console.log($('input[data-id="'+ data_id + '"]').data('id'));
				$.ajax({
					url: 'php/so_webservice.php',
					type: 'POST',
					data: {
					tag: hidden_tag,
					cats: categ,
					pol: date_exec,
					eventid: hidden_id
					},
					success : function(response){
						location.reload();
						//alert(response);
					}
				});// end of ajax
		}
});// end of onchange

$('.btn-comment').click(function(){
		var section="RFQ";
		var role=$(this).data("role");
		var hidden_id="<?php echo $_GET['eventid']; ?>";
		var username="<?php echo $_SESSION['s_username']; ?>";
		var comment_input=$('#comment_text_rfq').val();
		$('#addCommentRFQ').modal('toggle');
			$.ajax({
				url: 'php/so_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				module: section,
				inputed_comment: comment_input,
				s_username: username,
				eventid: hidden_id
				},
				success : function(response){
					swal({
						title: "Success",
						text: "You Successfully commented!",
						type: "success",
						timer: 1100,
						showConfirmButton: false
					})
					.then((value) => {
						location.reload();
					});
				}
			});
});

</script>