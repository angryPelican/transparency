<?php
include('dynamic_dates.php');
$year=date("y");
$pr_number=$get_pr;
$whole_pr_code="DepEd-" . $year . "-NCR-PR-" . $get_pr;
?>

<!--PROJECT TITLE-->
<h5>PURCHASE REQUEST</h5>
<table class="table table-striped table-bordered display mb-4" id="table_pr">
	<thead>
		<tr>
			<td colspan="2" rowspan="2">Office/Section<br /><b>ICT Unit</b></td>
			<td colspan="3">PR No. <b><?php echo $whole_pr_code; ?></b></td><!--AUTO-GENERATED, AUTO INCREMENT-->
			<td colspan="3" rowspan="2">Date <br />
				<?php
				echo "<b>";
				include('get_dates.php');
				if($pr_date == '0000-00-00'){
				}
				else if ($pr_date != '0000-00-00'){
				echo date('F d, Y', strtotime($pr_date));
				}
				echo "</b>";
				?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><b><?php include('php/dynamic_title.php'); ?></b></td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th>Stock No.</th>
			<th>Unit</th>
			<th colspan="2">Item Description</th>
			<th>Quantity</th>
			<th>Unit Cost</th>
		</tr>
		<?php
		include('connectdb.php');
		//FIRST QUERY
		$get_id = $_GET['eventid'];
		$common="Common Office Supplies";
		$first_query = "SELECT * FROM items_table WHERE p_title_handler=? AND classification<>?";
		$stmt1=$pdo->prepare($first_query);
		$stmt1->execute([$get_id, $common]);
		$count = $stmt1->rowCount();
		//SECOND QUERY
		$second_query = "SELECT * FROM items_table WHERE p_title_handler=? AND classification<>?";
		$stmt2=$pdo->prepare($second_query);
		$stmt2->execute([$get_id, $common]);
		$i1 = 0;
		$i2 = 0;
		$sum = 0;
		//loop each items
		while($row1 = $stmt2->fetch(PDO::FETCH_ASSOC)){
		$sx = $row1['p_title_name'];
		$s1 = $row1['p_description'];
		$s2 = $row1['p_quantity_size'];
		$s3 = $row1['p_unit'];
		$s4 = $row1['p_estimated_budget'];
		$formattedNum = number_format($s4, 2);
		$sum = $sum + $row1['p_estimated_budget'];
		$formattedNum1 = number_format($sum, 2);
		echo'<tr>';
			//Code Column Rowspan
			if($i1 == 0) {
			echo'<td rowspan='.$count.'></td>';
			$i1=1;
			}
			echo "<td>$s3</td>
			<td colspan='2'>$s1</td>
			<td>$s2</td>
			<td align='right'>$formattedNum</td>";
			}
		echo "</tr>";
		?>
		<tr>
			<td colspan="2"><b>Purpose</b></td>
			<td colspan="2">
				<?php
				include('php/connectdb.php');
				//get the purpose
				$get_id = $_GET['eventid'];
				$get_date="SELECT * FROM project_table WHERE project_id=?";
				$stmt3=$pdo->prepare($get_date);
				$stmt3->execute([$get_id]);
				while($row=$stmt3->fetch(PDO::FETCH_ASSOC)):
				$purpose=$row['purpose'];
				endwhile;
				//get the total cost
				$get_id = $_GET['eventid'];
				$get_cost="SELECT * FROM items_table WHERE p_title_handler=?";
				$stmt4=$pdo->prepare($get_cost);
				$stmt4->execute([$get_id]);
				while($row=$stmt4->fetch(PDO::FETCH_ASSOC)):
				$total_cost=$row['p_estimated_budget'];
				$sum = $sum + $row1['p_estimated_budget'];
				$formattedNum1 = number_format($sum, 2);
				endwhile;
				echo $purpose;
			echo"</td>
			<td><b>Total Cost:</b></td>
			<td align='right'><b>$formattedNum1</b></td>";
			?>
		</tr>
	</tbody>
	<tfoot>
	<tr>
		<?php
		// user type function
		$user_id=$_SESSION['s_id'];
		$query_select_user="SELECT * FROM accounts WHERE user_id=?";
			$query_exec_user=$pdo->prepare($query_select_user);
			$query_exec_user->execute([$user_id]);
				$row_user=$query_exec_user->fetch(PDO::FETCH_ASSOC);
					$user_type=$row_user['user_type'];

		//check if approved or not
		$query_check_status = "SELECT * FROM project_table WHERE project_id=?";
		$stmt5=$pdo->prepare($query_check_status);
		$stmt5->execute([$get_id]);
		while($row_get_status=$stmt5->fetch(PDO::FETCH_ASSOC)){
		$pr_approved_date = $row_get_status['pr_approved_date'];
		}

				switch($user_type){

					case'3':
						if($pr_approved_date == '0000-00-00') {
		echo'<td colspan="5" style="border-right: 0;">
			<span class="badge badge-info">For Approval</span>
		</td>
		<td class="text-right" style="border-left: 0;">
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<form action="" method="post">
						<li><a href="#addCommentPR" data-toggle="modal" class="btn btn-link">Comment</a></li>
					</form>
				</ul>
			</div>
		</td>';
		} else if($pr_approved_date != '0000-00-00') {
		echo'<td colspan="5" style="border-right: 0;">
			<span class="badge badge-success">Approved</span>
		</td>
		<td class="text-right" style="border-left: 0;">
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<form action="" method="post">
						<li><a href="#addCommentPR" data-toggle="modal" class="btn btn-link">Comment</a></li>
					</form>
				</ul>
			</div>
		</td>';
		}
					break;

					case '1':
						if($pr_approved_date == '0000-00-00') {
		echo'<td colspan="5" style="border-right: 0;">
			<span class="badge badge-info">For Approval</span>
		</td>
		<td class="text-right" style="border-left: 0;">
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<form action="" method="post">
						<li><button type="button" data-role="pr_approve" name="approve_pr" class="btn btn-link btn-pr_approved">Approve</button></li>
						<li><a href="#addCommentPR" data-toggle="modal" class="btn btn-link">Comment</a></li>
					</form>
				</ul>
			</div>
		</td>';
		} else if($pr_approved_date != '0000-00-00') {
		echo'<td colspan="5" style="border-right: 0;">
			<span class="badge badge-success">Approved</span>
		</td>
		<td class="text-right" style="border-left: 0;">
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<form action="" method="post">
						<li><button type="button" data-role="pr_disapprove" name="disapprove_pr" class="btn btn-link btn-pr-disapproved">Disapprove</button></li>
						<li><a href="#addCommentPR" data-toggle="modal" class="btn btn-link">Comment</a></li>
					</form>
				</ul>
			</div>
		</td>';
		}
	break;
				}
		?>
	</tr>
	</tfoot>
</table>

<!-- Modal -->
	<div id="addCommentPR" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="background: #0652DD; color: white;">
					<h5 class="modal-title">Add Comment</h5>
					<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form action="" method="post" id="formcomment">
						<div class="form-group">
							<label>Comment</label>
							<textarea class="form-control" id="comment_text_pr" form="formcomment" rows="5" ></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" data-role="comment_ppmp" form="formcomment" class="btn btn-default btn-default btn-comment-pr" style="background: #0652DD; color: white;">Add</button>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">

	$('.btn-comment-pr').click(function() {
		var section="PR";
		var role=$(this).data("role");
		var hidden_id="<?php echo $_GET['eventid']; ?>";
		var username="<?php echo $_SESSION['s_username']; ?>";
		var comment_input=$('#comment_text_pr').val();
		$('#addCommentPR').modal('toggle');
			$.ajax({
				url: 'php/sds_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				module: section,
				inputed_comment: comment_input,
				s_username: username,
				eventid: hidden_id
				},
				success : function(response) {
					swal({
						title: "Success",
						text: "You Successfully commented!",
						type: "success",
						timer: 1100,
						showConfirmButton: false
					})
					.then((value) => {
						location.reload();
					});
				}
			});
	});

	$('.btn-pr_approved').click(function(){
		 var role=$(this).data("role");
		 var hidden_id="<?php echo $get_id; ?>"
		 	console.log(hidden_id);

	$.ajax({
		url: 'php/sds_webservice.php',
		type: 'POST',
		data: {
		tag: role,
		id: hidden_id
	},
	success : function(response){
	swal({
		title: "Approved",
		text: "PR was approved successfully!",
		type: "success",
		timer: 1100,
		showConfirmButton: false
	})
	.then((value) => {
		location.reload();
	});
	//console.log(response);
	}
});

});

$('.btn-pr-disapproved').click(function(){
		 var role=$(this).data("role");
		 var hidden_id="<?php echo $get_id; ?>"
		console.log(role);
	$.ajax({
	url: 'php/sds_webservice.php',
	type: 'POST',
	data: {
	tag: role,
	id: hidden_id
	},
	success : function(response){
		swal({
			title: "Disapproved",
			text: "PR was disapproved.",
			type: "error",
			timer: 1100,
			showConfirmButton: false
			})
			.then((value) => {
			location.reload();
		});
	}
});

});

</script>