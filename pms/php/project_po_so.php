<h5>PURCHASE ORDER</h5>
<table class="table table-bordered display mb-4" id="table_po">
<thead>
	<tr>
		<th>Categories</th>
		<th>Quotation No</th>
		<th>Total ABC</th>
		<th>Download</th>
	</tr>
</thead>
<tbody>
	<?php
		include('php/connectdb.php');
			$common="Common Office Supplies";
			$get_id=$_GET['eventid'];
			$arr_c=array();
			$sel_proj="SELECT * FROM po_table WHERE project_id=? AND category<>?";
			$sel_proj_exec=$pdo->prepare($sel_proj);
			$sel_proj_exec->execute([$get_id,$common]);
			$year=date("y");
			while($row_sel=$sel_proj_exec->fetch(PDO::FETCH_ASSOC)) {
				$po_no=$row_sel['po_no'];
				$categ=$row_sel['category'];
				array_push($arr_c, $categ);
				echo"<tr>
					<td>$categ</td>
					<td>01-$year-$po_no-DO</td>";
				$getwinner="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
                $getwinner_exec=$pdo->prepare($getwinner);
                $getwinner_exec->execute([$get_id, $categ]);
                $row_winner=$getwinner_exec->fetchall(PDO::FETCH_ASSOC);
                $winner=$row_winner[0]['total_actual_price'];
					echo'<td align="right">'.number_format($winner, 2).'</td>
					<td align="center"><a class="btn btn-primary" href="download_po.php?eventid='.$get_id.'&category='.$categ.'"><i class="fas fa-download"></i></a></td>';
					echo"</tr>";
			}
	?>
</tbody>
	<tfoot>
	<tr>
		<?php
		include('connectdb.php');
		$get_id=$_GET['eventid'];
		$user_id=$_SESSION['s_user_type'];
		$select_project="SELECT * FROM project_table WHERE project_id=?";
		$res=$pdo->prepare($select_project);
		$res->execute([$get_id]);
		while($row=$res->fetch(PDO::FETCH_ASSOC)):
		$po_approved_date=$row['po_approved_date'];
		endwhile;
		switch($user_id) {
		case '4':
		if($po_approved_date == '0000-00-00'){
		echo'<td style="border-right: 0;">
			<span class="badge badge-info">For Approval</span>
		</td>
		<td style="border-left: 0;" colspan="3" class="text-right">
			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<form action="" method="post" id="form1">
						<li><a class="btn btn-link" href="#addCommentPO" data-toggle="modal">Comment</a></li>
					</div>
				</td>';
				}
				else if ($po_approved_date != '0000-00-00'){
				echo'<td style="border-right: 0;">
					<span class="badge badge-success">Approved</span>
				</td>
				<td style="border-left: 0;" colspan="3" class="text-right">
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Action
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<form action="" method="post" id="form1">
								<li><a class="btn btn-link" href="#addCommentPO" data-toggle="modal">Comment</a></li>
							</div>
						</td>';
						}
						break;
		case '5':
		if($po_approved_date == '0000-00-00'){
		echo'<td style="border-right: 0;">
			<span class="badge badge-info">For Approval</span>
		</td>
		<td style="border-left: 0;" colspan="3" class="text-right">
			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<form action="" method="post" id="form1">
						<li><a class="btn btn-link" href="#addCommentPO" data-toggle="modal">Comment</a></li>
					</div>
				</td>';
				}
				else if ($po_approved_date != '0000-00-00'){
				echo'<td style="border-right: 0;">
					<span class="badge badge-success">Approved</span>
				</td>
				<td style="border-left: 0;" colspan="3" class="text-right">
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Action
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<form action="" method="post" id="form1">
								<li><a class="btn btn-link" href="#addCommentPO" data-toggle="modal">Comment</a></li>
							</div>
						</td>';
						}
						break;
						case '3':
						if($po_approved_date == '0000-00-00'){
						echo'<td style="border-right: 0;">
							<span class="badge badge-info">For Approval</span>
						</div>
					</td>
					<td style="border-left: 0;" colspan="3" class="text-right">
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							Action
							<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<form action="" method="post" id="form1">
									<li><a class="btn btn-link" href="#addCommentPO" data-toggle="modal">Comment</a></li>
								</div>
							</div>';
							}
							else if ($po_approved_date != '0000-00-00'){
							echo'<td style="border-right: 0;">
								<span class="badge badge-success">Approved</span>
							</td>
							<td style="border-left: 0;" colspan="3">
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									Action
									<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
										<form action="" method="post" id="form1">
											<li><a class="btn btn-link" href="#addCommentPO" data-toggle="modal">Comment</a></li>
										</div>
									</td>';
									}
									break;
									//lock ng switch
									}
									?>
								</form>
							</ul>
						</tr>
						</tfoot>
					</table>

<!-- Modal -->
	<div id="addCommentPO" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="background: #0652DD; color: white;">
					<h5 class="modal-title">Add Comment</h5>
					<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form action="" method="post" id="formcomment">
						<div class="form-group">
							<label>Comment</label>
							<textarea class="form-control" id="comment_text_po" form="formcomment" name="inputed_comment" rows="5" ></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" name="comment" data-role="comment_ppmp" form="formcomment" class="btn btn-default btn-default btn-comment-po" style="background: #0652DD; color: white;">Add</button>
				</div>
			</div>
		</div>
	</div>

		<script type="text/javascript">
			$('.btn-comment-po').click(function(){
				var section="PO";
				var role=$(this).data("role");
				var hidden_id="<?php echo $_GET['eventid']; ?>";
				var username="<?php echo $_SESSION['s_username']; ?>";
				console.log(username);
				var comment=$("#comment_text_po").val();
				$('#addCommentPO').modal('toggle');
					$.ajax({
						url: 'php/so_webservice.php',
						type: 'POST',
						data: {
						tag: role,
						module: section,
						inputed_comment: comment,
						s_username: username,
						eventid: hidden_id
						},
					success : function(response){
						swal({
						title: "Success",
						text: "You Successfully commented!",
						type: "success",
						timer: 1000,
						showConfirmButton: false
						})
						.then((value) => {
							location.reload();
						});
        			//location.reload(true);
					//console.log(response);
					}
					});
			});
		</script>