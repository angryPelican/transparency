<?php
include('connectdb.php');

	$tag=$_POST['tag'];

		switch ($tag) {

			case 'check_project':
				# code...
				//project_table
				$get_id=$_POST['proj_id'];
				$check_project="SELECT * FROM project_table WHERE project_id=?";
				$check_proj_exec=$pdo->prepare($check_project);
				$check_proj_exec->execute([$get_id]);
				$row=$check_proj_exec->fetch(PDO::FETCH_ASSOC);
				$abs_date=$row['abstract_date'];
				if($abs_date != '0000-00-00') {
					echo"approved";
				} else if ($abs_date == '0000-00-00') {
					echo "for approval";
				}
			break;
			case 'check_approval':
				$get_id=$_POST['proj_id'];
				$check_items="SELECT * FROM items_table WHERE p_title_handler=? GROUP BY classification";
				$check_exec=$pdo->prepare($check_items);
				$check_exec->execute([$get_id]);
				$cnt_row=$check_exec->rowCount();
				//check abstract_table
				$check_abs="SELECT * FROM abstract_table WHERE project_id=?";
				$check_abs_exec=$pdo->prepare($check_abs);
				$check_abs_exec->execute([$get_id]);
				$cnt_abs_row=$check_abs_exec->rowCount();
				if($cnt_row==$cnt_abs_row) {
					echo"eligible";
				} else if ($cnt_row!=$cnt_abs_row) {
					echo"not eligible" . $cnt_row . $cnt_abs_row;
				}
			break;

			case 'sel_approval_date':
				//check if approved or not
				$get_id=$_POST['eventid'];
				$query_check_status = "SELECT * FROM project_table WHERE project_id=?";
				$stmt1=$pdo->prepare($query_check_status);
				$stmt1->execute([$get_id]);
				while($row_get_status=$stmt1->fetch(PDO::FETCH_ASSOC)){
					$status = $row_get_status['approval_date'];
					if($status=='0000-00-00'){
						echo"null";
					} else if ($status!='0000-00-00') {
						echo $status;
					}
				}
			break;

			case 'ppmp_approve':
				$get_id=$_POST['id'];
				$status=2;
				$date = date('Y-m-d');
					$query_status = "UPDATE project_table SET approval_date=?, status=? WHERE project_id=?";
					$stmt2=$pdo->prepare($query_status);
					if($stmt2->execute([$date,$status,$get_id])){
						echo"yehey";
					} else {
						$stmt2->errorInfo();
					}
			break;
			
			case 'ppmp_disapprove':
				$date = '0000-00-00';
				$get_id=$_POST['id'];
				$status=0;
					$query_status = "UPDATE project_table SET submit_date=?, approval_date=? WHERE project_id=?";
					$stmt3=$pdo->prepare($query_status);
					if($stmt3->execute([$date, $date, $get_id])) {
						echo"yehey";
					} else {
						$stmt3->errorInfo();
					}
			break;

			case 'pr_approve':
				$date=date('Y-m-d');
				$get_id=$_POST['id'];
				$status=4;
				$query_status = "UPDATE project_table SET pr_approved_date=?, status=? WHERE project_id=?";
				$stmt6=$pdo->prepare($query_status);
				if($stmt6->execute([$date,$status,$get_id])) {
				echo"success";
				} else {
					$stmt6->errorInfo();
				}
			break;

			case 'pr_disapprove':
				$date = '0000-00-00';
				$get_id=$_POST['id'];
				$query_status = "UPDATE project_table SET pr_approved_date=? WHERE project_id=?";
				$stmt7=$pdo->prepare($query_status);
				if($stmt7->execute([$date,$get_id])){
					echo "success";
				} else {
					echo $stmt7->errorInfo();
				}
			break;

			case 'approve_aoq':
				$date = date('Y-m-d');
				$get_id=$_POST['id'];
				$status=7;
				$query_status = "UPDATE project_table SET abstract_date=?, status=? WHERE project_id=?";
				$ac2=$pdo->prepare($query_status);
				if($ac2->execute([$date,$status,$get_id])){
					echo "yehey";
				}

			break;

			case 'disapprove_aoq':
				$date = '0000-00-00';
				$get_id=$_POST['id'];
				$status=6;
				$query_statuss = "UPDATE project_table SET status=?, abstract_date=? WHERE project_id=?";
				$ac3=$pdo->prepare($query_statuss);
				$ac3->execute([$status,$date,$get_id]);
			break;

			case 'approve_p_order':
				$date = date('Y-m-d');
				$get_id = $_POST['id'];
				$status=9;
				$query_status = "UPDATE project_table SET po_approved_date=?, status=? WHERE project_id=?";
				$key3=$pdo->prepare($query_status);
				$key3->execute([$date,$status,$get_id]);
			break;

			case 'disapprove_p_order':
				$date = '0000-00-00';
				$get_id = $_POST['id'];
				$status=8;
				$query_status = "UPDATE project_table SET status=?, po_approved_date=? WHERE project_id=?";
				$key4=$pdo->prepare($query_status);
				$key4->execute([$status,$date,$get_id]);
			break;

			case 'comment_ppmp':
				$get_id = $_POST['eventid'];
				$user = $_POST['s_username'];
				$module_input=$_POST['module'];
				$comment_value = $_POST['inputed_comment'];
				$update_comment = "INSERT INTO comment_table (project_id, module, comment, user_comment) VALUES (?,?,?,?)";
				$stmt4=$pdo->prepare($update_comment);
				if($stmt4->execute([$get_id,$module_input,$comment_value,$user])) {
					echo "yehey";
				} else {
					$stmt4->errorInfo();
				}
			break;

			case 'select_complete':
				# code...
				$status=10;
				$sel_query_complete="SELECT * FROM project_table WHERE delivery_status=? ORDER BY approval_date DESC";
				$executor=$pdo->prepare($sel_query_complete);
				$executor->execute([$status]);
				$outer_array=array();
				while($row=$executor->fetch(PDO::FETCH_ASSOC)) {
					$inner_array=array();
					$id_catcher = $row['project_id'];
					$title = $row['project_title'];
					$approval_date = $row['approval_date'];
					$pr_no=$row['pr_no'];
					$rfq_no=$row['rfq_no'];
					$po_no=$row['po_no'];
					$total_actual_cost=$row['total_actual_cost'];
					$formatted_AC=number_format($total_actual_cost, 2);
					$formattedDate=date('F d, Y', strtotime($approval_date));
					$total_estimated_budget = $row['total_estimated_budget'];
					$formattedNum=number_format($total_estimated_budget, 2);
					$year=date('y');
					$fullyear=date('Y');
					if ($pr_no == "") {
					$pr_no = "";
					}
					if ($pr_no != "") {
					$pr_no = "01-$fullyear-$pr_no-DO";
					}
					if ($rfq_no == "") {
					$rfq_no = "";
					}
					if ($rfq_no != "") {
					$rfq_no = $rfq_no . "&nbsp;";
					}
					if ($po_no == "") {
					$po_no = "";
					}
					if ($po_no != "") {
					$po_no = $po_no;
					}
					$inner_array[]='<a href="view_project_sds.php?eventid=' .$id_catcher. '">' .$title. '</a>';
					$inner_array[]=$formattedDate;
					$inner_array[]=$pr_no;
					$inner_array[]=$rfq_no;
					$inner_array[]=$po_no;
					$inner_array[]=$formattedNum;
					$inner_array[]=$formatted_AC;
					$outer_array[]=$inner_array;
				}
				echo json_encode($outer_array);
		break;

		case 'select_complete2':
			# code...
			$null_status=0;
			$var2 = '0000-00-00';
			$querytitle = "SELECT * FROM project_table WHERE approval_date<>? AND delivery_status=? ORDER BY approval_date DESC";
			$stmt=$pdo->prepare($querytitle);
			$stmt->execute([$var2, $null_status]);
			$sum = 0;
			$i = 0;
			$outer_array=array();
			while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$inner_array=array();
			$id_catcher = $row['project_id'];
			$title = $row['project_title'];
			$approval_date = $row['approval_date'];
			$pr_no=$row['pr_no'];
			$rfq_no=$row['rfq_no'];
			$po_no=$row['po_no'];
			$formattedDate=date('F d, Y', strtotime($approval_date));
			$total_estimated_budget = $row['total_estimated_budget'];
			$formattedNum=number_format($total_estimated_budget, 2);
			$year=date('y');
			$fullyear=date('Y');
			if ($pr_no == "") {
			$pr_no = "";
			}
			if ($pr_no != "") {
			$pr_no = "01-$fullyear-$pr_no-DO";
			}
			if ($rfq_no == "") {
			$rfq_no = "";
			}
			if ($rfq_no != "") {
			$rfq_no = $rfq_no . "&nbsp;";
			}
			if ($po_no == "") {
			$po_no = "";
			}
			if ($po_no != "") {
			$po_no = $po_no;
			}
			$inner_array[]='<a href="view_project_sds.php?eventid=' .$id_catcher. '">' .$title. '</a>';
			$inner_array[]=$formattedDate;
			$inner_array[]=$pr_no;
			$inner_array[]=$rfq_no;
			$inner_array[]=$po_no;
			$inner_array[]='<p style="text-align: right;">'.$formattedNum.'</p>';
			$outer_array[]=$inner_array;
			}
			echo json_encode($outer_array);
		break;

		case 'select_complete3':
			# code...
			$var1 = '0000-00-00';
			$querytitle = "SELECT * FROM project_table WHERE approval_date=? AND submit_date<>? GROUP BY project_title";
			$stmt1=$pdo->prepare($querytitle);
			$stmt1->execute([$var1, $var1]);
			$sum = 0;
			$i = 0;
			$outer_array=array();
			while ($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
				$inner_array=array();
				$id_catcher = $row['project_id'];
				$user_id = $row['user_id'];
			// GET THE USERNAME BASED ON USER ID
				$username = $_POST['s_username'];
				$query_select_user = "SELECT * FROM accounts WHERE user_id=?";
				$stmt2=$pdo->prepare($query_select_user);
				$stmt2->execute([$user_id]);
				$row_select = $stmt2->fetch(PDO::FETCH_ASSOC);
				$get_username = $row_select['user_name'];
			//END OF QUERY
				$title = $row['project_title'];
				$title_date = $row['date_created'];
				$total_estimated_budget = $row['total_estimated_budget'];
				$formattedNum = number_format($total_estimated_budget, 2);
				$inner_array[]='<a href="view_project_sds.php?eventid=' . $id_catcher . '">' . $title . '</a>';
				$inner_array[]=$title_date;
				$inner_array[]=$get_username;
				$inner_array[]=$formattedNum;
				$outer_array[]=$inner_array;
			}
			echo json_encode($outer_array);
		break;
		case 'sel_project':
			$get_id=$_POST['eventid'];
			$sel_query="SELECT * FROM project_table WHERE project_id=?";
			$sel_query_exec=$pdo->prepare($sel_query);
			$sel_query_exec->execute([$get_id]);
			$row_query=$sel_query_exec->fetch(PDO::FETCH_ASSOC);
			$abs_date=$row_query['abstract_date'];
			if ($abs_date=='0000-00-00'){
				echo "null";
			} else if ($abs_date!='0000-00-00') {
				echo $abs_date;
			} 
		break;
	}
?>
