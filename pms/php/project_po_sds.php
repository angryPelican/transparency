<h5>PURCHASE ORDER</h5>
<table class="table table-bordered display mb-4" id="table_po">
<thead>
	<tr>
		<th>Categories</th>
		<th>Quotation No</th>
		<th>Total ABC</th>
	</tr>
</thead>
<tbody>
	<?php
		include('php/connectdb.php');
			$common="Common Office Supplies";
			$get_id=$_GET['eventid'];
			$arr_c=array();
			$sel_proj="SELECT * FROM po_table WHERE project_id=? AND category<>?";
			$sel_proj_exec=$pdo->prepare($sel_proj);
			$sel_proj_exec->execute([$get_id,$common]);
			$year=date("y");
			while($row_sel=$sel_proj_exec->fetch(PDO::FETCH_ASSOC)) {
				$po_no=$row_sel['po_no'];
				$categ=$row_sel['category'];
				array_push($arr_c, $categ);
				echo"<tr>
					<td>$categ</td>
					<td>01-$year-$po_no-DO</td>";
				$getwinner="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
                $getwinner_exec=$pdo->prepare($getwinner);
                $getwinner_exec->execute([$get_id, $categ]);
                $row_winner=$getwinner_exec->fetchall(PDO::FETCH_ASSOC);
                $winner=$row_winner[0]['total_actual_price'];
					echo'<td align="right">'.number_format($winner, 2).'</td>';
					echo"</tr>";
			}
	?>
</tbody>
	<tfoot>
	<tr>
		<?php
		$get_user_id=$_SESSION['s_id'];
		$select_user="SELECT * FROM accounts WHERE user_id=?";
			$exec_select_user=$pdo->prepare($select_user);
			$exec_select_user->execute([$get_user_id]);
			$row_select_user=$exec_select_user->fetch(PDO::FETCH_ASSOC);
				$get_user_type=$row_select_user['user_type'];
			switch($get_user_type){
				case '1':
		$query_check_status = "SELECT * FROM project_table WHERE project_id=?";
		$key2=$pdo->prepare($query_check_status);
		$key2->execute([$get_id]);
		while($row_get_status=$key2->fetch(PDO::FETCH_ASSOC)){
		$po_approved_date = $row_get_status['po_approved_date'];
		}
		if($po_approved_date == '0000-00-00') {
		echo'<td style="border-left:0;">
			<span class="badge badge-info">For Approval</span>
		</td>
		<td></td>
		<td class="text-right" style="border-left:0;">
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<form action="" method="post">
						<li><button type="button" data-role="approve_p_order" class="btn btn-link btn_approve_po">Approve</button></li>
						<li><a href="#addCommentPO" data-toggle="modal" class="btn btn-link">Comment</a></li>
					</form>
				</ul>
			</div>
		</td>';
		} else if($po_approved_date != '0000-00-00') {
		echo'<td style="border-right:0;">
			<span class="badge badge-success">Approved</span>
		</td>
		<td></td>
		<td class="text-right" style="border-left:0;">
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<form action="" method="post">
						<li><button type="button" data-role="disapprove_p_order" class="btn btn-link btn-disapprove-po">Disapprove</button></li>
						<li><a href="#addCommentPO" data-toggle="modal" class="btn btn-link">Comment</a></li>
					</form>
				</ul>
			</div>
		</td>';
	}
		break;
		case '3':
		$query_check_status = "SELECT * FROM project_table WHERE project_id=?";
		$key2=$pdo->prepare($query_check_status);
		$key2->execute([$get_id]);
		while($row_get_status=$key2->fetch(PDO::FETCH_ASSOC)){
		$po_approved_date = $row_get_status['po_approved_date'];
		}
		if($po_approved_date == '0000-00-00') {
		echo'<td style="border-left:0;">
			<span class="badge badge-info">For Approval</span>
		</td>
		<td></td>
		<td class="text-right" style="border-right:0;">
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<form action="" method="post">
						<li><a href="#addCommentPO" data-toggle="modal" class="btn btn-link">Comment</a></li>
					</form>
				</ul>
			</div>
		</td>';
		} else if($po_approved_date != '0000-00-00') {
		echo'<td style="border-right:0;">
			<span class="badge badge-success">Approved</span>
		</td>
		<td></td>
		<td class="text-right" style="border-left:0;">
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				Action
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<form action="" method="post">
						<li><a href="#addCommentPO" data-toggle="modal" class="btn btn-link">Comment</a></li>
					</form>
				</ul>
			</div>
		</td>';
		}
		break;
	}
			?>
		</tr>
	</tfoot>
</table>
<!-- Modal -->
	<div id="addCommentPO" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="background: #0652DD; color: white;">
					<h5 class="modal-title">Add Comment</h5>
					<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form action="" method="post" id="formcomment">
						<div class="form-group">
							<label>Comment</label>
							<textarea class="form-control" id="comment_text_po" form="formcomment" rows="5" ></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" data-role="comment_ppmp" class="btn btn-default btn-default btn-comment-po" style="background: #0652DD; color: white;">Add</button>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">

	$('.btn-disapprove-po').click(function(){
		 var role=$(this).data("role");
		 var hidden_id="<?php echo $get_id; ?>";
		 	console.log(role);

		 	$.ajax({
	url: 'php/sds_webservice.php',
	type: 'POST',
	data: {
	tag: role,
	id: hidden_id
	},
	success : function(response){
	swal({
	title: "Disapproved",
	text: "PO was disapproved.",
	type: "error",
	timer: 1100,
	showConfirmButton: false
	})
	.then((value) => {
	location.reload();
	});
	//console.log(response);
	}
	});

});

	$('.btn_approve_po').click(function() {
		 var role=$(this).data("role");
		 var hidden_id="<?php echo $get_id; ?>";
		 	console.log(role);

		 	$.ajax({
	url: 'php/sds_webservice.php',
	type: 'POST',
	data: {
	tag: role,
	id: hidden_id
	},
	success : function(response){
	swal({
	title: "Approved",
	text: "PO was approved successfully!",
	type: "success",
	timer: 1100,
	showConfirmButton: false
	})
	.then((value) => {
	location.reload();
	});
	//console.log(response);
	}
		});
});

	$('.btn-comment-po').click(function() {
		var section="PO";
		var role=$(this).data("role");
		var hidden_id="<?php echo $_GET['eventid']; ?>";
		var username="<?php echo $_SESSION['s_username']; ?>";
		var comment_input=$('#comment_text_po').val();
		$('#addCommentPO').modal('toggle');
			$.ajax({
				url: 'php/sds_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				module: section,
				inputed_comment: comment_input,
				s_username: username,
				eventid: hidden_id
				},
				success : function(response){
					swal({
						title: "Success",
						text: "You Successfully commented!",
						type: "success",
						timer: 1100,
						showConfirmButton: false
					})
					.then((value) => {
						location.reload();
					});
				}
		});
	});

</script>