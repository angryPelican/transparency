<?php
include('connectdb.php');
//FIRST QUERY
$get_id = $_GET['eventid'];
$first_query = "SELECT * FROM items_table WHERE p_title_handler=?";
$stmt1=$pdo->prepare($first_query);
$stmt1->execute([$get_id]);
$count = $stmt1->rowCount();
//SECOND QUERY
$second_query = "SELECT * FROM items_table WHERE p_title_handler=?";
$stmt2=$pdo->prepare($second_query);
$stmt2->execute([$get_id]);
$i = 0;
$i1 = 0;
$i2 = 0;
		//loop each items
		while($row1 = $stmt2->fetch(PDO::FETCH_ASSOC)){
		$sx = $row1['p_title_name'];
		$s1 = $row1['p_description'];
		$s2 = $row1['p_quantity_size'];
		$s3 = $row1['p_unit'];
		$s4 = $row1['p_estimated_budget'];
		$code=$row1['code'];
		$formattedNum = number_format($s4, 2);
		echo'<tr>';
				//Code Column Rowspan
				if($i1 == 0) {
				echo'<td rowspan='.$count.'>'.$code.'</td>';
				$i1=1;
				}
				echo "<td>$s1</td>
				<td>$s2 - $s3</td>
				<td align='right'>$formattedNum</td>";
				//MODE OF PROCUREMENT QUERY
				$querymile = "SELECT * FROM project_table JOIN milestone_table ON project_table.project_id = milestone_table.milestone_handler WHERE project_id=?";
				$stmt3=$pdo->prepare($querymile);
				$stmt3->execute([$get_id]);
				while($row_get=$stmt3->fetch(PDO::FETCH_ASSOC)){
				$mode = $row_get['mode_of_procurement'];
				$milestone_check1 = $row_get['approval_of_request'];
				$milestone_check2 = $row_get['philgeps_posting'];
				$milestone_check3 = $row_get['pre_bid'];
				$milestone_check4 = $row_get['opening_of_bid'];
				$milestone_check5 = $row_get['post_qualification'];
				$milestone_check6 = $row_get['delivery_date'];
				$milestone_check7 = $row_get['notice_of_proceed'];
				$remarks=$row_get['source_of_fund'];
				if($i == 0){
				echo"<td rowspan='$count' align='center'>$mode</td>
				<td rowspan='$count'><ol>
				";

					if($milestone_check1 != '0000-00-00'){
						$formattedDate1=date('F d, Y', strtotime($milestone_check1));
						echo"<li>Purchase Request - $formattedDate1</li>";
					}
					else if($milestone_check1 == '0000-00-00'){

					}
					if($milestone_check2 != '0000-00-00'){
						$formattedDate2=date('F d, Y', strtotime($milestone_check2));
						echo"<li>Request for Quotation - $formattedDate2</li>";
					}
					else if($milestone_check2 == '0000-00-00'){
						
					}
					if($milestone_check3 != '0000-00-00'){
						$formattedDate3=date('F d, Y', strtotime($milestone_check3));
						echo"<li>Abstract of Quotation - $formattedDate3</li>";
					}
					else if($milestone_check3 == '0000-00-00'){
						
					}
					if($milestone_check4 != '0000-00-00'){
						$formattedDate4=date('F d, Y', strtotime($milestone_check4));
						echo"<li>Purchase Order - $formattedDate4</li>";
					}
					else if($milestone_check4 == '0000-00-00'){
						
					}
					if($milestone_check5 != '0000-00-00'){
						$formattedDate5=date('F d, Y', strtotime($milestone_check5));
						echo"<li>Delivery - $formattedDate5</li>";
					}
					else if($milestone_check5 == '0000-00-00'){
						
					}
				echo "</ol>
		</td>
		<td rowspan='$count'>$remarks</td>";
		$i=1;
			}
		}
	echo "</tr>";
	}
?>