<?php
include('Connect.php');
$get_id=$_GET['eventid'];
	function comment_so_sds () {
		global $pdo;
		global $get_id;
		$select_comment="SELECT * FROM comment_table WHERE project_id=?";
		$stmt1=$pdo->prepare($select_comment);
		$stmt1->execute([$get_id]);
		while($row=$stmt1->fetch(PDO::FETCH_ASSOC)):
			$comment=$row['comment'];
			$user_commented=$row['user_comment'];
			$module=$row['module'];
			if (!empty($comment)) {
			echo "<tr>
				<td><em>$user_commented</em></td>
				<td><small><em>$module</em></small> - $comment</td>
			</tr>";
			} else if (empty($comment)) {
			echo "<tr>
				<td>NONE</td>
				<td><em></em></td>
			</tr>";
			}
		endwhile;
	}
	function get_title () {
		global $pdo;
		$get_id=$_GET['eventid'];
		$select_project="SELECT * FROM project_table WHERE project_id=?";
		$key1=$pdo->prepare($select_project);
		if($key1->execute([$get_id])) {
			$row=$key1->fetch(PDO::FETCH_ASSOC);
			echo $row['project_title'];
		}
	}
	function get_rfq_date () {
		global $pdo;
		global $get_id;
		$query_rfq="SELECT * FROM rfq_table WHERE project_id=?";
		$query_rfq_exec=$pdo->prepare($query_rfq);
		$query_rfq_exec->execute([$get_id]);
		$count_rfq=$query_rfq_exec->rowCount();
		if($count_rfq==0) {
			//do nothing
		} else if ($count_rfq != 0) {
			while($row_rfq_exec=$query_rfq_exec->fetch(PDO::FETCH_ASSOC)) {
				$rfq_opening_date=$row_rfq_exec['opening_date'];
			}
			$sel_open_dates="SELECT * FROM rfq_table WHERE project_id=?";
			$sel_exec=$pdo->prepare($sel_open_dates);
			$sel_exec->execute([$get_id]);
			while($row_open=$sel_exec->fetch(PDO::FETCH_ASSOC)) {
				$var_open=$row_open['opening_date'];
				$var_cat=$row_open['category'];
				echo"$var_open" . "<br>";
			}
		}
	}
	function get_po () {
		global $pdo;
		global $get_id;
		$query_po="SELECT * FROM po_table WHERE project_id=?";
		$query_po_exec=$pdo->prepare($query_po);
		if($query_po_exec->execute([$get_id])) {
			$count_po=$query_po_exec->rowCount();
			if($count_po==0) {	
					//do nothing	
			} else if ($count_po != 0) {
				while($row_po=$query_po_exec->fetch(PDO::FETCH_ASSOC)) {
					$po_date=$row_po['po_date'];
					echo "$po_date" . 	"<br>";
				}
			}
		}
	}
	function get_delivery() {
		global $pdo;
		global $get_id;
		$query_po="SELECT * FROM project_table WHERE project_id=?";
		$query_po_exec=$pdo->prepare($query_po);
		$query_po_exec->execute([$get_id]);
		while($row_po=$query_po_exec->fetch(PDO::FETCH_ASSOC)) {
			$status=$row_po['delivery_status'];
			if($status==0) {
				//do nothing
			} else if ($status==11) {
				echo"Partial Delivery";
			} else if ($status==10) {
				echo"Delivered";
			}
		}
	}
	function comment_display() {
		global $pdo;
		global $get_id;
		$select_comment="SELECT * FROM comment_table WHERE project_id=?";
		$key2=$pdo->prepare($select_comment);
		if($key2->execute([$get_id])) {
			while($row=$key2->fetch(PDO::FETCH_ASSOC)):
				$comment=$row['comment'];
				$user_comment=$row['user_comment'];
			if($comment != ""){
			echo"<tr>
				<td>$comment</td>
				<td><em>$user_comment</em></td>
			</tr>";
			} else if ($comment == ""){
			echo"<tr>
				<td>NONE</td>
				<td><em>$user_comment</em></td>
					</tr>";
			}
			endwhile;
		}
	}
	function comment() {
		global $pdo;
		global $get_id;
		$username=$_SESSION['s_username'];
		$user_type=$_SESSION['s_user_type'];
		if(isset($_POST['comment'])) {
			$get_id=$_GET['eventid'];
			$comment_value=$_POST['inputed_comment'];
			$module="ppmp";
			$update_comment="INSERT INTO comment_table (project_id, comment, module, user_comment) VALUES (?,?,?,?)";
			$key4=$pdo->prepare($update_comment);
			$key4->execute([$get_id, $comment_value, $module, $username]);
				echo "<meta http-equiv='refresh' content='0'>";
		}
	}
?>