<h5>REQUEST FOR QUOTATION</h5>
<table class="table table-bordered mb-4" id="table_rfq">
	<form action="" method="post" id="form1" style="table-layout: auto; width: 100%;">
		<tr>
			<td width="60%" colspan="2">Date Created: <b><i><?php
				$get_id = $_GET['eventid'];
				$get_date="SELECT * FROM project_table WHERE project_id=?";
				$res=$pdo->prepare($get_date);
				$res->execute([$get_id]);
				while($row=$res->fetch(PDO::FETCH_ASSOC)):
				$rfq_date=$row['rfq_date'];
				endwhile;
				if($rfq_date != '0000-00-00'){
				echo date('F d, Y', strtotime($rfq_date));
				}else if($rfq_date = '0000-00-00'){
				echo "";
				}
			?></i></b></td>
			<td width='40%'>Opening Date</td>
		</tr>
		<?php
			$sel_rfq="SELECT * FROM rfq_table WHERE project_id=?";
			$sel_rfq_exec=$pdo->prepare($sel_rfq);
			$sel_rfq_exec->execute([$get_id]);
			$row_count=$sel_rfq_exec->rowCount();
			$year=date("y");

				while($row_rfq=$sel_rfq_exec->fetch(PDO::FETCH_ASSOC)) {
					$rfq_no=$row_rfq['rfq_number'];
					$categ=$row_rfq['category'];
					$open_date=$row_rfq['opening_date'];
					echo"<tr>
							<td><p id='cats'>$categ</p></td>
							<td>DepEd-$year-NCR-QN-$rfq_no</td>
							<td><input type='date' id='pol' data-categ='$categ' data-id='$rfq_no' data-role='set_opening_date' class='m-0 form-control form-control-sm datepicker' value='$open_date' disabled>
						</td>";
						echo"</tr>";
				}
		?>
	</form>
</div>
<tfoot>
<tr>
	<td style="border-right: 0;">
		<span class="badge badge-success">Approved</span>
	</td>
			<td style="border-right: 0; border-left: 0;"></td>
			<td class="text-right" style="border-left: 0;">
				<div class="dropdown">
					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Action
					<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li><a href="#" name="comment" class="btn btn-link" data-toggle="modal" data-target="#addCommentRFQ">Comment</a></li>
					</ul>
				</div>
			</td>
		</tr>
	</tfoot>
</table>

<!--modal-->
	<div id="addCommentRFQ" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="background: #0652DD; color: white;">
					<h5 class="modal-title">Add Comment</h5>
					<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form action="" method="post" id="formcomment">
						<div class="form-group">
							<label>Comment</label>
							<textarea class="form-control" id="comment_text_rfq" form="formcomment" rows="5" ></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" data-role="comment_ppmp" form="formcomment" class="btn btn-default btn-default btn-comment-rfq" style="background: #0652DD; color: white;">Add</button>
				</div>
			</div>
		</div>
	</div>
	
<script type="text/javascript">
	$('.btn-comment-rfq').click(function() {
		var section="RFQ";
		var role=$(this).data("role");
		var hidden_id="<?php echo $_GET['eventid']; ?>";
		var username="<?php echo $_SESSION['s_username']; ?>";
		var comment_input=$('#comment_text_rfq').val();
		$('#addCommentRFQ').modal('toggle');
			$.ajax({
				url: 'php/sds_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				module: section,
				inputed_comment: comment_input,
				s_username: username,
				eventid: hidden_id
				},
				success : function(response){
					swal({
						title: "Success",
						text: "You Successfully commented!",
						type: "success",
						timer: 1100,
						showConfirmButton: false
					})
					.then((value) => {
						location.reload();
					});
				}
			});
	});
</script>