<div class="card border-dark">
	<div class="card-header text-white bg-dark">
		<h6 class="m-0">Project Procurement Management Plan</h6>
	</div>
	<div class="card-body table-responsive p-0">
		<table class="table table-striped table-bordered display m-0" id="table_ppmp">
			<thead>
				<tr>
					<th>Code</th>
					<th>General Description</th>
					<th>Qty/Size</th>
					<th>Estimated Budget</th>
					<th>Mode of Procurement</th>
					<th>Milestone of Activities</th>
					<th>Remarks</th>
				</tr>
			</thead>
			<tbody>
				<?php
					include('view_items_so.php');	
				?>
			</tbody>
		</table>
	</div>
	<div class="card-footer">
		<div class="row">
		<?php
			include('check_purpose.php');
		?>
		</div>
	</div>
</div>
<!--modal-->
<div id="addPurpose" class="modal fade" role="dialog">
	<form action="" method="post">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Add Purpose</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control purpose" name="purpose" rows="5"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" name="save" data-role="add_purpose" class="btn btn-success btn_add_purpose">Add</button>
				</div>
			</div>
		</div>
	</form>
</div>
<script type="text/javascript">
	$(".btn_add_purpose").click(function() {
		var role=$(this).data("role");
		var purpose=$(".purpose").val();
		var hidden_id="<?php echo $_GET['eventid']; ?>";
		$('#addPurpose').modal('toggle');

		$.ajax({
			url: 'php/so_webservice.php',
			type: 'POST',
			data: {
			tag: role,
			eventid: hidden_id,
			purpose: purpose
			},
			success : function(response){
			swal({
			title: "Success",
			text: "PR was generated successfully!",
			type: "success",
			timer: 1100,
			showConfirmButton: false
			})
			.then((value) => {
			location.reload();
			});
			//console.log(response);
			}
		});

	});
</script>