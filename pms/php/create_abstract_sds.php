<h5 style="text-transform: uppercase;">ABSTRACT OF QUOTATION</h5>
<style type="text/css">
	.modal-lg {
    max-width: 65% !important;
}
input[type="number"]::-webkit-outer-spin-button,
input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input[type="number"] {
    -moz-appearance: textfield;
}
</style>
<table class="table table-striped table-bordered display mb-4" id="table_aoq">
	<thead>
		<tr>
			<th width="50%">Categories</th>
			<th width="20%">Supplier</th>
			<th>Amount</th><!--AUTO-GENERATED, AUTO INCREMENT-->
		</tr>
	</thead>
	<tbody id="tb_dark">
		<?php
			include('php/connectdb.php');
			$get_id=$_GET['eventid'];
			$common="Common Office Supplies";
			$sel_items="SELECT * FROM items_table WHERE p_title_handler=? AND classification<>? GROUP BY classification";
			$sel_exec=$pdo->prepare($sel_items);
			$sel_exec->execute([$get_id,$common]);
			$i=0;
			$cc=0;
			$arr_c=array();
			
			$merger=array();
				while($row=$sel_exec->fetch(PDO::FETCH_ASSOC)) {
					$categories=$row['classification'];
					array_push($arr_c, $categories);
					$count_cat=count($arr_c);
					echo"<tr>
							<td><a href='#' data-toggle='modal' id='abs_exec' data-cat='$categories' data-target='#modal_quote'>$categories</a></td>";
				for ($i_c=0; $i_c < $count_cat; $i_c++) {
					$sel_abs="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
					$sel_abs_exec=$pdo->prepare($sel_abs);
					$sel_abs_exec->execute([$get_id,$categories]);
				}		
					$rowcount=$sel_abs_exec->rowCount();
					$i=0;
						if($rowcount==0) {
							echo"<td></td>
									<td></td>
								</tr>";
						} else if($rowcount!=0) {
								$row_abs=$sel_abs_exec->fetchall(PDO::FETCH_ASSOC);
								$json_decode=json_decode($row_abs[0]['supplier_data'], true);
								$total=$row_abs[0]['total_actual_price'];
							$q_sel_items="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
							$q_exec=$pdo->prepare($q_sel_items);
							$q_exec->execute([$get_id, $categories]);
							$cnt_row_items=$q_exec->rowCount();
							while($rs_quan=$q_exec->fetch(PDO::FETCH_ASSOC)) {
								$arr_q=array();
								$quantity=$rs_quan['p_quantity_size'];
								array_push($arr_q, $quantity);
								$json_encode=json_decode(json_encode(array_values($json_decode)), true);
								$arr1=array();
								$arr2=array();
								$arr3=array();
									for ($i=0; $i < $cnt_row_items; $i++) {
										$arr_winner=array();
										$arr_names=array();
										$prices1=$json_encode[0]['supp_price'][$i];
										$prices2=$json_encode[1]['supp_price'][$i];
										$prices3=$json_encode[2]['supp_price'][$i];
										$names1=$json_encode[0]['supp_name'];
										$names2=$json_encode[1]['supp_name'];
										$names3=$json_encode[2]['supp_name'];
										$arr1[]=$prices1;
										$arr2[]=$prices2;
										$arr3[]=$prices3;
										$summed1=array_sum($arr1);
										$summed2=array_sum($arr2);
										$summed3=array_sum($arr3);
										$arr_names[$names1]=$summed1;
										$arr_names[$names2]=$summed2;
										$arr_names[$names3]=$summed3;
									}
								}
								$xxsupp=array_keys($arr_names, min(array_filter($arr_names)));
								$total=number_format($total, 2);
										echo'<td>'.$xxsupp[0].'</td>
										<td align="right">'.$total.'</td>';
									echo"</tr>";
							}
						}
						
		?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2" style="border-right: none;" id="td_dark">
				<script type="text/javascript">
					var role="sel_project";
					var id="<?php echo $_GET['eventid']; ?>";
					$.ajax({
						url: "php/sds_webservice.php",
						type: "POST",
						data: {
							tag: role,
							eventid: id,
						},
						success: function(response) {
							console.log(response);
							if(response=="null"){
								document.getElementById('td_dark').innerHTML += "<span class='badge badge-info'>For Approval</span>";
							} else if(response!="null") {
								document.getElementById('td_dark').innerHTML += "<span class='badge badge-success'>Approved</span>";
								$("#approve").hide();
							}
						}
					});
				</script>
			</td>
			<td style="border-left: none;" align="right">
				<div class="dropdown show">
					<button class="btn btn-default dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
					<ul class="dropdown-menu" id="menu" aria-labelledby="dropdownMenuLink">
						<li id="approve_list"><a class='btn btn-link' href='#' id='approve'>Approve</a></li>
						<li id="disapprove_list"><a class="btn btn-link" href="#" id="disapprove">Disapprove</a></li>
						<li><a class="btn btn-link" href="#" data-target='#comment_modal' data-toggle='modal'>Comment</a></li>
					</ul>
				</div>
			</td>
		</tr>
	</tfoot>
</table>
<script type="text/javascript">
	var role="check_project";
	var id="<?php echo $_GET['eventid']; ?>";
	$.ajax({
		url: "php/sds_webservice.php",
		type: "post",
		data: {
			tag: role,
			proj_id: id,
		},
		success: function(response){
			console.log(response);
			if(response=="approved") {
				$('#dropdownMenuLink').click(function(){
					$('#approve').hide();
				});
			} else if(response=="for approval") {
				$('#approve').show();
			}
		}
	});
		var role="check_approval";
		var id="<?php echo $_GET['eventid']; ?>";
			$.ajax({
				url: "php/sds_webservice.php",
				type: "POST",
				data: {
					tag: role,
					proj_id: id,
				},
				success: function(response){
					console.log(response);
					if(response=="not eligible") {
						$('#approve').hide();
						$('#disapprove').hide();
					} else if (response=="eligible") {
						$('#approve').show();
						$('#disapprove').show();
					}
				}
			});
	$('#approve').click(function(){
		var role="approve_aoq";
		var id="<?php echo $_GET['eventid']; ?>";
		$.ajax({
			url: "php/sds_webservice.php",
			type: "post",
			data: {
				tag: role,
				id: id,
			},
			success: function(response){
				swal({
					title: "Successfully approved AOQ.",
					type: "success",
					timer: 1100,
					showConfirmButton: false
				})
				.then((value) => {
					location.reload();
				});
			}
		});
	});
	$('#disapprove').click(function(){
		var role="disapprove_aoq";
		var id="<?php echo $_GET['eventid']; ?>";
		$.ajax({
			url: "php/sds_webservice.php",
			type: "post",
			data: {
				tag: role,
				id: id,
			},
			success: function(response){
				swal({
					title: 'AOQ was disapproved',
					type: "success",
					timer: 1100,
					showConfirmButton: false
				})
				.then((value) => {
					location.reload();
				});
			}
		});
	});
	$('a[id="abs_exec"]').click(function(){
		var cat=$(this).data('cat');
			$('#dynamic_cat').text(cat);
			$('#hidden_cat').val(cat);
		document.getElementById("tbod-dark").html("");
	});
</script>
<!-- Comment Modal -->
<div class="modal fade" id="comment_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-white" style="background: #0652DD; color: white;">
        <h5 class="modal-title" id="exampleModalLabel">Add Comment</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="container">
	        <div class="form-group row">
	        	Comment
	        </div>
	        <div class="form-group row">
	        	<textarea id="cmnt_abs" class="form-control form-control-sm"></textarea>
	        </div>
	    </div>
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-comment-abstract" data-role="comment_ppmp" style="background: #0652DD; color: white;">Add</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<script type="text/javascript">
	$('.btn-comment-abstract').click(function(){
		var section="AOQ";
		var role=$(this).data("role");
		var hidden_id="<?php echo $_GET['eventid']; ?>";
		var username="<?php echo $_SESSION['s_username']; ?>";
		var comment_input=$('#cmnt_abs').val();
		$('#comment_modal').modal('toggle');
			$.ajax({
				url: 'php/so_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				module: section,
				inputed_comment: comment_input,
				s_username: username,
				eventid: hidden_id
				},
				success : function(response){
					// console.log(response);
					swal({
						title: "Success",
						text: "You Successfully commented!",
						type: "success",
						timer: 1100,
						showConfirmButton: false
					})
					.then((value) => {
						location.reload();
					});
				}
			});
	});
</script>
<div class="modal fade" id="modal_quote" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header bg-dark text-white">
			<h5 class="modal-title" id="exampleModalLongTitle">Add Quotations - <input type="hidden" value="" name="hidden_cat" id="hidden_cat"><span id="dynamic_cat"></span></h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<form id="the-form">
				<table class="table table-bordered table-striped table-sm" style="margin: 0; table-layout: auto; width: 100%;">
					<thead id="thead_dark">
						<script type="text/javascript">
							$(document).ready(function(){
								$('#modal_quote').on('show.bs.modal', function (e) {
									$('#thead_dark').html("");
									var role="sel_existing_tab";
									var get_cat=$('#hidden_cat').val();
									var get_id="<?php echo $_GET['eventid']; ?>";
									$.ajax({
										url: "php/so_webservice.php",
										type: 'POST',
										data: {
											tag: role,
											proj_id: get_id,
											hidden_cat: get_cat
										}, 
										success: function(response){	
											// console.log(response);
											if(response=="none") {
												document.getElementById("thead_dark").innerHTML += "<tr><th width='40%' rowspan='2' style='vertical-align: middle;'>Items</th><th width='60%' colspan='3' style='text-align: center;'>Suppliers</th></tr><tr><th><input class='form-control form-control-sm form-inline' type='text' placeholder='Type Supplier Here ..' id='supp_1' required><div class='custom-control custom-checkbox my-1 mr-sm-2'><input type='checkbox' class='custom-control-input' id='inline1'><label class='custom-control-label' for='inline1'>DNMS(s)</label></div></th><th><input class='form-control form-control-sm' type='text' placeholder='Type Supplier Here ..' id='supp_2' required><div class='custom-control custom-checkbox my-1 mr-sm-2'><input type='checkbox' class='custom-control-input' id='inline2'><label class='custom-control-label' for='inline2'>DNMS(s)</label></div></th><th><input class='form-control form-control-sm' type='text' placeholder='Type Supplier Here ..' id='supp_3' required><div class='custom-control custom-checkbox my-1 mr-sm-2'><input type='checkbox' class='custom-control-input' id='inline3'><label class='custom-control-label' for='inline3'>DNMS(s)</label></div></th></tr>";
												$('input[name="supp_p1"]').each(function() {
													$(this).attr('disabled');
													$(this).val("");
												});
												$('input[name="supp_p2"]').each(function() {
													$(this).attr('disabled');
													$(this).val("");
												});
												$('input[name="supp_p3"]').each(function() {
													$(this).attr('disabled');
													$(this).val("");
												});
											} else {
												var datas=JSON.parse(response);
												var flex1=datas[0].supp_name;
												var flex2=datas[1].supp_name;
												var flex3=datas[2].supp_name;
												$('input[name="supp_p1"]').each(function() {
													$(this).attr('disabled', true);
												});
												$('input[name="supp_p2"]').each(function() {
													$(this).attr('disabled', true);
												});
												$('input[name="supp_p3"]').each(function() {
													$(this).attr('disabled', true);
												});
												document.getElementById("thead_dark").innerHTML += "<tr><th width='40%' rowspan='2' style='vertical-align: middle;'>Items</th><th width='60%' colspan='3' style='text-align: center;'>Suppliers</th></tr><tr><th><input class='form-control form-control-sm form-inline data1' type='text' placeholder='Type Supplier Here ..' id='supp_1' value='" + flex1 + "' required><div class='custom-control custom-checkbox my-1 mr-sm-2'><input type='checkbox' class='custom-control-input' id='inline1'><label class='custom-control-label' for='inline1'>DNMS(s)</label></div></th><th><input class='form-control form-control-sm data2' type='text' placeholder='Type Supplier Here ..' id='supp_2' value='" + flex2 + "' required><div class='custom-control custom-checkbox my-1 mr-sm-2'><input type='checkbox' class='custom-control-input' id='inline2'><label class='custom-control-label' for='inline2'>DNMS(s)</label></div></th><th><input class='form-control form-control-sm data3' type='text' placeholder='Type Supplier Here ..' id='supp_3' value='" + flex3 + "' required><div class='custom-control custom-checkbox my-1 mr-sm-2'><input type='checkbox' class='custom-control-input' id='inline3'><label class='custom-control-label' for='inline3'>DNMS(s)</label></div></th></tr>";
														$('.data1').attr('disabled', true);
														$('#inline1').attr('checked','checked');
														$('.data2').attr('disabled', true);
														$('#inline2').attr('checked','checked');
														$('.data3').attr('disabled', true);
														$('#inline3').attr('checked','checked');
											}
										}
									});
								});
							});
						</script>
						</thead>
						<tbody id="tbod-dark">
							<script type="text/javascript">
								$('#modal_quote').on('show.bs.modal', function (e) {
									$('#tbod-dark').html("");
									var role="sel_items_by_cat";
									var id="<?php echo $_GET['eventid']; ?>";
									var categ=$('#hidden_cat').val();
										$.ajax({
											url: "php/so_webservice.php",
											type: "POST",
											data: {
												tag: role,
												proj_id: id,
												category: categ,
											}, success: function(response){
												var data=JSON.parse(response);
												for (var key in data){
													var items=data[key].p_description;
													var item_id=data[key].item_id;
													var quantity=data[key].p_quantity_size;
													document.getElementById("tbod-dark").innerHTML += "<tr id='"+ items +"' data-items='"+ items +"'><td><input type='hidden' id='item_name' value='"+ items +"'>"+ items + "<small> x " + quantity +"</small></td><td><input class='form-control form-control-sm' type='number' data-item='"+ items +"' id='star' step='any' style='text-align:right;' name='supp_p1' required></td><td><input class='form-control form-control-sm' type='number' data-item='"+ items +"' id='star' step='any' data-item='"+ items +"' style='text-align:right;' name='supp_p2' required></td><td><input class='form-control form-control-sm' type='number' data-item='"+ items +"' id='star' step='any' data-item='"+ items +"' style='text-align:right;' name='supp_p3' required></td></tr>";
												}
											}
										});
								});
							</script>
							<script type="text/javascript">
								$(document).ready(function(){
									$('#modal_quote').on('show.bs.modal', function (e) {
										var get_cat=$('#hidden_cat').val();
										var role="sel_items_abs";
										var get_id="<?php echo $_GET['eventid']; ?>";
										$.ajax({
											url: "php/so_webservice.php",
											type: "POST",
											data: {
												tag: role,
												hidden_cat: get_cat,
												proj_id: get_id
											}, success: function(response) {
												if(response!="none") {
													var data=JSON.parse(response);
													// console.log(data[2].supp_price);
													var prices1=data[0].supp_price;
													// console.log(prices1);
													var prices2=data[1].supp_price;
													var prices3=data[2].supp_price;
													var ix1 = 0;
													var ix2 = 0;
													var ix3 = 0;
													$('input[name="supp_p1"]').each(function() {
														ix1++;
															for (var i = 0; i < ix1; i++) {
																if(prices1[i]=="no data available in json"){
																	$(this).attr("disabled", true);
																	$()
																} else {
															// alert(prices[i]);
																	$(this).val(prices1[i]);
																}
														}
													});
													$('input[name="supp_p2"]').each(function() {
														ix2++;
															for (var i = 0; i < ix2; i++) {
															if(prices2[i]=="no data available in json"){
																	$(this).attr("disabled", true);
																} else {
															// alert(prices[i]);
																	$(this).val(prices2[i]);
																}
														}
													});
													$('input[name="supp_p3"]').each(function() {
														ix3++;
															for (var i = 0; i < ix3; i++) {
															if(prices3[i]=="no data available in json"){
																	$(this).attr("disabled", true);
																} else {
															// alert(prices[i]);
																	$(this).val(prices3[i]);
																}
														}
													});
												}
											}
										});
									});
								});
							</script>
						</tbody>
					</table>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
