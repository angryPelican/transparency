<!--PROJECT TITLE-->
<div class="card border-dark">
	<div class="clearfix card-header text-white bg-dark">
		<h5 style="margin: 0;">Project Procurement Management Plan</h5>
	</div>
	<div class="card-body p-0">
			<table class="table table-striped table-bordered table-sm" id="table_ppmp" style="margin:0; font-size: 1em;">
				<thead>
					<tr>
						<th>Code</th>
						<th width="300">General Description</th>
						<th>Qty/Size</th>
						<th>Estimated Budget</th>
						<th>Mode of Procurement</th>
						<th>Milestone of Activities</th>
						<th>Remarks</th>
					</tr>
				</thead>
				<?php
					include('view_items_sds.php');
				?>
				<tfoot>
				<tr>
					<td colspan="5" id="badge" style="border-right: 0;"></td>
					<td colspan="2" class="text-right" style="border-left:0;">					
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							Action
							<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
								<li id="approve_list"><button type="button" name="approve" data-role="ppmp_approve" class="btn btn-link btn-approve">Approve</button></li>
								<li id="disapprove_list"><button type="button" class="btn btn-link btn-disapprove" data-role="ppmp_disapprove">Disapprove</button></li>
								<li><a href="#addCommentPPMP" data-toggle="modal" class="btn btn-link">Comment</a></li>
							</ul>
						</div>
					</td>
				</tr>
					<script type="text/javascript">
						var role="sel_approval_date";
						var user_type="<?php echo $_SESSION['s_user_type']; ?>";
						var id="<?php echo $_GET['eventid']; ?>";
						$.ajax({
							url: "php/sds_webservice.php",
							type: "post",
							data: {
								tag: role,
								eventid: id,
							},
							success: function(response){
								console.log(response);
								switch(user_type) {
									case '3':
										if(response=="null" || response!="null"){
											$('#approve_list').hide();
											$('#disapprove_list').hide();
										}
									break;
									case '1':
										if(response=="null") {
											document.getElementById("badge").innerHTML += '<span class="badge badge-pill badge-info">For Approval</span>';
										} else if (response!="null") {
											$('#approve_list').hide();
											document.getElementById("badge").innerHTML += '<span class="badge badge-pill badge-success">Approved</span>';
										}
								}
							}
						});
					</script>
				</tfoot>
			</table>
		</div>
	</div>

<!-- Modal -->
	<div id="addCommentPPMP" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="background: #0652DD; color: white;">
					<h5 class="modal-title">Add Comment</h5>
					<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form action="" method="post" id="formcomment">
						<div class="form-group">
							<label>Comment</label>
							<textarea class="form-control" id="comment_text" form="formcomment" name="inputed_comment" rows="5" ></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" form="formcomment" data-role="comment_ppmp" class="btn btn-default btn-default btn-comment-ppmp" style="background: #0652DD; color: white;">Add</button>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	$('.btn-comment-ppmp').click(function() {
		var section="PPMP";
		var role=$(this).data("role");
		var hidden_id="<?php echo $_GET['eventid']; ?>";
		var username="<?php echo $_SESSION['s_username']; ?>";
		var comment_input=$('#comment_text').val();
		$('#addCommentPPMP').modal('toggle');
			$.ajax({
				url: 'php/sds_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				module: section,
				inputed_comment: comment_input,
				s_username: username,
				eventid: hidden_id
				},
				success : function(response){
					swal({
						title: "Success",
						text: "You Successfully commented!",
						type: "success",
						timer: 1100,
						showConfirmButton: false
					})
					.then((value) => {
						location.reload();
					});
				}
			});
	});


	$(".btn-approve").click(function() {
				var role=$(this).data("role");
				var hidden_id="<?php echo $get_id; ?>";
				console.log(hidden_id);
				$.ajax({
	url: 'php/sds_webservice.php',
	type: 'POST',
	data: {
	tag: role,
	id: hidden_id
	},
	success : function(response){
	swal({
	title: "Approved",
	text: "PPMP was approved successfully!",
	type: "success",
	timer: 1100,
	showConfirmButton: false
	})
	.then((value) => {
	location.reload();
	});
	//console.log(response);
	}
});//end of ajax
});//end of click function

$(".btn-disapprove").click(function() {

	var role=$(this).data("role");
	var hidden_id="<?php echo $_GET['eventid']; ?>";
	//console.log(role);

		$.ajax({
	url: 'php/sds_webservice.php',
	type: 'POST',
	data: {
	tag: role,
	id: hidden_id
	},
	success : function(response){
	swal({
		title: "Dissaproved",
		text: "PPMP was disapproved.",
		type: "error",
		timer: 1100,
		showConfirmButton: false
	})
	.then((value) => {
		window.location="dashboard_sds.php";
	});
	// console.log(response);
	}
});//end of ajax*/

});//end of click function

</script>