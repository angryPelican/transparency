<?php
include('php/connectdb.php');
$get_id = $_GET['eventid'];
$get_date="SELECT * FROM project_table WHERE project_id=?";
$key1=$pdo->prepare($get_date);
$key1->execute([$get_id]);
while($row=$key1->fetch(PDO::FETCH_ASSOC)):
// DATA FETCHED FROM DATABASE
$date_created=$row['date_created'];
$submit_date=$row['submit_date'];
$approval_date=$row['approval_date'];
$pr_date=$row['pr_date'];
$pr_approved_date=$row['pr_approved_date'];
$rfq_date=$row['rfq_date'];
$abstract_date=$row['abstract_date'];
$po_date=$row['po_date'];
$po_approved_date=$row['po_approved_date'];
$delivery_status=$row['delivery_status'];
//numbers
$get_pr=$row['pr_no'];
$get_rfq=$row['rfq_no'];
$get_po=$row['po_no'];
endwhile;
if($delivery_status==10) {
	$delivery_dates="Delivered";
} else if ($delivery_status==11) {
	$delivery_dates="Partial Delivery";
} else if ($delivery_status!=10 || $delivery_status!=11){
	$delivery_dates="";
}
if ($date_created != '0000-00-00'){
// IF SUBMIT DATE HAS A VALUE
$echo_date_created=date("d-m-Y", strtotime($date_created));
}
if ($date_created == '0000-00-00'){
// IF SUBMIT DATE HAS NOTHING
$echo_date_created=null;
}
if ($submit_date != '0000-00-00'){
// IF SUBMIT DATE HAS A VALUE
$echo_submit_date=date("d-m-Y", strtotime($submit_date));
}
if ($submit_date == '0000-00-00'){
// IF SUBMIT DATE HAS NOTHING
$echo_submit_date=null;
}
if ($approval_date != '0000-00-00'){
// IF APPROVAL DATE HAS A VALUE
$approval_date=date("d-m-Y", strtotime($approval_date));
}
if ($approval_date == '0000-00-00'){
// IF APPROVAL DATE HAS NOTHING
$approval_date=null;
}
if ($pr_approved_date != '0000-00-00'){
$pr_approved_date=date("d-m-Y", strtotime($pr_approved_date));
}
if ($pr_approved_date == '0000-00-00'){
$pr_approved_date="";
}
if ($pr_date != '0000-00-00'){
$pr_date=date("d-m-Y", strtotime($pr_date));
}
if ($pr_date == '0000-00-00'){
$pr_date="";
}
if ($rfq_date != '0000-00-00'){
$rfq_dates=date("d-m-Y", strtotime($rfq_date));
}
if ($rfq_date == '0000-00-00'){
$rfq_dates="";
}
if ($po_date != '0000-00-00'){
$po_dates=date("d-m-Y", strtotime($po_date));
}
if ($po_date == '0000-00-00') {
$po_dates="";
}
if ($po_approved_date != '0000-00-00') {
$po_approved_dates=date("d-m-Y", strtotime($po_approved_date));
}
if ($po_approved_date == '0000-00-00') {
$po_approved_dates="";
}
if ($abstract_date != '0000-00-00') {
$abstract_dates=date("d-m-Y", strtotime($abstract_date));
}
if ($abstract_date == '0000-00-00') {
$abstract_dates="";
}
$milestone_target_date="SELECT * FROM milestone_table WHERE milestone_handler=?";
$milestone_target_date_query=$pdo->prepare($milestone_target_date);
$milestone_target_date_query->execute([$get_id]);
while($row_mile=$milestone_target_date_query->fetch(PDO::FETCH_ASSOC)) {
$date11=$row_mile['approval_of_request'];//pr
$date22=$row_mile['philgeps_posting'];//rfq
$date33=$row_mile['pre_bid'];//abs
$date44=$row_mile['opening_of_bid'];//po
$date55=$row_mile['post_qualification'];//delivery
}
if($date11 == '0000-00-00'){
$date1="";
}
if($date11 != '0000-00-00'){
$date1=date("d-m-Y", strtotime($date11));
}
if($date22 == '0000-00-00'){
$date2="";
}
if($date22 != '0000-00-00'){
$date2=date("d-m-Y", strtotime($date22));
}
if($date33 == '0000-00-00'){
$date3="";
}
if($date33 != '0000-00-00'){
$date3=date("d-m-Y", strtotime($date33));
}
if($date44 == '0000-00-00'){
$date4="";
}
if($date44 != '0000-00-00'){
$date4=date("d-m-Y", strtotime($date44));
}
if($date55 == '0000-00-00'){
$date5="";
}
if($date55 != '0000-00-00'){
$date5=date("d-m-Y", strtotime($date55));
}
?>