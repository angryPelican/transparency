<!--PROJECT TITLE-->
<h5 style="text-transform: uppercase;" class="mt-4">Purchase Request</h5>
<table class="table table-striped table-bordered display mb-4" id="table_pr">
	<thead>
		<tr>
			<?php
			include ('dynamic_title.php');
				echo'<td colspan="2" rowspan="2">Office/Section<br /><b>'.$get_unit.'	</b></td>';
			?>
			<td colspan="3">PR No. <b><?php include ('dynamic_dates.php');
				echo "DepEd-";
				echo date("y");
				echo "-";
				echo "NCR-PR-" . $get_pr;?></b></td><!--AUTO-GENERATED, AUTO INCREMENT-->
				<td colspan="3" rowspan="2">Date <br />
					<?php
					echo "<b>";
					include 'get_dates.php';
					if ($pr_date == '0000-00-00') {
					} else if ($pr_date != '0000-00-00') {
					echo date('F d, Y', strtotime($pr_date));
					}
					echo "</b>";
					?>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b><?php include 'php/dynamic_title.php';?></b></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th width="100">Stock No.</th>
				<th>Unit</th>
				<th colspan="3">Item Description</th>
				<th>Quantity</th>
				<th>Unit Cost</th>
			</tr>
			<?php
				include('connectdb.php');
				$common="Common Office Supplies";
				//FIRST QUERY
				$get_id = $_GET['eventid'];
				$first_query = "SELECT * FROM items_table WHERE p_title_handler=? AND classification<>?";
				$stmt1=$pdo->prepare($first_query);
				$stmt1->execute([$get_id,$common]);
				$count = $stmt1->rowCount();
				//SECOND QUERY
				$second_query = "SELECT * FROM items_table WHERE p_title_handler=? AND classification<>?";
				$stmt2=$pdo->prepare($second_query);
				$stmt2->execute([$get_id,$common]);
				$i = 0;
				$i1 = 0;
				$i2 = 0;
				$sum = 0;
						//loop each items
						while($row1 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
						$sx = $row1['p_title_name'];
						$s1 = $row1['p_description'];
						$s2 = $row1['p_quantity_size'];
						$s3 = $row1['p_unit'];
						$s4 = $row1['p_estimated_budget'];
						$formattedNum = number_format($s4, 2);
						$sum = $sum + $row1['p_estimated_budget'];
						$formattedNum1 = number_format($sum, 2);
						echo'<tr>';
								//Code Column Rowspan
								if($i1 == 0) {
								echo'<td rowspan='.$count.'></td>';
								$i1=1;
								}
								echo "<td>$s3</td>
								<td colspan='3'>$s1</td>
								<td>$s2</td>
								<td class='text-right'>$formattedNum</td>";
						}
				echo "</tr>";
			?>
			<tr>
				<td colspan="3"><b>Purpose</b></td>
				<td colspan="3">
				<?php
					$get_id = $_GET['eventid'];
					$get_date="SELECT * FROM project_table WHERE project_id=?";
					$stmt1=$pdo->prepare($get_date);
					$stmt1->execute([$get_id]);
					while($row=$stmt1->fetch(PDO::FETCH_ASSOC)):
						$purpose=$row['purpose'];
					endwhile;
				echo $purpose;
				?></td>
				<td class="text-right"><b><?php $get_id = $_GET['eventid'];
					$get_date="SELECT * FROM items_table WHERE p_title_handler=?";
					$stmt1=$pdo->prepare($get_date);
					$stmt1->execute([$get_id]);	
					while($row=$stmt1->fetch(PDO::FETCH_ASSOC)):
						$total_cost=$row['p_estimated_budget'];
						$sum = $sum + $row1['p_estimated_budget'];
						$formattedNum1 = number_format($sum, 2);
					endwhile;
					echo $formattedNum1;
				?></b></td>
			</tr>
		</tbody>
		<?php
			include ('check_rfq.php');
		?>

		<!-- Modal -->
	<div id="addCommentPR" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="background: #0652DD; color: white;">
					<h5 class="modal-title">Add Comment</h5>
					<button type="button" class="close text-white" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form action="" method="post" id="formcomment">
						<div class="form-group">
							<label>Comment</label>
							<textarea class="form-control" id="comment_text_pr" form="formcomment" name="inputed_comment" rows="5" ></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" name="comment" data-role="comment_ppmp" form="formcomment" class="btn btn-default btn-default btn-comment" style="background: #0652DD; color: white;">Add</button>
					</div>
				</div>
			</div>
		</div>

<script type="text/javascript">
	$(".btn-comment").click(function() {
		var section="PR";
		var role=$(this).data("role");
		var hidden_id="<?php echo $_GET['eventid']; ?>";
		var username="<?php echo $_SESSION['s_username']; ?>";
		console.log(username);
		var comment=$("#comment_text_pr").val();
		$('#addCommentPR').modal('toggle');
			$.ajax({
				url: 'php/so_webservice.php',
				type: 'POST',
				data: {
				tag: role,
				module: section,
				inputed_comment: comment,
				s_username: username,
				eventid: hidden_id
				},
				success : function(response){
					swal({
						title: "Success",
						text: "You have successfully added a comment!",
						type: "success",
						timer: 1000,
						showConfirmButton: false
					})
					.then((value) => {
						location.reload();
					});
				}
					});
				});
</script>