<div class="accordion" id="accordionExample">
<?php

	include('connect.php');
		$select_office="SELECT * FROM office_table";
			$select_o_exec=$pdo2->prepare($select_office);
			$select_o_exec->execute();
				$arr1=array();
				while($row=$select_o_exec->fetch(PDO::FETCH_ASSOC)) {
					array_push($arr1, $row['office']);
				}
			$i=0;
		foreach ($arr1 as $key => $value) {
			# code...
        echo'<div class="card">
				<div class="card-header" id="headingOne">
        			<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#'.$key.'">'.$value.'</button>
    			</div>
    				<div id="'.$key.'" class="collapse" data-parent="#accordionExample">
     					<div class="card-body">
     						<table class="table table-bordered display table-sm">
     							<thead>
     								<tr>
     									<th>Unit</th>
     									<th>Description</th>
     									<th>Issued</th>
     									<th>Date Issued</th>
     									<th>Remarks</th>
     								</tr>
     							</thead>
     								<tbody>';
     									$select_by_office="SELECT * FROM supply_issuances_table WHERE office_name=?";
     										$select_by_office=$pdo2->prepare($select_by_office);
     										$select_by_office->execute([$value]);
     											if($i==0) {
	     											while($row_office=$select_by_office->fetch(PDO::FETCH_ASSOC)) {
	     												$unit=$row_office['item_unit'];
	     												$desc=$row_office['item_name'];
	     												$qty=$row_office['qty'];
	     												$date_issued=$row_office['date_issued'];
	     												echo"<tr>
	     														<td>$unit</td>
	     														<td>$desc</td>
	     														<td>$qty</td>
	     														<td>$date_issued</td>
	     														<td></td>
	     													</tr>";
	     											}
     											}

     						echo'</table>
      					</div>
    				</div>
			</div>';
		}
?>
</div>
