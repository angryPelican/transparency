<!DOCTYPE html>
<html>
	<head>
		<title>Catalogue Section</title>
		<?php
			include ('Connect.php'); 
			include('auth.php');
			require ('inc/header.php');
		?>
	</head>
	<style type="text/css">
		.td-limit {
		    white-space: nowrap;
		    overflow: hidden;
		}
		.price {
			vertical-align: middle;
			/*text-align: right;*/
		}
		.btn_add {
			transition: 0.3s all;
			background: #2e86de;
			color: white;
		}
		.btn_add:hover {
			transform: scale(1.1);
		}
	</style>
	<body>
		<?php
			
			require ('inc/navbar_transparency.php');
		?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-2 col-md-2" style="padding: 0;">
					<div class="list-group" id="myList" role="tablist" style="margin: 0 !important;">
						<a style="border-radius: 0px;" class="list-group-item list-group-item-action" id="list" data-toggle="list" href="#recent" role="tab" aria-controls="settings" data-href="recent">Recent Added Items</a>
						<?php
							$get_cat="SELECT * FROM category_table";
							$exec_qry=$pdo->prepare($get_cat);
							$exec_qry->execute();
								while($row=$exec_qry->fetch(PDO::FETCH_ASSOC)) {
								$categ=$row['category'];
								$unique=$row['unique_code'];
									echo'<a style="border-radius: 0px;" class="list-group-item list-group-item-action" id="list" data-href="'.$categ.'" data-toggle="list" href="#recent" role="tab" aria-controls="settings">'.$categ.'</a>';
								}
						?>
					</div>
				</div>
				<script type="text/javascript">
					$("a[id=list]").click(function(){
						var jinx=$(this).data('href');
						var role="global_pane";
						localStorage.setItem('req_category', jinx);
						console.log(localStorage.getItem('req_category'));
					});
					$('#myList a').on('click', function (e) {
					  e.preventDefault();
					  $(this).tab('show');
					});

				</script>
				<div class="col-sm-10 col-md-10">
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade" id="recent" role="tabpanel" aria-labelledby="list-profile-list">
							<div class="form-group">
								<button type="button" data-target="#add_item" data-toggle="modal" class="btn_add btn btn-default"><i class="fas fa-cart-plus"></i> Add item</button>
							</div>
							<div class="form-group">
								<table id="table_catalogue" width="100%" class="table table-bordered table-sm" style="table-layout: auto; width: 100%;">
									<thead>
										<tr>
											<th width="20%">Image</th>
											<th width="15%">Product Name</th>
											<th width="35%">Specification</th>
											<th width="15%">Supplier</th>
											<th width="15%">Unit Price</th>
										</tr>
									</thead>
									<tbody id="table_dark">
										<script type="text/javascript">
											$('a[data-toggle="list"]').on('shown.bs.tab', function (e) {
												$('#table_dark').html("");
												var category=localStorage.getItem('req_category');
												if(category=="recent"){
													var role="sort_items";
													var recent="recent";
													$.ajax({
														url: "catalogue_webservice.php",
														type: "POST",
														data: {
															tag: role,
															categories: recent
														},
														success: function(response){	
															var data_json=JSON.parse(response);
															// console.log(response);
															if ($.fn.DataTable.isDataTable("#table_catalogue")) {
															  $('#table_catalogue').DataTable().clear().destroy();
															}
															 $('#table_catalogue').DataTable({
											                    "aaData": data_json,
											                    'columnDefs': [{
																    "targets": [0,1,2,3,4], // your case first column
																    "className": "td-limit",
																}],
											                });
														}
													});
												} else if(category!="recent") {
													var role="sort_items";
													$.ajax({
														url: "catalogue_webservice.php",
														type: "POST",
														data: {
															tag: role,
															categories: category
														},
														success: function(response){
															var data=JSON.parse(response);
														if ($.fn.DataTable.isDataTable("#table_catalogue")) {
														  $('#table_catalogue').DataTable().clear().destroy();
														}
															$('#table_catalogue').DataTable({
											                    "aaData": data,
												                'columnDefs': [{
																    "targets": [0,1,2,3,4], // your case first column
																    "className": "td-limit",
																}],
											                });
														}
													});
												}
											});
											$("td").addClass("price");
										</script>
									</tbody>
								</table>
							</div>
							<!-- Modal -->
							<div class="modal fade" id="add_item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Add Item</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
												<div class="form-group">
													<label><b>Image/Url</b></label>
													<input class="form-control" name="img_url" type="text" placeholder="Input URL here ..." name="" />
												</div>
												<div class="form-group">
													<label><b>Image/Browse</b></label>
													<input id="file" name="file" class="form-control-file" name="FileUpload" multiple="" type="file" />
												</div>
												<div class="form-group">
													<script type="text/javascript">
														$('a[data-toggle="list"]').on('shown.bs.tab', function (e) {
															var categ=localStorage.getItem('req_category');
															console.log(categ + "eto yun");
															var role="categories";
															if(categ != "recent") {
																$('#categ_link').html("");
																$.ajax({
																	url: "catalogue_webservice.php",
																	type: "POST",
																	data: {
																		tag: role,
																		category: categ,
																	},
																	success: function(response) {
																		var data=JSON.parse(response);
																		var category=data[0];
																		document.getElementById("categ_link").innerHTML += "<option value='" + category + "'>" + category + "</option>";
																		$('#categ_link').attr("disabled", true);
																	}
																});
															} else if (categ=="recent") {
																$('#categ_link').html("");
																$('#categ_link').attr("disabled", false);
																$.ajax({
																	url: "catalogue_webservice.php",
																	type: "POST",
																	data: {
																		tag: role,
																		category: "recent",
																	},
																	success: function(response){
																		var data=JSON.parse(response);
																		for (var key in data) {
																			var category=data[key];
																			document.getElementById("categ_link").innerHTML += "<option value='" + category + "'>" + category + "</option>";
																		}
																	}
																});
															}
														});
													</script>
													<label><b>Category</b></label>
													<select class="form-control" name="categories" id="categ_link">
													</select>
												</div>
												<div class="form-group">
													<label for="item_name"><b>Item Name</b></label>
													<input type="text" class="form-control" rows="5" name="item_name" placeholder="Place item name here ..." />
												</div>
												<div class="form-group">
													<label for="item_name"><b>Item Description</b></label>
													<textarea type="text" class="form-control" rows="5" name="item_desc" placeholder="Place item description here ..." ></textarea>
												</div>
												<div class="form-group row">
													<div class="col">
														<label for="item_name"><b>Supplier Name</b></label>
														<input type="text" name="supplier" class="form-control" placeholder="Place supplier name here ..." />
													</div>
													<div class="col">
														<label for="item_name"><b>Price</b></label>
														<input type="number" name="price" class="form-control" placeholder="Place price name here ..." />
													</div>
												</div>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary" name="submit" form="uploadimage" id="save_item">Save changes</button>
										</div>
									
									</div>
								</div>
							</div>
							<script type="text/javascript">
								$("#uploadimage").on('submit',(function(e) {
									e.preventDefault();
									$.ajax({
										url: "catalogue_webservice.php", // Url to which the request is send
										type: "POST",             // Type of request to be send, called as method
										data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
										contentType: false,       // The content type used when sending data to the server.
										cache: false,             // To unable request pages to be cached
										processData:false,        // To send DOMDocument or non processed data file it is set to false
										success: function(data)   // A function to be called if request succeeds
										{
											location.reload();
										}
									});
								}));
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>
	<script type="text/javascript">
		
	</script>