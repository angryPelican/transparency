<!DOCTYPE html>
<html lang="en">
<head>
	<title>PPMP | Division Procurement Projects</title> 
	<?php
		include ('Connect.php'); 
		include('auth.php');
		require ('inc/header.php');
	?>
	<link rel="stylesheet" type="text/css" href="css/view_proj_sds.css">
</head>
<body>
	<?php
	require ('inc/navbar_transparency.php');
	?>

<div class="container-fluid">

	<div class="row">
		<div class="col">	
			<h2 class="clearfix mt-3">
			<?php include 'php/dynamic_title.php';
			echo ucfirst($project_title);
			echo " <i style='font-size: 16px; color: #95a5a6;'>";
			echo ucfirst($get_username); echo " "; echo ucfirst($get_lastname);
			echo "</i>";
			?></h2>
			<nav aria-label="breadcrumb">
  				<ol class="breadcrumb">
    				<li class="breadcrumb-item active"><a href="#">Home</a></li>
    				<li class="breadcrumb-item active"><a href="#">Division Procurement Projects</a></li>
    				<li class="breadcrumb-item active" aria-current="page"><a href="#"></a></li>
  				</ol>
			</nav>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 col-sm-12 table-responsive">
			<!--DISPLAY PANELS BASED ON PROJECT PROGRESS-->
			<?php
			include ('php/project_ppmp_so.php');
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-sm-4">
			<h5 class="mt-4">MILESTONES</h5>
			<table class="table table-striped table-bordered table-sm display" id="mile_table">
				<thead>
					<tr>
						<th>Req</th>
						<th>Target</th>
						<th>Created</th>
						<th>Approved</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>PPMP</td>
						<td></td>
						<td><?php
							include 'php/dynamic_dates.php';
						echo $echo_submit_date;?></td>
						<td><?php
							include 'php/dynamic_dates.php';
						echo $approval_date;?></td>
					</tr>
					<tr>
						<td>PR</td>
						<td><?php include('php/dynamic_dates.php');
						echo $date5; ?></td>
						<td><?php
							include 'php/dynamic_dates.php';
						echo $pr_date;?></td>
						<td><?php
							include 'php/dynamic_dates.php';
						echo $pr_approved_date;?></td>
					</tr>
					<tr>
						<td>RFQ</td>
						<td><?php include('php/dynamic_dates.php');
						echo $date4; ?></td>
						<td><?php
							include 'php/dynamic_dates.php';
						echo $rfq_dates;?></td>
						<td>
							<?php
								$query_rfq="SELECT * FROM rfq_table WHERE project_id=?";
								$query_rfq_exec=$pdo->prepare($query_rfq);
								$query_rfq_exec->execute([$get_id]);
								$count_rfq=$query_rfq_exec->rowCount();
							if($count_rfq==0) {
									//do nothing
							} else if ($count_rfq != 0) {
								while($row_rfq_exec=$query_rfq_exec->fetch(PDO::FETCH_ASSOC)) {
									$rfq_opening_date=$row_rfq_exec['opening_date'];
								}
								$sel_open_dates="SELECT * FROM rfq_table WHERE project_id=?";
								$sel_exec=$pdo->prepare($sel_open_dates);
								$sel_exec->execute([$get_id]);
								while($row_open=$sel_exec->fetch(PDO::FETCH_ASSOC)) {
								$var_open=$row_open['opening_date'];
								if($var_open=='0000-00-00') {
									$var_open="";
								}
								$var_cat=$row_open['category'];
								echo"$var_open" . "<br>";
								}
							}
							?>
						</td>
					</tr>
					<tr>
						<td>AOQ</td>
						<td><?php include('php/dynamic_dates.php');
						echo $date3; ?></td>
						<td><?php
							include 'php/dynamic_dates.php';
						echo $rfq_dates;?></td>
						<td>
							<?php
							include 'php/dynamic_dates.php';
							echo $abstract_dates; ?>
						</td>
					</tr>
					<tr>
						<td>PO</td>
						<td><?php include('php/dynamic_dates.php');
						echo $date2; ?></td>
						<td><?php
							include 'php/dynamic_dates.php';
						echo $po_dates;?></td>
						<td><?php
								$query_po="SELECT * FROM po_table WHERE project_id=?";
								$query_po_exec=$pdo->prepare($query_po);
								$query_po_exec->execute([$get_id]);
								$count_po=$query_po_exec->rowCount();
							if($count_po==0) {
									//do nothing
							} else if ($count_po != 0) {
								while($row_po=$query_po_exec->fetch(PDO::FETCH_ASSOC)) {
									$po_date=$row_po['po_date'];
									echo "$po_date" . 	"<br>";
								}
							}
							?>
						</td>
					</tr>
					<tr>
						<td>ID</td>
						<td><?php include('php/dynamic_dates.php');
						echo $date1; ?></td>
						<td><?php
							include 'php/dynamic_dates.php';
							echo $delivery_dates;
						?></td>
						<td>
							<?php
							include('php/dynamic_dates.php');
							if($po_approved_date == '0000-00-00') {
							echo'<button type="submit" class="btn btn-info form-control" style="margin: 0;" name="delivery" form="formcomment" disabled>Delivery</button>';
							}
							else if($po_approved_date != '0000-00-00') {
							$get_delivery_date="SELECT * FROM items_table WHERE p_title_handler=?";
							$stmt1=$pdo->prepare($get_delivery_date);
							$stmt1->execute([$get_id]);
							if($stmt1->execute([$get_id])){
							$count=$stmt1->rowCount();
							while ($row_select=$stmt1->fetch(PDO::FETCH_ASSOC)) {
								$delivery_dates=$row_select['delivery_date'];
							}
							if($delivery_dates != '0000-00-00' && $count != 0)	{
								echo'<button type="button" data-role="delivery" data-toggle="modal" data-target="#deliveryModal" class="btn btn-default btn-primary form-control" disabled>Delivery</button>';
							}
							else if($delivery_dates == '0000-00-00' || $count == 0){
								echo'<button type="button" data-role="delivery" data-toggle="modal" data-target="#deliveryModal" class="btn btn-default btn-primary form-control">Delivery</button>';
							}
								}
									}
							?>
						</td>
					</tr>
				</tbody>
			</table>
			<h5 class="mt-4">REMARKS</h5>
			<table class="table table-striped table-bordered" id="remarks_table">
				<?php
				$select_comment = "SELECT * FROM comment_table WHERE project_id=?";
				$stmt2=$pdo->prepare($select_comment);
				$stmt2->execute([$get_id]);
				while ($row=$stmt2->fetch(PDO::FETCH_ASSOC)):
				$comment = $row['comment'];
				$user = $row['user_comment'];
				$module = ucfirst($row['module']);

				if (!empty($comment)) {
				echo "<tr>
					<td><em>$user</em></td>
					<td><small><em>$module</em></small> - $comment</td>
				</tr>";
				} else if (empty($comment)) {
				echo "<tr>
					<td>NONE</td>
					<td><em></em></td>
				</tr>";
				}
				endwhile;
				?> 
			</table>
		</div>
		<br>
		<div class="col-md-8 col-sm-8 table-responsive">
			<?php
			include 'php/connectdb.php';
			$get_id = $_GET['eventid'];
			$query = "SELECT * FROM project_table WHERE project_id=?";
			$stmt3=$pdo->prepare($query);
			$stmt3->execute([$get_id]);
			while ($row = $stmt3->fetch(PDO::FETCH_ASSOC)) {
			$check_purpose = $row['pr_date'];
			$check_rfq = $row['rfq_date'];
			$po_date = $row['po_date'];
			}
			if ($check_purpose != '0000-00-00') {
			include 'php/project_pr_so.php';
			}
			if ($check_rfq != '0000-00-00') {
			include 'php/project_rfq_so.php';
			}
			$query_rfq="SELECT * FROM rfq_table WHERE project_id=?";
			$query_rfq_exec=$pdo->prepare($query_rfq);
			$query_rfq_exec->execute([$get_id]);
			$count_rfq=$query_rfq_exec->rowCount();
			if($count_rfq==0){
				//do nothing
			} else if ($count_rfq!=0) {
				while($row_rfq_exec=$query_rfq_exec->fetch(PDO::FETCH_ASSOC)) {
				$rfq_opening_date=$row_rfq_exec['opening_date'];
			}
				if ($rfq_opening_date != '0000-00-00') {
					include ('php/create_abstract_so.php');
				}
			}
				if ($po_date != '0000-00-00') {
					include 'php/project_po_so.php';
				}
			?>
		</div>
	</div>
	
	</body>
</html>
<?php
	if (isset($_POST['comment'])) {

	$get_id = $_GET['eventid'];
	$user = $_SESSION['s_username'];
	$comment_value = $_POST['inputed_comment'];
	$update_comment = "INSERT INTO comment_table (project_id, comment, user_comment) VALUES (?,?,?)";
	$stmt4=$pdo->prepare($update_comment);
	$stmt4->execute([$get_id,$comment_value,$user]);
	echo "<meta http-equiv='refresh' content='0'>";

	}
?>

<!-- Modal inventory -->
<div class="modal fade" id="deliveryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add to Inventory</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body table-responsive">
      	<div class="form-group-row">
      		<?php
      			$get_id=$_GET['eventid'];
      			$select_checker="SELECT * FROM project_table WHERE project_id=?";
      				$select_checker_exec=$pdo->prepare($select_checker);
      				$select_checker_exec->execute([$get_id]);
      				$row_check=$select_checker_exec->fetch(PDO::FETCH_ASSOC);
      				$delivery_status=$row_check['delivery_status'];
      					if($delivery_status == 11) {
							echo'<div class="col">
								<input class="" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
							 	<label class="" for="inlineRadio1">Full Delivery </label>
								<input type="date" id="full_date" class="form-control mb-2 full_date_input" style="width: 50%;"/>
							</div>
				<div class="col">
					<input class="" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
				  	<label class="" for="inlineRadio2">Partial Delivery</label>
	   			</div>';
	   		} else if ($count != 11) {
	   			echo'<div class="col">
								<input class="" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
							 	<label class="" for="inlineRadio1">Full Delivery </label>
								<input type="date" id="full_date" class="form-control mb-2 full_date_input" style="width: 50%;" />
							</div>
				<div class="col">
					<input class="" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
				  	<label class="" for="inlineRadio2">Partial Delivery</label>
	   			</div>';
	   		}
	   			?>
		</div>
			<div>&nbsp;</div>
         <table class="table table-bordered display">
            <form action="" method="post" id="form_delivery">
              <thead class="thead-dark">
                <tr>
                  <th>Item Description</th>
                  <th>Item Quantity/Size</th>
                  <th>Item Unit</th>
                  <th>Classification</th>
                  <th>Delivery Dates</th>
                </tr>
              </thead>
              <tbody>
                <?php
                include('php/connectdb.php');
                $get_id=$_GET['eventid'];
                $date_now='0000-00-00';
                $select_inventory="SELECT * FROM items_table WHERE p_title_handler=? AND delivery_date=?";
                $res=$pdo->prepare($select_inventory);
                $res->execute([$get_id,$date_now]);
                while($row=$res->fetch(PDO::FETCH_ASSOC)):
                $item_id=$row['item_id'];
                $i_description=$row['p_description'];
                $i_quantity_size=$row['p_quantity_size'];
                $i_unit=$row['p_unit'];
                $category=$row['classification'];
                $delivery_date=$row['delivery_date'];
                echo "<tr>
                  <td>$i_description</td>
                  <td>$i_quantity_size</td>
                  <td>$i_unit</td>
                  <td>$category</td>";
                if($delivery_date == '0000-00-00') {
                echo "<td><input type='date' class='form-control' name='delivery_date[]' id='delivery_date' form='form_delivery'></input></td>";
                echo"</tr>
                <input type='hidden' name='holder[]' id='item_id_handler' value='$item_id' form='form_delivery'></input>";
           		} else if ($delivery_date != '0000-00-00') {
           		echo "<td><input type='date' class='form-control' name='delivery_date[]' id='delivery_date' form='form_delivery'></input></td>";
                echo"</tr>
                <input type='hidden' name='holder[]' id='item_id_handler' value='$item_id' form='form_delivery'></input>";
            	}
                endwhile;
                ?>
              </tbody>
            </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-save-date">Save changes</button>
      </div>
    </div>
  </div>
</div>
	<script type="text/javascript">
		$("#full_date").hide();
		$('input[name="delivery_date[]"]').attr("disabled", true);
			var role="del_exec";

				$('#inlineRadio1').click(function() {
						$("#full_date").show();
						$('input[name="delivery_date[]"]').attr("disabled", true);
						$('.btn-save-date').attr("disabled", true);
					if($('#inlineRadio1').is(':checked')) {
						var date_handler=document.getElementById("full_date");
						date_handler.onchange = function(){
								var fulldate=$(this).val();
								var role="del_full";
								var hidden_id="<?php echo $_GET['eventid'] ?>";
							$.ajax({
								url: 'php/so_webservice.php',
								type: 'POST',
								data: {
								tag: role,
								date_full: fulldate,
								id: hidden_id
								},
								success : function(response){
									console.log(response);
						        	swal({
										title: "All set!",
										text: "Successfully added Delivery Date.",
										icon: "success",
										timer: 1100,
										buttons: false
									})
									.then((value) => {
										location.reload();
									});
								}
							});// ajax end
						}
					}
				});
				$('#inlineRadio2').click(function() {
					if($('#inlineRadio2').is(':checked')) {
						$("#full_date").hide();
						var partial_dates=$('input[name="delivery_date[]"]');
						partial_dates.removeAttr("disabled");
						$('.btn-save-date').removeAttr("disabled");

						var delivery_dates = new Array();
						var delivery_dates_rev = new Array();
						var hidden_id="<?php echo $get_id; ?>";
						var item_id_array= new Array();

						$('.btn-save-date').click(function() {
						$('input[name="delivery_date[]"]').each(function(e) {
    						var holder=$(this).val();
    						if(holder.length == 0) {
    							console.log("kulang");
    						}else if(holder.length != 0) {
    							delivery_dates.push(holder);
    							delivery_dates_rev.push(holder);
    							delivery_dates_rev.reverse();
    						}
						});

						var final_rev=delivery_dates_rev[0];

							$('input[name="holder[]"]').each(function(e) {
								var item_holder=$(this).val();
									if(item_holder.length == 0){
										console.log("kulang item");
									} else if (item_holder.length != 0){
										item_id_array.push(item_holder);
									}
							});
							$.ajax({
								url: 'php/so_webservice.php',
								type: 'POST',
								data: {
								tag: role,
								last_del_date: final_rev,
								item_id: item_id_array,
								id: hidden_id,
								actual: delivery_dates
								},
								success : function(response){
					        		swal({
									title: "All set!",
									text: "Successfully added Delivery Dates.",
									type: "success",
									timer: 1100,
									showConfirmButton: false
									})
								.then((value) => {
									location.reload();
								});
								//console.log(response);
								}
							});// ajax end
						});
					}//end of radio button checker
				});
	</script>