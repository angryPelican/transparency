<?php
require_once 'plugins/dompdf/autoload.inc.php';
use Dompdf\Dompdf;
include 'php/connectdb.php';
$selected_category = $_GET['category'];
$select_project = "SELECT * FROM project_table WHERE project_id=?";
$result_project = $pdo->prepare($select_project);
$result_project->execute([$selected_category]);
while ($row = $result_project->fetch(PDO::FETCH_ASSOC)):
$title = $row['project_title'];
$pr_no = $row['pr_no'];
$user_id = $row['user_id'];
$pr_date = date('F, d, Y', strtotime($row['pr_date']));
endwhile;
// GET THE USERNAME BASED ON USER ID
$username = $_SESSION['s_username'];
$query_select_user = "SELECT * FROM accounts WHERE user_id=?";
$res = $pdo->prepare($query_select_user);
$res->execute([$user_id]);
$row_select = $res->fetch(PDO::FETCH_ASSOC);
$get_username = $row_select['user_name'];
$get_last_name = $row_select['last_name'];
$get_section = $row_select['section'];
$get_position = $row_select['position'];
$wrapped_position = wordwrap($get_position, 25, "<br />\n");
$wrapped_section = wordwrap($get_section, 25, "<br />\n");
$space=" ";
$oic="OIC-Office of the Schools Division Superintendent";
$wrapped_oic=wordwrap($oic, 30, "<br />\n");
$final_username=strtoupper($get_username);
$final_lastname=strtoupper($get_last_name);
$formatted_usertype=strtoupper($get_section);
//END OF QUERY
$year = date("y");
$output = '<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Purchase Request</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript" src="boots/js/jquery-3.3.1.min.js"></script>
		<link rel="stylesheet" href="boots/boots3/dist/css/bootstrap.min.css">
		<script src="boots/boots3/dist/js/bootsrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
	</head>
	<body class="body_style">
		<center>
		<p class="p_title">NGAS APPENDIX-51</p>
		<p class="p_title">REVISED JUNE 15,2002</p>
		<p class="p_title2">Purchase Request</p></center>
		<p class="tiny1"><b> Department: DepEd Marikina City</b></p>
		<div class="panel panel-default" id="card_body_style">
			<div class="panel-body table-responsive" id="card_body_style">
				<table class="table table-striped table-bordered display table_style" style="table-layout: fixed;">
						<tr>
							<td colspan="2" width="30% !important;">Office/Section<br /><b>'.$wrapped_position.' <br> '.$wrapped_section.'</b></td>
							<td width="40%">PR No.: DepEd-' . $year . '-NCR-PR-' . $pr_no . '<b></b><br />Responsibility Code:</td>
							<td colspan="2" width="30% !important;">Date: ' . $pr_date . '<br/></td>
						</tr>
						<tr>
							<th width="15%">Stock No.</th>
							<th width="15%">Unit</th>
							<th width="40%">Item Description</th>
							<th width="15%">Quantity</th>
							<th width="15%">Unit Cost</th>
						</tr>';
						$get_id = $_GET['category'];
						$get_date = "SELECT * FROM project_table WHERE project_id=?";
						$res = $pdo->prepare($get_date);
						$res->execute([$get_id]);
						while ($row = $res->fetch(PDO::FETCH_ASSOC)):
						$purpose = $row['purpose'];
						endwhile;
						include 'connectdb.php';
						//FIRST QUERY
						$selected_category = $_GET['category'];
						$first_query = "SELECT * FROM items_table WHERE p_title_handler=?";
						$result_1 = $pdo->prepare($first_query);
						$result_1->execute([$selected_category]);
						$count = $result_1->rowCount();
						//SECOND QUERY
						$second_query = "SELECT * FROM items_table WHERE p_title_handler=?";
						$result_11 = $pdo->prepare($second_query);
						$result_11->execute([$selected_category]);
						$i = 0;
						$i1 = 0;
						$i2 = 0;
						$sum = 0;
						//loop each items
						while ($row1 = $result_11->fetch(PDO::FETCH_ASSOC)) {
						$sx = $row1['p_title_name'];
						$s1 = $row1['p_description'];
						$newtext = wordwrap($s1, 50, "<br />\n");
						$s2 = $row1['p_quantity_size'];
						$s3 = $row1['p_unit'];
						$s4 = $row1['p_estimated_budget'];
						$formattedNum = number_format($s4, 2);
						$sum = $sum + $row1['p_estimated_budget'];
						$formattedNum1 = number_format($sum, 2);
						$output .= '<tr>';
							$output .= '<td style="width: 15%;"></td>';
							$output .= "<td style='width: 15%;'>$s3</td>
							<td style='width: 40%;'>$newtext</td>
							<td tyle='width 15%;'>$s2</td>
							<td style='text-align: right; width: 15%;'>$formattedNum</td>";
							}
						$output .= "</tr>";
						$output .= '<tr>
							<td colspan="2"><b>Purpose: </b></td>
							<td>' . $purpose . '</td>
							<td><b>Total: </b></td>
							<td style="text-align:right;">' . $formattedNum1 . '</td>
						</tr>
					<tfoot>
						<tr>
							<td id="hh" colspan="2">&nbsp;</td>
							<td id="mm"><b>Requested by:</b></td>
							<td colspan="2" id="approved"><b>Approved by: </b></td>
						</tr>
						<tr>
							<td id="hh" colspan="2" >Signature <br />Printed Name <br /> Designation</td>
							<td id="username">
								<b>' . $final_username . ' '  . $final_lastname . '</b> <br/>
									'.$get_section.'
							</td>
							<td colspan="2" id="oic">
								<b>JOEL T. TORRECAMPO </b><br/>
								<span>'.$wrapped_oic.'</span>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</body>
</html>';
$document = new Dompdf();
$document->loadHtml($output);
ob_end_clean();
$document->setPaper('letter', 'portrait');
$document->render();
$pdf = $document->output();
$document->stream("$title : " . time(), array("Attachment" => 1));
?>