<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Issuances Dashboard</title>
		<meta charset="utf-8">
		<meta name = "viewport" content = "width=device-width, initial-scale=1">
			<?php
				require('links.php');
			?>
	</head>
	<?php
		include ('Connect.php'); 
		include('auth.php');
		require ('inc/navbar_transparency.php');
	?>
	<body>
		<div class="container-fluid">
			<table class="table" id="table_header" style="margin:0; border: none;">
				<input type="hidden" name="hidden_id" id="hidden_trans_id" value="" />
					<tr style="border: none;">
						<td style="border: none;" width="15%" align="center"><img src="seal1.png" alt="Smiley face" width="100"></img></td>
						<td style="border: none;" width="70%" style="margin: 0; font-size: 0.8em;" align="center">REPUBLIC OF THE PHILIPPINES<br>
							<h5 style="margin: 0;">DEPARTMENT OF EDUCATION</h5>
							NATIONAL CAPITAL REGION<br>
							<h5 style="margin: 0;">SCHOOLS DIVISION OFFICE - MARIKINA CITY</h5>
						</td>
						<td style="border: none;" width="15%" align="center"><img src="deped_logo.png" alt="Smiley face" width="100"></img></td>
					</tr>
				</table>
			<h3><center>Requisition and Issuance</center></h3>
			<table class="table table-bordered display table-sm" id="table_header2">
				<tr>
					<td>Entity Name: <span>SDO MARIKINA</span></td>
					<td>Fund Cluster: <span>1101101</span></td>
				</tr>
				<tr>
					<td>Division: </td>
					<td>Responsibility Center Code: </td>
				</tr>
				<tr>
					<td>Office: </td>
					<td>RIS No. : </td>
				</tr>
			</table>
			<table class="table table-striped table-bordered display table-sm">
				<thead>
					<tr>
						<th colspan="4"><center>Requisition</center></th>
						<th colspan="3"><center>Issue</center></th>
					</tr>
					<tr>
						<th width="7%">Stock No.</th>
						<th width="7%">Unit</th>
						<th width="45%">Description</th>
						<th width="15%">Quantity</th>
						<th>Quantity</th>
						<th colspan="2">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(empty($_GET['eventid'])) {
					$trans_id_sess=0;
					} else {
					$trans_id_sess=$_GET['eventid'];
					}
					$select_s="SELECT * FROM supply_issuances_table WHERE trans_id=?";
					$sel_s_exec=$pdo->prepare($select_s);
					$sel_s_exec->execute([$trans_id_sess]);
					$count_trans=$sel_s_exec->rowCount();
					if($count_trans==0) {
					//do nothing
					} else if ($count_trans != 0) {
						while($row_s=$sel_s_exec->fetch(PDO::FETCH_ASSOC)) {
							$item_name=$row_s['item_name'];
							$item_unit=$row_s['item_unit'];
							$item_qty=$row_s['qty'];
							echo'<tr>
								<td></td>
								<td>'.$item_unit.'</td>
								<td>'.$item_name.'</td>
								<td>'.$item_qty.'</td>
								<td>'.$item_qty.'</td>
								<td colspan="2"></td>
							</tr>';
						}
					}
					?>
					<tr>
						<td colspan="2">Purpose	</td>
						<td></td>
						<td colspan="4"></td>
					</tr>
				</tbody>
			</table>
							<table class="table table-bordered table-sm" id="table_footer">
								<tr>
									<td width="10%" valign="bottom" style="padding: 0; margin: 0; vertical-align: bottom; border-bottom: none;"></td>
									<td width="30%" style="border-bottom: none;">Requested By:</td>
									<td width="20%" style="border-bottom: none;">Approved By:</td>
									<td width="20%" style="border-bottom: none;">Issued By: </td>
									<td width="20%" style="border-bottom: none;">Received By: </td>
								</tr>
								<tr>
									<td style="border-top: none;">Signature: </td>
									<td style="border-top: none;"></td>
									<td style="border-top: none;"></td>
									<td style="border-top: none;"></td>
									<td style="border-top: none;"></td>
								</tr>
								<tr>
									<td valign="bottom">Printed Name: </td>
									<td></td>
									<td>ANNA MARIE P. EXEQUIEL</td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td valign="bottom">Designation: </td>
									<td></td>
									<td>AO IV-SUPPLY UNIT</td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td valign="bottom">Date: </td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</table>
						</div>
					</body>
				</html>
