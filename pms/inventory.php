<?php
include 'auth.php';
echo "<p>Welcome <strong>" . $_SESSION['s_user_type'] . " " . $_SESSION['s_username'] . " - <a href='logoutadmin.php'>Logout</a></strong></p>";
?>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <div class="card">
        <div class="card-header">
          <h5 style="text-transform: uppercase;">Add to Inventory</h5>
        </div>
        <div class="card-body">
          <table class="table table-bordered display">
            <form action="" method="post" id="form_delivery">
              <thead class="thead-dark">
                <tr>
                  <th>Item Description</th>
                  <th>Item Quantity/Size</th>
                  <th>Item Unit</th>
                  <th>Classification</th>
                  <th>Item Unit Price</th>
                  <th>Delivery Dates</th>
                </tr>
              </thead>
              <tbody>
                <?php
                include('php/connectdb.php');
                $get_id=$_GET['eventid'];
                $date_now='0000-00-00';
                $select_inventory="SELECT * FROM inventory_table WHERE project_id=? AND delivery_date=?";
                $res=$pdo->prepare($select_inventory);
                $res->execute([$get_id,$date_now]);
                while($row=$res->fetch(PDO::FETCH_ASSOC)):
                $item_id=$row['item_id'];
                $i_description=$row['i_description'];
                $i_quantity_size=$row['i_quantity_size'];
                $i_unit=$row['i_unit'];
                $i_delivery_date=$row['delivery_date'];
                $category=$row['category'];
                $unit_price=$row['unit_price'];
                $format_unit_price=number_format($unit_price, 2);
                echo "<tr>
                  <td>$i_description</td>
                  <td>$i_quantity_size</td>
                  <td>$i_unit</td>
                  <td>$category</td>
                  <td>Php$format_unit_price</td>";
                  if($i_delivery_date == '0000-00-00') {
                  echo "<td><input type='date' class='form-control' name='delivery_date[]' form='form_delivery'></input></td>";
                  }
                  if($i_delivery_date != '0000-00-00') {
                  echo "<td><input type='date' class='form-control' name='delivery_date[]' form='form_delivery' disabled></input></td>";
                  }
                echo"</tr>
                <input type='hidden' name='holder[]' value='$item_id' form='form_delivery'></input>";
                endwhile;
                ?>
              </tbody>
            </table>
          </div>
          <div class="card-footer">
            <button class="btn btn-lg btn-success" style="float: right;" type="submit" name="save_delivery" form='form_delivery'>Save</button>
          </form>
          <?php
          if(isset($_POST['save_delivery'])){
          $get_id=$_GET['eventid'];
          $count = count($_POST['delivery_date']);
          for ($i = 0; $i < $count; $i++) {
          $easy = $_POST['delivery_date'][$i];
          $id = $_POST['holder'][$i];
          $sql = "UPDATE inventory_table SET delivery_date=? WHERE item_id = ? AND project_id=?";
          $success = $pdo->prepare($sql);
          $success->execute([$easy,$id,$get_id]);
          echo'<script>window.location="view_project_so.php?eventid=' .$get_id. '";</script>';
          }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</body>
</html>