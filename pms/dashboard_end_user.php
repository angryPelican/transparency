<!DOCTYPE html>
<html lang="en">
<head>
	<title>PPMP | Create Project Dashboard</title>
	<link rel="stylesheet" type="text/css" href="css/dboard_end_user.css">
	<?php
		include ('Connect.php'); 
		include('auth.php');
		require ('inc/header.php');
	?>
	
</head>
<body>
	<?php
		require ('inc/navbar_transparency.php');
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<h2 class="mt-4">Dashboard</h2>
				<nav aria-label="breadcrumb">
	  				<ol class="breadcrumb">
	    				<li class="breadcrumb-item active">Home</li>
	  				</ol>
				</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-9">
				<div class="card bg-light border-dark">
					<div class="card-header bg-dark" id="card_header">
						<p class="text-right" id="par"><button class="btn btn-primary" id="btn_add_proj" type="button" data-toggle="modal" data-target="#addTitle" ><i class="fas fa-pen-square"></i><b> Add Project</b></button></p>
					</div>
					<div class="card-body table-responsive">
						<!--PPMP TABLE -->
						<?php
							include('php/end_user/project_title_table.php');
						?>
						<!--END OF TABLE -->
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="card mb-4 p-0">
					<div class="card-header bg-dark text-white">
						<h6 class="m-0">Project Accomplishment Report</h6>
					</div>
					<div class="card-body table-responsive">
						<table class="table table-striped table-bordered table-sm" id="table_report">
							<thead>
								<tr>
									<th align="center">Active</th>
									<th align="center">Completed</th>
									<th align="center">% of Completion</th>
								</tr>
							</thead>
							<tbody id="tbod-dark">
							</tbody>
						</table>
					</div>
				</div>	
				<div class="card">
					<div class="card-header bg-dark text-white">
						<h6 class="m-0">Project Implementation</h6>
					</div>
					<?php
						$m=date('n');
						$q1="SELECT * FROM milestone_table WHERE user_id=? AND MONTH(approval_of_request)=?";
						$q1_exec=$pdo->prepare($q1);
						$q1_exec->execute([$_SESSION['s_id'], $m]);
						$cnt_row=$q1_exec->rowCount();
						if($cnt_row==0) {
							echo"<a class='list-group-item'>No data available in table.</a>";
						} else {
							while ($row=$q1_exec->fetch(PDO::FETCH_ASSOC)) {
								$mile1=date('F d, Y', strtotime($row['approval_of_request']));
								$title=$row['milestone_title_handler'];
								echo"<a class='list-group-item'>".$mile1." - <strong>".$title."</strong></a>";
							}
						}
					?>
				</div>	
			</div>
		</div>
	</div>
	<!-- ADD PROJECT MODAL -->
		<div class="modal fade" id ="addTitle" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header bg-primary text-white">
							<h5 class="modal-title">PMS | Create Project Title</h5></td>
							<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label><b>Project Name</b></label>
									<textarea placeholder="Project Name" rows="4" cols="50" class="form-control form-control-sm" type="text" class="modal-title" id="get_title" required></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="add" class="btn btn-primary">Proceed</button>
					</div>
				</div>
			</div>
		</div>
	<!-- END OF MODAL -->
	</body>
</html>
<script type="text/javascript">
var role="project_status";
var s_id=<?php echo $_SESSION['s_id']; ?>;
	$.ajax({
		url: "login_webservice.php",
		type: "post",
		data: {
			tag: role,
			user_id: s_id,
		},
		success: function(response){
			console.log(response);
			if(response=="none") {
				document.getElementById("tbod-dark").innerHTML += "<tr><td colspan='3'>No data available in table.</td></tr>";
			} else if (response!="none"){
				var data=JSON.parse(response);
				for (var key in data) {
					document.getElementById("tbod-dark").innerHTML += "<tr><td>" + data[key].row1 + "</td>" + "<td>" + data[key].row2 +  "</td>" + "<td>" + data[key].row3 + "</td></tr>";
				}
			}
		}
	})
// Datatable
$(document).ready(function(){
	$("#myTable").DataTable({
	"bSort" : false,
	"lengthMenu": [[5], [5]],
	"iDisplayLength": 5,
	});
	$('#add').click(function(){
		var role="add_project";
		var get_title=$('#get_title').val();
		if(get_title=="") {
			$('#addTitle').modal('toggle');
			swal({
				title: "Oops!",
				text: "Please provide project title.",
				type: "error",
				showConfirmButton: false,
				timer: 1700
			})
			.then((value) => {
				location.reload();
			});
		} else {
			var sess_id="<?php echo $_SESSION['s_id']; ?>"
			// console.log(get_title);
			$.ajax({
				url: 'login_webservice.php',
				type: 'POST',
				data: {
					tag: role,
					s_id: sess_id,
					get_title: get_title,
				},
				success: function(response) {
					if(response=="wala"){
						
					} else if(response!="wala") {
						$('#addTitle').modal('toggle');
						swal({
							title: "Nice!",
							text: "Project created. Please wait while redirecting you to page.",
							type: "success",
							showConfirmButton: false,
							timer: 3500
						})
						.then((value) => { 
							// location.reload();
							window.location='create_project_items.php'+ '?eventid=' + response + '';
						});
					}
				}
			})
		}
	});
});
</script>
