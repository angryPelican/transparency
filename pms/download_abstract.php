<?php
require_once('plugins/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
include('php/connectdb.php');
$get_id = $_GET['eventid'];
$get_category = $_GET['category'];
$select_project="SELECT * FROM project_table WHERE project_id=?";
$result_project=$pdo->prepare($select_project);
$result_project->execute([$get_id]);
while($row=$result_project->fetch(PDO::FETCH_ASSOC)):
$title=$row['project_title'];
endwhile;
$output = '<html lang="en">
  <head>
    <title>AOQ</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="boots/boots/css/bootstrap.min.css">
    <link rel = "stylesheet" type = "text/css" href="style.css" />
    <script src="boots/boots/js/jquery-2.1.4.min.js"></script>
    <script src="boots/boots/js/bootstrap.min.js"></script>
  </head>
  <body style="margin: auto;">
    <div class="panel panel-default">
      <div class="panel-heading">
              <p style="font-size: 1.2em; margin-bottom: 0px;"><center>National Capital Region</center></p>
              <p style="text-transform: uppercase; font-size: 1.2em; margin-bottom: 0px;"><center><b>Division of Marikina City</b></center></p>
              <p style="text-transform: uppercase; font-size: 1.2em; margin-bottom: 0px;"><center>Abstract of Quotation/Proposal/Bid Submitted</center></p>';
                $output .= '
                  <!--PROJECT TITLE-->
                  <p style="text-transform: uppercase; font-size: 5em;"><center>ABSTRACT OF QUOTATION</center></p>
                  </div>
                  <div class="panel-body" style="padding: 0;">
                  <table class="table table-striped table-bordered table-condensed" style="margin: 0%; table-layout: auto; width: 100%;">
                      <tr>
                        <th colspan="3" align="center" style="text-align:center; width: 50%;">Materials</th>
                        <th colspan="6" align="center" style="text-align:center; width: 50%;">Quotation Submitted</th>
                      </tr>
                      <tr>
                        <th rowspan="2" style="text-align: center; width: 30%; vertical-align: middle;"><b>Item Description</b></th>
                        <th rowspan="2" style="text-align: center; width: 5%; vertical-align: middle;"><b>Quantity</b></th>
                        <th rowspan="2" style="text-align: center; width: 5%; vertical-align: middle;"><b>Unit</b></th>';
        $check_rows="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
        $result_supp=$pdo->prepare($check_rows);
        $result_supp->execute([$get_id, $get_category]);
        $row_abs=$result_supp->fetchall(PDO::FETCH_ASSOC);
        $json_decode=json_decode($row_abs[0]['supplier_data'], true);
        $arr_names=array();
          for ($i=0; $i < count($json_decode); $i++) {
            $supp1=$json_decode[$i]['supp_name'];
            array_push($arr_names, $supp1);
            $output.='<th colspan="2" style="text-align: center; width: 20%;"><b>'.$supp1.'</b></th>';
          }
          // echo "<script>console.log('" . json_encode($arr_names) . "');</script>";
      $output.='</tr>
        <tr>
          <th>Price</th>
          <th>Total</th>
          <th>Price</th>
          <th>Total</th>
          <th>Price</th>
          <th>Total</th>
       </tr>
      <tbody>';
        $sel_items="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
        $sel_items_exec=$pdo->prepare($sel_items);
        $sel_items_exec->execute([$get_id, $get_category]);
        $cnt_items=$sel_items_exec->rowCount();
        $xx=0;
        $handler=0;
        $stopper=0;
        while($row=$sel_items_exec->fetch(PDO::FETCH_ASSOC)) {
          $items=$row['p_description'];
          $quantity=$row['p_quantity_size'];
          $unit=$row['p_unit'];
          $output.= "<tr>
            <td>$items</td>
            <td>$quantity</td>
            <td>$unit</td>";
            $arr1=array();
            $arr2=array();
            $arr3=array();
            $check_rows="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
            $result_supp=$pdo->prepare($check_rows);
            $result_supp->execute([$get_id, $get_category]);
            $row_abs=$result_supp->fetchall(PDO::FETCH_ASSOC);
            $json_decode=json_decode($row_abs[0]['supplier_data'], true);
            $q1="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
            $q1_exec=$pdo->prepare($q1);
            $q1_exec->execute([$get_id, $get_category]);
            $cnt_row=$q1_exec->rowCount();
            $low=0;
            for ($i=0; $i < $cnt_row; $i++) {
            $prices=$json_decode[0]['supp_price'][$i];
            array_push($arr1, $prices);
            $prices2=$json_decode[1]['supp_price'][$i];
            array_push($arr2, $prices2);
            $prices3=$json_decode[2]['supp_price'][$i];
            array_push($arr3, $prices3);
            }// for loop
            $sk=$xx++;
            $multiplied1[]=$arr1[$sk] * $quantity;
            $multiplied2[]=$arr2[$sk] * $quantity;
            $multiplied3[]=$arr3[$sk] * $quantity;
            $added1=array_sum($multiplied1);
            $added2=array_sum($multiplied2);
            $added3=array_sum($multiplied3);
            if($arr2[$sk]==0) {
              $output.="<td>".number_format($arr1[$sk], 2)."</td>
              <td>".number_format($multiplied1[$sk], 2)."</td>";
              if($handler==0) {
              $output .= "<td colspan='2' rowspan='".$cnt_row."' align='center' style='vertical-align: middle;'>Did not meet Specification</td>";
              $handler=1;
              }
              $output.= "<td>".number_format($arr3[$sk], 2)."</td>
              <td>".number_format($multiplied3[$sk], 2)."</td>";
            } else if ($arr1[$sk]==0) {
              if($handler==0) {
                $output .= "<td colspan='2' rowspan='".$cnt_row."' align='center' style='vertical-align: middle;'>Did not meet Specification</td>";
                $handler=1;
              }
              $output.= "<td>".number_format($arr2[$sk], 2)."</td>
              <td>".number_format($multiplied2[$sk], 2)."</td>
              <td>".number_format($arr3[$sk], 2)."</td>
              <td>".number_format($multiplied3[$sk], 2)."</td>";
            } else if ($arr3[$sk]==0) {
              $output.= "<td>".number_format($arr1[$sk], 2)."</td>
              <td>".number_format($multiplied1[$sk], 2)."</td>
              <td>".number_format($arr2[$sk], 2)."</td>
              <td>".number_format($multiplied2[$sk], 2)."</td>";
              if($handler==0) {
                $output .= "<td colspan='2' rowspan='".$cnt_row."' align='center' style='vertical-align: middle;'>Did not meet Specification</td>";
                $handler=1;
              }
            } else {
              $output.= "<td align='right'>".number_format($arr1[$sk], 2)."</td>
                    <td align='right'>".number_format($multiplied1[$sk], 2)."</td>
                    <td align='right'>".number_format($arr2[$sk], 2)."</td>
                    <td align='right'>".number_format($multiplied2[$sk], 2)."</td>
                    <td align='right'>".number_format($arr3[$sk], 2)."</td>
                    <td align='right'>".number_format($multiplied3[$sk], 2)."</td>";
            }
            $output.="</tr>";
        }// while loop
              $total1[]=$added1;
              $total2[]=$added2;
              $total3[]=$added3;
   $output.= '<tfoot>
                <tr>
                  <td colspan="3" align="center"><b>Total</b></td>
                  <td></td>
                  <td align="right">'.number_format($total1[0], 2).'</td>
                  <td></td>
                  <td align="right">'.number_format($total2[0], 2).'</td>
                  <td></td>
                  <td align="right">'.number_format($total3[0], 2).'</td>
                </tr>
              </tfoot>';
            // $output.= "<script>console.log('" . json_encode($total1) . "');</script>";
            // $output.= "<script>console.log('" . json_encode($added2) . "');</script>";
            // $output.= "<script>console.log('" . json_encode($added3) . "');</script>";
         
        
        $output .=" </tbody>
        </table></div>
                  </div>";
                $getwinner="SELECT * FROM abstract_table WHERE project_id=? AND category=?";
                $getwinner_exec=$pdo->prepare($getwinner);
                $getwinner_exec->execute([$get_id, $get_category]);
                $row_winner=$getwinner_exec->fetchall(PDO::FETCH_ASSOC);
                $decode_first=json_decode($row_winner[0]['supplier_data'], true);
                $arr1=array();
                $arr2=array();
                $arr3=array();
                $q1="SELECT * FROM items_table WHERE p_title_handler=? AND classification=?";
                $q1_exec=$pdo->prepare($q1);
                $q1_exec->execute([$get_id, $get_category]);
                $cnt_row=$q1_exec->rowCount();
                for ($i=0; $i < $cnt_row; $i++) {
                  $arr_winner=array();
                  $arr_names=array();
                  $prices1=$decode_first[0]['supp_price'][$i];
                  $prices2=$decode_first[1]['supp_price'][$i];
                  $prices3=$decode_first[2]['supp_price'][$i];
                  $names1=$decode_first[0]['supp_name'];
                  $names2=$decode_first[1]['supp_name'];
                  $names3=$decode_first[2]['supp_name'];
                  $arr1[]=$prices1;
                  $arr2[]=$prices2;
                  $arr3[]=$prices3;
                  $summed1=array_sum($arr1);
                  $summed2=array_sum($arr2);
                  $summed3=array_sum($arr3);
                  $arr_names[$names1]=$summed1;
                  $arr_names[$names2]=$summed2;
                  $arr_names[$names3]=$summed3;
                }
                $xxsupp=array_keys($arr_names, min(array_filter($arr_names)));
          $output .='<div class="container">
                    <div class="row">
                      <div class="col-sm-12" style="padding: 0;">
                        <table class="table table-condensed" style="margin: 0;">
                          <tr>
                            <td style="border: none;">AWARDED TO:</td>
                            <td rowspan="2" style="border: none; vertical-align: middle;"><span style="float: right; margin-right: 70px;">Recommended and Certified to be the lowest obtainable.<br>Reasonable and advantageous to government by:</span></td>
                          </tr>
                          <tr>
                            <td style="border: 1px 1px 1px 1px solid black; padding: 0;"><center>'.$xxsupp[0].'</center></td>
                          </tr>
                        </table>
                      </div>
                    <div>&nbsp;';

          $output .='<div class="row">
                      <div class="col-sm-12" style="margin: 0;">
                        <table class="table" style="margin: 0;">
                          <tr>
                            <td style="border: none;">Prepared by:</td>
                            <td style="border: none;">Approved by:</td>
                            <td style="border: none;"></td>
                          </tr>
                          <tr>
                            <td style="border: none;"><center><b>LEILANI N. VILLANUEVA</b><br>BAC Secretariat</center></td>
                            <td style="border: none;"><center><b>JOEL T. TORRECAMPO</b><br>Officer-In-Charge<br>Office of the Schools Division Superintendent</center></td>
                            <td style="border: none;"><center><b>ELISA O. CERVEZA</b><br>BAC Chairperson</center></td>
                          </tr>
                        </table>
                          </div>
                      </div>';
          $output .="</div>
          </body>
          </html>";
        $document = new Dompdf();
        $document->loadHtml($output);
        $document->setPaper('legal', 'landscape');
        $document->render();
        $pdf = $document->output();
        ob_end_clean();
        $document->stream("$title : ".time(), array("Attachment"=>1));
        ?>