<?php
	include ('Connect.php'); 
	$common="Common Office Supplies";
	$null_date='0000-00-00';
	$select="SELECT * FROM items_table WHERE classification='$common'";
		$select_exec=$pdo->prepare($select);
			if($select_exec->execute()) {
				$bud=0;
				while($row=$select_exec->fetch(PDO::FETCH_ASSOC)) {
					$est=$row['p_estimated_budget'];
					$bud = $bud + $est;
				}
			}
	$update_1="UPDATE category_table SET app_budget=? WHERE category=?";
	$exec_up1=$pdo->prepare($update_1);
	if($exec_up1->execute([$bud,$common])) {
		//success
	} else {
		$exec_up1->errorInfo();
	}
	$drug_meds="Drugs and Medicine Expenses";
	$null_date='0000-00-00';
	$select_drug="SELECT * FROM items_table WHERE classification='$drug_meds'";
		$select_drug_exec=$pdo->prepare($select_drug);
			if($select_drug_exec->execute()) {
				$bud_drug=0;
				while($row_med=$select_drug_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_drug = $row_med['p_estimated_budget'];
					$bud_drug = $bud_drug + $est_drug;
				}
			}
	$update_2="UPDATE category_table SET app_budget=? WHERE category=?";
	$exec_up2=$pdo->prepare($update_2);
	if($exec_up2->execute([$bud_drug,$drug_meds])) {
		//success
	} else {
		$exec_up2->errorInfo();
	}
	$drug_medical="Medical, Dental, Laboratory Supplies";
	$null_date='0000-00-00';
	$select_med="SELECT * FROM items_table WHERE classification='$drug_medical'";
		$select_med_exec=$pdo->prepare($select_med);
			if($select_med_exec->execute()) {
				$bud_med=0;
				while($row_medical=$select_med_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_med = $row_medical['p_estimated_budget'];
					$bud_med = $bud_med + $est_med;
				}
			}
	$update_3="UPDATE category_table SET app_budget=? WHERE category=?";
	$exec_up3=$pdo->prepare($update_3);
	if($exec_up3->execute([$bud_med,$drug_medical])) {
		//success
	} else {
		$exec_up2->errorInfo();
	}
	$drug_repair="Repair/Maintenance/Services";
	$null_date='0000-00-00';
	$select_repair="SELECT * FROM items_table WHERE classification='$drug_repair'";
		$select_rep_exec=$pdo->prepare($select_repair);
			if($select_rep_exec->execute()) {
				$bud_repair=0;
				while($row_repair=$select_rep_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_repair = $row_repair['p_estimated_budget'];
					$bud_repair = $bud_repair + $est_repair;
				}
			}
	$null_date='0000-00-00';
	$array_other=array();
	$other_supp="Other Supplies and Materials";
	$food_supp="Food Supplies";
	$acc_forms="Accountable Forms";
	$adv_print_public="Advertising, Printing, Publication";
	$select_other_supp="SELECT * FROM items_table WHERE classification='$other_supp'";
		$s1_other_exec1=$pdo->prepare($select_other_supp);
			if($s1_other_exec1->execute()) {
				$bud_other1=0;
				while($row_other1=$s1_other_exec1->fetch(PDO::FETCH_ASSOC)) {
					$est_other1 = $row_other1['p_estimated_budget'];
					$bud_other1 = $bud_other1 + $est_other1;
					array_push($array_other, $bud_other1);
				}
			}
	$select_other_food="SELECT * FROM items_table WHERE classification='$food_supp'";
		$s2_other_exec=$pdo->prepare($select_other_food);
			if($s2_other_exec->execute()) {
				$bud_othe2=0;
				while($row_other2=$s2_other_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_other2 = $row_other2['p_estimated_budget'];
					$bud_other2 = $bud_other2 + $est_other2;
					array_push($array_other, $bud_other2);
				}
			}
	$select_other_form="SELECT * FROM items_table WHERE classification='$acc_forms'";
		$s3_other_exec=$pdo->prepare($select_other_form);
			if($s3_other_exec->execute()) {
				$bud_other3=0;
				while($row_other3=$s3_other_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_other3 = $row_other3['p_estimated_budget'];
					$bud_other3 = $bud_other3 + $est_other3;
					array_push($array_other, $bud_other3);
				}
			}
		$bud_other=array_sum($array_other);
	$utilities="Utilities";
	$null_date='0000-00-00';
	$select_utilities="SELECT * FROM items_table WHERE classification='$utilities'";
		$select_rep_exec=$pdo->prepare($select_utilities);
			if($select_rep_exec->execute()) {
				$bud_util=0;
				while($row_util=$select_rep_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_util = $row_util['p_estimated_budget'];
					$bud_util = $bud_util + $est_util;
				}
			}
	$semi_expendable="Semi-Expendable";
	$null_date='0000-00-00';
	$select_semi="SELECT * FROM items_table WHERE classification='$semi_expendable'";
		$select_semi_exec=$pdo->prepare($select_semi);
			if($select_semi_exec->execute()) {
				$bud_semi=0;
				while($row_semi=$select_semi_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_semi = $row_semi['p_estimated_budget'];
					$bud_semi = $bud_semi + $est_semi;
				}
			}
	$bonds_traveling="Bonds/Traveling/Training";
	$null_date='0000-00-00';
	$select_bonds_traveling="SELECT * FROM items_table WHERE classification='$bonds_traveling'";
		$select_bonds_travel_exec=$pdo->prepare($select_bonds_traveling);
			if($select_bonds_travel_exec->execute()) {
				$bud_b_t=0;
				while($row_bonds_travel=$select_bonds_travel_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_b_t = $row_bonds_travel['p_estimated_budget'];
					$bud_b_t = $bud_b_t + $est_b_t;
				}
			}
	$bonds="Bonds";
	$null_date='0000-00-00';
	$select_bonds="SELECT * FROM items_table WHERE classification='$bonds'";
		$select_bonds_exec=$pdo->prepare($select_bonds);
			if($select_bonds_exec->execute()) {
				$bud_bonds=0;
				while($row_bonds=$select_bonds_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_bonds = $row_bonds['p_estimated_budget'];
					$bud_bonds = $bud_bonds + $est_bonds;
				}
			}
	$travelling="Travelling/Representation Expenses";
	$null_date='0000-00-00';
	$select_travelling="SELECT * FROM items_table WHERE classification='$travelling'";
		$select_travelling_exec=$pdo->prepare($select_travelling);
			if($select_travelling_exec->execute()) {
				$bud_travel=0;
				while($row_travel=$select_travelling_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_travel = $row_travel['p_estimated_budget'];
					$bud_travel = $bud_travel + $est_travel;
				}
			}
	$trainings="Training";
	$null_date='0000-00-00';
	$select_trainings="SELECT * FROM items_table WHERE classification='$trainings'";
		$select_trainings_exec=$pdo->prepare($select_trainings);
			if($select_trainings_exec->execute()) {
				$bud_trainings=0;
				while($row_trainings=$select_trainings_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_trainings = $row_trainings['p_estimated_budget'];
					$bud_trainings = $bud_trainings + $est_trainings;
				}
			}
			
	$update_1="UPDATE category_table SET app_budget=? WHERE category=?";
	$exec_up1=$pdo->prepare($update_1);
	if($exec_up1->execute([$bud_trainings,$trainings])) {
		//success
	} else {
		$exec_up1->errorInfo();
	}

	$total_mooe=$bud + $bud_drug + $bud_med + $bud_repair + $bud_other + $bud_util + $bud_semi + $bud_b_t + $bud_bonds + $bud_travel + $bud_trainings;

	$in_service="In-Service Trainings";
	$null_date='0000-00-00';
	$select_in_service="SELECT * FROM items_table WHERE classification='$in_service'";
		$select_in_service_exec=$pdo->prepare($select_in_service);
			if($select_in_service_exec->execute()) {
				$bud_inservice=0;
				while($row_inservice=$select_in_service_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_inservice = $row_inservice['p_estimated_budget'];
					$bud_inservice = $bud_inservice + $est_inservice;
				}
			}

	$office_equipment="Office Equipment";
	$null_date='0000-00-00';
	$select_office="SELECT * FROM items_table WHERE classification='$office_equipment'";
		$select_office_exec=$pdo->prepare($select_office);
			if($select_office_exec->execute()) {
				$bud_office=0;
				while($row_office = $select_office_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_inservice = $row_office['p_estimated_budget'];
					$bud_office = $bud_office + $est_inservice;
				}
			}

	$ict_equipment="ICT Equipment";
	$null_date='0000-00-00';
	$select_ict="SELECT * FROM items_table WHERE classification='$ict_equipment'";
		$select_ict_exec=$pdo->prepare($select_ict);
			if($select_ict_exec->execute()) {
				$bud_ict=0;
				while($row_ict = $select_ict_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_ict = $row_ict['p_estimated_budget'];
					$bud_ict = $bud_ict + $est_ict;
				}
			}
	$furnitures="Furnitures and Fixtures";
	$null_date='0000-00-00';
	$select_furniture="SELECT * FROM items_table WHERE classification='$furnitures'";
		$select_furniture_exec=$pdo->prepare($select_furniture);
			if($select_furniture_exec->execute()) {
				$bud_fix=0;
				while($row_fix = $select_ict_exec->fetch(PDO::FETCH_ASSOC)) {
					$est_fix = $row_fix['p_estimated_budget'];
					$bud_fix = $bud_fix + $est_fix;
				}
			}

	$capital=$bud_office + $bud_ict + $bud_fix;

	$grand_total=$total_mooe + $capital;
?>
	<div class="form-group text-center clearfix" id="diba1">
		<p>Department of Education</p>
		<p>SDO-MARIKINA CITY</p>
		<p>Shoe Avenue, Sta. Elena, Marikina City</p>
		<p>SUMMARY OF ANNUAL PROCUREMENT PLAN FOR FY 2018</p>
	</div>
		<div class="row">
			<div class="col-sm-12 table-responsive">
				<table class="table table-bordered" style="table-layout: fixed;">
					<thead>
						<tr>
							<th width="8%" rowspan="2" id="aux">Code (PAP)</th>
							<th width="15%" rowspan="2" id="aux">Procurement Program/Project</th>
							<th width="5%" rowspan="2" id="aux">PMO/<br>End-User</th>
							<th width="15%" rowspan="2" id="aux">Mode of Procurement</th>
							<th width="25%" colspan="4" id="aux">Schedule for Each Procurement</th>
							<th width="4%" rowspan="2" id="aux">Source <br> of <br>Funds</th>
							<th width="18%" colspan="3" id="aux">Estimated Budget (PhP)</th>
							<th width="10%" rowspan="2" id="aux">Remarks <br> (brief description of <br> <span>Program/Activity/<br>Project)</span></th>
						</tr>
						<tr>
							<th id="aux">Advertisement<br>/Posting of <br>IB/REI</th>
							<th id="aux">Submission<br>/Opening of Bids</th>
							<th id="aux">Notice <br> of <br>Award</th>
							<th id="aux">Contract <br> Signing</th>
							<th id="aux">Total</th>
							<th id="aux">MOOE</th>
							<th id="aux">CO</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><center>50-2030-1000</center></td>
							<td>Common Office Supplies</td>
							<td rowspan="27" id="aux">Division <br> Office</td>
							<td>NP-53.5 Agency to Agency</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><?php echo number_format($bud, 2); ?></td>
							<td align="right"><?php echo number_format($bud, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-2030-7000</center></td>
							<td>Drugs and Medicine Expenses</td>
							<td>NP-53.9 Small Value Procurement</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><?php echo number_format($bud_drug, 2); ?></td>
							<td align="right"><?php echo number_format($bud_drug, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-2030-8000</center></td>
							<td>Medical, Dental, Laboratory Supplies</td>
							<td>NP-53.9 Small Value Procurement</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><?php echo number_format($bud_med, 2); ?></td>
							<td align="right"><?php echo number_format($bud_med, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-2136-4000</center></td>
							<td><b>REPAIR/MAINTENANCE/ SERVICES</b></td>
							<td>NP-53.9 Small Value Procurement</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><?php echo number_format($bud_repair, 2); ?></td>
							<td align="right"><?php echo number_format($bud_repair, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-3039-9000</center></td>
							<td><b>OTHER EXPENSES</b></td>
							<td>NP-53.9 Small Value Procurement</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><?php echo number_format($bud_other, 2); ?></td>
							<td align="right"><?php echo number_format($bud_other, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Other Supplies and Materials</td> 
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Food Supplies</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Accountable Forms</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Advertising, Printing, Publication</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50204010/020</center></td>
							<td><b>UTILITIES</b></td>
							<td>Direct Contracting</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><?php echo number_format($bud_util, 2); ?></td>
							<td align="right"><?php echo number_format($bud_util, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-6040-5002</center></td>
							<td><b>SEMI-EXPENDABLE</b></td>
							<td>NP-53.9 Small Value Procurement</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><?php echo number_format($bud_semi, 2); ?></td>
							<td align="right"><?php echo number_format($bud_semi, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50205010/020/030</center></td>
							<td><b>BONDS/TRAVELING/TRAINING/</b></td>
							<td>NP-53.5 Agency-to-Agency</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><?php echo number_format($bud_b_t, 2); ?></td>
							<td align="right"><?php echo number_format($bud_b_t, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-2150-2000</center></td>
							<td>Bonds</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td align="right"><?php echo number_format($bud_bonds, 2); ?></td>
							<td align="right"><?php echo number_format($bud_bonds, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-2010-1000</center></td>
							<td>Travelling/Representation Expenses</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td align="right"><?php echo number_format($bud_travel, 2); ?></td>
							<td align="right"><?php echo number_format($bud_travel, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-2020-1000</center></td>
							<td>Trainings</td>
							<td>NP-53.9 Small Value Procurement</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><?php echo number_format($bud_trainings, 2); ?></td>
							<td align="right"><?php echo number_format($bud_trainings, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b>TOTAL MOOE</b></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td align="right"><b><?php echo number_format($total_mooe, 2); ?></b></td>
							<td align="right"><?php echo number_format($total_mooe, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-2020-1000</center></td>
							<td><b>IN-SERVICE-TRAININGS</b></td>
							<td>NP-53.9 Small Value Procurement</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><b><?php echo number_format($bud_inservice, 2); ?></b></td>
							<td align="right"><?php echo number_format($bud_inservice, 2); ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b>CAPITAL OUTLAY</b></td>
							<td>NP-53.9 Small Value Procurement</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>GoP</td>
							<td align="right"><b><?php echo number_format($capital, 2); ?></b></td>
							<td></td>
							<td align="right"><?php echo number_format($capital, 2); ?></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-6040-5002</center></td>
							<td>Office Equipment</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-6040-5003</center></td>
							<td>ICT Equipment</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><center>50-6040-7001</center></td>
							<td>Furnitures and Fixtures</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b>GRAND TOTAL</b></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Php</td>
							<td align="right"><b><?php echo number_format($grand_total, 2); ?></b></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
					<tfoot id="footer">
						<tr>
							<td colspan="2" style="border: none !important;">Prepared by: </td>
							<td colspan="3" style="border: none !important;">Certified Funds Available: </td>
							<td colspan="5" style="border: none !important;">Recommending Approval: </td>
							<td colspan="3" style="border: none !important;">Approved: </td>
						</tr>
						<tr>
							<td colspan="2" style="border: none !important;"></td>
							<td colspan="3" style="border: none !important;"></td>
							<td colspan="5" style="border: none !important;"></td>
							<td colspan="3" style="border: none !important;"></td>
						</tr>
						<tr>
							<td colspan="2" id="aux" style="border: none !important;"><b>LEILANI N. VILLANUEVA</b><br>
							<span style="font-size: 11px">EPS/ Division BAC Secretariat</span></td>
							<td colspan="3" id="aux" style="border: none !important;"><b>IVY R. RUALLO</b><br>
							<span style="font-size: 11px">OIC-ADAS III, Accounting Unit</span></td>
							<td colspan="5" id="aux" style="border: none !important;"><b>ELISA O. CERVEZA</b><br>
							<span style="font-size: 11px">CID Chief/ Division BAC Chairperson</span></td>
							<td colspan="3" id="aux" style="border: none !important;"><b>SHERYLL T. GAYOLA</b><br>
							<span style="font-size: 11px">OIC, Schools Division Superintendent</span></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>