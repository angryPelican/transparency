<!DOCTYPE html>
<html>
  <head>
   <meta charset="utf-8">
    <meta name = "viewport" content = "width=device-width, initial-scale=1">
      <?php
        include('links.php');
      ?>
  </head>
  <body>
    <?php
    $get_category=$_GET['category'];
    include 'Connect.php';
    include 'auth.php';
    require 'inc/navbar_transparency.php';
?>
    <div class="container-fluid">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5 style="text-transform: uppercase;"><?php echo $get_category; ?></h5>
        </div>
        <div class="panel-body">
          <table class="table table-bordered display table-sm">
            <form action="" method="post" id="form_delivery">
              <thead>
                <tr>
                  <th>Particular</th>
                  <th colspan="3">Purchases</th>
                  <th colspan="3">Available Stocks for the Month</th>
                  <th>Issuances</th>
                  <th>Balance as of <?php echo date('m/d/y'); ?></th>
                  <th>Per Count</th>
                  <th>Shortages</th>
                  <th style="border-bottom: none;">Remarks</th>
                </tr>
                <tr>
                  <th>Items</th>
                  <th>Qty</th>
                  <th>Unit Cost</th>
                  <th>Amount</th>

                  <th>Qty</th>
                  <th>Unit Cost</th>
                  <th>Amount</th>

                  <th>Qty</th>
                  <th>Qty</th>
                  <th>Qty</th>
                  <th>Qty</th>
                  <th style="border-top: none;"></th>
                </tr>
              </thead>
              <tbody>
                <?php
                include('Connect.php');
                $date_now='0000-00-00';
                $category=$_GET['category'];
                $select="SELECT * FROM inventory_table WHERE category=?";
                $select_exec=$pdo->prepare($select);
                $select_exec->execute([$category]);
                $row_count=$select_exec->rowCount();
                if($row_count == 0){
                echo"<tr>
                  <td colspan='12'>No Data Available in Table</td>
                </tr>";
                }
                elseif($row_count != 0) {
                      while($row=$select_exec->fetch(PDO::FETCH_ASSOC)) {
                        $item_name=$row['i_description'];
                        $item_qty=$row['i_quantity_size'];
                        $unit_price=$row['unit_price'];
                        $amount=$unit_price * $item_qty;
                        $item_unit_cost=number_format($row['unit_price'], 2);
                        $item_amount=number_format($amount, 2);
                        echo"<tr>
                            <td>$item_name</td>
                            <td>$item_qty</td>
                            <td align='right'>$item_unit_cost</td>
                            <td align='right'>$item_amount</td>
                            <td></td>
                            <td align='right'></td>
                            <td align='right'></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>";
                      }
                  }
                ?>
              </tbody>
            </table>
          </div>
          <div class="panel-footer">
          </form>
          <?php
          if(isset($_POST['save_delivery'])){
          $get_id=$_GET['eventid'];
          $count = count($_POST['delivery_date']);
          for ($i = 0; $i < $count; $i++) {
          $easy = $_POST['delivery_date'][$i];
          $id = $_POST['holder'][$i];
          $sql = "UPDATE inventory_table SET delivery_date=? WHERE item_id = ? AND project_id=?";
          $success = $pdo->prepare($sql);
          $success->execute([$easy,$id,$get_id]);
          echo'<script>window.location="view_project_so.php?eventid=' .$get_id. '";</script>';
          }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</body>
</html>