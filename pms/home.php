<?php
include('index1.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>PPMP | Create Project Dashboard</title>
	<?php
		include ('Connect.php'); 
		include('auth.php');
		require ('inc/header.php');
	?>	
</head>
	
	<style type="text/css">
		.btn_add_acc {
			transition: 0.3s all;
			background: #0abde3;
			box-shadow: none;
			outline: none;
			color: white;
		}
		.btn_add_acc:hover {
			transform: scale(1.2);
		}
		.btn_edit {
			background: #0abde3;
			transition: 0.3s all;
		}
		.btn_edit:hover {
			transform: scale(1.2);
		}
		.btn_delete {
			background: #ee5253;
			transition: 0.3s all;
		}
		.btn_delete:hover {
			transform: scale(1.2);
		}
	</style>
	<body>
		<?php
	require ('inc/navbar_transparency.php');
	?>
		<div class="container">
			<div class="form-group">
				<button type="button" class="btn_add_acc btn btn-default" data-toggle="modal" data-target="#exampleModal">
				Add Account
				</button>
			</div>
			<div class="form-group">
				<table class="table table-bordered table-sm display condensed" id="tabledarks" style="table-layout: auto; width: 100%;">
					<thead>
						<th width="10">First Name</th>
						<th width="10">Last Name</th>
						<th width="30">User Email</th>
						<th width="5">User Type</th>
						<th width="15">Section</th>
						<th width="30">Action</th>
					</thead>
					<tbody id="tabledark">
						<?php
							getallacc();
						?>
					</tbody>
				</table>
			</div>
			<div id="virmil">
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">PMS | Add Account</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="username">First name:</label>
							<input type="text" id="usernames" class="form-control" />
						</div>
						<div class="form-group">
							<label for="lastname">Last name:</label>
							<input type="text" id="lastname" class="form-control" />
						</div>
						<div class="form-group">
							<label for="email">User Email:</label>
							<input type="email" id="email" class="form-control" />
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="usertype">User Type:</label>
								<select class="form-control" id="usertype">
									<option value="0">0 - ICTU ADMIN</option>
									<option value="1">1 - SDS</option>
									<option value="2">2 - BAC Chairman</option>
									<option value="3">3 - Accounting Officer</option>
									<option value="3">3 - Budget Officer</option>
									<option value="4">4 - BAC Secretariat</option>
									<option value="5">5 - Supply Officer</option>
									<option value="6">6 - End-User</option>
								</select>
							</div>
							<div class="col-md-6">
								<label for="usertype">Section:</label>
								<select class="form-control" id="section">
									<option>SDS OFFICE</option>
									<option>ASDS OFFICE</option>
									<option>ADMIN SECTION</option>
									<option>CASH SECTION</option>
									<option>PERSONNEL SECTION</option>
									<option>RECORDS SECTION</option>
									<option>PROPERTY AND SUPPLY SECTION</option>
									<option>ACCOUNTING SECTION</option>
									<option>BUDGET SECTION</option>
									<option>LEGAL SERVICES</option>
									<option>ICT SERVICES</option>
									<option>GENERAL SERVICES</option>
									<option>CURRICULUM IMPLEMENTATION DIVISION</option>
									<option>SCHOOL GOVERNANCE AND OPERATIONS DIVISION</option>
									<option>YOUTH CAMP</option>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="saveinsert">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
<!-- Delete Modal -->
<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">PMS | Delete Account</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="id_todelete">
				<p class="modal-text"> Are you sure you want to delete <span id="del_user"></span>'s Account ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<button type="button" id="delete" class="btn btn-danger" data-role="delete_acc">Yes</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">PMS | Edit Account</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" id="user_id" />
				<div class="form-group row">
					<div class="col-sm-6">
						<label for="username_edit">First Name</label>
						<input type="text" class="form-control" id="first_n" value="" />
					</div>
					<div class="col-sm-6">
						<label for="username_edit">Last Name</label>
						<input type="text" class="form-control" id="last_n" value="" />
					</div>
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" id="email_edit" class="form-control" value="" />
				</div>
				<div class="form-group row">
					<div class="col-sm-6">
						<label for="user-type">User Type</label>
						<select class="form-control" id="user_type">
							<option value="0">0 - ICTU ADMIN</option>
							<option value="1">1 - SDS</option>
							<option value="2">2 - BAC Chairman</option>
							<option value="3">3 - Accounting Officer</option>
							<option value="3">3 - Budget Officer</option>
							<option value="4">4 - BAC Secretariat</option>
							<option value="5">5 - Supply Officer</option>
							<option value="6">6 - End-User</option>
						</select>
					</div>
					<div class="col-sm-6">
						<label class="off_section">Section</label>
						<select class="form-control" id="off_section">
							<option>SDS OFFICE</option>
							<option>ASDS OFFICE</option>
							<option>ADMIN SECTION</option>
							<option>CASH SECTION</option>
							<option>PERSONNEL SECTION</option>
							<option>RECORDS SECTION</option>
							<option>PROPERTY AND SUPPLY SECTION</option>
							<option>ACCOUNTING SECTION</option>
							<option>BUDGET SECTION</option>
							<option>LEGAL SERVICES</option>
							<option>ICT SERVICES</option>
							<option>GENERAL SERVICES</option>
							<option>CURRICULUM IMPLEMENTATION DIVISION</option>
							<option>SCHOOL GOVERNANCE AND OPERATIONS DIVISION</option>
							<option>YOUTH CAMP</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="save_acc" data-role="save_acc">Save changes</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#tabledarks').DataTable();
var datatosend = "displaydata";
$(document).on('click', 'a[data-role=update]', function(){
	var name=$(this).data('accname');
	var lastname=$(this).data('lastname');
	var email=$(this).data('email');
	var type=$(this).data('type');
	var section=$(this).data('section');
	var id=$(this).data('id');
	$('#first_n').val(name);
	$('#last_n').val(lastname);
	$('#email_edit').val(email);
	$('#user_type').val(type);
	$('#off_section').val(section);
	$('#user_id').val(id);
});
$('#save_acc').click(function(){
	var save_name=$('#first_n').val();
	var save_lname=$('#last_n').val();
	var save_email=$('#email_edit').val();
	var save_type=$('#user_type').val();
	var save_section=$('#off_section').val();
	var user_id=$('#user_id').val();
	var role=$(this).data('role');
	$.ajax({
		url: "datawall.php",
		type: "POST",
		data: {
			Renz: role,
			username_tosend: save_name,
			lastname_tosend: save_lname,
			email_tosend: save_email,
			usertype_tosend: save_type,
			section_tosend: save_section,
			userid_tosend: user_id
		},
		success: function(response) {
			location.reload();
		}
	})
});
$(document).on('click', 'a[data-role=delete]', function(){
	var id=$(this).data('id');
	var name=$(this).data('name');
	$('#del_user').text(name);
	$('#id_todelete').val(id);
});

$('#delete').click(function(){
	var role=$(this).data('role');
	var id_del=$('#id_todelete').val();
	$.ajax({
		url: "datawall.php",
		type: "post",
		data: {
			Renz: role,
			id: id_del,
		},
		success: function(response) {
			location.reload();
		}	
	});
});
$("#saveinsert").click(function(){
var datatoinsert="insertdata";
var user_name=$("#usernames").val();
var last_name=$("#lastname").val();
var email=$("#email").val();
var password=$("#email").val();
var usertype=$("#usertype").val();
var section=$("#section").val();
$.post("datawall.php",
{Renz: datatoinsert,
Username:user_name,
Lastname:last_name,
Email:email,
Password:password,
Usertype:usertype,
Section: section},
function(data){
location.reload();
// console.log(data);
document.getElementById("usernames").value="";
document.getElementById("lastname").value="";
document.getElementById("email").value="";
document.getElementById("usertype").value="";
document.getElementById("section").value="";
});
});
</script>