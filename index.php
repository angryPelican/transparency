<?php
	if(!isset($_SESSION)){
		session_start();
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="The official website of the Schools Division of Marikina City">
  <meta name="keywords" content="DepEd Marikina, DepEd, Marikina, SDO, Education, Schools, NCR">
  <meta name="author" content="Ryan Lee Regencia">
  
  <meta http-equiv='cache-control' content='no-cache'>
  <meta http-equiv='expires' content='0'>
  <meta http-equiv='pragma' content='no-cache'>

  <title>Schools Division of Marikina City</title>
  <link rel="shortcut icon" type="image/x-icon" href="/images/DO-logo.png" title=" Division of City Schools Marikina"/>
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
		<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<link rel="stylesheet" type="text/css" href="fade-in.css">
  
  <link rel="stylesheet" href="css/stylesheet.css" />
  <link rel="stylesheet" href="css/login.css" />
  
</head>

<body>

<div class="container round-top">
<?php include ('inc/navbar_transparency.php'); ?>
	<div class="row">
		<div class="col-sm-3">
			<p><img src="img/seal.jpg" width="100%" title="" /></p>
		</div>
		<div class="col-sm-9">
			<h1>Transparency Seal</h1>
			<p>A Transparency Seal, prominently displayed on the main page of the website of a particular government agency, is a certificate that it has complied with the requirements of Section 93. This Seal links to a page within the agency’s website which contains an index of downloadable items of each of the above-mentioned documents.</p>
			<h3>Symbolism</h3>
			<p>A pearl buried inside a tightly-shut shell is practically worthless. Government information is a pearl, meant to be shared with the public in order to maximize its inherent value. The Transparency Seal, depicted by a pearl shining out of an open shell, is a symbol of a policy shift towards openness in access to government information. On the one hand, it hopes to inspire Filipinos in the civil service to be more open to citizen engagement; on the other, to invite the Filipino citizenry to exercise their right to participate in governance.</p>
			<p>This initiative is envisioned as a step in the right direction towards solidifying the position of the Philippines as the Pearl of the Orient – a shining example for democratic virtue in the region.</p>
		</div>
	</div>
	<div class="row">		
		<div class="col-sm-7">
			<?php  include ('inc/index_collapsible_nav.php'); ?>
		</div>
		<div class="col-sm-5">
			<?php include ('inc/panel_rfq_.php'); ?>
		</div>
	</div>
</div>
<div class="container round-bottom">
	<div class="row footer">
		<div class="col-sm-5">
			191 Shoe Ave., Sta. Elena, Marikina City, 1800, Philippines<br />
			(02) 682-2472 / 682-3989 <br />
			sdo.marikina@deped.gov.ph
		</div>
	</div>
</div>
</body>
</html>
